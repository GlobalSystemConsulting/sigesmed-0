/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica;

import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.Organizacion;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

@Entity(name = "DocenteCarpetaPedagogicaSMA")
@Table(name = "docente_carpeta_pedagogica", schema = "pedagogico")
public class DocenteCarpetaPedagogica implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "doc_car_ped_id")
    private Integer docCarPedId;
    
    @Column(name = "ver")
    private Integer ver;
    
    @Size(max = 256)
    @Column(name = "des")
    private String des;
    
    @Column(name = "com")
    private Boolean com;
    
    @Column(name = "est_ava")
    private Integer estAva;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "est_reg")
    private Character estReg;
    
    @JoinColumn(name = "doc_id", referencedColumnName = "doc_id")
    @ManyToOne
    private Docente docente;
    
    @JoinColumn(name = "rut_con_car_id", referencedColumnName = "rut_con_car_id")
    @ManyToOne
    private RutaContenidoCarpeta rutaContenido;
    
    @JoinColumn(name = "org_id", referencedColumnName = "org_id")
    @ManyToOne
    private Organizacion organizacion;
    
    @JoinColumn(name = "car_dig_id", referencedColumnName = "car_dig_id")
    @ManyToOne
    private CarpetaPedagogica carpeta;

    public DocenteCarpetaPedagogica() {
    }

    public DocenteCarpetaPedagogica(Integer docCarPedId) {
        this.docCarPedId = docCarPedId;
    }

    public Integer getDocCarPedId() {
        return docCarPedId;
    }

    public void setDocCarPedId(Integer docCarPedId) {
        this.docCarPedId = docCarPedId;
    }

    public Integer getVer() {
        return ver;
    }

    public void setVer(Integer ver) {
        this.ver = ver;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public Boolean getCom() {
        return com;
    }

    public void setCom(Boolean com) {
        this.com = com;
    }

    public Integer getEstAva() {
        return estAva;
    }

    public void setEstAva(Integer estAva) {
        this.estAva = estAva;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public Docente getDocente() {
        return docente;
    }

    public void setDocente(Docente docente) {
        this.docente = docente;
    }

    public RutaContenidoCarpeta getRutaContenidoCarpeta() {
        return rutaContenido;
    }

    public void setRutaContenidoCarpeta(RutaContenidoCarpeta rutaContenido) {
        this.rutaContenido = rutaContenido;
    }
    
    public Organizacion getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacion(Organizacion organizacion) {
        this.organizacion = organizacion;
    }

    public CarpetaPedagogica getCarpeta() {
        return carpeta;
    }

    public void setCarpeta(CarpetaPedagogica carpeta) {
        this.carpeta = carpeta;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (docCarPedId != null ? docCarPedId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DocenteCarpetaPedagogica)) {
            return false;
        }
        DocenteCarpetaPedagogica other = (DocenteCarpetaPedagogica) object;
        if ((this.docCarPedId == null && other.docCarPedId != null) || (this.docCarPedId != null && !this.docCarPedId.equals(other.docCarPedId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica.DocenteCarpeta[ docCarPedId=" + docCarPedId + " ]";
    }
    
}
