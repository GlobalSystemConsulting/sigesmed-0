/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.documento_estado_prioridad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.std.PrioridadExpedienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.PrioridadExpediente;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;

/**
 *
 * @author abel
 */
public class InsertarPrioridadExpedienteTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        PrioridadExpediente nuevaPrioridadExpediente = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            String nombre = requestData.getString("nombre");
            String descripcion = requestData.getString("descripcion");
            String estado = requestData.getString("estado");
            
            nuevaPrioridadExpediente = new PrioridadExpediente(0, nombre, descripcion, new Date(), wr.getIdUsuario(), estado.charAt(0));
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar , datos incorrectos" );
        }
        //Fin
        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        PrioridadExpedienteDao prioridadDao = (PrioridadExpedienteDao)FactoryDao.buildDao("std.PrioridadExpedienteDao");
        try{
            prioridadDao.insert(nuevaPrioridadExpediente);
        
        }catch(Exception e){
            System.out.println("No se pudo registrar la prioridad de expediente\n"+e);
            return WebResponse.crearWebResponseError("No se pudo registrar la prioridad de expediente", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("prioridadExpedienteID",nuevaPrioridadExpediente.getPriExpId());
        oResponse.put("fecha",nuevaPrioridadExpediente.getFecMod().toString());
        return WebResponse.crearWebResponseExito("El registro de la Prioridad de Expediente se realizo correctamente", oResponse);
        //Fin
    }
    
}
