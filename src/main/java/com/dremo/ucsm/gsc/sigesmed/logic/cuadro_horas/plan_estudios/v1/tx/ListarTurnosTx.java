/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.plan_estudios.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.PlanEstudiosDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Turno;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;

/**
 *
 * @author abel
 */
public class ListarTurnosTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<Turno> turnos = null;
        PlanEstudiosDao planDao = (PlanEstudiosDao)FactoryDao.buildDao("mech.PlanEstudiosDao");
        try{
            turnos = planDao.listarTurnos();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los turnos\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los turnos, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(Turno turno:turnos ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("turnoID",""+turno.getTurId() );
            oResponse.put("nombre",turno.getNom());
            oResponse.put("fecha",turno.getFecMod().toString());
            oResponse.put("estado",""+turno.getEstReg());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente los turnos",miArray);        
        //Fin
    }
    
}

