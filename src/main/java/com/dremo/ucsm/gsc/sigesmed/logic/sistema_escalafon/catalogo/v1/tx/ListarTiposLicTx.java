/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.catalogo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TipoLicenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TipoLicencia;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class ListarTiposLicTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ListarTiposLicTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        List<TipoLicencia> licencias = null;
        TipoLicenciaDao tipoLicDao = (TipoLicenciaDao)FactoryDao.buildDao("se.TipoLicenciaDao");
        
        try{
            licencias = tipoLicDao.listarAll();
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar los tipos de licencias",e);
            System.out.println("No se pudo listar los tipos de licencias.Error: "+e);
            return WebResponse.crearWebResponseError("No se pudo listar los tipos de licencias.Error: ", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(TipoLicencia d: licencias ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("id", d.getId());
            oResponse.put("nom", d.getNom());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Los tipos de licencias fueron listados exitosamente", miArray);
    }
    
}
