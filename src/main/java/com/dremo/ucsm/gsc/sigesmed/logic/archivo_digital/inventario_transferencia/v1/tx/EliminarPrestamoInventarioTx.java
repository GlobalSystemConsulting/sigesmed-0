/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.inventario_transferencia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.InventarioTransferenciaDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.PrestamoSerieDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.InventarioTransferencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.PrestamoSerieDocumental;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author angell
 */
public class EliminarPrestamoInventarioTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
    
    int inv_tra_id = 0;
    int pre_serie_id = 0;
       
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            inv_tra_id = requestData.getInt("inv_tra_id");
            pre_serie_id =  requestData.getInt("pre_serie_id");
            System.out.print("presID  "+pre_serie_id+"  invenID "+inv_tra_id);
        }catch(Exception e){
                System.out.println(e);
           return  WebResponse.crearWebResponseError("No se pudo eliminar Prestamo de Serie Documental, datos incorrectos", e.getMessage() );
        }
        
        //Operaciones con la Base de Datos
            PrestamoSerieDAO presSeriDao = (PrestamoSerieDAO)FactoryDao.buildDao("sad.PrestamoSerieDAO");
            
        try{
            presSeriDao.deleteAbsolute(new PrestamoSerieDocumental(pre_serie_id, inv_tra_id));
            
        }catch(Exception e){
            System.out.println(e);
             return  WebResponse.crearWebResponseError("No se pudo eliminar el Prestamo de Serie Documental - BD , datos incorrectos", e.getMessage() );
        }
         return WebResponse.crearWebResponseExito("El Prestamo de Serie Documental se elimino correctamente");
         
        
    //    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
