/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.mech;


import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.DistribucionHoraGrado;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.HorarioDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.HorarioDetalleModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.HorarioEscolar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author abel
 */

public interface HorarioEscolarDao extends GenericDao<HorarioEscolar>{
    
    public HorarioEscolar buscarHorario(int planNivelID,int gradoID, char seccionID);
    public List<Integer> buscarAulasDisponibles(int horarioID,char diaID,Date horaInicio, Date horaFin);
    public List<DistribucionHoraGrado> buscarDocentesPorIntervaloHora(int planID,char diaID,Date horaInicio, Date horaFin);
    public List<DistribucionHoraGrado> buscarDocentesConHorario(int planID);
    
    public List<HorarioDetalleModel> buscarDistribucionPlazasEnHorario(int planId);
    public List<HorarioDetalleModel> buscarDistribucionPorDocente(int docenteId);
    
    public void mergeHorarioDetalle(HorarioDetalle horarioDetalle);
    public void eliminarHorarioDetalle(int horarioDetalleID);
}