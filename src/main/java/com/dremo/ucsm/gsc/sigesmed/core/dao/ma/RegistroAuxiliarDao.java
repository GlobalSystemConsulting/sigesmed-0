package com.dremo.ucsm.gsc.sigesmed.core.dao.ma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.HistoricoNotasEstudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.RegistroAuxiliar;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.RegistroAuxiliarCompetencia;

import java.util.List;

/**
 * Created by Administrador on 25/01/2017.
 */
public interface RegistroAuxiliarDao extends GenericDao<RegistroAuxiliar>{
    
    RegistroAuxiliar buscarNotaEstudiante(int idIndicador,int idDoc,int idPer,int idArea,int idMatGra);
    
    RegistroAuxiliarCompetencia buscarNotaCompetenciaEsp(int idComp, int idArea, int idPer, int idAlum);
    HistoricoNotasEstudiante buscaHistoricoNotasEstudiante(int idArea, int idPer,int idAlum);
    RegistroAuxiliarCompetencia buscarNotaCompetencia(int idNot);
    void registrarNotaCompetencia(RegistroAuxiliarCompetencia reg);
    void guardarActualizarNotaArea(HistoricoNotasEstudiante historico);
    List<RegistroAuxiliarCompetencia> buscarNotaCompetenciasEstudiante(int grad_ie_est , int area , int periodo);
    Boolean buscarNotaFinalIndicador(int idIndicador, int idDoc, int idPer, int idArea, int idMatGra);
    Boolean buscarNotaFinalCompetencia(int idComp, int idArea, int idPer, int idAlum);
    Boolean buscarNotaAreaEnPeriodo(int idArea, int idPer, int idAlum);
    

}
