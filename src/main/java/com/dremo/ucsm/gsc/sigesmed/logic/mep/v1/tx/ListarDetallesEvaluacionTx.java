/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.mep.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mep.EvaluacionPersonalDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mep.FichaEvaPerslDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.*;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 *
 * @author geank
 */
public class ListarDetallesEvaluacionTx implements ITransaction{

    private static Logger  logger = Logger.getLogger(ListarDetallesEvaluacionTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        String idInst = wr.getMetadataValue("id");
        String rol = wr.getMetadataValue("ty");
        try{
            int resumenId = Integer.parseInt(idInst);
            return !rol.toUpperCase().equals("DOCENTE")? detalleEvaluacionToWebResponse(resumenId):detalleEvaluacionDocenteToResponse(resumenId);
            //return detalleEvaluacionToWebResponse(Integer.parseInt(idInst));
        }catch(NumberFormatException e){
            logger.log(Level.SEVERE,logger.getName() + ": execute()",e);
        }
        return null;
    }
    public WebResponse detalleEvaluacionDocenteToResponse(int resumenId){
        WebResponse response = new WebResponse();
        response.setScope("web");
        response.setResponseSta(true);
        EvaluacionPersonalDaoHibernate epdh = new EvaluacionPersonalDaoHibernate();
        JSONObject responseData = new JSONObject();
        try{
            ResumenEvaluacionPersonal rep = epdh.getResumentEvaluacionById(resumenId);
            List<Object[]> dominiosEvaluados = epdh.getDominiosEvaluacionDocente(resumenId);
            List<IndicadoresEvaluarPersonal> indicadoresEvaluados = epdh.getIndicadoresEvaluadosResumen(resumenId);
            responseData.put("des",rep.getRes());
            responseData.put("dominios",getDominiosEvaluacionDocente(resumenId,dominiosEvaluados));
            responseData.put("etapa",rep.getEta());
            responseData.put("inds",indicadoresToJSON(indicadoresEvaluados));
            responseData.put("total",rep.getPun().doubleValue());
            response.setData(responseData);
        }catch (JSONException e){
            response.setResponseSta(false);
            response.setData(new JSONObject());
            logger.log(Level.INFO,logger.getName() + ":detalleEvaluacionToWebResponse",e);
        }
        return response;
    }
    private JSONArray getDominiosEvaluacionDocente(int idResumen,List<Object[]> dominiosEvaluados){
        EvaluacionPersonalDaoHibernate eva = new EvaluacionPersonalDaoHibernate();
        FichaEvaPerslDaoHibernate feh = new FichaEvaPerslDaoHibernate();
        JSONArray adominio = new JSONArray();
        try{
            for(Object[] res : dominiosEvaluados){
                JSONObject jobj = new JSONObject(feh.getContenidoById((int) res[0]).toString());
                adominio.put(jobj);
            }
            for(int i = 0 ; i < adominio.length(); i++){
                JSONObject obj = adominio.getJSONObject(i);
                obj.put("sumDom",(double)dominiosEvaluados.get(i)[2]);
                JSONArray ahijos = obj.getJSONArray("cont");
                for(int j = 0; j < ahijos.length(); j++){
                    JSONObject hobj = ahijos.getJSONObject(j);
                    hobj.remove("indicadores");
                    logger.log(Level.INFO,":getDominiosEvaluacionDocente: {0}",idResumen);
                    logger.log(Level.INFO,":getDominiosEvaluacionDocente hijo: {0}",hobj.getInt("id"));
                    hobj.put("sumComp",eva.getDetalleEvaluacionByContenido(idResumen,hobj.getInt("id"))[3]);
                }
            }
        }catch (Exception e){
            logger.log(Level.SEVERE,logger.getName() + ":getDominiosEvaluacionDocente",e);
        }

        return adominio;
    }
    private WebResponse detalleEvaluacionToWebResponse(int resumenId){
        WebResponse response = new WebResponse();
        response.setScope("web");
        response.setResponseSta(true);
        EvaluacionPersonalDaoHibernate epdh = new EvaluacionPersonalDaoHibernate();
        try{
            ResumenEvaluacionPersonal rep = epdh.getResumentEvaluacionById(resumenId);
            JSONObject resumen = new JSONObject(rep.toString());


            String cargo = rep.getTrabajador().getTraCar().getCrgTraNom();

            JSONArray arrayEscalas = elementoToJSON(epdh.getEscalasByRol(cargo));

            JSONArray arrayDetalle = elementoToJSON(epdh.getDetalleFichaEvaluacion(resumenId));
            JSONArray arrayIndicadores = indicadoresToJSON(epdh.getIndicadoresEvaluadosResumen(resumenId));

            JSONObject jsonData = new JSONObject();

            jsonData.put("res",resumen)
                    .put("cols",arrayEscalas)
                    .put("dets", arrayDetalle)
                    .put("inds", arrayIndicadores);
            response.setData(jsonData);
            response.setResponse(MEP.SUCC_RESPONSE_COD);
            response.setResponseMsg(MEP.SUCC_RESPONSE_MESS_LIST);
        }catch (JSONException e){
            logger.log(Level.INFO,logger.getName() + ":detalleEvaluacionToWebResponse",e);
            response.setResponse(MEP.ERR_RESPONSE_COD);
            response.setResponseMsg(MEP.ERR_RESPONSE_MESS_LIST);
        }
        return response;
    }
    /*private JSONArray detalleResumenToJSON(List<DetalleResumenEvaluacionPersonal> detallesResumen){
        JSONArray jsonDetalle = new JSONArray();
        try{
            for(DetalleResumenEvaluacionPersonal detalle : detallesResumen){
                JSONObject jsonObj = new JSONObject(detalle.toString());
                jsonDetalle.put(jsonObj);
            }
        }catch (JSONException e){
            logger.log(Level.INFO,logger.getName() + ":detalleResumenToJSON",e);
            return null;
        }
        return jsonDetalle;
    }*/
    private JSONArray elementoToJSON(List<Object> elementos){
        JSONArray jsonDetalle = new JSONArray();
        try{
            for(Object elemento: elementos){
                JSONObject jsonObj = new JSONObject(elemento.toString());
                jsonDetalle.put(jsonObj);
            }
        }catch (JSONException e){
            logger.log(Level.INFO,logger.getName() + ":elementoToJSON",e);
            return null;
        }
        return jsonDetalle;
    }

    private JSONArray indicadoresToJSON(List<IndicadoresEvaluarPersonal> indicadores){
        JSONArray jsonContenidos = new JSONArray();
        Map<Integer,ContenidoFichaEvaluacion> mcontenido = new HashMap<>();
        int i=0;
        try{
            for(IndicadoresEvaluarPersonal ind : indicadores){
                ContenidoFichaEvaluacion cf = ind.getIndicadorFichaEvaluacion().getContenidoFichaEvaluacion();
                if(!mcontenido.containsValue(cf)){
                    mcontenido.put(i,cf);
                    jsonContenidos.put(new JSONObject(EntityUtil.objectToJSONString(new String[]{"conFicEvaId","nom","tip"},new String[]{"id","nom","tip"},cf)));
                    ++i;
                }
                Iterator<Integer> iter = mcontenido.keySet().iterator();
                while (iter.hasNext()){
                    int val = iter.next();
                    JSONObject obj = jsonContenidos.optJSONObject(val);
                    if(obj != null && (obj.getInt("id") == cf.getConFicEvaId())){
                        JSONArray aind = obj.optJSONArray("indicadores");
                        if(aind == null){
                            aind = obj.put("indicadores",new JSONArray()).optJSONArray("indicadores");

                        }
                        JSONObject jsonind = new JSONObject(ind.getIndicadorFichaEvaluacion().toString());
                        jsonind.put("pun",ind.getPun());
                        aind.put(jsonind);
                    }
                }
            }
        }catch(JSONException e){
            logger.log(Level.INFO,logger.getName() + ": indicadoresToJSON",e);
            return null;
        }

        return jsonContenidos;
    }
}
