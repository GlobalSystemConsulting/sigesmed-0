package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.CriterioCertificacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.CriterioCertificacion;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

public class CriterioCertificacionDaoHibernate extends GenericDaoHibernate<CriterioCertificacion> implements CriterioCertificacionDao {
    
    private static final Logger logger = Logger.getLogger(CriterioCertificacionDaoHibernate.class.getName());
    
    @Override
    public List<CriterioCertificacion> buscarPorCapacitacion(int codCap) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            Criteria query = session.createCriteria(CriterioCertificacion.class)
                    .add(Restrictions.eq("estReg", 'A'))
                    .createAlias("cursoCapacitacion", "cap")
                    .add(Restrictions.eq("cap.curCapId", codCap))
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarPorCapacitacion", e);
            throw e;
        } finally {
            session.close();
        }
    }
}
