/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.desplazamiento.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DesplazamientoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Desplazamiento;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx.AgregarOperacionAuditoria;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class ActualizarDesplazamientoByTipoTx implements ITransaction{
    
    private static final Logger logger = Logger.getLogger(ActualizarDesplazamientoByTipoTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
         /*
        *   Parte para la lectura, verificacion y validacion de datos
         */
        Desplazamiento desplazamiento = null;
        int desId = 0;
        int opcion = 0;
        int tipDocId = 0;
        int jorLabId = 0;
        String numDoc = "";
        Date fecDoc = null;
        Date fecIni = null;
        String numDocTer = "";
        Date fecDocTer = null;
        Date fecTer = null;
        String motRet = "";
        
        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat sdo = new SimpleDateFormat("dd-MM-yyyy");

        try {
            JSONObject requestData = (JSONObject) wr.getData();
            opcion = requestData.getInt("opcion");
            
            desId = requestData.getJSONObject("desplazamiento").getInt("desId");
            tipDocId = requestData.getJSONObject("desplazamiento").getInt("tipDocId");
            jorLabId = requestData.getJSONObject("desplazamiento").getInt("jorLabId");
            System.out.println("TIPO DOC Y JORN LAB:"+ tipDocId+" " + jorLabId);
            switch(opcion){
                case 1:if(requestData.getJSONObject("desplazamiento").getString("fecDoc").length() > 0){
                            fecDoc = sdi.parse(requestData.getJSONObject("desplazamiento").getString("fecDoc").substring(0, 10));
                        }
                       if(requestData.getJSONObject("desplazamiento").getString("numDoc").length() > 0)
                            numDoc = requestData.getJSONObject("desplazamiento").getString("numDoc");
                        fecIni = sdi.parse(requestData.getJSONObject("desplazamiento").getString("fecIni").substring(0, 10));
                        
                        break;
                case 2: numDoc = requestData.getJSONObject("desplazamiento").getString("numDoc");
                        if(requestData.getJSONObject("desplazamiento").getString("fecDoc").length() > 0){
                            fecDoc = sdi.parse(requestData.getJSONObject("desplazamiento").getString("fecDoc").substring(0, 10));
                        }
                        fecIni = sdi.parse(requestData.getJSONObject("desplazamiento").getString("fecIni").substring(0, 10));
                        
                        break;
                        
                case 3: 
                        if(requestData.getJSONObject("desplazamiento").getString("numDocTer").length() > 0)
                            numDocTer = requestData.getJSONObject("desplazamiento").getString("numDocTer");
                        
                        fecDocTer = null;
                        if(requestData.getJSONObject("desplazamiento").getString("fecDocTer").length() > 0)
                            fecDocTer = sdi.parse(requestData.getJSONObject("desplazamiento").getString("fecDocTer").substring(0, 10));
                        
                        if(requestData.getJSONObject("desplazamiento").getString("fecTer").length() > 0)
                            fecTer = sdi.parse(requestData.getJSONObject("desplazamiento").getString("fecTer").substring(0, 10));
                        
                        if(requestData.getJSONObject("desplazamiento").getString("motRet").length() > 0){
                            motRet = requestData.getJSONObject("desplazamiento").getString("motRet");
                        }
            }
            
        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar desplazamiento",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        }

        try {
            DesplazamientoDao desplazamientoDao = (DesplazamientoDao) FactoryDao.buildDao("se.DesplazamientoDao");
            desplazamiento = desplazamientoDao.buscarPorId(desId);
            switch(opcion){
                case 1:
                        desplazamiento.setFecDoc(fecDoc);
                        desplazamiento.setFecIni(fecIni);
                        desplazamiento.setNumDoc(numDoc);
                       break;
                case 2: 
                        desplazamiento.setNumDoc(numDoc);
                        desplazamiento.setFecDoc(fecDoc);
                        desplazamiento.setJorLabId(jorLabId);
                        desplazamiento.setFecIni(fecIni);
                        break;
                        
                        
                case 3: 
                        desplazamiento.setNumDocTer(numDocTer);
                        desplazamiento.setFecDocTer(fecDocTer);
                        desplazamiento.setFecTer(fecTer);
                        desplazamiento.setMotRet(motRet);
                        break;
            }
            desplazamiento.setTipDocId(tipDocId);
            desplazamiento.setJorLabId(jorLabId); 
            
            desplazamientoDao.update(desplazamiento);
            ///////////AUDITORIA/////////////
            AgregarOperacionAuditoria.agregarAltaAuditoria("Se actualizo desplazamiento", "Detalle: "+desId+" desplazamiento ", wr.getIdUsuario() , "---", "xxx.xxx.xxx.xxx");
            ///////////AUDITORIA/////////////
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Actualizar desplazamiento",e);
            System.out.println(e);
        }

        JSONObject oResponse = new JSONObject();
        oResponse.put("desId", desplazamiento.getDesId());
        oResponse.put("tip", desplazamiento.getTip());
        oResponse.put("tipDes", "");
        switch(desplazamiento.getTip()){
            case '1':
            case '2': oResponse.put("numeroDoc", null != desplazamiento.getNumDoc() ? desplazamiento.getNumDoc():"");
                    oResponse.put("fechaDoc", null != desplazamiento.getFecIni() ? desplazamiento.getFecIni() : "");
                    oResponse.put("fechaDes", null != desplazamiento.getFecIni() ? desplazamiento.getFecIni() : "");
                    break;
            case '3': oResponse.put("numeroDoc", null != desplazamiento.getNumDocTer() ? desplazamiento.getNumDocTer() : ""); 
                    oResponse.put("fechaDoc", null != desplazamiento.getFecDocTer() ? desplazamiento.getFecDocTer() : "");
                    oResponse.put("fechaDes", null != desplazamiento.getFecTer() ? desplazamiento.getFecTer() : "");
                    break;
        }
        oResponse.put("numDoc", null != desplazamiento.getNumDoc() ? desplazamiento.getNumDoc():"");
        oResponse.put("fecDoc", null != desplazamiento.getFecDoc() ? sdo.format(desplazamiento.getFecDoc()) : "");
        oResponse.put("tipDocId", null != desplazamiento.getTipDocId()? desplazamiento.getTipDocId() : 0);
        oResponse.put("numDocTer", null != desplazamiento.getNumDocTer() ? desplazamiento.getNumDocTer() : "");
        oResponse.put("fecDocTer", null != desplazamiento.getFecDocTer() ? sdo.format(desplazamiento.getFecDocTer()) : "");
        oResponse.put("tipDocTer", null != desplazamiento.getTipDocTer() ? desplazamiento.getTipDocTer() : "");
        oResponse.put("jorLabId", null != desplazamiento.getJorLabId() ? desplazamiento.getJorLabId() : 0);
        oResponse.put("fecIni", null != desplazamiento.getFecIni() ? sdo.format(desplazamiento.getFecIni()) : "");
        oResponse.put("fecTer", null != desplazamiento.getFecTer() ? sdo.format(desplazamiento.getFecTer()) : "");
        oResponse.put("motRet", null != desplazamiento.getFecTer() && desplazamiento.getMotRet().length()>0 ? desplazamiento.getMotRet() : "");
                
        return WebResponse.crearWebResponseExito("El registro del desplazamiento se realizo correctamente", oResponse);
        //Fin
    }
    
}
