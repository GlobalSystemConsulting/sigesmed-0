/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sad;


import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Jeferson
 */
@Entity
@Table (name="tabla_retencion_documentos" , schema ="administrativo")
public class TablaRetencionDocumentos implements java.io.Serializable {
    
    @Id
    private int ser_doc_id;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ser_doc_id", insertable = false , updatable = false)
    private SerieDocumental serie;
    
    @Column(name="ag")
    private int ag;
    
    @Column(name="ap")
    private int ap;
    
    @Column(name="oaa")
    private int oaa;
    
    public TablaRetencionDocumentos(){
        
    }
    
    public TablaRetencionDocumentos(int ser_doc_id){
        this.ser_doc_id = ser_doc_id;
        
        serie = new SerieDocumental(ser_doc_id);
        
    }
    public TablaRetencionDocumentos(int ser_doc_id,int ag , int ap , int oaa){
        this.ser_doc_id = ser_doc_id;
        serie = new SerieDocumental(ser_doc_id);
        
        this.ag = ag;
        this.ap = ap;
        this.oaa = oaa;
    }
    public void setag(int ag){
        this.ag = ag;
    }
    public int getag(){
        return this.ag;
    }
    public void setap(int ap){
        this.ap = ap;
    }
    public int getap(){
        return this.ap;
    }
    public void setoaa(int oaa){
        this.oaa = oaa;
    }
    public int getoaa(){
        return this.oaa;
    }
    
    
}
