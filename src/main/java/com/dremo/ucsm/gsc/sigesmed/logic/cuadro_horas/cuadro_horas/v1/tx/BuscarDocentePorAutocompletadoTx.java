/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.cuadro_horas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.DistribucionHoraGradoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.Ambientes;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import java.util.List;

/**
 *
 * @author abel
 */
public class BuscarDocentePorAutocompletadoTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        JSONArray responseData = new JSONArray();
        String docenteAutocompletado = "";
        
        try{            
            JSONObject requestData = (JSONObject)wr.getData();
            docenteAutocompletado = requestData.optString("valorAutocompletado");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo encontrar el docente, datos incorrectos", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        try{
            DistribucionHoraGradoDao disDao = (DistribucionHoraGradoDao)FactoryDao.buildDao("mech.DistribucionHoraGradoDao");            
            List<Docente> docentes = disDao.buscarDocentePorAutocompletado(docenteAutocompletado);
            //System.out.println("Cantidad:" + docentes.size());
            if (docentes != null) {
                for (Docente docente : docentes) {
                    JSONObject oResponse = new JSONObject();
                    oResponse.put("docenteID",docente.getDocId());
                    oResponse.put("especialidad",docente.getEsp());
                    oResponse.put("codigoModular",docente.getCodMod());
                    oResponse.put("nivelMagisterial",docente.getNivMag());
                    oResponse.put("DNI",docente.getPersona().getDni());
                    oResponse.put("nombre",docente.getPersona().getNombrePersona());
                    //System.out.println("JSON:"+oResponse.toString());
                    responseData.put(oResponse);
                }

            } 
        
        }catch(Exception e) {
            System.out.println("No se pudo listar los docentes\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los docentes", e.getMessage() );    
        }
        
        return WebResponse.crearWebResponseExito("Se listo los docentes correctamente", responseData);

        //Fin
    }
    
}

