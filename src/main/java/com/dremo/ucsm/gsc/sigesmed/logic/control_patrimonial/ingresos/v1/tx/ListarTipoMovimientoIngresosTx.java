/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import java.util.List;

import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.TipoMovimientoIngresos;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.MovimientoIngresos;        

import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.MovimientoIngresosDAO;
/**
 *
 * @author Administrador
 */
public class ListarTipoMovimientoIngresosTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        JSONArray miArray = new JSONArray();
        try{
            List<TipoMovimientoIngresos> tmi = null;
            JSONObject requestData = (JSONObject)wr.getData();

            MovimientoIngresosDAO tmi_dao = (MovimientoIngresosDAO)FactoryDao.buildDao("scp.MovimientoIngresosDAO");
             
            tmi = tmi_dao.listarTipoMovimiento();
            
             for(TipoMovimientoIngresos tipo_mov : tmi){
               JSONObject oResponse = new JSONObject();
               
               oResponse.put("tip_mov_id",tipo_mov.getTip_mov_ing_id());
               oResponse.put("tip_mov_des",tipo_mov.getDes());
               miArray.put(oResponse);
             }
            
            
            
        }catch(Exception e){
            System.out.println("No se pudo Listar los Tipos de Movimiento de Ingresos\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los Tipos de Movimiento de Ingresos", e.getMessage() );
        }
        
        return WebResponse.crearWebResponseExito("Se Listo los Tipos de Movimientos de Ingreso Correctamente",miArray); 
 
        
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
    
}
