/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.area.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.TipoAreaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TipoArea;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

/**
 *
 * @author Administrador
 */
public class EliminarTipoAreaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int tipoAreaID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            tipoAreaID = requestData.getInt("tipoAreaID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo eliminar, datos incorrectos");
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        TipoAreaDao areaDao = (TipoAreaDao)FactoryDao.buildDao("TipoAreaDao");
        try{
            areaDao.delete(new TipoArea(tipoAreaID));
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar el Tipo de Area\n"+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el Tipo de Area", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Tipo de Area se elimino correctamente");
        //Fin
    }
}
