/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

@Entity
@Table(name = "indicador_compromiso_gestion_pedagogica", schema="pedagogico")
public class IndicadorCompromisoGestionPedagogica implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ind_com_ges_ped_id")
    private Integer indComGesPedId;
    
    @Size(max = 2147483647)
    @Column(name = "ind_com_ges_ped_des")
    private String indComGesPedDes;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.DATE)
    private Date fecMod;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Size(max = 1)
    @Column(name = "est_reg")
    private String estReg;
        
    @JoinColumn(name = "ins_lle_ind_id", referencedColumnName = "ins_lle_ind_id")
    @ManyToOne
    private InstruccionLlenadoIndicadores instruccionLI;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "indicadorCGP")
    private List<FichaEvaluacionDetalle> fichasEvaluacionDetalle;

    public IndicadorCompromisoGestionPedagogica() {
    }

    public IndicadorCompromisoGestionPedagogica(Integer indComGesPedId) {
        this.indComGesPedId = indComGesPedId;
    }

    public Integer getIndComGesPedId() {
        return indComGesPedId;
    }

    public void setIndComGesPedId(Integer indComGesPedId) {
        this.indComGesPedId = indComGesPedId;
    }

    public String getIndComGesPedDes() {
        return indComGesPedDes;
    }

    public void setIndComGesPedDes(String indComGesPedDes) {
        this.indComGesPedDes = indComGesPedDes;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public InstruccionLlenadoIndicadores getInstruccionLI() {
        return instruccionLI;
    }

    public void setInstruccionLI(InstruccionLlenadoIndicadores instruccionLI) {
        this.instruccionLI = instruccionLI;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (indComGesPedId != null ? indComGesPedId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IndicadorCompromisoGestionPedagogica)) {
            return false;
        }
        IndicadorCompromisoGestionPedagogica other = (IndicadorCompromisoGestionPedagogica) object;
        if ((this.indComGesPedId == null && other.indComGesPedId != null) || (this.indComGesPedId != null && !this.indComGesPedId.equals(other.indComGesPedId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.IndicadorCompromisoGestionPedagogica[ indComGesPedId=" + indComGesPedId + " ]";
    }
    
}
