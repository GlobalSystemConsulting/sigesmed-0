/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.sad;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.SerieDocumental;
import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jeferson
 */
public interface SerieDocumentalDAO extends GenericDao<SerieDocumental> {
    
    public List<SerieDocumental> buscarPorOrganizacion();
    public List<SerieDocumental> buscarPorArea(int id_area);
    public List<SerieDocumental> buscarPorOrganizacion(int tipoOrganizacionID);
    public List<SerieDocumental> buscarPorCodigo(int codigo);
    public List<SerieDocumental> buscarPorNombre(String nombre);
    public void registrarPrestamo(SerieDocumental serie);
    public List<SerieDocumental> listaPrestamosOrganizacion();
    /*Agregado adicionalmente*/
    public List<SerieDocumental> buscarPorUnidadOrganica(int uni_orgID);
    public List<SerieDocumental> listaInventariosTransferencia();
    public void registrarInventario(SerieDocumental serie);
    
}   
