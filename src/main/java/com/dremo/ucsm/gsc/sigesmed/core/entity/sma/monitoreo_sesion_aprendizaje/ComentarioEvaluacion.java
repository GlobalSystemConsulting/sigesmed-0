/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

@Entity
@Table(name = "comentario_evaluacion", schema="pedagogico")
public class ComentarioEvaluacion implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "com_eva_id")
    private Integer comEvaId;
    
    @Size(max = 2147483647)
    @Column(name = "com_eva_des")
    private String comEvaDes;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "est_reg")
    private Character estReg;
    
    @JoinColumn(name = "fic_eva_id", referencedColumnName = "fic_eva_id")
    @ManyToOne
    private FichaEvaluacion fichaEvaluacion;

    public ComentarioEvaluacion() {
    }

    public ComentarioEvaluacion(Integer comEvaId) {
        this.comEvaId = comEvaId;
    }

    public Integer getComEvaId() {
        return comEvaId;
    }

    public void setComEvaId(Integer comEvaId) {
        this.comEvaId = comEvaId;
    }

    public String getComEvaDes() {
        return comEvaDes;
    }

    public void setComEvaDes(String comEvaDes) {
        this.comEvaDes = comEvaDes;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (comEvaId != null ? comEvaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ComentarioEvaluacion)) {
            return false;
        }
        ComentarioEvaluacion other = (ComentarioEvaluacion) object;
        if ((this.comEvaId == null && other.comEvaId != null) || (this.comEvaId != null && !this.comEvaId.equals(other.comEvaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.ComentarioEvaluacion[ comEvaId=" + comEvaId + " ]";
    }
    
}
