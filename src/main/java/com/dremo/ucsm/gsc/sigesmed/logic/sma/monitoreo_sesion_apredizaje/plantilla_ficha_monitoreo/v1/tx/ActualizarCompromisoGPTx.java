/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.monitoreo_sesion_apredizaje.plantilla_ficha_monitoreo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.CompromisoGestionPedagogicaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.CompromisoGestionPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class ActualizarCompromisoGPTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ActualizarCompromisoGPTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            Integer comId = requestData.getInt("comId");
            String comDes = requestData.getString("comDes");  
            Character comTip = requestData.getString("comTip").charAt(0);
            return actualizarCompromisoGP(comId, comDes, comTip);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar compromiso de gestión pedagógica. Error: ",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        } 
    }
    
    private WebResponse actualizarCompromisoGP(Integer comId, String comDes, Character comTip) {
        try{
            CompromisoGestionPedagogicaDao comGesPedDao = (CompromisoGestionPedagogicaDao)FactoryDao.buildDao("sma.CompromisoGestionPedagogicaDao");        
            CompromisoGestionPedagogica comGP = comGesPedDao.buscaPorId(comId);

            comGP.setComGesPedDes(comDes);
            comGP.setComGesPedTip(comTip);
            comGesPedDao.update(comGP);
            
            JSONObject oResponse = new JSONObject();
            oResponse.put("comId", comGP.getComGesPedId());
            oResponse.put("comDes", comGP.getComGesPedDes());
            oResponse.put("comTip", comGP.getComGesPedTip());
            return WebResponse.crearWebResponseExito("Compromiso de gestión pedagógica actualizado exitosamente",oResponse);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"actualizarCompromisoGestionPedagogica",e);
            return WebResponse.crearWebResponseError("Error, el compromiso de gestión pedagógica no fue actualizado");
        }
    }
    
}
