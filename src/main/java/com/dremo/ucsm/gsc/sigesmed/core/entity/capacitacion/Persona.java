package com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name = "PersonaCapacitacion")
@Table(name = "persona", schema = "pedagogico")
public class Persona implements Serializable {
    @Id
    @Column(name="per_id", unique = true, nullable = false)
    @SequenceGenerator(name = "secuencia_persona", sequenceName="pedagogico.persona_per_id_seq" )
    @GeneratedValue(generator="secuencia_persona")
    private int perId;
    
    @Column(name = "ape_pat",nullable = false,length = 60)
    private String apePat;
    
    @Column(name = "ape_mat",nullable = false,length = 60)
    private String apeMat;
    
    @Column(name = "nom",nullable = false,length = 60)
    private String nom;
    
    @Temporal(TemporalType.DATE)
    @Column(name = "fec_nac",nullable = false)
    private Date fecNac;
    
    @Column(name = "dni", precision=8)
    private String dni;
    
    @Column(name = "email",length = 60)
    private String email;
    
    @Column(name = "est_reg",length = 1)
    private Character estReg;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "persona")
    private Set<ParticipanteCapacitacion> participantes = new HashSet<>(0);
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "persona")
    private Set<Trabajador> trabajadores = new HashSet<>(0);
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "per")
    private Set<CapacitadorCursoCapacitacion> capacitadores = new HashSet<>(0);

    public Persona() {}
    
    public Persona(String apePat, String apeMat, String nom, Date fecNac, String dni, String email) {
        this.apePat = apePat;
        this.apeMat = apeMat;
        this.nom = nom;
        this.fecNac = fecNac;
        this.dni = dni;
        this.email = email;
        this.estReg = 'A';
    }

    public int getPerId() {
        return perId;
    }

    public void setPerId(int perId) {
        this.perId = perId;
    }

    public String getApePat() {
        return apePat;
    }

    public void setApePat(String apePat) {
        this.apePat = apePat;
    }

    public String getApeMat() {
        return apeMat;
    }

    public void setApeMat(String apeMat) {
        this.apeMat = apeMat;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getFecNac() {
        return fecNac;
    }

    public void setFecNac(Date fecNac) {
        this.fecNac = fecNac;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public Set<Trabajador> getTrabajadores() {
        return trabajadores;
    }

    public void setTrabajadores(Set<Trabajador> trabajadores) {
        this.trabajadores = trabajadores;
    }

    public Set<CapacitadorCursoCapacitacion> getCapacitadores() {
        return capacitadores;
    }

    public void setCapacitadores(Set<CapacitadorCursoCapacitacion> capacitadores) {
        this.capacitadores = capacitadores;
    }
    
    public Set<ParticipanteCapacitacion> getParticipantes() {
        return participantes;
    }

    public void setParticipantes(Set<ParticipanteCapacitacion> participantes) {
        this.participantes = participantes;
    }
}
