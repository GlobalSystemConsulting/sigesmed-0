/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.directorio.directorio_externo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.di.DirectorioExternoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.DirectorioExterno;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ActualizarDirectorioExternoTx implements ITransaction{
    
    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        DirectorioExterno dexAct = null;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            int dexid = requestData.getInt("dexId");
            String dexDir = requestData.optString("dexDir");
            String dexEma = requestData.optString("dexEma");
            String dexNom = requestData.optString("dexNom");
            Integer dexTel = (requestData.optString("dexTel").isEmpty()? null: Integer.parseInt(requestData.optString("dexTel")));
            
            dexAct = new DirectorioExterno(dexid, dexNom, dexDir, dexTel, dexEma);
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        DirectorioExternoDao dexDao = (DirectorioExternoDao)FactoryDao.buildDao("di.DirectorioExternoDao");
        try{
            dexDao.update(dexAct);
        
        }catch(Exception e){
            System.out.println("No se pudo actualizar el Directorio Externo\n"+e);
            return WebResponse.crearWebResponseError("No se pudo actualizar el Directorio Externo", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Directorio Externo se actualizo correctamente");
        //Fin
    }
    
}
