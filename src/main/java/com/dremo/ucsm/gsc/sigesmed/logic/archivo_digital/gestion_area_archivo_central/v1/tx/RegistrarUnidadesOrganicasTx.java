/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.gestion_area_archivo_central.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.UnidadOrganicaDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.UnidadOrganica;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author angell
 */
public class RegistrarUnidadesOrganicasTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        
        
        UnidadOrganica uni_org = null;
        UnidadOrganica uni_org2 = null;
       List<UnidadOrganica> listaUnidadesOrganicas = new ArrayList<UnidadOrganica>();
        try{
            
                       
            JSONObject requestData = (JSONObject)wr.getData();
            
            int areaID = requestData.getInt("areaID");
            System.out.println("el Area a agregar es : "+ areaID);
            JSONArray UnidadesOrganicas = requestData.getJSONArray("unidadesOrg");

           if(UnidadesOrganicas.length() > 0){
               
                for(int i = 0; i < UnidadesOrganicas.length();i++){
                    JSONObject bo =UnidadesOrganicas.getJSONObject(i);

                    int codOrgUn = bo.optInt("uni_org_id",0);
                    String abr = bo.getString("abreviacion");
                    String nombre = bo.getString("nombre");
                    String des = bo.getString("descripcion");
                    String estado = bo.getString("estado");

                    if(codOrgUn == 0){
                        uni_org = new UnidadOrganica(0,areaID,abr,nombre,des,estado.charAt(0));
                    }else{
                       uni_org = new UnidadOrganica(codOrgUn,areaID,abr,nombre,des,estado.charAt(0));
                    }                       
                    
                   listaUnidadesOrganicas.add(uni_org);
                }
                
            }
            
           for(int i = 0 ; i < listaUnidadesOrganicas.size(); i++){
               
            System.out.println("Nombre de la lista  : "+i+"---"+  listaUnidadesOrganicas.get(i).getnombre());
            System.out.println("Codigo UO de la lista  : "+i+"---"+  listaUnidadesOrganicas.get(i).getUniOrgId());
            System.out.println("Estado registo  : "+i+"---"+  listaUnidadesOrganicas.get(i).getEst_reg());
           }
        }catch(Exception e){
             System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar la Unidad Organica, datos incorrectos", e.getMessage() );
        }
        
        // Operaciones con la Base de Datos
        UnidadOrganicaDAO uni_org_dao = (UnidadOrganicaDAO)FactoryDao.buildDao("sad.UnidadOrganicaDAO");
        
        
        
        try{
            for(int i = 0 ; i < listaUnidadesOrganicas.size(); i++){
                
                if(listaUnidadesOrganicas.get(i).getUniOrgId() == 0){
                           //uni_org = new UnidadOrganica(0,areaID,abr,nombre,des,estado.charAt(0));
                           uni_org_dao.insert(listaUnidadesOrganicas.get(i));
                 }else{
                    System.out.println("Registro a editar con id -->: "+listaUnidadesOrganicas.get(i).getUniOrgId());
                    int corOrg = listaUnidadesOrganicas.get(i).getUniOrgId();
                    System.out.println("el area a actualizar es : "+ listaUnidadesOrganicas.get(i).getArea().getAreId());

                    int areaIDd = listaUnidadesOrganicas.get(i).getArea().getAreId();
                    System.out.println("el area a actualizar es : "+ areaIDd);
                    String abre = listaUnidadesOrganicas.get(i).getabr();
                    String nom = listaUnidadesOrganicas.get(i).getnombre();
                    String desc = listaUnidadesOrganicas.get(i).getdescripcion();
                    char est = listaUnidadesOrganicas.get(i).getEst_reg();
                            
                         uni_org2 = new UnidadOrganica(corOrg,areaIDd,abre,nom,desc,est);
                          uni_org_dao.merge(uni_org2);
                          
                 }   
            }
            
           //
            
        }catch(Exception e){
            
              System.out.println("No se pudo registrar Unidad Organica \n"+e);
            return WebResponse.crearWebResponseError("No se pudo registrar Unidad Organica", e.getMessage() );
            
        }
        
        
       // JSONObject oResponse = new JSONObject();
        //oResponse.put("uni_org_id","");
        return WebResponse.crearWebResponseExito("El registro de las Unidades Organicas  se realizo correctamente");
        
        
     //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
