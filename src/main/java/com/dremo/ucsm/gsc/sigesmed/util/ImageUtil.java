/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.util;

import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author harold
 */
public class ImageUtil {
    
    public static String pathImg = ServicioREST.PATH_SIGESMED+ File.separator +"archivos"+File.separator;

    public ImageUtil() {
    }    
        
    /**
     * Resizes an image to a absolute width and height (the image may not be
     * proportional)
     * @param directoryFile  Name of the image directory
     * @param inputImageName Name of the original image
     * @param outputImageName Name to save the resized image
     * @param scaledWidth absolute width in pixels
     * @param scaledHeight absolute height in pixels
     * @throws IOException
     */
    public static void resize(String directoryFile, String inputImageName,
            String outputImageName, int scaledWidth, int scaledHeight)
            throws IOException {
        
        String inputImagePath = pathImg + directoryFile + File.separator + inputImageName;
        String outputImagePath = pathImg + directoryFile + File.separator + outputImageName;
        
        // reads input image
        File inputFile = new File(inputImagePath);
        BufferedImage inputImage = ImageIO.read(inputFile);
 
        // creates output image
        BufferedImage outputImage = new BufferedImage(scaledWidth,
                scaledHeight, inputImage.getType());
 
        // scales the input image to the output image
        Graphics2D g2d = outputImage.createGraphics();
        g2d.drawImage(inputImage, 0, 0, scaledWidth, scaledHeight, null);
        g2d.dispose();
 
        // extracts extension of output file
        String formatName = outputImagePath.substring(outputImagePath
                .lastIndexOf(".") + 1);
 
        // writes to output file
        ImageIO.write(outputImage, formatName, new File(outputImagePath));
    }
    
}
