/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import java.util.List;

import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.CausalAlta;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.CausalAltaDAO;


/**
 *
 * @author Administrador
 */
public class ListarCausalAltaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
          JSONArray miArray = new JSONArray();
        try{
            List<CausalAlta> ca = null;
            JSONObject requestData = (JSONObject)wr.getData();

            CausalAltaDAO ca_dao = (CausalAltaDAO)FactoryDao.buildDao("scp.CausalAltaDAO");
            ca = ca_dao.listarCausalAlta();
            
            for(CausalAlta causal : ca){
                JSONObject oResponse = new JSONObject();
                oResponse.put("id_cau",causal.getCau_alt_id());
                oResponse.put("nom_cau",causal.getDes());
                miArray.put(oResponse);
            }
        }
        catch(Exception e){
              System.out.println("No se pudo Listar las Causales de Alta\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar las Causales de Alta", e.getMessage() );
        }
        
            return WebResponse.crearWebResponseExito("Se Listo las causales Correctamente",miArray); 
 
      }
    
    
    
}
