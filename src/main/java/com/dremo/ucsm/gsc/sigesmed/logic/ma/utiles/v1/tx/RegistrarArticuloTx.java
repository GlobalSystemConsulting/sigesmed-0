/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.ma.utiles.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.ArticuloEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ArticuloEscolar;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class RegistrarArticuloTx implements ITransaction{
    
    private final static Logger logger = Logger.getLogger(RegistrarArticuloTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject dataRequest = (JSONObject)wr.getData();
        JSONObject jsonTipo = dataRequest.getJSONObject("tip");
        String tipArticulo = jsonTipo.getString("nomt");
        String nomArticulo = dataRequest.getString("nom");
        String desArticulo = dataRequest.getString("des");
        String resMinArticulo = dataRequest.optString("res");
        double preArticulo = dataRequest.getDouble("pre");
        
        ArticuloEscolar articulo = new ArticuloEscolar(tipArticulo,nomArticulo.toUpperCase(),preArticulo,desArticulo,resMinArticulo);
        //ArticuloEscolar articulo = new ArticuloEscolar("aseo","toalla n",5.00,"prena algodon","R.M. 1021");
        return registrarArticulo(articulo);
    }
    private WebResponse registrarArticulo(ArticuloEscolar art) {
        art.setEstReg('A');
        art.setFecMod(new Date());

        ArticuloEscolarDao articuloEscolarDao = (ArticuloEscolarDao) FactoryDao.buildDao("ma.ArticuloEscolarDao");
        try{
            articuloEscolarDao.insert(art);
            String strjson = EntityUtil.objectToJSONString(new String[]{"artEscId","tipoArticulo","preArticulo","nomArticulo","desArticulo","resMinArticulo"},new String[]{"id","tip","pre","nom","des","res"},art);
            JSONObject respuesta = new JSONObject(strjson);
            
            return WebResponse.crearWebResponseExito("Se realizo el registro correctamente",WebResponse.OK_RESPONSE).setData(
                    respuesta
            );
        }catch (Exception e){
            logger.log(Level.SEVERE,logger.getName()+":registrarArticulo",e);
            return WebResponse.crearWebResponseError("Error al realizar el registro",WebResponse.BAD_RESPONSE);
        }
    }
}
