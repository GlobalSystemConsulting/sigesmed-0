/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.monitoreo_sesion_apredizaje.plantilla_ficha_monitoreo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.PlantillaFichaMonitoreoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica.DocenteCarpetaPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica.RutaContenidoCarpeta;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.PlantillaFichaMonitoreo;
import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.sma.carpeta_pedagogica.ArchivoCarPed;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class ListarPlantillasPorIETx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ListarPlantillasPorIETx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject requestData = (JSONObject) wr.getData();
        int orgId = requestData.getInt("orgId");
        return listar(orgId);
    }
    private WebResponse listar(int orgId){
        PlantillaFichaMonitoreoDao plaFicMonDao = (PlantillaFichaMonitoreoDao) FactoryDao.buildDao("sma.PlantillaFichaMonitoreoDao");
        List<PlantillaFichaMonitoreo> plantillas = new ArrayList<PlantillaFichaMonitoreo>();
        try{
            plantillas = plaFicMonDao.listarPlantillasPorIE(orgId);
        }catch (Exception e){
            logger.log(Level.SEVERE,"Error al listar las carpetas",e);
            return WebResponse.crearWebResponseError("Error al listar los datos");
        }
        
        JSONArray miArray = new JSONArray();
        for(PlantillaFichaMonitoreo p:plantillas ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("plaId",p.getPlaFicMonId());
            oResponse.put("plaNom",p.getPlaFicMonNom());
            oResponse.put("plaCod",p.getPlaFicMonCod());
            oResponse.put("plaDes",p.getPlaFicMonDes());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se listo correctamente",miArray);
    }
}
