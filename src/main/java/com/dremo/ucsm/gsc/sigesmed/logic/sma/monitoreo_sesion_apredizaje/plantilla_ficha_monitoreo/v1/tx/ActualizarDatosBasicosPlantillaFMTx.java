/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.monitoreo_sesion_apredizaje.plantilla_ficha_monitoreo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.PlantillaFichaMonitoreoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.PlantillaFichaMonitoreo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class ActualizarDatosBasicosPlantillaFMTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ActualizarDatosBasicosPlantillaFMTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            Integer plaId = requestData.getInt("plaId");
            String plaCod = requestData.getString("plaCod");
            String plaNom = requestData.getString("plaNom");  
            String plaDes = requestData.getString("plaDes");
            return actualizarDatosBasicosPFM(plaId, plaCod, plaNom, plaDes);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar estado de avance. Error: ",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        } 
    }
    
    private WebResponse actualizarDatosBasicosPFM(Integer plaId, String plaCod, String plaNom, String plaDes) {
        try{
            PlantillaFichaMonitoreoDao plaFMDao = (PlantillaFichaMonitoreoDao)FactoryDao.buildDao("sma.PlantillaFichaMonitoreoDao");        
            PlantillaFichaMonitoreo plaFM = plaFMDao.buscarPorId(plaId);

            plaFM.setPlaFicMonCod(plaCod);
            plaFM.setPlaFicMonNom(plaNom);
            plaFM.setPlaFicMonDes(plaDes);
            plaFMDao.update(plaFM);
            
            JSONObject oResponse = new JSONObject();
            oResponse.put("plaId", plaFM.getPlaFicMonId());
            oResponse.put("plaCod", plaFM.getPlaFicMonCod());
            oResponse.put("plaNom", plaFM.getPlaFicMonNom());
            oResponse.put("plaDes", plaFM.getPlaFicMonDes());
            return WebResponse.crearWebResponseExito("Plantilla actualizada exitosamente",oResponse);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"actualizarPlantillaFichaMonitoreo",e);
            return WebResponse.crearWebResponseError("Error, la plantilla no fue actualizada");
        }
    } 
    
}
