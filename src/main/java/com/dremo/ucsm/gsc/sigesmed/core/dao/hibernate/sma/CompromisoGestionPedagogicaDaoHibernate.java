/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.CompromisoGestionPedagogicaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.CompromisoGestionPedagogica;
import org.hibernate.Session;

public class CompromisoGestionPedagogicaDaoHibernate extends GenericDaoHibernate<CompromisoGestionPedagogica> implements CompromisoGestionPedagogicaDao{

    @Override
    public CompromisoGestionPedagogica buscaPorId(int comGesPedId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        CompromisoGestionPedagogica cgp = (CompromisoGestionPedagogica)session.get(CompromisoGestionPedagogica.class, comGesPedId);
        session.close();
        return cgp;
    }
    
}
