/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity(name = "SeccionCarpetaSMA")
@Table(name = "seccion_carpeta_pedagogica", schema = "pedagogico")
public class SeccionCarpetaPedagogica implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "sec_car_ped_id")
    private Integer secCarPedId;
    
    @Column(name = "ord")
    private Integer ord;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "nom")
    private String nom;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "est_reg")
    private Character estReg;
    
    @OneToMany(mappedBy = "seccionCarpetaPedagogica")
    private List<ContenidoSeccionesCarpetaPedagogica> contenidosSecCar;
    
    @JoinColumn(name = "car_dig_id", referencedColumnName = "car_dig_id")
    @ManyToOne
    private CarpetaPedagogica carpetaPedagogica;

    public SeccionCarpetaPedagogica() {
    }

    public SeccionCarpetaPedagogica(Integer secCarPedId) {
        this.secCarPedId = secCarPedId;
    }

    public SeccionCarpetaPedagogica(Integer secCarPedId, String nom) {
        this.secCarPedId = secCarPedId;
        this.nom = nom;
    }

    public Integer getSecCarPedId() {
        return secCarPedId;
    }

    public void setSecCarPedId(Integer secCarPedId) {
        this.secCarPedId = secCarPedId;
    }

    public Integer getOrd() {
        return ord;
    }

    public void setOrd(Integer ord) {
        this.ord = ord;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public List<ContenidoSeccionesCarpetaPedagogica> getContenidosSecCar() {
        return contenidosSecCar;
    }

    public void setContenidosSecCar(List<ContenidoSeccionesCarpetaPedagogica> contenidosSecCar) {
        this.contenidosSecCar = contenidosSecCar;
    }

    public CarpetaPedagogica getCarpetaPedagogica() {
        return carpetaPedagogica;
    }

    public void setCarpetaPedagogica(CarpetaPedagogica carpetaPedagogica) {
        this.carpetaPedagogica = carpetaPedagogica;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (secCarPedId != null ? secCarPedId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SeccionCarpetaPedagogica)) {
            return false;
        }
        SeccionCarpetaPedagogica other = (SeccionCarpetaPedagogica) object;
        if ((this.secCarPedId == null && other.secCarPedId != null) || (this.secCarPedId != null && !this.secCarPedId.equals(other.secCarPedId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica.SeccionCarpetaPedagogica[ secCarPedId=" + secCarPedId + " ]";
    }
    
}
