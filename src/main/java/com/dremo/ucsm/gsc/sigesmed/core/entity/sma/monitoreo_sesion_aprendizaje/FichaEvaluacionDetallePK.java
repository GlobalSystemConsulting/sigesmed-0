/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Yemi
 */
@Embeddable
public class FichaEvaluacionDetallePK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "fic_eva_id")
    private int ficEvaId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "ind_com_ges_ped_id")
    private int indComGesPedId;

    public FichaEvaluacionDetallePK() {
    }

    public FichaEvaluacionDetallePK(int ficEvaId, int indComGesPedId) {
        this.ficEvaId = ficEvaId;
        this.indComGesPedId = indComGesPedId;
    }

    public int getFicEvaId() {
        return ficEvaId;
    }

    public void setFicEvaId(int ficEvaId) {
        this.ficEvaId = ficEvaId;
    }

    public int getIndComGesPedId() {
        return indComGesPedId;
    }

    public void setIndComGesPedId(int indComGesPedId) {
        this.indComGesPedId = indComGesPedId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) ficEvaId;
        hash += (int) indComGesPedId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FichaEvaluacionDetallePK)) {
            return false;
        }
        FichaEvaluacionDetallePK other = (FichaEvaluacionDetallePK) object;
        if (this.ficEvaId != other.ficEvaId) {
            return false;
        }
        if (this.indComGesPedId != other.indComGesPedId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.FichaEvaluacionDetallePK[ ficEvaId=" + ficEvaId + ", indComGesPedId=" + indComGesPedId + " ]";
    }
    
}
