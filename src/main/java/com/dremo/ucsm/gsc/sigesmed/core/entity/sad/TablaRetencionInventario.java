/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sad;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Jeferson
 */
@Entity
@Table(name="tabla_retencion_inventario" , schema="administrativo")
public class TablaRetencionInventario implements java.io.Serializable{
   
   @Id 
   private int ser_doc_id;
   @ManyToOne(fetch=FetchType.LAZY)
   @JoinColumn(name="ser_doc_id" , insertable=false , updatable = false)
   private SerieDocumental serie;
   
   @Id
   private int det_inv_tra;
   @ManyToOne(fetch=FetchType.LAZY)
   @JoinColumn(name="det_inv_tra",insertable = false , updatable= false)
   private DetalleInventarioTransferencia detalle_inv_trans;
   
   public TablaRetencionInventario(){
       
   }
   public TablaRetencionInventario(int ser_doc_id ,int det_inv_tra ){
       this.ser_doc_id = ser_doc_id;
       this.det_inv_tra = det_inv_tra;
       
       this.serie = new SerieDocumental(ser_doc_id);
       this.detalle_inv_trans = new DetalleInventarioTransferencia(det_inv_tra);
   }
   
}
