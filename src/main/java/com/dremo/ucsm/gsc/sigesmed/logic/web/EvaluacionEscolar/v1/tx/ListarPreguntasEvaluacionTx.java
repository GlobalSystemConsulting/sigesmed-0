/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.tx.*;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.EvaluacionEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.TareaEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.TareaDocumento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.PreguntaEvaluacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
/**
 *
 * @author abel
 */
public class ListarPreguntasEvaluacionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        int eva_esc_id = 0;
        List<PreguntaEvaluacion> preguntas;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            eva_esc_id = requestData.getInt("eva_esc_id");
            
            EvaluacionEscolarDao eva_esc_dao = (EvaluacionEscolarDao)FactoryDao.buildDao("web.EvaluacionEscolarDao");
            preguntas = eva_esc_dao.buscarPreguntasPorEvaluacion(eva_esc_id);
            
             
            JSONArray  oResponse = new JSONArray();
            for(PreguntaEvaluacion pregunta : preguntas){
                JSONObject o = new JSONObject();
                o.put("preguntaID",pregunta.getPreEvaId());
                o.put("tit",pregunta.getTitulo());
                o.put("punt",pregunta.getPuntos());
                o.put("tie",pregunta.getTiempo());
                o.put("cont",pregunta.getPregunta());
                o.put("resp",pregunta.getRespuesta());
                o.put("alt",pregunta.getAlternativa());
                o.put("sel",false);
                o.put("tipo",String.valueOf(pregunta.getTipo()));
                o.put("orden",pregunta.getOrden());
                
                oResponse.put(o);
            }
            return WebResponse.crearWebResponseExito("Se obtuvo la Evaluacion Correctamente",oResponse);        
        }catch(Exception e){
              System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Listar las Preguntas de la Evaluacion, datos incorrectos", e.getMessage() );
        }
    
    }
    
    
    
}
