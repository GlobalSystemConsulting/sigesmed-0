package com.dremo.ucsm.gsc.sigesmed.logic.scec.comision.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.UsuarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.CargoComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.CargoDeComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.ComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.IntegranteComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Rol;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.CargoComision;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.CargoDeComision;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.Comision;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.IntegranteComision;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import static com.dremo.ucsm.gsc.sigesmed.logic.scec.comision.v1.tx.RegistrarComisionTx.registrarPresidente;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by geank on 29/09/16.
 */
public class EditarComisionTx implements ITransaction {
    private static Logger logger = Logger.getLogger(EditarComisionTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        String org = wr.getMetadataValue("org");
        JSONObject data = (JSONObject) wr.getData();
        JSONObject jsonComision = data.getJSONObject("com");
        JSONArray jsonCargos = data.getJSONArray("car");
        JSONObject jsonPresidente = data.getJSONObject("pres");
        Boolean newPresidente= data.getBoolean("newP");
        Boolean updCargos=data.getBoolean("updCarg");
        return editarComision(jsonComision,jsonCargos,jsonPresidente, newPresidente,Integer.parseInt(org),updCargos);
    }
    public WebResponse editarComision(JSONObject jsonComision, JSONArray jsonCargos, JSONObject jsonPresidente, Boolean newBoss, int idOrg,Boolean updCar ){
        try{
            ComisionDao comisionDao = (ComisionDao) FactoryDao.buildDao("scec.ComisionDao");
            CargoDeComisionDao cargoDeComisionDao = (CargoDeComisionDao)FactoryDao.buildDao("scec.CargoDeComisionDao");
            IntegranteComisionDao integranteComisionDao2 = (IntegranteComisionDao)FactoryDao.buildDao("scec.IntegranteComisionDao");

            Comision comision = comisionDao.buscarPorId(jsonComision.getInt("cod"));
            comision.setNomCom(jsonComision.optString("nom",comision.getNomCom()));
            comision.setDesCom(jsonComision.optString("des",comision.getDesCom()));
            comision.setFecMod(new Date());
            comisionDao.update(comision);
            
            //Update cargos
             CargoComisionDao cargoComisionDao = (CargoComisionDao)FactoryDao.buildDao("scec.CargoComisionDao");
            if(updCar){
            //buscamos los cargos asignados
            //primero eliminamos
                cargoDeComisionDao.eliminarCargosDeComision(comision.getComId());
            //volvemos a insertar
                RegistrarComisionTx.registrarCargoDeComision(jsonCargos,comision,cargoDeComisionDao);                         
            }               
           
            if(newBoss){
                  System.out.println("Actualizando");
//                System.out.println("INICIO:ACTUALIZAR AL PRESIDENTE");
//                //integranteComisionDao2.delete(dato);
//                System.out.println("verificando persona:"+integranteComisionDao2.buscarPresidenteComision(comision.getComId()).getPersona().getNombrePersona());
//                        
//                integranteComisionDao2.deleteAbsolute(integranteComisionDao2.buscarPresidenteComision(comision.getComId()));
//                System.out.println("MEDIO:ACTUALIZAR AL PRESIDENTE");
//                editarPresidente(jsonPresidente, comision, idOrg);
//                System.out.println("FIN:ACTUALIZAR AL PRESIDENTE");
            }
            return WebResponse.crearWebResponseExito("exito al realizar la operacion",WebResponse.OK_RESPONSE);
        }catch (Exception e){
            return WebResponse.crearWebResponseError("error al realizar la operacion", WebResponse.BAD_RESPONSE);
        }
    }
    public static void editarPresidente(JSONObject jsonPresidente, Comision comision, int org){
        //integranteComisionDao.insert(jsonPresidente);
            String dni = jsonPresidente.getString("dni");
      
            PersonaDao personaDao = (PersonaDao) FactoryDao.buildDao("PersonaDao");
            UsuarioDao userDao = (UsuarioDao) FactoryDao.buildDao("UsuarioDao");
            OrganizacionDao organizacionDao = (OrganizacionDao) FactoryDao.buildDao("OrganizacionDao");
            IntegranteComisionDao integranteComisionDao = (IntegranteComisionDao)FactoryDao.buildDao("scec.IntegranteComisionDao");
            ComisionDao comisionDao = (ComisionDao)FactoryDao.buildDao("scec.ComisionDao");
            CargoComisionDao cargoComisionDao = (CargoComisionDao)FactoryDao.buildDao("scec.CargoComisionDao");
            System.out.println("INICIO:ACTUALIZApersona");
            Persona persona = personaDao.buscarPorDNI(dni);
             System.out.println("INICIO:cargoComision");
            CargoComision cargoComision = cargoComisionDao.buscarCargoPorNombre("PRESIDENTE");
             System.out.println("INICIO:integranteComision");
            IntegranteComision integranteComision = new IntegranteComision(persona,comision,cargoComision,new Date(),null);
            integranteComision.setEstReg('A');
            integranteComision.setUsuMod(1);
            integranteComision.setFecMod(new Date());

            JSONObject jsonIntegrante = new JSONObject(EntityUtil.objectToJSONString(new String[]{"perId","dni","nom","apeMat","apePat","num1","num2","email"},new String[]{"cod","dni","nom","apem","apep","cel","fij","ema"},persona));
            jsonIntegrante.put("car",cargoComision.getNomCar());
             System.out.println("INICIO:insert");
            integranteComisionDao.insert(integranteComision);
            persona = personaDao.buscarPorOrganizacionConUsuario(dni, org);
            Usuario user = persona.getUsuario();
            Organizacion organizacion = organizacionDao.buscarConTipoOrganizacionYPadre(org);
            Rol rol = integranteComisionDao.buscarRolPorNombre("Presidente de Comision");
            user.getSessiones().add(new UsuarioSession(0, organizacion, rol, user, new Date(), new Date(), 1, 'A'));
            userDao.update(user);
    }
}

