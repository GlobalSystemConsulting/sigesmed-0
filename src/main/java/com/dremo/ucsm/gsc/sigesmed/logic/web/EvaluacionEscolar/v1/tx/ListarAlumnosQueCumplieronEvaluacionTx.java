/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.tx.*;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.EvaluacionEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.TareaEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaEvaluacionModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaTareaModel;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 *
 * @author abel
 */
public class ListarAlumnosQueCumplieronEvaluacionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int evaluacionID = 0;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            
            evaluacionID = requestData.getInt("evaluacionID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Listar los alumnos que cumplieron la Evaluacion", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<BandejaEvaluacionModel> evaluaciones = null;
        
        EvaluacionEscolarDao evaluacionDao = (EvaluacionEscolarDao)FactoryDao.buildDao("web.EvaluacionEscolarDao");
      //  TareaEscolarDao tareaDao = (TareaEscolarDao)FactoryDao.buildDao("web.TareaEscolarDao");
        try{
          //  tareas = tareaDao.buscarAlumnosQueCumplieronTarea(evaluacionID);
            evaluaciones = evaluacionDao.buscarAlumnosQueCumplieronEvaluacion(evaluacionID);
         
        }catch(Exception e){
            System.out.println("No se pudo Listar los alumnos que cumplieron Evaluacion\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los alumnos que cumplieron Evaluacion", e.getMessage() );
        }
        //Fin        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        int i = 0;
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        for(BandejaEvaluacionModel evaluacion:evaluaciones ){
            JSONObject oResponse = new JSONObject();
            
            oResponse.put("bandejaEvaluacionID",evaluacion.banEvaId);
            oResponse.put("fechaEntrega",sf.format(evaluacion.fecEnt));
            oResponse.put("fechaVisto",evaluacion.fecVis);
            oResponse.put("estado",""+evaluacion.estado);
            oResponse.put("nota",evaluacion.nota);
            oResponse.put("not_lit",evaluacion.not_lit);
            oResponse.put("nombres",evaluacion.nombres);
            oResponse.put("apellidos",evaluacion.apellido1+ " "+evaluacion.apellido2);
            oResponse.put("eva_esc_id",evaluacion.eva_esc_id);
            oResponse.put("per_id",evaluacion.per_id);
            oResponse.put("i",i++);
            
            miArray.put(oResponse);
            
            
          
            
            
            
            
            
        }
        
        return WebResponse.crearWebResponseExito("Se listaron los alumnos que cumplieron con la Evaluacion correctamente",miArray);        
        //Fin
    }
    
}

