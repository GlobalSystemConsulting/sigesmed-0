/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;



/**
 *
 * @author User
 */
public class ConstruirReporteIGATx implements ITransaction{
    int usuCod=0;
    int orgID=0;
    boolean flagUgel=false;
    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject requestData = (JSONObject) wr.getData();
            usuCod=requestData.getInt("usuCod");
            orgID=requestData.getInt("organizacionID");
            flagUgel=requestData.getBoolean("flagUgel");            
        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se pudo leer los datos de entrada");
        }
        
         return WebResponse.crearWebResponseExito("Reporte generado con éxito");       
    }
}
