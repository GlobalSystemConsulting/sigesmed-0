/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.std;

import java.io.Serializable;

/**
 *
 * @author abel
 */
public final class HistorialExpedienteId implements Serializable {
    
    private static final long serialVersionUID = 1562260205094677677L;
    private Integer expediente;
    private int hisExpId;

    public HistorialExpedienteId() {
    }

    public HistorialExpedienteId(Integer expediente, int hisExpId) {
        this.setExpediente(expediente);
        this.setHisExpId(hisExpId);
    }

    @Override
    public int hashCode() {
        return ((this.getExpediente() == null
                ? 0 : this.getExpediente().hashCode())
                ^ ((int) this.getHisExpId()));
    }

    @Override
    public boolean equals(Object otherOb) {

        if (this == otherOb) {
            return true;
        }
        if (!(otherOb instanceof HistorialExpedienteId)) {
            return false;
        }
        HistorialExpedienteId other = (HistorialExpedienteId) otherOb;
        return ((this.getExpediente() == null
                ? other.getExpediente() == null : this.getExpediente()
                .equals(other.getExpediente()))
                && (this.getHisExpId() == other.getHisExpId()));
    }

    @Override
    public String toString() {
        return "" + getExpediente() + "-" + getHisExpId();
    }

    public Integer getExpediente() {
        return expediente;
    }
    public void setExpediente(Integer expediente) {
        this.expediente = expediente;
    }

    public int getHisExpId() {
        return hisExpId;
    }
    public void setHisExpId(int hisExpId) {
        this.hisExpId = hisExpId;
    }
    
}
