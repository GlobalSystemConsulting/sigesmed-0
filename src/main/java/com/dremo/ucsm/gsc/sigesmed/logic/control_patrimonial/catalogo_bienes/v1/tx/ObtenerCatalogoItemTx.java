/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.catalogo_bienes.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.CatalogoBienesDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.CatalogoBienes;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import org.json.JSONArray;
import org.json.JSONObject;

public class ObtenerCatalogoItemTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        JSONArray miArray = new JSONArray();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try{
            CatalogoBienes cbi = null;
            
            JSONObject requestData = (JSONObject) wr.getData();
            int id_cat_ite = requestData.getInt("id_cat_ite");
            System.out.println("item dato: " +  id_cat_ite);
            
            CatalogoBienesDAO cat_dao = (CatalogoBienesDAO) FactoryDao.buildDao("scp.CatalogoBienesDAO");
            cbi = cat_dao.obtenerCatalogoItem(id_cat_ite);
            
            JSONObject oResponse = new JSONObject();
            
            System.out.println("objeto sacado: " + cbi);
            
            oResponse.put("cod", cbi.getCod());
            oResponse.put("den_bie", cbi.getDen_bie());
            oResponse.put("uni_med_id", cbi.getUni_med_id());
            oResponse.put("uni_med_nom", cbi.getUnidad_medida().getNom());
            oResponse.put("gru_gen_nom", cbi.getFamilia().getClase_generica().getGrupo().getNom());
            oResponse.put("cla_gen_nom", cbi.getFamilia().getClase_generica().getNom());
            oResponse.put("fam_gen_nom", cbi.getFamilia().getNom());
            
            miArray.put(oResponse);
            
            System.out.println("mi array final" + miArray);
        }catch(Exception e){
            System.out.println("No se obtuvo el item del catalogo\n" + e);
            return WebResponse.crearWebResponseError("No se obtuvo el item del catalogo\n", e.getMessage());
        }
        
        return WebResponse.crearWebResponseExito("Se obtuvo el item del catalogo", miArray);
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
