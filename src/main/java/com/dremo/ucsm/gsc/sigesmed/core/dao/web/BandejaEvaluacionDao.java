/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.web;

/**
 *
 * @author abel
 */

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaEvaluacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaEvaluacionModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.RespuestaEvaluacion;

import java.util.List;

public interface BandejaEvaluacionDao extends GenericDao<BandejaEvaluacion>{
    
    BandejaEvaluacion buscar_bandeja_alumno(int band_esc_id);
    List<RespuestaEvaluacion> buscar_respuestas_alumno(int band_esc_id);
    
}
