/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje;

import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.Organizacion;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

@Entity
@Table(name = "plantilla_ficha_monitoreo", schema="pedagogico")
public class PlantillaFichaMonitoreo implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pla_fic_mon_id")
    private Integer plaFicMonId;
    
    @Size(max = 2147483647)
    @Column(name = "pla_fic_mon_cod")
    private String plaFicMonCod;
    
    @Size(max = 2147483647)
    @Column(name = "pla_fic_mon_nom")
    private String plaFicMonNom;
    
    @Size(max = 2147483647)
    @Column(name = "pla_fic_mon_des")
    private String plaFicMonDes;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.DATE)
    private Date fecMod;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Size(max = 1)
    @Column(name = "est_reg")
    private String estReg;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="org_id", nullable=false)
    private Organizacion organizacion;
    
    @OneToMany(mappedBy = "plantillaFM")
    private List<CompromisoGestionPedagogica> compromisosGestionPedagogica;
    
    @OneToMany(mappedBy = "plantillaFM")
    private List<ValoracionIndicador> valoracionesIndicador;
    
    @OneToMany(mappedBy = "plantillaFM")
    private List<FichaEvaluacion> fichasEvaluacion;

    public PlantillaFichaMonitoreo() {
    }

    public PlantillaFichaMonitoreo(Integer plaFicMonId) {
        this.plaFicMonId = plaFicMonId;
    }

    public Integer getPlaFicMonId() {
        return plaFicMonId;
    }

    public void setPlaFicMonId(Integer plaFicMonId) {
        this.plaFicMonId = plaFicMonId;
    }

    public String getPlaFicMonCod() {
        return plaFicMonCod;
    }

    public void setPlaFicMonCod(String plaFicMonCod) {
        this.plaFicMonCod = plaFicMonCod;
    }

    public String getPlaFicMonNom() {
        return plaFicMonNom;
    }

    public void setPlaFicMonNom(String plaFicMonNom) {
        this.plaFicMonNom = plaFicMonNom;
    }

    public String getPlaFicMonDes() {
        return plaFicMonDes;
    }

    public void setPlaFicMonDes(String plaFicMonDes) {
        this.plaFicMonDes = plaFicMonDes;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public Organizacion getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacion(Organizacion organizacion) {
        this.organizacion = organizacion;
    }

    @Override
    public String toString() {
        return "com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.PlantillaFichaMonitoreo[ plaFicMonId=" + plaFicMonId + " ]";
    }
    
}
