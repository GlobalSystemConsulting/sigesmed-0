package com.dremo.ucsm.gsc.sigesmed.logic.web.CalendarioInstitucional.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.ActividadCalendarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.ActividadCalendario;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.DatatypeConverter;
import org.json.JSONObject;

public class EditarActividadCalendarioTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(EditarActividadCalendarioTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject data = (JSONObject) wr.getData();
            ActividadCalendarioDao actividadDao = (ActividadCalendarioDao) FactoryDao.buildDao("web.ActividadCalendarioDao");
            ActividadCalendario activity = actividadDao.buscarActividadConPadre(data.getInt("cod"));
            int actCod;

            if (activity == null) {
                activity = actividadDao.buscarActividad(data.getInt("cod"));
                actCod = activity.getActCalId();
            } else {
                actCod = activity.getActividadPadre().getActCalId();
            }

            String tit = data.getString("tit");
            String des = data.getString("des");
            Date ini = DatatypeConverter.parseDateTime(data.getString("ini")).getTime();
            Date fin = DatatypeConverter.parseDateTime(data.getString("fin")).getTime();

            activity.setTit(tit);
            activity.setDes(des);
            activity.setFecIni(ini);
            activity.setFecFin(fin);
            actividadDao.update(activity);

            if (activity.getActividadPadre() != null) {
                ActividadCalendario father = activity.getActividadPadre();
                father.setTit(tit);
                father.setDes(des);
                father.setFecIni(ini);
                father.setFecFin(fin);
                actividadDao.update(father);
            }

            if (activity.getTipAct() != 'P') {
                edit(tit, des, ini, fin, actCod);
            }

            return WebResponse.crearWebResponseExito("La actividad fue modificada correctamente", WebResponse.OK_RESPONSE);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "editarActividad", e);
            return WebResponse.crearWebResponseError("No se pudo modificar la actividad", WebResponse.BAD_RESPONSE);
        }
    }

    private void edit(String tit, String des, Date ini, Date fin, int actCod) {
        ActividadCalendarioDao actividadDao = (ActividadCalendarioDao) FactoryDao.buildDao("web.ActividadCalendarioDao");
        List<ActividadCalendario> activities = actividadDao.buscarActividades(actCod);

        for (ActividadCalendario activity : activities) {
            if (activity.getEstReg() == 'P') {
                edit(tit, des, ini, fin, activity.getActCalId());
            }

            activity.setTit(tit);
            activity.setDes(des);
            activity.setFecIni(ini);
            activity.setFecFin(fin);
            actividadDao.update(activity);

            if (activity.getFecFin().before(new Date())) {
                activity.setEstReg('F');
                actividadDao.update(activity);
            }
        }
    }
}
