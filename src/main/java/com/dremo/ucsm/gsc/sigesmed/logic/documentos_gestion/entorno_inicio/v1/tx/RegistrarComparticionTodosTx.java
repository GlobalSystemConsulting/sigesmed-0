/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.UsuarioSessionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.ItemFileDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.ItemFileDetalleDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.ItemFile;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.ItemFileDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONObject;
/**
 *
 * @author User
 */
public class RegistrarComparticionTodosTx implements ITransaction{
    ItemFileDetalle iteFilDet = null;
    Organizacion orgn=null;
    List<UsuarioSession> usuarios = null;
    Integer ideUsu = 0;
    Integer ideUsuOrg = 0;
    Integer ideDoc = 0;
    Integer ideUsuRol=0;

@Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject requestData = (JSONObject) wr.getData();

            ideDoc = requestData.getInt("ideDoc");
            ideUsu = requestData.getInt("ideUsu");
            ideUsuOrg = requestData.getInt("ideUsuOrg");
           // ideUsuRol=requestData.getInt("ideUsuRol");
         
        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se pudo leer datos para compartir a todos las instituciones de un nivel inferior");
        }
        
        OrganizacionDao org=(OrganizacionDao) FactoryDao.buildDao("OrganizacionDao");
        orgn=org.buscarConTipoOrganizacion(ideUsuOrg);
        System.out.println("LUMA COMPARTIR"+ orgn.getNom());
         if (orgn.getNom().contains("DRE") || orgn.getNom().contains("UGEL")  ){
           
            ideUsuRol=orgn.getNom().contains("DRE")? 6:7;
            UsuarioSessionDao usuario=(UsuarioSessionDao)FactoryDao.buildDao("UsuarioSessionDao");
            usuarios=usuario.obtenerUsuariosInferior(ideUsuOrg, ideUsuRol);
            System.out.print("LUMA CONTADOR: "+usuarios.size()+" "+ ideUsuRol);

            ItemFileDao filDao = (ItemFileDao) FactoryDao.buildDao("rdg.ItemFileDao");
                ItemFile iteFil = filDao.buscarPorID(ideDoc);

            ItemFileDetalleDao iteFilDao = (ItemFileDetalleDao) FactoryDao.buildDao("rdg.ItemFileDetalleDao");
            for (UsuarioSession user:usuarios){
                if(iteFilDao.verificarExisteComparticion(user.getUsuId(),iteFil)){
                    iteFilDet = new ItemFileDetalle();
                    iteFilDet.setEstReg("A");
    //                iteFilDet.setIfdPrmIde(perm);
                    iteFilDet.setIfdUsuSes(user.getUsuSesId());
                    iteFilDet.setIfdUsu(user.getUsuId());          
                    iteFilDet.setIfdIteIde(iteFil);
                    iteFilDao.insert(iteFilDet);
                     return WebResponse.crearWebResponseExito("Se registro correctamente la comparticion, Org: " +user.getOrganizacion().getNom());
                }else{
                    return WebResponse.crearWebResponseError("El documento ya esta compartido con esa persona");
                }
            
            }
            return WebResponse.crearWebResponseExito("No hay usuarios Director o Especialista UGEL a quien compartir");
        }
         else
             return WebResponse.crearWebResponseError("Restringido solo a DRE y UGEL");
    
    }
}
