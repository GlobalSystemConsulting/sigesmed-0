/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje;

import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.Docente;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

@Entity
@Table(name = "monitoreo_detalle", schema="pedagogico")
public class MonitoreoDetalle implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "mon_det_id")
    private Integer monDetId;
    
    @Column(name = "mon_det_fec_vis")
    @Temporal(TemporalType.DATE)
    private Date monDetFecVis;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.DATE)
    private Date fecMod;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Size(max = 1)
    @Column(name = "est_reg")
    private String estReg;
    
    @JoinColumn(name = "mon_id", referencedColumnName = "mon_id")
    @ManyToOne(optional = false)
    private Monitoreo monitoreo;
    
    @JoinColumn(name = "tra_id", referencedColumnName = "tra_id")
    @ManyToOne(optional = false)
    private Trabajador especialista;
    
    @JoinColumn(name = "doc_id", referencedColumnName = "doc_id")
    @ManyToOne(optional = false)
    private Docente docente;
    
    @OneToMany(mappedBy = "monitoreoDetalle")
    private List<FichaEvaluacion> fichasEvaluacion;

    public MonitoreoDetalle() {
    }

    public MonitoreoDetalle(Integer monDetId) {
        this.monDetId = monDetId;
    }

    public Integer getMonDetId() {
        return monDetId;
    }

    public void setMonDetId(Integer monDetId) {
        this.monDetId = monDetId;
    }

    public Date getMonDetFecVis() {
        return monDetFecVis;
    }

    public void setMonDetFecVis(Date monDetFecVis) {
        this.monDetFecVis = monDetFecVis;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (monDetId != null ? monDetId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MonitoreoDetalle)) {
            return false;
        }
        MonitoreoDetalle other = (MonitoreoDetalle) object;
        if ((this.monDetId == null && other.monDetId != null) || (this.monDetId != null && !this.monDetId.equals(other.monDetId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.MonitoreoDetalle[ monDetId=" + monDetId + " ]";
    }
    
}
