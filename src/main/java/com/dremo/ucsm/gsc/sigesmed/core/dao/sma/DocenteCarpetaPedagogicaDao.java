/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.sma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica.DocenteCarpetaPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica.RutaContenidoCarpeta;
import java.util.List;
import java.util.Map;

public interface DocenteCarpetaPedagogicaDao extends GenericDao<DocenteCarpetaPedagogica>{
    public List<DocenteCarpetaPedagogica> listarPorIE(int orgId);
    public RutaContenidoCarpeta buscarRutaConId(int docCarPedId);
    public DocenteCarpetaPedagogica buscarPorId(int docCarPedId);
    public List<DocenteCarpetaPedagogica> listarContenidosDocente(int orgId,int carId,int docId);
    public List<DocenteCarpetaPedagogica> listarPorUGEL(int orgPadId);
    public List<DocenteCarpetaPedagogica> listarPorDRE(int orgAbuId);
}
