/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.cpe;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TrabajadorCargo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.DiaSemana;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.DiasEspeciales;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.HorarioCab;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.HorarioDet;
import java.util.List;

/**
 *
 * @author carlos
 */
public interface HorarioDao extends GenericDao<HorarioCab>{
   
//       public Persona buscarPorDNI(Integer dni,Organizacion org);
    public List<HorarioCab> listarHorario(Organizacion org);
    public List<HorarioDet> listarHorarioById(HorarioCab hc);
    public List<DiasEspeciales> listarCalendario(Organizacion org);
    public List<DiaSemana> listarDiaSemana();
    public Boolean asignarHorarioByCargo(Integer horarioCabecera,TrabajadorCargo cargo);
    public HorarioCab buscarHorarioCargoByTrabajador(TrabajadorCargo trab);
    public HorarioCab buscarHorarioActualByTrabajador(Trabajador trab);
    public Boolean asignarHorarioPersonalizado(Trabajador trabajador, HorarioCab horario);
    public Boolean actualizarHorariosByCargo(HorarioCab horarioCab,TrabajadorCargo trabajadorCargo, Organizacion organizacion);
    public Boolean liberarHorarioById(Integer horarioCabecera);
    public Boolean actualizarHorarioFromTrabajador(HorarioCab horarioCabeceraInicial,HorarioCab horarioCabeceraFinal,Organizacion org);
    public Boolean liberarHorarioPersonalizado(Trabajador trabajador);
    public Boolean actualizarHorarioEnTrabajador(HorarioCab idHorarioCabOrigen,HorarioCab horarioDestino);
}