
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.catalogo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.CatalogoTablaDao;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.CatalogoTabla;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Administrador
 */
public class ListarRegistrosTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
      /*
        *  Parte para la operacion en la Base de Datos
        */
        //List<CatalogoTabla> catalogosTablas = null;
        CatalogoTablaDao catalogoTablaDao = (CatalogoTablaDao)FactoryDao.buildDao("CatalogoTablaDao");
        String nombretabla;
        String nombreclase = null;
        List<Object> listaRegsitro3 = null;
        Object obj = null ;
        Object entidad =null;
        
        try{
            
           // catalogosTablas = catalogoTablaDao.buscarTodos(CatalogoTabla.class);
            
            try{
                JSONObject requestData = (JSONObject)wr.getData();
                nombretabla = requestData.getString("nombretabla");
                nombreclase = requestData.getString("nombreclase");
                
              
                obj = Class.forName("com.dremo.ucsm.gsc.sigesmed.core.entity."+nombreclase).newInstance();
                
                
                listaRegsitro3 = catalogoTablaDao.buscarRegistrosTabla3(nombreclase);
                System.out.println("DATOS -->>>>>>>>>>>>>>>>>" +  listaRegsitro3);
                System.out.println("DATOS333333 -->>>>>>>>>>>>>>>>>" +  listaRegsitro3.toString());  
                
              
                
              // System.out.println("probando-----<<<"+obj.getClass().getMethod("getNombre").invoke(obj) + " " +obj.getClass().getMethod("getCodigo").invoke(obj) + " " + obj.getClass().getMethod("getDescripcion").invoke(obj));
            }catch(Exception e){
                System.out.println(e);
            }
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los Registros Consulta\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los Catalogos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        
        try {
              entidad = Class.forName("com.dremo.ucsm.gsc.sigesmed.core.entity."+nombreclase).newInstance();
              
         }catch(Exception e){
                System.out.println("ERROR de creacion de la intacncia " +e);
         }
        
        for(int i=0 ; i < listaRegsitro3.size(); i++){
           
            JSONObject oResponse = new JSONObject();
            
             try {
               
               //obj y listar.... acen referencia al clase que se intanciara   
               oResponse.put("id", obj.getClass().getMethod("getId").invoke(listaRegsitro3.get(i)));
               oResponse.put("Nombre",listaRegsitro3.get(i).getClass().getMethod("getNom").invoke(listaRegsitro3.get(i)));
               oResponse.put("Descripcion", listaRegsitro3.get(i).getClass().getMethod("getDes").invoke(listaRegsitro3.get(i)));
               oResponse.put("estado", listaRegsitro3.get(i).getClass().getMethod("getEstReg").invoke(listaRegsitro3.get(i)));

                miArray.put(oResponse);
            } catch (Exception e) {
                System.out.println("No se pudo Listar los Registros Metodos de acceso\n"+e);
            } 
           
            
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente los Registros",miArray);        
        //Fin
    }
    
}

