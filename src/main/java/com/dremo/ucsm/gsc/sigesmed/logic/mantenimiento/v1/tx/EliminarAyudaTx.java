/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.mantenimiento.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mnt.AyudaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mnt.PasosAyudaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mnt.Ayuda;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mnt.PasosAyuda;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class EliminarAyudaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        int funcionID=0;
        char tipoAyuda;
        try{
            JSONObject requestData=(JSONObject)wr.getData();
            funcionID=requestData.getInt("funcionId");
            tipoAyuda=requestData.getString("ayudaTipo").charAt(0);
        }catch(Exception e){
            System.out.println(e);
             return WebResponse.crearWebResponseError("No se pudo eliminar la ayuda, datos incorrectos", e.getMessage() );
        }
        
        /*
        *Parte de la BD
        */
         AyudaDao ayudaDao=(AyudaDao)FactoryDao.buildDao("mnt.AyudaDao");
        try{
             //Obtenemos el id de la ayuda a la que corresponse
            int id_ayuda=ayudaDao.buscarIdAyudaPorFuncionalidad(funcionID,tipoAyuda);  
            List<PasosAyuda> pasos=ayudaDao.buscarPasosPorAyuda(id_ayuda);
            //Desactivando pasos
            PasosAyudaDao pasosDao=(PasosAyudaDao)FactoryDao.buildDao("mnt.PasosAyudaDao");
            for(PasosAyuda paso: pasos){
                pasosDao.delete(paso);                
            }

            //Desactivando ayuda
            ayudaDao.delete(new Ayuda(id_ayuda));

            
            
        }catch(Exception e){
            System.out.println("No se pudo obtener la ayuda \n"+e.getMessage());
            return WebResponse.crearWebResponseError("No se pudo obtener la ayuda", e.getMessage());
        }
        return WebResponse.crearWebResponseExito("La Ayuda se elimino correctamente");
    }
    
}
