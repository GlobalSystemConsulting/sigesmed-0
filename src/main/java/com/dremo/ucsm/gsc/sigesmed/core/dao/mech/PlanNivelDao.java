/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.mech;


import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.AreaModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.GradoModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.MetaAtencion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.MetaAtencionModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.NivelModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Periodo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanHoraArea;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanNivel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Turno;
import java.util.List;
import java.util.Date;


/**
 *
 * @author abel
 */

public interface PlanNivelDao extends GenericDao<PlanNivel>{
    
    
}