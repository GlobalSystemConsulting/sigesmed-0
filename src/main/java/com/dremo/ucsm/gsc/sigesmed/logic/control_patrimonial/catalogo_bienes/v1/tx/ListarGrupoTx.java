/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.catalogo_bienes.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.GrupoGenericoDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.GrupoGenerico;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 *
 * @author Administrador
 */
public class ListarGrupoTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
            JSONArray miArray = new JSONArray();
        try{
            
            List<GrupoGenerico> grupos_genericos = null;
            JSONObject requestData = (JSONObject)wr.getData();
            GrupoGenericoDAO gru_gen_dao = (GrupoGenericoDAO)FactoryDao.buildDao("scp.GrupoGenericoDAO");
            grupos_genericos = gru_gen_dao.listarGrupos();
            
           
            for(GrupoGenerico gru_gen : grupos_genericos){
               JSONObject oResponse = new JSONObject();
               oResponse.put("gru_gen_id",gru_gen.getGru_gen_id());
               oResponse.put("nom",gru_gen.getNom()); 
               oResponse.put("cod_gru",gru_gen.getCod());
               miArray.put(oResponse);
           }            
        }catch(Exception e){
            System.out.println("No se pudo Listar los Grupos Genericos\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los Grupos Genericos", e.getMessage() );        
        }
          return WebResponse.crearWebResponseExito("Se Listo Los Grupos Genericos Correctamente",miArray); 
    }
}
