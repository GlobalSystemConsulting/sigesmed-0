/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.me.EstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.TareaEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaTarea;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.TareaDocumento;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.TareaEscolar;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Alertas;
import com.dremo.ucsm.gsc.sigesmed.util.AlertManager;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
/**
 *
 * @author abel
 */
public class InsertarTareaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */        
        
        TareaEscolar nuevaTarea = null;
        FileJsonObject miF = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            String nombre = requestData.getString("nombre");
            String descripcion = requestData.optString("descripcion");
            int numeroDoc = requestData.getInt("numeroDoc");
            
            int planID = requestData.getInt("planID");
            int gradoID = requestData.getInt("gradoID");
            char seccionID = requestData.getString("seccionID").charAt(0);
            int areaID = requestData.getInt("areaID");            
            
            int docenteID = requestData.getInt("docenteID"); 
            
            //CAMPOS QUE HACEN REFERENCIA A LA TAREA CREADA EN EL MODULO MAESTRO
            int ses_id = requestData.getInt("sesionID");
            String tip_per = requestData.getString("tip_per");
            int id_per = requestData.getInt("num_per");
            int id_tar = requestData.getInt("tar_id_ses");
         
            String tipo = requestData.getString("tipo");
            int org = requestData.getInt("org");
            
            String adjunto = requestData.optString("adjunto");
            JSONObject jsonArchivo = requestData.optJSONObject("archivo");
            if( jsonArchivo !=null && jsonArchivo.length() > 0 ){
                //nombre del documento adjunto tarea -> planID_gradoID_areaID_tar_tareaID
                miF = new FileJsonObject( jsonArchivo ,planID+"_"+gradoID+"_"+areaID+"_tar_");
                adjunto = miF.getName();
            }
            
            nuevaTarea = new TareaEscolar(0,nombre,descripcion,null,null,numeroDoc,0,0,adjunto,Tarea.ESTADO_NUEVO,planID,gradoID,seccionID,areaID,new Date(),docenteID,'A');
            nuevaTarea.setSes_id(ses_id);
            nuevaTarea.setTip_per(tip_per);
            nuevaTarea.setId_per(id_per);
            nuevaTarea.setid_tar(id_tar);
            
             /*Si la Tarea es de Tipo Fisico registramos la tarea en la bandeja de alumnos matriculados por curso  */
            if(tipo.equals("F")){
                return enviar_tarea_fisica_alumnos(nuevaTarea,org);
            }
               
                   
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar la tarea, datos incorrectos", e.getMessage() );
        }
        //Fin       
        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        TareaEscolarDao tareaDao = (TareaEscolarDao)FactoryDao.buildDao("web.TareaEscolarDao");
        try{
            tareaDao.insert(nuevaTarea);            
            
            //si ya se registro la tarea
            //verificamos si hay algun archivo adjunto
            if(miF!=null){
                nuevaTarea.setDocAdj( miF.concatName(""+nuevaTarea.getTarEscId()) );
                BuildFile.buildFromBase64(Tarea.TAREA_PATH, nuevaTarea.getDocAdj(), miF.getData());
                tareaDao.cambiarNombreAdjunto(nuevaTarea.getTarEscId(), nuevaTarea.getDocAdj() );
            }
            
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar la tarea", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("tareaID",nuevaTarea.getTarEscId());
        oResponse.put("adjunto",nuevaTarea.getDocAdj());
        oResponse.put("estado",""+nuevaTarea.getEstado());
        
       
        
        
        
        return WebResponse.crearWebResponseExito("El registro de la tarea se realizo correctamente", oResponse);
        //Fin
    }
    public WebResponse enviar_tarea_fisica_alumnos(TareaEscolar tarea,int organizacionID){
        TareaEscolarDao tareaDao = (TareaEscolarDao)FactoryDao.buildDao("web.TareaEscolarDao");
        EstudianteDao estDao = (EstudianteDao)FactoryDao.buildDao("me.EstudianteDao");
        try{
            
            List<Object[]> alumnos = estDao.buscarEstudiantesIdsPorGradoYSeccion(organizacionID, tarea.getGraId(), tarea.getSecId());
            
            //si hay alumnos se envia la tarea
            if(alumnos==null || alumnos.size()==0){
                System.out.println("No hay alumnos para enviar tarea");
                return WebResponse.crearWebResponseError("No hay alumnos para enviar tarea" );
            }
            tarea.setEstado('E');
            tareaDao.insert(tarea);
            
            List<BandejaTarea> bandejas = new ArrayList<BandejaTarea>();
            List<Integer> ids = new ArrayList<Integer>();
            for(Object[] alumno : alumnos){
                
                BandejaTarea nueva_bandeja = new BandejaTarea(0,new Date(),new Date(),0,Tarea.ESTADO_CALIFICADO,tarea.getTarEscId(),((BigInteger)alumno[0]).intValue() );
                nueva_bandeja.setDocumentos( new ArrayList<TareaDocumento>());
                nueva_bandeja.getDocumentos().add( new TareaDocumento(1, nueva_bandeja,"Tarea : " +tarea.getNom() ) );
                bandejas.add(nueva_bandeja);  
                ids.add((Integer)alumno[1]);                              
            }            
            tareaDao.enviarTarea(tarea,bandejas);
            AlertManager.sendAll(Alertas.WEB_NUEVA_TAREA, ids);
        }catch(Exception e){
            System.out.println("No se pudo enviar la tarea\n"+e);
            return WebResponse.crearWebResponseError("No se pudo enviar la tarea", e.getMessage() );
        }
        JSONObject oResponse = new JSONObject();
        oResponse.put("tareaID",tarea.getTarEscId());
        oResponse.put("adjunto",tarea.getDocAdj());
        oResponse.put("estado",""+tarea.getEstado());
        
        return WebResponse.crearWebResponseExito("El registro de la tarea se realizo correctamente", oResponse);
    }
    
    
}
