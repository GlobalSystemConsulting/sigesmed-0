/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.cuenta_contable.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sci.CuentaContableDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.CuentaContableDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentaContable;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author angel
 */
public class InsertarCuentaContableExcelTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {

        /*
        *   Parte para la lectura, verificacion y validacion de datos
         */
        CuentaContable nuevaCuentaContable = null;
        CuentaContable verificar = null;
        ArrayList<CuentaContable> CuentasContables = new ArrayList<CuentaContable>();

        FileJsonObject newFile = null;
        String fileName = "";

        try {

            String pathArchivo = "";
            int tamaño = 0;
            JSONObject requestData = (JSONObject) wr.getData();
            JSONObject arc = requestData.getJSONObject("documento");
            //leendo los documentos
            JSONObject jsonArchivo = arc.optJSONObject("archivo");
            if (jsonArchivo != null && jsonArchivo.length() > 0) {

                newFile = new FileJsonObject(jsonArchivo);
                tamaño = newFile.getSize();

                SimpleDateFormat fileFormatDate = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
                String newFileDate = fileFormatDate.format(new Date());
                fileName = newFileDate + "-" + newFile.getName();

                //System.out.println("nombre de archivo --> " + fileName+"  Tamaño :-->"+tamaño);
                BuildFile.buildFromBase64("tramite_documentario", fileName, newFile.getData());
                pathArchivo = BuildFile.getRuta();
                System.out.println("páth: " + BuildFile.getRuta());

            }

            //lectura excel
            try {
                FileInputStream file = new FileInputStream(new File(pathArchivo));
                //Crear el objeto que tendra el libro de Excel 	
                XSSFWorkbook workbook = new XSSFWorkbook(file);
                /**
                 * Obtenemos la primera pestaña a la que se quiera procesar
                 * indicando el indice. Una vez obtenida la hoja excel con las
                 * filas que se quieren leer obtenemos el iterator que nos
                 * permite recorrer cada una de las filas que contiene.
                 */
                XSSFSheet sheet = workbook.getSheetAt(0);
                Iterator<Row> rowIterator = sheet.iterator();
                Row row;
                String[] DatCel = new String[2];
                int con = 0;//permitara acceder al segundo if para el registro de cuenta contable luegio de validar las cabezaras de cada columna del excel
                // Recorremos todas las filas para mostrar el contenido de cada celda
                while (rowIterator.hasNext()) {
                    row = rowIterator.next();
                    // Obtenemos el iterator que permite recorres todas las celdas de una fila
                    Iterator<Cell> cellIterator = row.cellIterator();
                    Cell celda;
                    int i = 0;
                    while (cellIterator.hasNext()) {
                        celda = cellIterator.next();
                        // Dependiendo del formato de la celda el valor se debe mostrar como String, Fecha, boolean, entero...
                        switch (celda.getCellType()) {
                            case Cell.CELL_TYPE_NUMERIC: {
                                DatCel[i] = String.valueOf(celda.getNumericCellValue());
                                break;
                            }
                            case Cell.CELL_TYPE_STRING: {
                                DatCel[i] = celda.getStringCellValue();
                                break;
                            }
                        }
                        i++;

                    }
                    if (DatCel[0].equals("Numero Cuenta") && DatCel[1].equals("Nombre Cuenta")) {
                        con = 1;
                        continue;
                    }
                    if (con == 1) {
                        nuevaCuentaContable = new CuentaContable(0, DatCel[0], DatCel[1]);
                        nuevaCuentaContable.setEstReg('A');
                        nuevaCuentaContable.setCueEnc(Boolean.FALSE);
                        nuevaCuentaContable.setEfeReg(Boolean.FALSE);

                        nuevaCuentaContable.setUsaInfIva(Boolean.FALSE);
                        CuentasContables.add(nuevaCuentaContable);
                    } else {
                        return WebResponse.crearWebResponseError("Error en el formato de Excel");
                    }
                }

                workbook.close();

            } catch (IOException e) {
                return WebResponse.crearWebResponseError("Error en el formato de Excel .-", e.getMessage());
            } 

            //Fin de lectura de Excel .xlsx
            for (int j = 0; j < CuentasContables.size(); j++) {
                System.out.print(CuentasContables.get(j).getNumCue() + "--");
                System.out.println(CuentasContables.get(j).getNomCue());

            }
            // System.out.println("Excel ok");

            //Ingresando el Excel a la Base de Datos
            CuentaContableDao cuentaContableDao = new CuentaContableDaoHibernate();
            try {
                for (int j = 0; j < CuentasContables.size(); j++) {
                    verificar = cuentaContableDao.buscarCuentaPorNumero(CuentasContables.get(j).getNumCue());

                    if (verificar == null) {
                        cuentaContableDao.insert(CuentasContables.get(j));
                    }

                }
            } catch (Exception e) {
                return WebResponse.crearWebResponseError("No se pudo registrar la cuenta contable ", e.getMessage());
            }

        } catch (Exception e) {

            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage());
        }
        //Fin

        return WebResponse.crearWebResponseExito("El registro del expediente se realizo correctamente");
    }

}
