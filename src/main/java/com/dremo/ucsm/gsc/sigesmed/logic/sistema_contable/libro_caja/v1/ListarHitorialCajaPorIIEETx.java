/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.LibroCajaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.HechosLibro;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.cantidadModel;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
 

/**
 *
 * @author G-16
 */
public class ListarHitorialCajaPorIIEETx implements ITransaction{
    
    @Override
    public WebResponse execute(WebRequest wr) {

        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int organizacionID = 0;
        Date desde = null;
        Date hasta = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
           
            organizacionID = requestData.getInt("organizacionID");
            if(!requestData.getString("desde").contentEquals(""))
                desde = new SimpleDateFormat("dd/M/yyyy").parse( requestData.getString("desde"));
            if(!requestData.getString("hasta").contentEquals(""))
                hasta = new SimpleDateFormat("dd/M/yyyy HH:mm:ss").parse( requestData.getString("hasta") + " 23:59:59" );
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo realizar la estadistica", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        List<HechosLibro> hitorial = null;
      
        LibroCajaDao hisDao = (LibroCajaDao)FactoryDao.buildDao("sci.LibroCajaDao");
        try{
            hitorial = hisDao.HistorialCajaDeInstitucionesPorFecha( organizacionID ,desde, hasta);
            
        }catch(Exception e){
            System.out.println("E"+e);
            return WebResponse.crearWebResponseError("No se pudo realizar la estadistica Consulta", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        JSONArray debe = new JSONArray();
        JSONArray haber = new JSONArray();
        JSONArray fecha = new JSONArray();
        
         for(HechosLibro a:hitorial){
          
           debe.put(a.getImpDeb());
           haber.put(a.getImpHab());
           fecha.put(a.getFecMod());
           
        }
         
        JSONObject res = new JSONObject();
              
        res.put("debe", debe);
        
        res.put("haber", haber);
       
        res.put("fecha", fecha);

        
         
        return WebResponse.crearWebResponseExito("Se Listo correctamente",res);        
        //Fin     
        
    }
}
