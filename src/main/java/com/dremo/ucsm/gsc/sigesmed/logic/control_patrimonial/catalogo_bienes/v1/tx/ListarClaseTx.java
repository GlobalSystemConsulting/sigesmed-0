/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.catalogo_bienes.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.ClaseGenericaDAO;

import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.ClaseGenerica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.GrupoGenerico;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class ListarClaseTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        
            JSONArray miArray = new JSONArray();
        try{
            List<ClaseGenerica> clases_generica = null;
            JSONObject requestData = (JSONObject)wr.getData();
            int gru_gen_id  = requestData.getInt("gru_gen_id");
            
            ClaseGenericaDAO cla_gen_dao = (ClaseGenericaDAO)FactoryDao.buildDao("scp.ClaseGenericaDAO");
            clases_generica = cla_gen_dao.listarClases(gru_gen_id);
            
            for(ClaseGenerica cla_gen : clases_generica){
               JSONObject oResponse = new JSONObject();
               oResponse.put("cla_gen_id",cla_gen.getCla_gen_id());
               oResponse.put("gru_gen_id",cla_gen.getGru_gen_id());
               oResponse.put("cod",cla_gen.getCod());
               oResponse.put("nom",cla_gen.getNom());
               oResponse.put("cod_cla",cla_gen.getCod());
               miArray.put(oResponse);
           }
        }catch(Exception e){
            System.out.println("No se pudo Listar los Grupos Genericos\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los Clases Genericos", e.getMessage() );
        }   
          return WebResponse.crearWebResponseExito("Se Listo las Clases Genericas Correctamente",miArray); 

    }
    
}
