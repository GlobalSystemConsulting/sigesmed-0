/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.ma.notas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.ConfiguracionNotaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ConfiguracionNota;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author abel
 */
public class ListarConfiguracionNotaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        try{
            JSONObject data = (JSONObject)wr.getData();
            int orgID = data.getInt("organizacionID");
            int docID = data.getInt("docenteID");
            return listarConfiguracionNota(docID,orgID);
        }catch(Exception e){
              return WebResponse.crearWebResponseError("Error al Leer los Datos");
        }
    }
    public WebResponse listarConfiguracionNota(int docID ,int orgID){
        
        
        ConfiguracionNotaDao configNot = (ConfiguracionNotaDao) FactoryDao.buildDao("ma.ConfiguracionNotaDao");
        List<ConfiguracionNota> config_docente = configNot.listarConfiguracionDocente(docID, orgID);
        JSONArray configJSON = new JSONArray();
        for(ConfiguracionNota configuracion : config_docente){
            
            JSONObject oResponse = new JSONObject();
            oResponse.put("conf_id",configuracion.getConf_not_id());
            oResponse.put("cod",configuracion.getConf_cod());
            oResponse.put("des",configuracion.getConf_des());
            oResponse.put("not_min",configuracion.getConf_not_min());
            oResponse.put("not_max",configuracion.getConf_not_max());
            configJSON.put(oResponse);
        }
        return WebResponse.crearWebResponseExito("Se Listo correctamente la Configuracion de Nota Docente",configJSON);
        
    }
    
    
}
