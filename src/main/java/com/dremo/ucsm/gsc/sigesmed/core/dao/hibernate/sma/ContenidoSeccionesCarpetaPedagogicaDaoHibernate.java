/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.ContenidoSeccionesCarpetaPedagogicaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica.ContenidoSeccionesCarpetaPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica.EsquemaCarpeta;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;

public class ContenidoSeccionesCarpetaPedagogicaDaoHibernate extends GenericDaoHibernate<ContenidoSeccionesCarpetaPedagogica> implements ContenidoSeccionesCarpetaPedagogicaDao{

    @Override
    public List<EsquemaCarpeta> listarEsquemaCarpeta(int carId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria criteria = session.createCriteria(ContenidoSeccionesCarpetaPedagogica.class, "con")
                    .createCriteria("con.seccionCarpetaPedagogica", "sec", JoinType.INNER_JOIN)
                    .createCriteria("sec.carpetaPedagogica", "car", JoinType.INNER_JOIN)
                    .add(Restrictions.eq("car.carDigId", carId))
                    .add(Restrictions.eq("con.estReg", 'A'))
                    .add(Restrictions.eq("sec.estReg", 'A'))
                    .setProjection(Projections.projectionList()
                            .add(Projections.property("car.carDigId"),"carDigId")
                            .add(Projections.property("sec.secCarPedId"),"secCarPedId")
                            .add(Projections.property("sec.ord"),"ord")
                            .add(Projections.property("sec.nom"), "secNom")
                            .add(Projections.property("con.conSecCarPedId"),"conSecCarPedId")
                            .add(Projections.property("con.nom"), "conNom"))
                    .setResultTransformer(Transformers.aliasToBean(EsquemaCarpeta.class));
            List<EsquemaCarpeta> esquema = criteria.list();
            return esquema;
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
    
}
