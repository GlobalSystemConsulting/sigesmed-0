package com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.ParticipanteCapacitacion;
import java.math.BigInteger;
import java.util.List;

public interface ParticipanteCapacitacionDao extends GenericDao<ParticipanteCapacitacion> {
    boolean verificarParticipante (String dniDoc,int codSed);
    ParticipanteCapacitacion buscarPorId(int codPar, int codSed);
    List<Integer> listarMensaje(int codSed);
    List<BigInteger> listarParticipantes(int codSed);
    List<ParticipanteCapacitacion> listarPorSede(int codSed);
    ParticipanteCapacitacion buscarPorSedeDni(String dni, int codSed);
}
