/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity(name = "CarpetaPedagogicaSMA")
@Table(name = "carpeta_pedagogica", schema = "pedagogico")
public class CarpetaPedagogica implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "car_dig_id")
    private Integer carDigId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "eta")
    private Integer eta;
    
    @Column(name = "act")
    private Boolean act;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "fec_cre")
    @Temporal(TemporalType.DATE)
    private Date fecCre;
    
    @Size(max = 2147483647)
    @Column(name = "des")
    private String des;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "est_reg")
    private Character estReg;
    
    @OneToMany(mappedBy = "carpetaPedagogica")
    private List<SeccionCarpetaPedagogica> seccionesCarPed;
    
    @OneToMany(mappedBy = "carpeta")
    private List<DocenteCarpetaPedagogica> docenteCarpetas;

    public CarpetaPedagogica() {
    }

    public CarpetaPedagogica(Integer carDigId) {
        this.carDigId = carDigId;
    }

    public CarpetaPedagogica(Integer carDigId, Integer eta, Date fecCre) {
        this.carDigId = carDigId;
        this.eta = eta;
        this.fecCre = fecCre;
    }

    public Integer getCarDigId() {
        return carDigId;
    }

    public void setCarDigId(Integer carDigId) {
        this.carDigId = carDigId;
    }

    public Integer getEta() {
        return eta;
    }

    public void setEta(Integer eta) {
        this.eta = eta;
    }

    public Boolean getAct() {
        return act;
    }

    public void setAct(Boolean act) {
        this.act = act;
    }

    public Date getFecCre() {
        return fecCre;
    }

    public void setFecCre(Date fecCre) {
        this.fecCre = fecCre;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public List<SeccionCarpetaPedagogica> getSeccionesCarPed() {
        return seccionesCarPed;
    }

    public void setSeccionesCarPed(List<SeccionCarpetaPedagogica> seccionesCarPed) {
        this.seccionesCarPed = seccionesCarPed;
    }

    public List<DocenteCarpetaPedagogica> getDocenteCarpetas() {
        return docenteCarpetas;
    }

    public void setDocenteCarpetas(List<DocenteCarpetaPedagogica> docenteCarpetas) {
        this.docenteCarpetas = docenteCarpetas;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (carDigId != null ? carDigId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CarpetaPedagogica)) {
            return false;
        }
        CarpetaPedagogica other = (CarpetaPedagogica) object;
        if ((this.carDigId == null && other.carDigId != null) || (this.carDigId != null && !this.carDigId.equals(other.carDigId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica.CarpetaPedagogica[ carDigId=" + carDigId + " ]";
    }
    
}
