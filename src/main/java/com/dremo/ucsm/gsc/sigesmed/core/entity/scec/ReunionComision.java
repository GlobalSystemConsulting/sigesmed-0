/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.scec;

import org.hibernate.annotations.DynamicUpdate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

/**
 *
 * @author ucsm
 */
@Entity
@Table(name="reunion_comision", schema="pedagogico")
@DynamicUpdate(value = true)
public class ReunionComision implements java.io.Serializable{

    @Id
    @Column(name="reu_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_reunion_comision", sequenceName="pedagogico.reunion_comision_reu_id_seq" )
    @GeneratedValue(generator="secuencia_reunion_comision")
    private int reuId;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_reu", length = 29)
    private Date fecReu;

    @Column(name="res_con_reu", length = 20)//responsable convocar reunion
    private String resConReu;
    
    @Column(name="lug_reu", length = 20) // lugar de la reunion
    private String lugReu;

    @Column(name = "usu_mod")
    private Integer usuMod;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length=29)
    private Date fecMod;

    @Column(name = "est_reg",length = 1)
    private Character estReg;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="com_id")
    private Comision comision;

    @OneToMany(mappedBy = "reunionComision", fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<AsistenciaReunionComision> asistenciasReunion = new ArrayList<>();

    @OneToMany(mappedBy = "reunionComision", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    private List<ActasReunionComision> actasReunion = new ArrayList<>();

    public ReunionComision() {
    }

    public ReunionComision(Date fecReu, String resConReu, String lugReu) {
        this.fecReu = fecReu;
        this.resConReu = resConReu;
        this.lugReu = lugReu;

        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public ReunionComision(Date fecReu, String resConReu, String lugReu, Comision comision) {
        this.fecReu = fecReu;
        this.resConReu = resConReu;
        this.lugReu = lugReu;
        this.comision = comision;
    }

    public int getReuId() {
        return reuId;
    }

    public void setReuId(int reuId) {
        this.reuId = reuId;
    }

    public Date getFecReu() {
        return fecReu;
    }

    public void setFecReu(Date fecReu) {
        this.fecReu = fecReu;
    }

    public String getResConReu() {
        return resConReu;
    }

    public void setResConReu(String resConReu) {
        this.resConReu = resConReu;
    }

    public String getLugReu() {
        return lugReu;
    }

    public void setLugReu(String lugReu) {
        this.lugReu = lugReu;
    }

    public int getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(int usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public char getEstReg() {
        return estReg;
    }

    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }

    public Comision getComision() {
        return comision;
    }

    public void setComision(Comision comision) {
        this.comision = comision;
    }

    public List<AsistenciaReunionComision> getAsistenciasReunion() {
        return asistenciasReunion;
    }

    public void setAsistenciasReunion(List<AsistenciaReunionComision> asistenciasReunion) {
        this.asistenciasReunion = asistenciasReunion;
    }

    public List<ActasReunionComision> getActasReunion() {
        return actasReunion;
    }

    public void setActasReunion(List<ActasReunionComision> actasReunion) {
        this.actasReunion = actasReunion;
    }
}

