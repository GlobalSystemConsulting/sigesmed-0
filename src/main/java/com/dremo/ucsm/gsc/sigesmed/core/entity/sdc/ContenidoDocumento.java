package com.dremo.ucsm.gsc.sigesmed.core.entity.sdc;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="contenido_documento" ,schema="administrativo")
public class ContenidoDocumento  implements java.io.Serializable {


    @Id
    @Column(name="ctn_doc_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_contenido_documento", sequenceName="administrativo.contenido_documento_ctn_doc_id_seq" )
    @GeneratedValue(generator="secuencia_contenido_documento")
    private Integer conDocId;
    
    @Column(name="ctn_doc_tip")
    private String conDocTip;
    
    @Column(name="ctn_doc_con")
    private String conDocCon;
  
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ctn_doc_doc_id", nullable=false)
    private Documento docId;
   
    public ContenidoDocumento() {
    }

    public ContenidoDocumento(String conDocTip, String conDocCon, Documento docId) {
        this.conDocTip = conDocTip;
        this.conDocCon = conDocCon;
        this.docId = docId;
    }

    public ContenidoDocumento(Integer conDocId, String conDocTip, String conDocCon, Documento docId) {
        this.conDocId = conDocId;
        this.conDocTip = conDocTip;
        this.conDocCon = conDocCon;
        this.docId = docId;
    }

    public Integer getConDocId() {
        return conDocId;
    }

    public void setConDocId(Integer conDocId) {
        this.conDocId = conDocId;
    }

    public String getConDocTip() {
        return conDocTip;
    }

    public void setConDocTip(String conDocTip) {
        this.conDocTip = conDocTip;
    }

    public String getConDocCon() {
        return conDocCon;
    }

    public void setConDocCon(String conDocCon) {
        this.conDocCon = conDocCon;
    }

    public Documento getDocId() {
        return docId;
    }

    public void setDocId(Documento docId) {
        this.docId = docId;
    }

   
    
    
}


