/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.monitoreo_sesion_apredizaje.plantilla_ficha_monitoreo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.IndicadorCompromisoGestionPedagogicaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.IndicadorCompromisoGestionPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class ActualizarIndicadorCGPTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ActualizarIndicadorCGPTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            Integer indId = requestData.getInt("indId");
            String indDes = requestData.getString("indDes");  
            return actualizarIndicadorCGP(indId, indDes);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar indicador de compromiso de gestión pegagógica. Error: ",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        } 
    }
    
    private WebResponse actualizarIndicadorCGP(Integer indId, String indDes) {
        try{
            IndicadorCompromisoGestionPedagogicaDao indComGesPedDao = (IndicadorCompromisoGestionPedagogicaDao)FactoryDao.buildDao("sma.IndicadorCompromisoGestionPedagogicaDao");        
            IndicadorCompromisoGestionPedagogica indCGP = indComGesPedDao.buscarPorId(indId);

            indCGP.setIndComGesPedDes(indDes);
            indComGesPedDao.update(indCGP);
            
            JSONObject oResponse = new JSONObject();
            oResponse.put("indId", indCGP.getIndComGesPedId());
            oResponse.put("indDes", indCGP.getIndComGesPedDes());
            return WebResponse.crearWebResponseExito("Indicador de compromiso de gestión pedagógica actualizado exitosamente",oResponse);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"actualizarIndicadorCompromisoGestionPedagogica",e);
            return WebResponse.crearWebResponseError("Error, el indicador de compromiso de gestión pedagógica no fue actualizado");
        }
    }
    
}
