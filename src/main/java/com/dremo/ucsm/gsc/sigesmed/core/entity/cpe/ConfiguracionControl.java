package com.dremo.ucsm.gsc.sigesmed.core.entity.cpe;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="configuracion_control" ,schema="administrativo")
public class ConfiguracionControl  implements java.io.Serializable {


    @Id
    @Column(name="con_per_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_configuracion_control", sequenceName="administrativo.configuracion_control_con_per_id_seq" )
    @GeneratedValue(generator="secuencia_configuracion_control")
    private Integer conPerId;

    @Column(name="con_per_tol_hor")
    private Integer conPerTolHora;
     
    @Column(name="con_per_tol_min")
    private Integer conPerTolMin;
    
    @Column(name="con_per_doc")
    private String conPerDoc;
    
    @Column(name="con_per_des")
    private String conPerDes;
    
    @Column(name="est_reg")
    private String estReg;
    
    @Column(name="con_per_org")
    private Integer conPerOrg;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="con_per_fec_ini")
    private Date conPerFecIni;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="con_per_fec_fin")
    private Date conPerFecFin;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="tra_id", nullable=false )
    private Trabajador trabajadorId;  

    @OneToMany(mappedBy="sisBioConPerId",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
    private List<SistemaBiometrico> sistemasBiometricos;

    public ConfiguracionControl() {
    }

    public ConfiguracionControl(Integer conPerId) {
        this.conPerId = conPerId;
    }

    
    public ConfiguracionControl(Integer conPerId, Integer conPerTolHora, Integer conPerTolMin, String conPerDoc, String conPerDes, String estReg, Integer conPerOrg, Date conPerFecIni, Date conPerFecFin, Trabajador trabajadorId, List<SistemaBiometrico> sistemasBiometricos) {
        this.conPerId = conPerId;
        this.conPerTolHora=conPerTolHora;
        this.conPerTolMin=conPerTolMin;
        this.conPerDoc = conPerDoc;
        this.conPerDes = conPerDes;
        this.estReg = estReg;
        this.conPerOrg = conPerOrg;
        this.conPerFecIni = conPerFecIni;
        this.conPerFecFin = conPerFecFin;
        this.trabajadorId = trabajadorId;
        this.sistemasBiometricos = sistemasBiometricos;
    }
     
    
    public ConfiguracionControl(Integer conPerTolHora,Integer conPerTolMin, String conPerDoc,String conPerDes, String estReg, Integer conPerOrg, Date conPerFecIni, Date conPerFecFin, Trabajador trabajadorId) {
        this.conPerTolHora=conPerTolHora;
        this.conPerTolMin=conPerTolMin;
        this.conPerDoc = conPerDoc;
        this.estReg = estReg;
        this.conPerOrg = conPerOrg;
        this.conPerFecIni = conPerFecIni;
        this.conPerFecFin = conPerFecFin;
        this.trabajadorId = trabajadorId;
        this.conPerDes = conPerDes;
    }

    public ConfiguracionControl(Integer conPerTolHora,Integer conPerTolMin, String conPerDoc,String conPerDes, String estReg, Integer conPerOrg, Date conPerFecIni, Date conPerFecFin, Trabajador trabajadorId, List<SistemaBiometrico> sistemasBiometricos) {
        this.conPerTolHora=conPerTolHora;
        this.conPerTolMin=conPerTolMin;
        this.conPerDoc = conPerDoc;
        this.estReg = estReg;
        this.conPerOrg = conPerOrg;
        this.conPerFecIni = conPerFecIni;
        this.conPerFecFin = conPerFecFin;
        this.trabajadorId = trabajadorId;
        this.sistemasBiometricos = sistemasBiometricos;
        this.conPerDes=conPerDes;
    }

    public Integer getConPerId() {
        return conPerId;
    }

    public void setConPerId(Integer conPerId) {
        this.conPerId = conPerId;
    }

    public Integer getConPerTolHora() {
        return conPerTolHora;
    }

    public void setConPerTolHora(Integer conPerTolHora) {
        this.conPerTolHora = conPerTolHora;
    }

    public Integer getConPerTolMin() {
        return conPerTolMin;
    }

    public void setConPerTolMin(Integer conPerTolMin) {
        this.conPerTolMin = conPerTolMin;
    }

   
    public String getConPerDoc() {
        return conPerDoc;
    }

    public void setConPerDoc(String conPerDoc) {
        this.conPerDoc = conPerDoc;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public Integer getConPerOrg() {
        return conPerOrg;
    }

    public void setConPerOrg(Integer conPerOrg) {
        this.conPerOrg = conPerOrg;
    }

    public Date getConPerFecIni() {
        return conPerFecIni;
    }

    public void setConPerFecIni(Date conPerFecIni) {
        this.conPerFecIni = conPerFecIni;
    }

    public Date getConPerFecFin() {
        return conPerFecFin;
    }

    public void setConPerFecFin(Date conPerFecFin) {
        this.conPerFecFin = conPerFecFin;
    }

    public Trabajador getTrabajadorId() {
        return trabajadorId;
    }

    public void setTrabajadorId(Trabajador trabajadorId) {
        this.trabajadorId = trabajadorId;
    }

    public List<SistemaBiometrico> getSistemasBiometricos() {
        return sistemasBiometricos;
    }

    public void setSistemasBiometricos(List<SistemaBiometrico> sistemasBiometricos) {
        this.sistemasBiometricos = sistemasBiometricos;
    }

    public String getConPerDes() {
        return conPerDes;
    }

    public void setConPerDes(String conPerDes) {
        this.conPerDes = conPerDes;
    }

   
}


