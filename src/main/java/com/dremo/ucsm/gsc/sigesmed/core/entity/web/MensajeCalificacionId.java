/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.web;

import java.io.Serializable;

/**
 *
 * @author abel
 */
public final class MensajeCalificacionId implements Serializable {
    
    private static final long serialVersionUID = 1562260205094677677L;
    private Integer evaluacion;
    private int menCalId;

    public MensajeCalificacionId() {
    }

    public MensajeCalificacionId(Integer evaluacion, int menCalId) {
        this.setEvaluacion(evaluacion);
        this.setMenCalId(menCalId);
    }

    @Override
    public int hashCode() {
        return ((this.getEvaluacion() == null
                ? 0 : this.getEvaluacion().hashCode())
                ^ ((int) this.getMenCalId()));
    }

    @Override
    public boolean equals(Object otherOb) {

        if (this == otherOb) {
            return true;
        }
        if (!(otherOb instanceof MensajeCalificacionId)) {
            return false;
        }
        MensajeCalificacionId other = (MensajeCalificacionId) otherOb;
        return ((this.getEvaluacion() == null
                ? other.getEvaluacion() == null : this.getEvaluacion()
                .equals(other.getEvaluacion()))
                && (this.getMenCalId() == other.getMenCalId()));
    }

    @Override
    public String toString() {
        return "" + getEvaluacion() + "-" + getMenCalId();
    }

    public Integer getEvaluacion() {
        return evaluacion;
    }

    public void setEvaluacion(Integer evaluacion) {
        this.evaluacion = evaluacion;
    }

    public int getMenCalId() {
        return menCalId;
    }

    public void setMenCalId(int menCalId) {
        this.menCalId = menCalId;
    }
    
}
