/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author gscadmin
 */
@Entity
@Table(name = "compromisos_gestion", schema = "pedagogico")

public class CompromisosGestion implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cge_id")
    private Integer cgeId;
    
    @Column(name = "cge_des")
    private String cgeDes;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.DATE)
    private Date fecMod;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "est_reg")
    private String estReg;
    
    @JoinColumn(name = "plf_id", referencedColumnName = "plf_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private PlantillaFicha plfId;
    
    @OneToMany(mappedBy = "cgeId", fetch = FetchType.LAZY)
    private List<Items> itemsList;

    public CompromisosGestion() {
    }

    public CompromisosGestion(Integer cgeId) {
        this.cgeId = cgeId;
    }
    
    public CompromisosGestion(Integer cgeId, PlantillaFicha plfId) {        
        this.cgeId = cgeId;        
        this.plfId = plfId;
    }
    
    public CompromisosGestion(String cgeDes, PlantillaFicha plfId) {        
        this.cgeDes = cgeDes;        
        this.plfId = plfId;
    }
    
    public CompromisosGestion(Integer cgeId, String cgeDes, PlantillaFicha plfId) {        
        this.cgeId = cgeId;        
        this.cgeDes = cgeDes;
        this.plfId = plfId;
    }

    public Integer getCgeId() {
        return cgeId;
    }

    public void setCgeId(Integer cgeId) {
        this.cgeId = cgeId;
    }

    public String getCgeDes() {
        return cgeDes;
    }

    public void setCgeDes(String cgeDes) {
        this.cgeDes = cgeDes;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public PlantillaFicha getPlfId() {
        return plfId;
    }

    public void setPlfId(PlantillaFicha plfId) {
        this.plfId = plfId;
    }

    @XmlTransient
    public List<Items> getItemsList() {
        return itemsList;
    }

    public void setItemsList(List<Items> itemsList) {
        this.itemsList = itemsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cgeId != null ? cgeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CompromisosGestion)) {
            return false;
        }
        CompromisosGestion other = (CompromisosGestion) object;
        if ((this.cgeId == null && other.cgeId != null) || (this.cgeId != null && !this.cgeId.equals(other.cgeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades_sma.CompromisosGestion[ cgeId=" + cgeId + " ]";
    }
    
}
