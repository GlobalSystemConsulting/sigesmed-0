/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.cuenta_contable.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sci.CuentaContableDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.CuentaContableDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentaContable;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class ListarCuentaContableTx implements ITransaction{
    
    @Override
    public WebResponse execute(WebRequest wr){
        //Parte de la operacion con la Base de Datos
        
        List<CuentaContable> cuentas=null;
        CuentaContableDao cuentasDao = new CuentaContableDaoHibernate();
        
        try {
            cuentas= cuentasDao.buscarTodos(CuentaContable.class);
            
        } catch (Exception e) {
            System.out.println("No se pudo Listar las Cuentas\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar las Cuentas", e.getMessage() );
        }
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        
       
            for(CuentaContable cuenta:cuentas ){
                JSONObject oResponse = new JSONObject();
                oResponse.put("cuentaContableID",cuenta.getCueConId());
                oResponse.put("numero",cuenta.getNumCue());
                oResponse.put("nombre",cuenta.getNomCue());
                oResponse.put("tipo",cuenta.getTip());
                oResponse.put("encabezado",cuenta.getCueEnc());
                oResponse.put("efectivo",cuenta.getEfeReg());

                oResponse.put("subClase",cuenta.getSubCla());


                oResponse.put("iva", cuenta.getUsaInfIva());


                oResponse.put("estado",""+cuenta.getEstReg());
                miArray.put(oResponse);
            }
     
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
        
        
        
    }
    
}
