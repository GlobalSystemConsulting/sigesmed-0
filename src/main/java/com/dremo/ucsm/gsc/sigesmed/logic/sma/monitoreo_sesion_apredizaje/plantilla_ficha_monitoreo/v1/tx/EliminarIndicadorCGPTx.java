/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.monitoreo_sesion_apredizaje.plantilla_ficha_monitoreo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.IndicadorCompromisoGestionPedagogicaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.IndicadorCompromisoGestionPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class EliminarIndicadorCGPTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(EliminarIndicadorCGPTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject jsonData = (JSONObject)wr.getData();
        int indId = jsonData.getInt("indId");
        return eliminarIndicadorCGP(indId);
    }

    private WebResponse eliminarIndicadorCGP(int indId) {
        try{
            IndicadorCompromisoGestionPedagogicaDao indComGesPedDao = (IndicadorCompromisoGestionPedagogicaDao) FactoryDao.buildDao("sma.IndicadorCompromisoGestionPedagogicaDao");
            IndicadorCompromisoGestionPedagogica indCGP = indComGesPedDao.buscarPorId(indId);
            indComGesPedDao.delete(indCGP);
            return WebResponse.crearWebResponseExito("Se elimino correctamente el indicador de compromiso de gestión pedagógica");

        }catch (Exception e){
            logger.log(Level.SEVERE,"eliminarIndicadorCompromisoGestionPedagogica",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el indicador de compromiso de gestión pedagógica");
        }
    }
    
}
