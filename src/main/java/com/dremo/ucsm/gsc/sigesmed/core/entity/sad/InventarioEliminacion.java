/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sad;


import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
/**
 *
 * @author Jeferson
 */
@Entity
@Table(name="inventario_eliminacion_series_documentales" , schema = "administrativo")
public class InventarioEliminacion  implements java.io.Serializable {
    @Id
    @Column(name="inv_eli_ser_doc_id" , unique = true , nullable=false)
    @SequenceGenerator(name="secuencia_inv_eli_ser_doc" , sequenceName = "administrativo.inventario_eliminacion_series_documental_inv_eli_ser_doc_id_seq")
    @GeneratedValue(generator ="secuencia_inv_eli_ser_doc")
    private int inv_eli_ser_doc_id;
    
    @Id
    private int ser_doc_id;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ser_doc_id" , insertable = false , updatable= false)
    private SerieDocumental serie;
    
    @Column(name="fec")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fec;
    
    @Column(name="res")
    private String res;
    
    @Column(name="apro")
    private boolean apro;
    
    public InventarioEliminacion(){
        
    }
    public InventarioEliminacion(int inv_eli_ser_doc_id , int ser_doc_id){
        this.inv_eli_ser_doc_id = inv_eli_ser_doc_id;
        this.serie = new SerieDocumental(ser_doc_id);
    }
    public InventarioEliminacion(int inv_eli_ser_doc_id , int ser_doc_id , Date fec , String res , boolean apro ){
        this.inv_eli_ser_doc_id = inv_eli_ser_doc_id;
        this.serie = new SerieDocumental(ser_doc_id);
        this.fec = fec;
        this.res =res;
        this.apro =apro;
    }
    public void setfec(Date fec){
        this.fec = fec;
    }
    public Date getfec(){
        return this.fec;
    }
    public void setres(String res){
        this.res = res;
    }
    public String getres(){
        return this.res;
    }
    public void setapro(boolean apro){
        this.apro = apro;
    }
    public boolean getapro(){
        return this.apro;
    }
 }
