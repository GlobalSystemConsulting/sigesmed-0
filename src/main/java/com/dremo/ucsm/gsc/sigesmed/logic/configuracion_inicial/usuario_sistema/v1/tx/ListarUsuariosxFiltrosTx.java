/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.usuario_sistema.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.UsuarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author ang
 */
public class ListarUsuariosxFiltrosTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        long canTotReg = 0;
        int limit=0;
        int offset =0;
        int opcion = 0;
        JSONArray filtros = new JSONArray();
        /*
        *  Parte para la operacion en la Base de Datos
        */
        try {
            JSONObject requestData = (JSONObject) wr.getData();
            limit = requestData.getInt("limit");
            offset = requestData.getInt("offset");
            opcion = requestData.getInt("opcion");
            filtros = requestData.getJSONArray("filtros");
        } catch (Exception e) {
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo listar usuarios por organizacion, datos incorrectos", e.getMessage());
        }
        
        List<Usuario> usuarios = null;
        UsuarioDao usuarioDao = (UsuarioDao)FactoryDao.buildDao("UsuarioDao");
        try{
            switch(opcion){
                case 0: usuarios = usuarioDao.buscarConRolYOrganizacion(limit, offset);
                        canTotReg = usuarioDao.mostrarNumeroRegistros(Usuario.class);
                        System.out.println("canTotReg: " + canTotReg);
                        break;
                
                case 1: 
                        canTotReg = usuarioDao.canRegBusqxFiltros(filtros); 
                        System.out.println("canTotReg: " + canTotReg);  
                        
                        usuarios = usuarioDao.searchWithFilters(limit, offset, filtros);
                        break;
            }
            
        }catch(Exception e){
            System.out.println("No se pudo Listar los usuarios del Sistema\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los usuarios del Sistema ", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(Usuario usuario:usuarios ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("usuarioID",usuario.getUsuId());
            oResponse.put("nombreUsuario",usuario.getNom());            
            oResponse.put("password",usuario.getPas());
            oResponse.put("estado",""+usuario.getEstReg());
            oResponse.put("perId", usuario.getPersona().getPerId());
            oResponse.put("DNI", usuario.getPersona().getDni());
            oResponse.put("nombres", usuario.getPersona().getNombrePersona());
            oResponse.put("nombre", usuario.getPersona().getNom());
            oResponse.put("paterno", usuario.getPersona().getApePat());
            oResponse.put("materno", usuario.getPersona().getApeMat());
            if( usuario.getPersona().getEmail()!= null)
                oResponse.put("email", usuario.getPersona().getEmail());
            else
                oResponse.put("email", "");
            
            if( usuario.getPersona().getNum1()!= null)
                oResponse.put("numero1", usuario.getPersona().getNum1());
            else
                oResponse.put("numero1", "");
            if(usuario.getPersona().getNum2()!= null)
                oResponse.put("numero2", usuario.getPersona().getNum2());
            else
                oResponse.put("numero2", "");
            
            //datos de session
            
            JSONArray aSessiones = new JSONArray();
            for( UsuarioSession session: usuario.getSessiones() ){
                JSONObject oSession = new JSONObject();
                oSession.put("rolID",session.getRol().getRolId() );
                oSession.put("rol",session.getRol().getNom() );
                oSession.put("sessionID",session.getUsuSesId() );
                oSession.put("areaID",session.getAreId());
                oSession.put("organizacionID",session.getOrganizacion().getOrgId() );
                oSession.put("organizacion",session.getOrganizacion().getNom() );                
                oSession.put("estado",""+session.getEstReg());

                aSessiones.put(oSession);
            }
            oResponse.put("sessiones", aSessiones);
            miArray.put(oResponse);
        }
        
        JSONObject respuesta = new JSONObject();
        respuesta.put("dataUsuario",miArray);
        respuesta.put("canTotReg", canTotReg);
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",respuesta);        
        //Fin
    }
    
}
