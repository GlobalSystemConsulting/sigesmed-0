/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.control_libro.v1.tx;


import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sci.ControlLibroDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.ControlLibroDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.ControlLibro;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class InsertarControlTx implements ITransaction{   
    
    @Override    
    public WebResponse execute(WebRequest wr)  {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        ControlLibro nuevoControl = null;        
        int year ;
        int organizacionId ;
        List<ControlLibro> controles=null;
        try{            
             
            JSONObject requestData = (JSONObject)wr.getData();           
            String descripcion = requestData.getString("descripcion");            
            String fechaP = requestData.getString("fechaPreCierre");
            String fechaC = requestData.getString("fechaCierre");
            String fechaR = requestData.getString("fechaReApertura");
            organizacionId = requestData.getInt("organizacionID");
            year = requestData.getInt("year");
            
            Date fechaPreCierre= new Date(fechaP);
            Date fechaCierre= new Date(fechaC);            
            Date fechaReApertura= new Date(fechaR);   
            
            nuevoControl = new ControlLibro((short)0,new Organizacion(organizacionId),descripcion,fechaPreCierre,fechaCierre,wr.getIdUsuario(),new Date(), 'A');
            nuevoControl.setFecRea(fechaReApertura);
            
        }catch(Exception e){
          
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage() );
        }
         //Fin
        
        /*
        *   Parte de Logica de Negocio    
        *
        */
        
     

        ControlLibroDao cuentaContableDao = new ControlLibroDaoHibernate();
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        try{  
            controles = cuentaContableDao.listarControles(organizacionId,year);
            System.out.println("longitud de lista de contorloes....."+controles.size());
            if(controles.size() <= 0){
                cuentaContableDao.insert(nuevoControl);        
            }
            else if(controles.size() > 0){
                return WebResponse.crearWebResponseError("No se pudo registrar control del libro , Ya Existe Uno Activo  " );
            }
            
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo registrar control del libro ", e.getMessage() );
        }
        //Fin
        
      
        /*
        *  Repuesta Correcta
        */
        DateFormat fechaHora = new SimpleDateFormat("dd/MM/yyyy");
        JSONObject oResponse = new JSONObject();

        
            oResponse.put("controlID",nuevoControl.getConLibId());
            oResponse.put("descripcion",nuevoControl.getNom());
            oResponse.put("fechaPreCierre",fechaHora.format(nuevoControl.getFecPre()));        
            oResponse.put("fechaCierre",fechaHora.format(nuevoControl.getFecCie()));
            
            oResponse.put("estado",""+nuevoControl.getEstReg());
        

        return WebResponse.crearWebResponseExito("El registro del Control realizo correctamente", oResponse);
        //Fin
    }    
    
    
}

