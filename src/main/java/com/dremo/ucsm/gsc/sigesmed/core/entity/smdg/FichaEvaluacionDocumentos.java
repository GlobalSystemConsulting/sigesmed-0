/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.smdg;

import java.io.Serializable;

import java.util.Date;

import javax.persistence.Basic;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "ficha_evaluacion_documentos", schema = "institucional")

public class FichaEvaluacionDocumentos implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "fev_doc_id")
    private Integer fevDocId;
    @Basic(optional = false)
    @Column(name = "fev_fec")
    @Temporal(TemporalType.DATE)
    private Date fevFec;
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "est_reg")
    private String estReg = "A";
    @Column(name = "fev_tot")
    private Integer fevTot;
    @Column(name = "fev_eva")
    private Integer fevEva;
    @Column(name = "fev_esp")
    private Integer fevEsp;
    @Column(name = "pla_id")
    private Integer plaid;
    
//    @OneToMany(cascade = CascadeType.ALL)
//    @JoinColumn(name="fev_doc_id")
//    @LazyCollection(LazyCollectionOption.FALSE)
//    private Set<FichaDetalle> detalles;
    
    @JoinColumn(name = "ite_ide", referencedColumnName = "ite_ide")    
    @ManyToOne(fetch=FetchType.LAZY)
    private ItemFile file;

    public FichaEvaluacionDocumentos() {
    }

    public FichaEvaluacionDocumentos(Integer fevDocId) {
        this.fevDocId = fevDocId;
    }

    public FichaEvaluacionDocumentos(Integer fevDocId, Date fevFec) {
        this.fevDocId = fevDocId;
        this.fevFec = fevFec;
    }

    public FichaEvaluacionDocumentos(Date fevFec, Integer fevTot, Integer fevEva, Integer fevEsp, ItemFile file, Integer plaid) {
        this.fevFec = fevFec;        
        this.fevTot = fevTot;
        this.fevEva = fevEva;
        this.fevEsp = fevEsp;        
        this.file = file;
        this.plaid = plaid;
    }    

    public Integer getFevDocId() {
        return fevDocId;
    }

    public void setFevDocId(Integer fevDocId) {
        this.fevDocId = fevDocId;
    }

    public Date getFevFec() {
        return fevFec;
    }

    public void setFevFec(Date fevFec) {
        this.fevFec = fevFec;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public Integer getFevTot() {
        return fevTot;
    }

    public void setFevTot(Integer fevTot) {
        this.fevTot = fevTot;
    }

    public Integer getFevEva() {
        return fevEva;
    }

    public void setFevEva(Integer fevEva) {
        this.fevEva = fevEva;
    }

    public Integer getFevEsp() {
        return fevEsp;
    }

    public void setFevEsp(Integer fevEsp) {
        this.fevEsp = fevEsp;
    }

    public Integer getPlaid() {
        return plaid;
    }

    public void setPlaid(Integer plaid) {
        this.plaid = plaid;
    }
    
//    @XmlTransient
//    public Set<FichaDetalle> getDetalles() {
//        return detalles;
//    }
//
//    public void setDetalles(Set<FichaDetalle> detalles) {
//        this.detalles = detalles;
//    }

    public ItemFile getFile() {
        return file;
    }

    public void setFile(ItemFile file) {
        this.file = file;
    }

//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (fevDocId != null ? fevDocId.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof FichaEvaluacionDocumentos)) {
//            return false;
//        }
//        FichaEvaluacionDocumentos other = (FichaEvaluacionDocumentos) object;
//        if ((this.fevDocId == null && other.fevDocId != null) || (this.fevDocId != null && !this.fevDocId.equals(other.fevDocId))) {
//            return false;
//        }
//        return true;
//    }
//
//    @Override
//    public String toString() {
//        return "entidades.smdg.FichaEvaluacionDocumentos[ fevDocId=" + fevDocId + " ]";
//    }
    
}
