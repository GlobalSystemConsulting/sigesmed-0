package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.CapacitadorCursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.CapacitadorCursoCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.CapacitadorCursoCapacitacionId;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class RegistrarCapacitadorTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(RegistrarCapacitadorTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject data = (JSONObject) wr.getData();
            JSONArray capacitadores = data.getJSONArray("capacitadores");

            CapacitadorCursoCapacitacionDao capacitadorCursoCapacitacionDao = (CapacitadorCursoCapacitacionDao) FactoryDao.buildDao("capacitacion.CapacitadorCursoCapacitacionDao");

            if (data.getString("tip").equals("Presencial")) {
                for (int i = 0; i < capacitadores.length(); i++) {
                    int sedCapId = capacitadores.getJSONObject(i).getInt("sed");
                    int perId = capacitadores.getJSONObject(i).getInt("id");

                    CapacitadorCursoCapacitacionId idCapacitador = new CapacitadorCursoCapacitacionId(sedCapId, perId);
                    CapacitadorCursoCapacitacion capacitador = new CapacitadorCursoCapacitacion(idCapacitador, capacitadores.getJSONObject(i).getString("car"));

                    capacitadorCursoCapacitacionDao.insert(capacitador);
                }
            } else {
                int sedCapId = data.getInt("sed");

                for (int i = 0; i < capacitadores.length(); i++) {
                    int perId = capacitadores.getJSONObject(i).getInt("id");
                    CapacitadorCursoCapacitacionId idCapacitador = new CapacitadorCursoCapacitacionId(sedCapId, perId);
                    CapacitadorCursoCapacitacion capacitador = new CapacitadorCursoCapacitacion(idCapacitador, capacitadores.getJSONObject(i).getString("car"));

                    capacitadorCursoCapacitacionDao.insert(capacitador);
                }
            }

            return WebResponse.crearWebResponseExito("Los capacitadores fueron asignados correctamente", WebResponse.OK_RESPONSE);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "registrarCapacitador", e);
            return WebResponse.crearWebResponseError("No se pudo asignar capacitadores a la capacitación", WebResponse.BAD_RESPONSE);
        }
    }
}
