/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.vacacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import static com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mep.MepReportDaoHibernate.logger;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.VacacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Vacacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author felipe
 */
public class HistoricoVacacionTx implements ITransaction{
    
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject requestData = (JSONObject)wr.getData();
        Integer vacId = requestData.getInt("vacId");
                
        Vacacion vacacion = null;
        VacacionDao vacacionDao = (VacacionDao)FactoryDao.buildDao("se.VacacionDao");
        
        try{
            vacacion = vacacionDao.buscarPorId(vacId);
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Datos de vacacion",e);
            System.out.println("No se pudo listar los datos de vacacion\n"+e);
            return WebResponse.crearWebResponseError("No se pudo listar los datos de vacaciones", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        
        JSONObject oResponse = new JSONObject();
        
        if(vacacion.getOrgiActId()!=null)
                oResponse.put("orgiActId", vacacion.getOrgiActId());
        else
            oResponse.put("orgiActId", -1);

        if(vacacion.getCatActId()!=null)
                oResponse.put("catActId", vacacion.getCatActId());
            else
                oResponse.put("catActId", -1);

        if(vacacion.getCarActId()!=null)
                oResponse.put("carActId", vacacion.getCarActId());
            else
                oResponse.put("carActId", -1);
            
        return WebResponse.crearWebResponseExito("Los datos de la vacacion fue listada exitosamente", vacacion);
    }
    
}
