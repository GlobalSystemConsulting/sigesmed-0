package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.EvaluacionCursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.EvaluacionCursoCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.DatatypeConverter;
import org.json.JSONObject;

public class EditarEvaluacionTx implements ITransaction {
    
    private static final Logger logger = Logger.getLogger(EditarEvaluacionTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject data = (JSONObject) wr.getData();
            EvaluacionCursoCapacitacionDao evaluacionCursoCapacitacionDao = (EvaluacionCursoCapacitacionDao) FactoryDao.buildDao("capacitacion.EvaluacionCursoCapacitacionDao");
            EvaluacionCursoCapacitacion evaluacion = evaluacionCursoCapacitacionDao.buscarPorId(data.getJSONObject("eva").getInt("id"));
            
            evaluacion.setFecIni(HelpTraining.getStartOfDay(DatatypeConverter.parseDateTime(data.getJSONObject("eva").getString("ini")).getTime()));
            evaluacion.setFecFin(HelpTraining.getEndOfDay(DatatypeConverter.parseDateTime(data.getJSONObject("eva").getString("fin")).getTime()));
            evaluacion.setTip(data.getJSONObject("eva").getString("tip").charAt(0));
            evaluacion.setNom(data.getJSONObject("eva").getString("nom"));
            evaluacion.setObs(data.getJSONObject("eva").getString("obs"));
            evaluacion.setOrdAle(data.getJSONObject("eva").getBoolean("ord"));
            evaluacion.setNumInt(data.getJSONObject("eva").getInt("int"));
            
            if (evaluacion.getFecIni().before(new Date()))
                if (evaluacion.getFecFin().before(new Date()))
                    evaluacion.setEstReg('F');
                else
                    evaluacion.setEstReg('I');
            else
                evaluacion.setEstReg('A');   
                    
            evaluacionCursoCapacitacionDao.update(evaluacion);
            
            return WebResponse.crearWebResponseExito("Exito al editar la evaluacion de la capacitación", WebResponse.OK_RESPONSE);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "editarEvaluacion", e);
            return WebResponse.crearWebResponseError("Error al editar la evaluacion de la capacitación", WebResponse.BAD_RESPONSE);
        }        
    }
}
