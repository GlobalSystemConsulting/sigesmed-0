/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TipoFormacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TipoFormacion;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author alex
 */
public class TipoFormacionDaoHibernate extends GenericDaoHibernate<TipoFormacion> implements TipoFormacionDao{
    @Override
    public List<TipoFormacion> listarAll() {
        List<TipoFormacion> tiposForm = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT t from TipoFormacion as t "
                    + "WHERE t.estReg='A'";
            Query query = session.createQuery(hql);
            tiposForm = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los tipos de formacion\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los tipos de formacion\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return tiposForm;
    }

    @Override
    public TipoFormacion buscarPorId(Integer tipForId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        TipoFormacion des = (TipoFormacion)session.get(TipoFormacion.class, tipForId);
        session.close();
        return des;
    }
}
