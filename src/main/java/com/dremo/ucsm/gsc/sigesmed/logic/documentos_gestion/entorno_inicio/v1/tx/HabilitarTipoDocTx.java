/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.ItemFileDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.TipoEspecializadoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.TipoEspecializado;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class HabilitarTipoDocTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        Integer tipoDocID = 0;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            tipoDocID = requestData.getInt("tipoDocID");
            System.out.println("NUMEROHERE"+tipoDocID);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo habilitar, datos incorrectos", e.getMessage() );
        }
        //Fin
        /*
        *  Parte para la operacion en la Base de Datos
        */
        TipoEspecializadoDao tipoEspDao = (TipoEspecializadoDao)FactoryDao.buildDao("rdg.TipoEspecializadoDao");
        try{
            tipoEspDao.habilitarDoc(tipoDocID.shortValue());
            ItemFileDao itemDao=(ItemFileDao)FactoryDao.buildDao("rdg.ItemFileDao");
            itemDao.activarDocPorTipoDocumento(new TipoEspecializado(tipoDocID.shortValue()));
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo habilitar el tipo de documento ", e.getMessage() );
        }
        //Fin
               
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Tipo TipoTramite se habilito correctamente");
        //Fin
    }
}