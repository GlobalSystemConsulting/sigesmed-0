/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.plantilla_indicadores.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.PlantillaIndicadoresDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaFichaInstitucional;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaGrupo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaIndicadores;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.TipoGrupo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class InsertarIndicadorTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        PlantillaIndicadores indicador = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            int gruId = requestData.getInt("gruId");
            String indNom = requestData.optString("indNom");
                        
            indicador = new PlantillaIndicadores(indNom, new PlantillaGrupo(gruId));
            
        
        }catch(Exception e){            
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage() );
        }
        //Fin
                
        /*
        *  Parte para la operacion en la Base de Datos
        */
        PlantillaIndicadoresDao indicadorDao = (PlantillaIndicadoresDao)FactoryDao.buildDao("smdg.PlantillaIndicadoresDao");
        try{
            indicadorDao.insert(indicador);
        
        }catch(Exception e){
            System.out.println("No se pudo registrar\n"+e);
            return WebResponse.crearWebResponseError("No se pudo registrar", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("indId",indicador.getPinId());
        //oResponse.put("fecha",indicador.getFecMod().toString());
        return WebResponse.crearWebResponseExito("El registro del indicador se realizo correctamente", oResponse);
        //Fin
    }
    
}
