/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.LibroCajaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author RE
 */
public class ListarTrabajadorPorOrganizacionTx implements ITransaction{
 
    @Override
    public WebResponse execute(WebRequest wr) {        
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        
        JSONObject requestData = (JSONObject)wr.getData();
        int organizacionID = requestData.getInt("organizacionID");
       //System.out.println("id de la organizacion es  "+organizacionID);
        
       List< Trabajador> trabajadores = null;
        LibroCajaDao trabajadoresDao = (LibroCajaDao)FactoryDao.buildDao("sci.LibroCajaDao");
        
        try{
            trabajadores = trabajadoresDao.listarTrabajadoresPorOrganizacion(organizacionID);
            
        }catch(Exception e){
            System.out.println("No se pudo Listar Trabajadores \n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar Trabajadores ", e.getMessage() );
        }
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(Trabajador trabajador:trabajadores ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("personaID",trabajador.getPersona().getPerId());
            oResponse.put("datos",trabajador.getPersona().getApePat()+" "+trabajador.getPersona().getApeMat()+", "+trabajador.getPersona().getNom());
            oResponse.put("tipo",trabajador.getTraTip());
           
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
                
    }   
    
}
