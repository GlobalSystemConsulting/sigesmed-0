/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.carpeta_pedagogica.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.sma.carpeta_pedagogica.v1.tx.ActualizarEstadoAvanceTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sma.carpeta_pedagogica.v1.tx.GenerarInformeCompletitudByDocTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sma.carpeta_pedagogica.v1.tx.ListarCarpetasPorDRETx;
import com.dremo.ucsm.gsc.sigesmed.logic.sma.carpeta_pedagogica.v1.tx.ListarCarpetasPorIETx;
import com.dremo.ucsm.gsc.sigesmed.logic.sma.carpeta_pedagogica.v1.tx.ListarCarpetasPorUGELTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sma.carpeta_pedagogica.v1.tx.ListarCarpetasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sma.carpeta_pedagogica.v1.tx.VerDetalleCarpetaDocenteByIETx;

/**
 *
 * @author Administrador
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.SISTEMA_MONITOREO_ACOMPANIAMIENTO);        
        
        //Registrando el Nombre del componente
        component.setName("carpeta_pedagogica");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        //Lista de operaciones de logica, propias del componente
        component.addTransactionGET("listarCarpetas", ListarCarpetasTx.class);
        component.addTransactionGET("listarCarpetasPorIE", ListarCarpetasPorIETx.class);
        component.addTransactionGET("verDetalleCarpeta", VerDetalleCarpetaDocenteByIETx.class);
        component.addTransactionPUT("actualizarEstadoAvance", ActualizarEstadoAvanceTx.class);
        component.addTransactionPOST("generarInformeCompletitudCarPedDoc", GenerarInformeCompletitudByDocTx.class);
        component.addTransactionGET("listarCarpetasPorUGEL", ListarCarpetasPorUGELTx.class);
        component.addTransactionGET("listarCarpetasPorDRE", ListarCarpetasPorDRETx.class);
        return component;
    }
}


