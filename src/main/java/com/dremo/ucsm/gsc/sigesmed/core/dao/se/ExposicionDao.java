/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Exposicion;
import java.util.List;

/**
 *
 * @author gscadmin
 */
public interface ExposicionDao extends GenericDao<Exposicion>{
    public List<Exposicion> listarxFichaEscalafonaria(int ficEscId);
    public Exposicion buscarPorId(Integer expId);
    
}
