/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.diseno_curricular.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.DisenoCurricularDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.AreaCurricularHora;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class EliminarAreaHoraTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        AreaCurricularHora nuevo = null;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            int areaHoraID = requestData.getInt("areaHoraID");
            int jornadaID = requestData.getInt("jornadaID");
            int gradoID = requestData.getInt("gradoID");
            int areaID = requestData.getInt("areaID");
            int hora = requestData.getInt("hora");            
            
            nuevo = new AreaCurricularHora(areaHoraID,jornadaID,gradoID,areaID,hora);
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo aliminar la hora al area, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        DisenoCurricularDao disenoDao = (DisenoCurricularDao)FactoryDao.buildDao("mech.DisenoCurricularDao");
        try{
            disenoDao.eliminarAreaHora(nuevo);
        }catch(Exception e){
            System.out.println("No se pudo elimanr la hora al area\n"+e);
            return WebResponse.crearWebResponseError("No se pudo alimanr la hora al area", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("areaHoraID",nuevo.getAreHorId());
        return WebResponse.crearWebResponseExito("Se elimino la hora al area  correctamente", oResponse);
        //Fin
    }
}
