/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.ma.asistencia.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.ma.asistencia.v1.tx.RegistrarJustificacionInasistenciaTx;

/**
 *
 * @author ucsm
 */
public class ComponentRegister implements IComponentRegister{
    @Override

    public WebComponent createComponent() {
        WebComponent maComponent = new WebComponent(Sigesmed.SUBMODULO_ACADEMICO);
        maComponent.setName("asistencia_estudiante");
        maComponent.setVersion(1);
        maComponent.addTransactionPOST("registrarJustificacion",RegistrarJustificacionInasistenciaTx.class);
        return maComponent;
    }
}
