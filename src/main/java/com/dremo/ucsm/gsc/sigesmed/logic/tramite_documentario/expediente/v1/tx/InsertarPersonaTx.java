/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;

/**
 *
 * @author abel
 */
public class InsertarPersonaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        Persona nuevaPersona = null;
        try{
            JSONObject rPersona = (JSONObject)wr.getData();
            
            
            String dni = rPersona.getString("dni");
            String nombres = rPersona.getString("nombre");
            String materno = rPersona.getString("materno");
            String paterno = rPersona.getString("paterno");            
            String email = rPersona.getString("email");
            String numero1 = rPersona.getString("numero1");
            String numero2 = rPersona.optString("numero2");
            nuevaPersona = new Persona(0,dni,nombres,materno,paterno,new Date(),email,numero1,numero2);            
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar a la persona, datos incorrectos"); //, e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        PersonaDao personaDao = (PersonaDao)FactoryDao.buildDao("PersonaDao");
        try{
            Persona persona = personaDao.buscarPorDNI(nuevaPersona.getDni());
            if(persona == null )
                personaDao.insert(nuevaPersona);
            else
                nuevaPersona = persona;
        
        }catch(Exception e){
            System.out.println("No se pudo registrar a la Persona\n"+e);
            return WebResponse.crearWebResponseError("No se pudo registrar a la Persona", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("personaID",nuevaPersona.getPerId());
        return WebResponse.crearWebResponseExito("El registro de la Persona se realizo correctamente", oResponse);
        //Fin
    }
    
}
