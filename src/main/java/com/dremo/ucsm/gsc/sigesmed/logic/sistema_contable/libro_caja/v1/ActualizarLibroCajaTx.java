/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.LibroCajaDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class ActualizarLibroCajaTx implements ITransaction{   

    @Override
    public WebResponse execute(WebRequest wr) {
         
        
         int id;
         
    
        try{            
             
            JSONObject requestData = (JSONObject)wr.getData();  
             id = requestData.getInt("idLibro");
            
          
            
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo obtener datos", e.getMessage() );
        }
        
        LibroCajaDao libroDao = (LibroCajaDao)FactoryDao.buildDao("sci.LibroCajaDao");
      
              
        try {                        
            libroDao.actualizarEstadoLibro(id);    

              
            
        } catch (Exception e) {
            System.out.println("No se pudo encontrar datos del Libro \n"+e);
            return WebResponse.crearWebResponseError("No se pudo encontrar datos del Libro", e.getMessage() );
        }
        /*
        *  Repuesta Correcta
        */ 
     
        JSONObject oResponse = new JSONObject();
            
           
        oResponse.put("idLibro",id); 
        oResponse.put("Estado",'A'); 
       
        
        
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",oResponse);        
        //Fin
    }
    
    
}
