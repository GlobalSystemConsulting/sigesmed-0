/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao;

import com.dremo.ucsm.gsc.sigesmed.core.dao.*;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import java.util.List;

/**
 *
 * @author Carlos
 */
public interface UsuarioSessionDao extends GenericDao<UsuarioSession>{
    
	List<Integer> listarUsuariosCalendario(int orgCod, int funCod);
    
    List<Integer> listarAutoridadesCalendario(int usuCod, char tipAct);
    
    public List<UsuarioSession> obtenerUsuariosInferior(int org, int rol);
    
}

