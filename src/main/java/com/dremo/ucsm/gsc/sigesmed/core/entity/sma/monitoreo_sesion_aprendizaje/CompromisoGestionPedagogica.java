/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

@Entity
@Table(name = "compromiso_gestion_pedagogica", schema="pedagogico")
public class CompromisoGestionPedagogica implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "com_ges_ped_id")
    private Integer comGesPedId;
    
    @Size(max = 2147483647)
    @Column(name = "com_ges_ped_des")
    private String comGesPedDes;
    
    @Column(name = "com_ges_ped_tip")
    private Character comGesPedTip;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.DATE)
    private Date fecMod;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Size(max = 1)
    @Column(name = "est_reg")
    private String estReg;
    
    @JoinColumn(name = "pla_fic_mon_id", referencedColumnName = "pla_fic_mon_id")
    @ManyToOne
    private PlantillaFichaMonitoreo plantillaFM;
    
    @OneToMany(mappedBy = "compromisoGP")
    private List<InstruccionLlenadoIndicadores> instruccionesLI;
    
    public CompromisoGestionPedagogica() {
    }

    public CompromisoGestionPedagogica(Integer comGesPedId) {
        this.comGesPedId = comGesPedId;
    }

    public Integer getComGesPedId() {
        return comGesPedId;
    }

    public void setComGesPedId(Integer comGesPedId) {
        this.comGesPedId = comGesPedId;
    }

    public String getComGesPedDes() {
        return comGesPedDes;
    }

    public void setComGesPedDes(String comGesPedDes) {
        this.comGesPedDes = comGesPedDes;
    }

    public Character getComGesPedTip() {
        return comGesPedTip;
    }

    public void setComGesPedTip(Character comGesPedTip) {
        this.comGesPedTip = comGesPedTip;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public PlantillaFichaMonitoreo getPlantillaFM() {
        return plantillaFM;
    }

    public void setPlantillaFM(PlantillaFichaMonitoreo plantillaFM) {
        this.plantillaFM = plantillaFM;
    }

    public List<InstruccionLlenadoIndicadores> getInstruccionesLI() {
        return instruccionesLI;
    }

    public void setInstruccionesLI(List<InstruccionLlenadoIndicadores> instruccionesLI) {
        this.instruccionesLI = instruccionesLI;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (comGesPedId != null ? comGesPedId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CompromisoGestionPedagogica)) {
            return false;
        }
        CompromisoGestionPedagogica other = (CompromisoGestionPedagogica) object;
        if ((this.comGesPedId == null && other.comGesPedId != null) || (this.comGesPedId != null && !this.comGesPedId.equals(other.comGesPedId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.CompromisoGestionPedagogica[ comGesPedId=" + comGesPedId + " ]";
    }
    
}
