/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.sci;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.DetalleCuenta;

/**
 *DetalleCuenta

 * @author G-16
 */
public interface DetalleCuentaDao extends GenericDao<DetalleCuenta>{
        
}
