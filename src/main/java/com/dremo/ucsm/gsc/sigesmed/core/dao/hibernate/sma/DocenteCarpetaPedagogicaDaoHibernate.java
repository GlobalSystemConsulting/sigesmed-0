/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.DocenteCarpetaPedagogicaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica.DocenteCarpetaPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica.RutaContenidoCarpeta;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class DocenteCarpetaPedagogicaDaoHibernate extends GenericDaoHibernate<DocenteCarpetaPedagogica> implements DocenteCarpetaPedagogicaDao{

    @Override
    public List<DocenteCarpetaPedagogica> listarPorIE(int orgId) {
        List<DocenteCarpetaPedagogica> carpetas = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            String hql = "SELECT dcp FROM DocenteCarpetaPedagogicaSMA AS dcp "
                    + "INNER JOIN FETCH dcp.carpeta c "
                    + "INNER JOIN FETCH dcp.docente d "
                    + "INNER JOIN FETCH d.persona p "
                    + "WHERE dcp.organizacion.orgId=:orgId"; 
            Query query = session.createQuery(hql);
            query.setInteger("orgId",orgId);            
            carpetas = query.list();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudieron listar las carpetas de la I.E. Error: "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudieron listar las carpetas de la I.E. Error: "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return carpetas;
    }

    @Override
    public RutaContenidoCarpeta buscarRutaConId(int docCarPedId) {
        RutaContenidoCarpeta ruta = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            String hql = "SELECT dcp.rutaContenido FROM DocenteCarpetaPedagogicaSMA AS dcp "
                    + "WHERE dcp.docCarPedId=:docCarPedId"; 
            Query query = session.createQuery(hql);
            query.setInteger("docCarPedId",docCarPedId);            
            ruta = (RutaContenidoCarpeta) query.uniqueResult();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar la ruta del contenido. Error: "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar la ruta del contenido. Error: "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        System.out.println(ruta);
        return ruta;
    }

    @Override
    public DocenteCarpetaPedagogica buscarPorId(int docCarPedId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        DocenteCarpetaPedagogica docCarPed = (DocenteCarpetaPedagogica)session.get(DocenteCarpetaPedagogica.class, docCarPedId);
        session.close();
        return docCarPed;
    }

    @Override
    public List<DocenteCarpetaPedagogica> listarContenidosDocente(int orgId, int carId, int docId) {
        List<DocenteCarpetaPedagogica> contDoc = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            String hql = "SELECT dcp FROM DocenteCarpetaPedagogicaSMA AS dcp "
                    + "INNER JOIN FETCH dcp.organizacion o "
                    + "INNER JOIN FETCH dcp.carpeta c "
                    + "INNER JOIN FETCH dcp.docente d "
                    + "INNER JOIN FETCH d.persona p "
                    + "WHERE o.orgId=:orgId AND "
                    + "d.doc_id=:docId AND "
                    + "c.carDigId=:carId"; 
            Query query = session.createQuery(hql);
            query.setInteger("orgId",orgId); 
            query.setInteger("docId",docId);
            query.setInteger("carId",carId);
            contDoc = query.list();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudieron listar las carpetas de la I.E. Error: "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudieron listar las carpetas de la I.E. Error: "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return contDoc;
    }

    @Override
    public List<DocenteCarpetaPedagogica> listarPorUGEL(int orgPadId) {
        List<DocenteCarpetaPedagogica> carpetas = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            String hql = "SELECT dcp FROM DocenteCarpetaPedagogicaSMA AS dcp "
                    + "INNER JOIN FETCH dcp.carpeta c "
                    + "INNER JOIN FETCH dcp.organizacion o "
                    + "INNER JOIN FETCH dcp.docente d "
                    + "INNER JOIN FETCH d.persona p "
                    + "WHERE o.organizacionPadre.orgId=:orgPadId"; 
            Query query = session.createQuery(hql);
            query.setInteger("orgPadId",orgPadId);            
            carpetas = query.list();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudieron listar las carpetas de la UGEL. Error: "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudieron listar las carpetas de la UGEL. Error: "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return carpetas;
    }

    @Override
    public List<DocenteCarpetaPedagogica> listarPorDRE(int orgAbuId) {
        List<DocenteCarpetaPedagogica> carpetas = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            String hql = "SELECT dcp FROM DocenteCarpetaPedagogicaSMA AS dcp "
                    + "INNER JOIN FETCH dcp.carpeta c "
                    + "INNER JOIN FETCH dcp.organizacion ie "
                    + "INNER JOIN FETCH ie.organizacionPadre ugel "
                    + "INNER JOIN FETCH dcp.docente d "
                    + "INNER JOIN FETCH d.persona p "
                    + "WHERE ugel.organizacionPadre=:orgAbuId"; 
            Query query = session.createQuery(hql);
            query.setInteger("orgAbuId",orgAbuId);            
            carpetas = query.list();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudieron listar las carpetas de la DRE. Error: "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudieron listar las carpetas de la DRE. Error: "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return carpetas;
    }
    
}
