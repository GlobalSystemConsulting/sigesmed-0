/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.monitoreo_sesion_apredizaje.plantilla_ficha_monitoreo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.CompromisoGestionPedagogicaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.CompromisoGestionPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class EliminarCompromisoGPTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(EliminarCompromisoGPTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject jsonData = (JSONObject)wr.getData();
        int comId = jsonData.getInt("comId");
        return eliminarCompromisoGP(comId);
    }

    private WebResponse eliminarCompromisoGP(int comId) {
        try{
            CompromisoGestionPedagogicaDao comGesPedDao = (CompromisoGestionPedagogicaDao) FactoryDao.buildDao("sma.CompromisoGestionPedagogicaDao");
            CompromisoGestionPedagogica comGesPed = comGesPedDao.buscaPorId(comId);
            comGesPedDao.delete(comGesPed);
            return WebResponse.crearWebResponseExito("Se elimino correctamente el compromiso de gestión pedagógica");

        }catch (Exception e){
            logger.log(Level.SEVERE,"eliminarCompromisoGestionPedagogica",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el compromiso de gestión pedagógica");
        }
    }
    
}
