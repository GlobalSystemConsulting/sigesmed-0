/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.catalogo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.CatalogoTablaDao;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.CatalogoTabla;
import java.util.ArrayList;
import java.util.Date;
import org.json.JSONArray;

/**
 *
 * @author Administrador
 */
public class ActualizarCatalogoTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        CatalogoTabla catalogoTabla = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            int CatalogoID = requestData.getInt("catalogoID");
            String nombretabla = requestData.getString("nombretabla");
            String nombreclase = requestData.getString("nombreclase");
            String estado = requestData.getString("estado");
           
            
            catalogoTabla = new CatalogoTabla(CatalogoID, nombretabla, nombreclase, new Date(), wr.getIdUsuario(), estado.charAt(0));
            
           
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        CatalogoTablaDao catalogoTablaDao = (CatalogoTablaDao)FactoryDao.buildDao("CatalogoTablaDao");
        try{
            
            
            catalogoTablaDao.update(catalogoTabla);
        
        }catch(Exception e){
            System.out.println("No se pudo actualizar el Catalogo\n"+e);
            return WebResponse.crearWebResponseError("No se pudo actualizar Catalogo", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Catalogo se actualizo correctamente");
        //Fin
    }
    
}
