/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.catalogo_bienes.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.CatalogoBienesDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.CatalogoBienes;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author harold
 */
public class EliminarItemCatalogoTx implements ITransaction{
    
    int catBieId;
    
    @Override
    public WebResponse execute(WebRequest wr) {
        
        CatalogoBienesDAO catBienDao = (CatalogoBienesDAO)FactoryDao.buildDao("scp.CatalogoBienesDAO");
        
        JSONObject requestData = (JSONObject)wr.getData();
             
        catBieId = requestData.getInt("cat_bie_id");
        
        try {
            
            catBienDao.eliminarCatalagoItem(catBieId);

            return WebResponse.crearWebResponseExito("Se eliminó correctamente el item del catálogo");
            
        } catch (Exception e) {
            
            return WebResponse.crearWebResponseError("No se pudo eliminar el item del catálogo ", e.getMessage() );
        }
        
        
    }
    
}
