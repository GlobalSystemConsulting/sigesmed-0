/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Yemi
 */
@Entity
@Table(name = "ficha_evaluacion_detalle", schema="pedagogico")
public class FichaEvaluacionDetalle implements Serializable {

    @EmbeddedId
    protected FichaEvaluacionDetallePK fichaEvaluacionDetallePK;
    
    @Column(name = "pun")
    private Integer pun;
    
    @JoinColumn(name = "fic_eva_id", referencedColumnName = "fic_eva_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private FichaEvaluacion fichaEvaluacion;
    
    @JoinColumn(name = "ind_com_ges_ped_id", referencedColumnName = "ind_com_ges_ped_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private IndicadorCompromisoGestionPedagogica indicadorCGP;

    public FichaEvaluacionDetalle() {
    }

    public FichaEvaluacionDetalle(FichaEvaluacionDetallePK fichaEvaluacionDetallePK) {
        this.fichaEvaluacionDetallePK = fichaEvaluacionDetallePK;
    }

    public FichaEvaluacionDetalle(int ficEvaId, int indComGesPedId) {
        this.fichaEvaluacionDetallePK = new FichaEvaluacionDetallePK(ficEvaId, indComGesPedId);
    }

    public FichaEvaluacionDetallePK getFichaEvaluacionDetallePK() {
        return fichaEvaluacionDetallePK;
    }

    public void setFichaEvaluacionDetallePK(FichaEvaluacionDetallePK fichaEvaluacionDetallePK) {
        this.fichaEvaluacionDetallePK = fichaEvaluacionDetallePK;
    }

    public Integer getPun() {
        return pun;
    }

    public void setPun(Integer pun) {
        this.pun = pun;
    }

    public FichaEvaluacion getFichaEvaluacion() {
        return fichaEvaluacion;
    }

    public void setFichaEvaluacion(FichaEvaluacion fichaEvaluacion) {
        this.fichaEvaluacion = fichaEvaluacion;
    }

    public IndicadorCompromisoGestionPedagogica getIndicadorCompromisoGestionPedagogica() {
        return indicadorCGP;
    }

    public void setIndicadorCompromisoGestionPedagogica(IndicadorCompromisoGestionPedagogica indicadorCGP) {
        this.indicadorCGP = indicadorCGP;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fichaEvaluacionDetallePK != null ? fichaEvaluacionDetallePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FichaEvaluacionDetalle)) {
            return false;
        }
        FichaEvaluacionDetalle other = (FichaEvaluacionDetalle) object;
        if ((this.fichaEvaluacionDetallePK == null && other.fichaEvaluacionDetallePK != null) || (this.fichaEvaluacionDetallePK != null && !this.fichaEvaluacionDetallePK.equals(other.fichaEvaluacionDetallePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.FichaEvaluacionDetalle[ fichaEvaluacionDetallePK=" + fichaEvaluacionDetallePK + " ]";
    }
    
}
