package com.dremo.ucsm.gsc.sigesmed.core.entity.mep;

import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Date;

/**
 * @author geank
 */
@Entity
@Table(name="rango_porcentual_ficha",schema="pedagogico")
@DynamicUpdate(value=true)
public class RangoPorcentualFicha  implements java.io.Serializable, ElementoFicha {


    @Id 
    @Column(name="ran_por_fic_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_rango_porcentual_ficha", sequenceName="pedagogico.rango_porcentual_ficha_ran_por_fic_id_seq" )
    @GeneratedValue(generator="secuencia_rango_porcentual_ficha")
    private int ranPorFicId;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="fic_eva_per_id")
    private FichaEvaluacionPersonal fichaEvaluacionPersonal;
    
    @Column(name="min")
    private int min;
    
    @Column(name="max")
    private int max;
    
    @Column(name="des")
    private String des;

    @Column(name = "usu_mod")
    private Integer usuMod;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length = 29)
    private Date fecMod;

    @Column(name = "est_reg", length = 1)
    private Character estReg;

    public RangoPorcentualFicha() {
    }

	
    public RangoPorcentualFicha(int ranPorFicId) {
        this.ranPorFicId = ranPorFicId;
    }
    public RangoPorcentualFicha(int id,int min, int max, String des) {
        this.ranPorFicId = id;
        this.min = min;
        this.max = max;
        this.des = des;
    }
    public RangoPorcentualFicha(int min, int max, String des) {
       this.min = min;
       this.max = max;
       this.des = des;
    }
    public RangoPorcentualFicha(int min, int max, String des,Date fecMod,char estReg ,FichaEvaluacionPersonal fichaEvaluacionPersonal) {
        this.min = min;
        this.max = max;
        this.des = des;
        this.fecMod = fecMod;
        this.estReg = estReg;
        this.fichaEvaluacionPersonal = fichaEvaluacionPersonal;
    }
    public RangoPorcentualFicha(int ranPorFicId, FichaEvaluacionPersonal fichaEvaluacionPersonal, int min, int max, String des) {
       this.ranPorFicId = ranPorFicId;
       this.fichaEvaluacionPersonal = fichaEvaluacionPersonal;
       this.min = min;
       this.max = max;
       this.des = des;
    }
   
    public int getRanPorFicId() {
        return this.ranPorFicId;
    }
    
    public void setRanPorFicId(int ranPorFicId) {
        this.ranPorFicId = ranPorFicId;
    }
    
    @Override
    public FichaEvaluacionPersonal getFichaEvaluacionPersonal() {
        return this.fichaEvaluacionPersonal;
    }
    
    @Override
    public void setFichaEvaluacionPersonal(FichaEvaluacionPersonal fichaEvaluacionPersonal) {
        this.fichaEvaluacionPersonal = fichaEvaluacionPersonal;
    }

    public int getMin() {
        return this.min;
    }
    
    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return this.max;
    }
    
    public void setMax(int max) {
        this.max = max;
    }

    public String getDes() {
        return this.des;
    }
    
    public void setDes(String des) {
        this.des = des;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    @Override
    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    @Override
    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    @Override
    public ElementoFicha copyFromOther(ElementoFicha elemento) {
        if(elemento != null && elemento instanceof RangoPorcentualFicha){
            RangoPorcentualFicha rango = (RangoPorcentualFicha)elemento;
            this.setMin(rango.getMin());
            this.setMax(rango.getMax());
            this.setDes(rango.getDes());
        }
        return this;
    }

    @Override
    public String toString(){
        return "{\"id\":" + ranPorFicId +
                ",\"des\":\"" + des + "\",\"max\":" + max + ",\"min\":" + min + "}";
    }
}


