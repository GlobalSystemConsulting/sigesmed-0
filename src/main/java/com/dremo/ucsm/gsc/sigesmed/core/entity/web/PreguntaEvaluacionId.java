/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.web;

import java.io.Serializable;

/**
 *
 * @author abel
 */
public final class PreguntaEvaluacionId implements Serializable {
    
    private static final long serialVersionUID = 1562260205094677677L;
    private Integer evaluacion;
    private int preEvaId;

    public PreguntaEvaluacionId() {
    }

    public PreguntaEvaluacionId(Integer evaluacion, int preEvaId) {
        this.setEvaluacion(evaluacion);
        this.setPreEvaId(preEvaId);
    }

    @Override
    public int hashCode() {
        return ((this.getEvaluacion() == null
                ? 0 : this.getEvaluacion().hashCode())
                ^ ((int) this.getPreEvaId()));
    }

    @Override
    public boolean equals(Object otherOb) {

        if (this == otherOb) {
            return true;
        }
        if (!(otherOb instanceof PreguntaEvaluacionId)) {
            return false;
        }
        PreguntaEvaluacionId other = (PreguntaEvaluacionId) otherOb;
        return ((this.getEvaluacion() == null
                ? other.getEvaluacion() == null : this.getEvaluacion()
                .equals(other.getEvaluacion()))
                && (this.getPreEvaId() == other.getPreEvaId()));
    }

    @Override
    public String toString() {
        return "" + getEvaluacion() + "-" + getPreEvaId();
    }

    public Integer getEvaluacion() {
        return evaluacion;
    }

    public void setEvaluacion(Integer evaluacion) {
        this.evaluacion = evaluacion;
    }

    public int getPreEvaId() {
        return preEvaId;
    }

    public void setPreEvaId(int preEvaId) {
        this.preEvaId = preEvaId;
    }
    
}
