package com.dremo.ucsm.gsc.sigesmed.core.entity.ma;


import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrador on 26/01/2017.
 */
@Entity
@Table(name = "historico_notas_estudiante", schema = "pedagogico")
public class HistoricoNotasEstudiante implements java.io.Serializable{
    @Id
    @Column(name = "his_not_est_id",nullable = false, unique = true)
    @SequenceGenerator(name = "historico_notas_estudiante_his_not_est_id_seq",sequenceName = "pedagogico.historico_notas_estudiante_his_not_est_id_seq")
    @GeneratedValue(generator = "historico_notas_estudiante_his_not_est_id_seq")
    private int hisNotEstId;
    
    
    @Column(name = "not_are", length = 2)
    String nota;
    
    @Column(name = "not_are_lit" , length = 2)
    private String not_are_lit;
    
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length=29)
    private Date fecMod;
    
    @Column(name = "est_reg",length = 1)
    private Character estReg;

    public HistoricoNotasEstudiante() {
        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public HistoricoNotasEstudiante(String nota) {
        this.nota = nota;
        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public int getHisNotEstId() {
        return hisNotEstId;
    }

    public void setHisNotEstId(int hisNotEstId) {
        this.hisNotEstId = hisNotEstId;
    }

    

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public String getNot_are_lit() {
        return not_are_lit;
    }

    public void setNot_are_lit(String not_are_lit) {
        this.not_are_lit = not_are_lit;
    }
    
}
