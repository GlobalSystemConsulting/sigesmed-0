/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.di;

import com.dremo.ucsm.gsc.sigesmed.core.dao.di.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.Persona;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Administrador
 */
public class PersonaDaoHibernate extends GenericDaoHibernate<Persona> implements PersonaDao{
    @Override
    public String buscarUltimoCodigo() {
        String codigo = null;
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        try{
//            //listar ultimo codigo de tramite
//            String hql = "SELECT a.apoId FROM Apoderado a ORDER BY a.apoId DESC ";
//            Query query = session.createQuery(hql);
//            query.setMaxResults(1);
//            codigo = ((String)query.uniqueResult());
//            
//            //solo cuando no hay ningun registro aun
//            if(codigo==null)
//                codigo = "0000";
//        
//        }catch(Exception e){
//            System.out.println("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());
//            throw new UnsupportedOperationException("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());            
//        }
//        finally{
//            session.close();
//        }
        return codigo;
    }
    
    @Override
    public Persona buscarPersonaxDNI(String dni){
        Persona p = null;        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT p FROM com.dremo.ucsm.gsm.sigesmed.core.entity.di.Persona p WHERE p.dni=:p1";            
            
            Query query = session.createQuery(hql);
            query.setParameter("p1", dni);
            //query.setMaxResults(1);
            p = (Persona)query.uniqueResult();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            //System.out.println("No se encontro la persona \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se encontro la persona \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }        
        return p;
    }
    
    @Override
    public Persona buscarPersonaxId(Long perId){
        
        Session session = HibernateUtil.getSessionFactory().openSession();
                
        Persona p = (Persona)session.get(Persona.class, perId);
        
        session.close();
        
        return p;
    }

}
