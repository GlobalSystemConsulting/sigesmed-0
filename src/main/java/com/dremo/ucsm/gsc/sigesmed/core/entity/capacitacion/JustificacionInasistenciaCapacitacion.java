package com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "justificacion_inasistencia_capacitacion", schema = "pedagogico")
public class JustificacionInasistenciaCapacitacion implements Serializable {
    @Id
    @Column(name = "jus_ina_cap_id", unique = true, nullable = false)
    @SequenceGenerator(name = "secuencia_justificacion_inasistencia_capacitacion", sequenceName = "pedagogico.justificacion_inasistencia_capacitacion_jus_ina_cap_id_seq")
    @GeneratedValue(generator="secuencia_justificacion_inasistencia_capacitacion")
    private int jusInaCapId;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "asi_par_cap_id")
    private AsistenciaParticipante asistencia;
    
    @Column(name = "nom")
    private String nom;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec")
    private Date fec;

    public JustificacionInasistenciaCapacitacion() {}
    
    public JustificacionInasistenciaCapacitacion(AsistenciaParticipante asistencia, String nom) {
        this.asistencia = asistencia;
        this.nom = nom;
        this.fec = new Date();
    }

    public int getJusInaCapId() {
        return jusInaCapId;
    }

    public void setJusInaCapId(int jusInaCapId) {
        this.jusInaCapId = jusInaCapId;
    }

    public AsistenciaParticipante getAsistencia() {
        return asistencia;
    }

    public void setAsistencia(AsistenciaParticipante asistencia) {
        this.asistencia = asistencia;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getFec() {
        return fec;
    }

    public void setFec(Date fec) {
        this.fec = fec;
    }
}
