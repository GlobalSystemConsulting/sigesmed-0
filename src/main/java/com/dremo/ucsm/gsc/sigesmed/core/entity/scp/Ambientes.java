/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.scp;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import java.util.List;


/**
 *
 * @author Jeferson
 */
@Entity
@Table(name="ambientes", schema="administrativo")
public class Ambientes implements java.io.Serializable{
    
    @Id
    @Column(name="amb_id" , unique=true, nullable=false)
    @SequenceGenerator(name="secuencia_inv_tra",sequenceName="administrativo.ambientes_amb_id_seq")
    @GeneratedValue(generator="secuencia_inv_tra")
    private int amb_id;
    
    @Column(name="des")
    private String des;
    
    @Column(name="ubi")
    private String ubi;
    
    @Column(name="est_amb")
    private char est_amb;
    
    @Column(name="area")
    private int area; 
    
    @Column(name="inst_const")
    private String inst_const;
    
    @Column(name="fec_cons")
    private Date fec_cons;
    
    @Column(name="org_id")
    private int org_id;
    /*
    @Column(name="usu_mod")
    private int usu_mod;
    */

    @Column(name="est_reg")
    private char est_reg;

    public void setOrg_id(int org_id) {
        this.org_id = org_id;
    }

    public int getOrg_id() {
        return org_id;
    }

    public void setAmb_id(int amb_id) {
        this.amb_id = amb_id;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public void setUbi(String ubi) {
        this.ubi = ubi;
    }

    public void setEst_amb(char est_amb) {
        this.est_amb = est_amb;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public void setInst_const(String inst_const) {
        this.inst_const = inst_const;
    }

    public void setFec_cons(Date fec_cons) {
        this.fec_cons = fec_cons;
    }

    public void setEst_reg(char est_reg) {
        this.est_reg = est_reg;
    }

    public int getAmb_id() {
        return amb_id;
    }

    public String getDes() {
        return des;
    }

    public String getUbi() {
        return ubi;
    }

    public char getEst_amb() {
        return est_amb;
    }

    public int getArea() {
        return area;
    }

    public String getInst_const() {
        return inst_const;
    }

    public Date getFec_cons() {
        return fec_cons;
    }

    public char getEst_reg() {
        return est_reg;
    }

    public Ambientes() {
    }

    public Ambientes(int amb_id, String des, String ubi, char est_amb, int area, String inst_const, Date fec_cons, int org_id, int usu_mod, char est_reg) {
        this.amb_id = amb_id;
        this.des = des;
        this.ubi = ubi;
        this.est_amb = est_amb;
        this.area = area;
        this.inst_const = inst_const;
        this.fec_cons = fec_cons;
        this.org_id = org_id;
    //    this.usu_mod = usu_mod;
        this.est_reg = est_reg;
    }
    
    
    
}
