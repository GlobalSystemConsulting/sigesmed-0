/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.plan_estudios.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.PlanEstudiosDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author angell
 */
public class EliminarPlanNivelXEstRegTx implements ITransaction{
//eliminarPlanNivelXER
    @Override
    public WebResponse execute(WebRequest wr) {
        int planNivelID = 0;
        try{            
            JSONObject requestData = (JSONObject)wr.getData();
            planNivelID = requestData.getInt("planNivelID");
        
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo eliminar, datos incorrectos", e.getMessage() );
        }
        PlanEstudiosDao usuarioDao = (PlanEstudiosDao)FactoryDao.buildDao("mech.PlanEstudiosDao");
        try{
            usuarioDao.eliminarNivelXER(planNivelID);
        }catch(Exception e){
            System.out.println("No se pudo eliminar\n"+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar", e.getMessage() );
        }      
        return WebResponse.crearWebResponseExito("Se elimino correctamente el Plan");
    }

}
