/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.std.HistorialExpedienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.HistorialExpediente;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

/**
 *
 * @author abel
 */
public class VerHistorialAnteriorTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int expedienteID = 0;
        int historialID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            expedienteID = requestData.getInt("expedienteID");
            historialID = requestData.getInt("historialID");
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo ver el historial anterior, datos incorrectos", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        HistorialExpediente h = null;
        //List<RutaTramite> rutas = null;
        HistorialExpedienteDao historialDao = (HistorialExpedienteDao)FactoryDao.buildDao("std.HistorialExpedienteDao");
        try{
            h =historialDao.buscarHistorialAnterior(expedienteID,historialID);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo ver el historial anterior", e.getMessage() );
        }
        //Fin
        
        if(h==null)
            return WebResponse.crearWebResponseError("No hay un estado anterior al seleccionado");
        
        JSONObject oHistorial = new JSONObject();
        oHistorial.put("historialID",h.getHisExpId());
        oHistorial.put("observacion",h.getObservacion() );
        oHistorial.put("areaID",h.getAreaId());
        oHistorial.put("area",h.getArea().getNom());
        
        oHistorial.put("responsableID",h.getResId());
        oHistorial.put("responsable",h.getResponsable().getNombrePersona());
/*
        oHistorial.put("estadoID",h.getEstado().getEstExpId());
        oHistorial.put("estado",h.getEstado().getNom());*/
        
        return WebResponse.crearWebResponseExito("Se encontro el historial anterior",oHistorial);
    }
    
}

