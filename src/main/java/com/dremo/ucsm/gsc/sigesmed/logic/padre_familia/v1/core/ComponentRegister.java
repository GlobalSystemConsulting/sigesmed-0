/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.padre_familia.v1.core;

import com.dremo.ucsm.gsc.sigesmed.logic.padre_familia.v1.tx.ListarHijosTx;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.padre_familia.v1.tx.DatosEstudianteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.padre_familia.v1.tx.DatosMatriculaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.padre_familia.v1.tx.DatosTareasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.padre_familia.v1.tx.ListarAreasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.padre_familia.v1.tx.ListarNotasAnualesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.padre_familia.v1.tx.ListarTareasByAlumnoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.padre_familia.v1.tx.ReporteFichaEstudianteTx;

/**
 *
 * @author Carlos
 */
public class ComponentRegister implements IComponentRegister{

    @Override
    public WebComponent createComponent() {
        WebComponent mepComponent = new WebComponent(Sigesmed.MODULO_PADRE_FAMILIA);
        mepComponent.setName("padreFamilia");
        mepComponent.setVersion(1);
        mepComponent.addTransactionGET("listarHijos", ListarHijosTx.class);
        mepComponent.addTransactionGET("datosEstudiante", DatosEstudianteTx.class);
        mepComponent.addTransactionGET("datosMatricula", DatosMatriculaTx.class);
        mepComponent.addTransactionGET("datosTareas", DatosTareasTx.class);
        mepComponent.addTransactionGET("listarAreas", ListarAreasTx.class);//listarBandejaTareaByAlumno
        mepComponent.addTransactionGET("listarBandejaTareaByAlumno", ListarTareasByAlumnoTx.class);//listarAsistenciaByAlumno

        mepComponent.addTransactionPOST("reporteFichaEstudiante", ReporteFichaEstudianteTx.class);
        mepComponent.addTransactionGET("listarNotasAnuales", ListarNotasAnualesTx.class);

        return mepComponent;
    }
    
}
//$scope.generarEstadisticaNotasAnuales = function(){
//            var request = crud.crearRequest('padreFamilia', 1, 'listarNotasAnuales');
//            request.setData({matriculaID:$scope.mp});
//            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
//            //y las usuarios de exito y error
//            crud.listar("/padreFamilia", request, function (data) {
//                if (data.responseSta) {
//                    console.log(data);
//                }
//            }, function (data) {
//                console.info(data);
//            });
//        }