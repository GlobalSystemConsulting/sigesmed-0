/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.scp;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.CausalAltaDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.CausalAlta;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
/**
 *
 * @author Administrador
 */
public class CausalAltaDAOHibernate extends GenericDaoHibernate<CausalAlta> implements CausalAltaDAO{

    @Override
    public List<CausalAlta> listarCausalAlta() {
        
        
        List<CausalAlta> ca = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{

            String hql = "SELECT ca FROM CausalAlta ca  WHERE ca.est_reg!='E'";

            Query query = session.createQuery(hql); 
            ca = query.list();
            
        }catch(Exception e){
             System.out.println("No se pudo Mostrar las Causales de Altas \\n "+ e.getMessage());
             throw new UnsupportedOperationException("No se pudo Mostrar las Causales de Altas \\n "+ e.getMessage());
        }
        finally{
            session.close();
        }
        return ca; 
        
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
