/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.smdg;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaIndicadores;

/**
 *
 * @author Administrador
 */
public interface PlantillaIndicadoresDao extends GenericDao<PlantillaIndicadores>{
    public String buscarUltimoCodigo();
    public PlantillaIndicadores obtenerPlantillaIndicadores(int indId);
}
