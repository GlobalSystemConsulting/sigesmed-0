/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.estudiante.datos_estudiante.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mpf.EstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.Matricula;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.SaludControles;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author carlos
 */
public class DatosEstudianteTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {

        Long matriculaId;

        try {
            JSONObject requestData = (JSONObject) wr.getData();
            matriculaId = requestData.getLong("matriculaID");

        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se pudo verificar los datos", e.getMessage());
        }

        Matricula estudiante = null;
        EstudianteDao estudianteDao = (EstudianteDao) FactoryDao.buildDao("mpf.EstudianteDao");
        List<SaludControles> saludControles = null;
        try {
            estudiante = estudianteDao.getDatosEstudiante(matriculaId);
            if(estudiante!=null)
                saludControles = estudianteDao.getSaludControlesByEstudiante(estudiante.getEstudiante());
        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se pudo  obtener datos del Estudiante", e.getMessage());
        }
//

        DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
//        

        JSONObject oResponse = new JSONObject();
        oResponse.put("nombres", estudiante.getEstudiante().getPersona().getNombrePersonaAP());
        oResponse.put("nacimiento", sdf.format(estudiante.getEstudiante().getPersona().getFecNac()));
         if(estudiante.getEstudiante().getPersona().getEmail()!=null)
            oResponse.put("email", estudiante.getEstudiante().getPersona().getEmail());
        else
            oResponse.put("email", "");
        
        oResponse.put("dni", estudiante.getEstudiante().getPersona().getDni());
        if (estudiante.getEstudiante().getPersona().getSexo().equals('F')) {
            oResponse.put("sexo", "Femenino");
        } else {
            oResponse.put("sexo", "Masculino");
        }
        if (estudiante.getEstudiante().getPersona().getNum1() != null) {
            oResponse.put("numero1", estudiante.getEstudiante().getPersona().getNum1());
        } else {
            oResponse.put("numero1", "");
        }
        if (estudiante.getEstudiante().getPersona().getNum2() != null) {
            oResponse.put("numero2", estudiante.getEstudiante().getPersona().getNum2());
        } else {
            oResponse.put("numero2", "");
        }
        if (estudiante.getEstudiante().getPersona().getFijo() != null) {
            oResponse.put("fijo", estudiante.getEstudiante().getPersona().getFijo());
        } else {
            oResponse.put("fijo", "");
        }
        oResponse.put("direccion", estudiante.getEstudiante().getPersona().getDireccion());
        oResponse.put("apoderadoNombre", estudiante.getPersonaByApoId().getNombrePersonaAP());
        if (estudiante.getPersonaByApoId() == null) {
            oResponse.put("apoderadoNumero", "");
        } else if (estudiante.getPersonaByApoId().getNum1() != null) {
            oResponse.put("apoderadoNumero", estudiante.getPersonaByApoId().getNum1());
        } else {
            oResponse.put("apoderadoNumero", "");
        }
         if(estudiante.getPlanNivel().getDes()!=null)
            oResponse.put("nivel", estudiante.getPlanNivel().getDes().substring(0, estudiante.getPlanNivel().getDes().indexOf(" ")));
        else
            oResponse.put("nivel","");
        
        if(estudiante.getGrado().getNom()!=null)
            oResponse.put("grado", estudiante.getGrado().getNom());
        else
            oResponse.put("grado","");
        
         if(estudiante.getSeccion().getNom()!=null)
            oResponse.put("seccion", estudiante.getSeccion().getNom().substring(estudiante.getSeccion().getNom().indexOf(" ")));
         else
             oResponse.put("seccion","");
        if (estudiante.getEstudiante().getDatosNacimiento() == null) {
            oResponse.put("nacionalidad", "");
        } else {
            oResponse.put("nacionalidad", estudiante.getEstudiante().getDatosNacimiento().getPais().getPaiNom());
        }
        if (estudiante.getGradoIeEstudiantes() != null && estudiante.getGradoIeEstudiantes().size() > 0 && estudiante.getGradoIeEstudiantes().get(0).getNumOrd()!=null) {
            oResponse.put("orden", estudiante.getGradoIeEstudiantes().get(0).getNumOrd() + "");
        } else {
            oResponse.put("orden", "");
        }
        if(estudiante.getPlanNivel().getTurno().getNom()!=null)
             oResponse.put("turno", estudiante.getPlanNivel().getTurno().getNom().substring(0,1).toUpperCase()+ estudiante.getPlanNivel().getTurno().getNom().substring(1).toLowerCase());
        else
            oResponse.put("turno", "");
        
        if(estudiante.getEstudiante().getTipSan()!=null)
            oResponse.put("sangre", estudiante.getEstudiante().getTipSan());
        else
             oResponse.put("sangre", "");
        
        if( estudiante.getEstudiante().getAle()!=null)
            oResponse.put("alergia", estudiante.getEstudiante().getAle());
        else
             oResponse.put("alergia", "");
        
        if (saludControles != null && saludControles.size() > 0) {
            boolean p = true;
            boolean t = true;
            for (SaludControles sc : saludControles) {
                if (sc.getConSal().equals('P') && p) {
                    oResponse.put("peso", sc.getResCon());
                    p = false;
                } else if (sc.getConSal().equals('T') && t) {
                    oResponse.put("talla", sc.getResCon());
                    t = false;
                }
            }
        } else {
            oResponse.put("peso", "");
            oResponse.put("talla", "");
        }

        return WebResponse.crearWebResponseExito("Se Listo correctamente", oResponse);
        //Fin
    }

}
