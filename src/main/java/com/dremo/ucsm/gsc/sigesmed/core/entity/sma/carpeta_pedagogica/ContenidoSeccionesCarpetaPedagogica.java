/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

@Entity(name = "ContenidoCarpetaSMA")
@Table(name = "contenido_secciones_carpeta_pedagogica", schema = "pedagogico")
public class ContenidoSeccionesCarpetaPedagogica implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "con_sec_car_ped_id")
    private Integer conSecCarPedId;
    
    @Size(max = 256)
    @Column(name = "nom")
    private String nom;
    
    @Size(max = 10)
    @Column(name = "tip")
    private String tip;
    
    @Column(name = "tip_usu")
    private Short tipUsu;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "est_reg")
    private Character estReg;
    
    @JoinColumn(name = "sec_car_ped_id", referencedColumnName = "sec_car_ped_id")
    @ManyToOne(optional = false)
    private SeccionCarpetaPedagogica seccionCarpetaPedagogica;
    
    @OneToMany(mappedBy = "contenidoSeccionesCarpetaPedagogica")
    private List<RutaContenidoCarpeta> rutaContenidoCarpetaList;

    public ContenidoSeccionesCarpetaPedagogica() {
    }

    public ContenidoSeccionesCarpetaPedagogica(Integer conSecCarPedId) {
        this.conSecCarPedId = conSecCarPedId;
    }

    public Integer getConSecCarPedId() {
        return conSecCarPedId;
    }

    public void setConSecCarPedId(Integer conSecCarPedId) {
        this.conSecCarPedId = conSecCarPedId;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public Short getTipUsu() {
        return tipUsu;
    }

    public void setTipUsu(Short tipUsu) {
        this.tipUsu = tipUsu;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public SeccionCarpetaPedagogica getSeccionCarpetaPedagogica() {
        return seccionCarpetaPedagogica;
    }

    public void setSeccionCarpetaPedagogica(SeccionCarpetaPedagogica seccionCarpetaPedagogica) {
        this.seccionCarpetaPedagogica = seccionCarpetaPedagogica;
    }

    public List<RutaContenidoCarpeta> getRutaContenidoCarpetaList() {
        return rutaContenidoCarpetaList;
    }

    public void setRutaContenidoCarpetaList(List<RutaContenidoCarpeta> rutaContenidoCarpetaList) {
        this.rutaContenidoCarpetaList = rutaContenidoCarpetaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (conSecCarPedId != null ? conSecCarPedId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContenidoSeccionesCarpetaPedagogica)) {
            return false;
        }
        ContenidoSeccionesCarpetaPedagogica other = (ContenidoSeccionesCarpetaPedagogica) object;
        if ((this.conSecCarPedId == null && other.conSecCarPedId != null) || (this.conSecCarPedId != null && !this.conSecCarPedId.equals(other.conSecCarPedId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica.ContenidoSeccionesCarpetaPedagogica[ conSecCarPedId=" + conSecCarPedId + " ]";
    }
    
}
