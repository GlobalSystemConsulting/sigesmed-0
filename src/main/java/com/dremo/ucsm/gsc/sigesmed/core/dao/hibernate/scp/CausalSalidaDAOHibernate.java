/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.scp;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.CausalSalidaDAO;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.CausalSalida;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author harold
 */
public class CausalSalidaDAOHibernate extends GenericDaoHibernate<CausalSalida> implements CausalSalidaDAO {

    @Override
    public List<CausalSalida> listarCausalSalida() {

        List<CausalSalida> cauSal = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {

            String hql = "SELECT cs FROM CausalSalida cs";

            Query query = session.createQuery(hql);
            cauSal = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo Mostrar las Causales de Salidas \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Mostrar las Causales de Salidas \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return cauSal;
    }

}
