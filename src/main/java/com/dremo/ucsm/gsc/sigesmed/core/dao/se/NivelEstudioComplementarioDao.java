/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.se;

import com.dremo.ucsm.gsc.sigesmed.core.entity.se.NivelEstudioComplementario;
import java.util.List;

/**
 *
 * @author alex
 */
public interface NivelEstudioComplementarioDao {
    public List<NivelEstudioComplementario> listarAll();
    public NivelEstudioComplementario buscarPorId(Integer nivEstComId);
}
