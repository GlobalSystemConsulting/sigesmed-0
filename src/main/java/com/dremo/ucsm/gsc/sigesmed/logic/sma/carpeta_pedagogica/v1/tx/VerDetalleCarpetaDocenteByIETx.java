/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.carpeta_pedagogica.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.ContenidoSeccionesCarpetaPedagogicaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica.ContenidoSeccionesCarpetaPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica.EsquemaCarpeta;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;


public class VerDetalleCarpetaDocenteByIETx implements ITransaction{
    
    private static Logger logger = Logger.getLogger(VerDetalleCarpetaDocenteByIETx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int carId = data.getInt("car");
        int orgId = data.getInt("org");
        int docId = data.getInt("doc");
        return verDetalleCarpeta(carId, orgId, docId);
    }

    public WebResponse verDetalleCarpeta(int carId, int orgId, int docId) {
        try{
            ContenidoSeccionesCarpetaPedagogicaDao contDao = (ContenidoSeccionesCarpetaPedagogicaDao) FactoryDao.buildDao("sma.ContenidoSeccionesCarpetaPedagogicaDao");
            List<EsquemaCarpeta> esquema = contDao.listarEsquemaCarpeta(carId);
            JSONArray jsonEsquema = new JSONArray(EntityUtil.listToJSONString(
                    new String[]{"carDigId","secCarPedId","ord","secNom","conSecCarPedId","conNom"},
                    new String[]{"carId","secId","secOrd","secNom","conId","conNom"},
                    esquema
            ));
            return WebResponse.crearWebResponseExito("Se mostro con exito el detalle de la carpeta pedagógica del docente", jsonEsquema);
        }catch (Exception e){
            logger.log(Level.SEVERE,"verDetalleCarpetaDocenteByIE",e);
            return WebResponse.crearWebResponseError("No se pudo listar los detalles de la carpeta pedagógica del docente");
        }
    }
}
