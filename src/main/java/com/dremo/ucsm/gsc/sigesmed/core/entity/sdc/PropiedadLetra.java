package com.dremo.ucsm.gsc.sigesmed.core.entity.sdc;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="propiedad_letra" ,schema="administrativo")
public class PropiedadLetra  implements java.io.Serializable {


    @Id
    @Column(name="pro_let_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_propiedad_letra", sequenceName="administrativo.secuencia_propiedad_letra_id_seq" )
    @GeneratedValue(generator="secuencia_propiedad_letra")
    private Integer proLetId;
    
    @Column(name="pro_let_des")
    private String proLetDes;
 
    public PropiedadLetra() {
    }

    public PropiedadLetra(Integer proLetId) {
        this.proLetId = proLetId;
    }

    public PropiedadLetra(Integer proLetId, String proLetDes, Integer plaLetTam) {
        this.proLetId = proLetId;
        this.proLetDes = proLetDes;
      
    }

    public Integer getProLetId() {
        return proLetId;
    }

    public void setProLetId(Integer proLetId) {
        this.proLetId = proLetId;
    }

    public String getProLetDes() {
        return proLetDes;
    }

    public void setProLetDes(String proLetDes) {
        this.proLetDes = proLetDes;
    }

}


