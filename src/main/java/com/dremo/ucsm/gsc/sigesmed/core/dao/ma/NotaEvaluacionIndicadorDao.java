package com.dremo.ucsm.gsc.sigesmed.core.dao.ma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.NotaEvaluacionIndicador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.NotaIndicador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Periodo;

import java.util.List;
import org.json.JSONArray;

/**
 * Created by Administrador on 16/01/2017.
 */
public interface NotaEvaluacionIndicadorDao extends GenericDao<NotaEvaluacionIndicador> {
    List<NotaIndicador> listarNotasIndicadorAlumno(int idSes,int idGradAlumno);
    NotaEvaluacionIndicador buscarNotaIndicadorEstudiante(int idSesion, int idInd,int idGraOrEst,char idPerido,int numPer);
	List<NotaEvaluacionIndicador> buscarNotaIndicadoresEstudiante(int idInd,int idGraOrEst,char idPerido,int numPer);
    NotaEvaluacionIndicador buscarNotaPorId(int idNota);
    Periodo buscarPeriodoId(char periodo);
    
    JSONArray buscarNotasxIndicadorxEstudiante(int idInd, int idGraOrEst,char idPeriodo,int numPer);
    JSONArray buscarIndicadoresxSesionxEstudiante(int idSesion, int idGraOrEst,char idPeriodo,int numPer);
    Boolean buscarNota(int idSesion, int idInd,int idGraOrEst,char idPerido,int numPer);

}
