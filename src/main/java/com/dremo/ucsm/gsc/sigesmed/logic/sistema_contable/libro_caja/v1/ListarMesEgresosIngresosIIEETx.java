/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.LibroCajaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.cantidadModel;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author G-16
 */
public class ListarMesEgresosIngresosIIEETx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {

        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int organizacionID = 0;
        Date desde = null;
        Date hasta = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
           
            organizacionID = requestData.getInt("organizacionID");
            if(!requestData.getString("desde").contentEquals(""))
                desde = new SimpleDateFormat("dd/M/yyyy").parse( requestData.getString("desde"));
            if(!requestData.getString("hasta").contentEquals(""))
                hasta = new SimpleDateFormat("dd/M/yyyy HH:mm:ss").parse( requestData.getString("hasta") + " 23:59:59" );
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo realizar la estadistica", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        List<cantidadModel> compras = null;
        List<cantidadModel> ventas = null;
        LibroCajaDao hisDao = (LibroCajaDao)FactoryDao.buildDao("sci.LibroCajaDao");
        try{
            compras = hisDao.EgresosTotalDeInstitucionesPorMes( organizacionID ,desde, hasta);
            ventas = hisDao.IngresosTotalDeInstitucionesPorMes(organizacionID ,desde, hasta);
        }catch(Exception e){
            System.out.println("E"+e);
            return WebResponse.crearWebResponseError("No se pudo realizar la estadistica Consulta", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        System.out.println("tamaño :"+ ventas.size());
        
       
        
        
        JSONArray rCompras = new JSONArray();        
        JSONArray rVentas = new JSONArray();
        
        JSONArray anioMesV = new JSONArray();
        JSONArray anioMesC = new JSONArray();
        String [] meses = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
        /*
        for(cantidadModel a:compras){
            
            aOrgan1.put(a.nombre);
            nRegistros1.put(a.num1);
 
        }*/
        for(cantidadModel a:ventas){
          
           rVentas.put(a.num1);
           
           anioMesV.put(a.anio+"-"+meses[(a.mes)-1]);
        }
        for(cantidadModel a:compras){
          
           rCompras.put(a.num1);
           anioMesC.put(a.anio+"-"+meses[(a.mes)-1]);
        }
         
        for(int j=0;j<anioMesV.length();j++){
           System.out.println(anioMesV.get(j));    
           System.out.println(rVentas.get(j)); 
           

        }
        
        
        
        JSONObject res = new JSONObject();
              
        res.put("Ventas", rVentas);
        
        res.put("AnioMesV", anioMesV);
        
              
        res.put("Compras", rCompras);
        res.put("AnioMesC", anioMesC);
        return WebResponse.crearWebResponseExito("Se Listo correctamente",res);        
        //Fin     
        
    }
        
    
}
