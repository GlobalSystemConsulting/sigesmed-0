/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.monitoreo_sesion_apredizaje.plantilla_ficha_monitoreo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.InstruccionLlenadoIndicadoresDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.CompromisoGestionPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.InstruccionLlenadoIndicadores;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class RegistrarInstruccionLITx implements ITransaction{

    private static final Logger logger = Logger.getLogger(RegistrarInstruccionLITx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        String insDes = data.getString("insDes");
        int comId = data.getInt("comId");
        int usuId = data.getInt("usuId");

        return registrarInstruccionLI(insDes, comId, usuId);
    }

    private WebResponse registrarInstruccionLI(String insDes, int comId, int usuId) {
        try{
            InstruccionLlenadoIndicadoresDao insLleIndDao = (InstruccionLlenadoIndicadoresDao) FactoryDao.buildDao("sma.InstruccionLlenadoIndicadoresDao");

            InstruccionLlenadoIndicadores instruccion = new InstruccionLlenadoIndicadores();
            instruccion.setInsLleIndDes(insDes);
            instruccion.setCompromisoGP(new CompromisoGestionPedagogica(comId));
            instruccion.setFecMod(new Date());
            instruccion.setUsuMod(usuId);
            instruccion.setEstReg("A");
            insLleIndDao.insert(instruccion);
            
            JSONObject oResponse = new JSONObject();
            oResponse.put("insId", instruccion.getInsLleIndId());
            oResponse.put("insDes", instruccion.getInsLleIndDes());
            oResponse.put("comId", comId);

            return WebResponse.crearWebResponseExito("Se registro la instruccion de llenado de insdicador(es) correctamente",oResponse);
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarInstruccionLlenadoIndicadores",e);
            return WebResponse.crearWebResponseError("Error al registrar la instruccion de llenado de insdicador(es)");
        }
    }
    
}
