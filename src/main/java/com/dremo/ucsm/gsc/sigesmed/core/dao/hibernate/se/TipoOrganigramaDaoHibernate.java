/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TipoOrganigramaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TipoOrganigrama;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author felipe
 */
public class TipoOrganigramaDaoHibernate extends GenericDaoHibernate<TipoOrganigrama> implements TipoOrganigramaDao{

    @Override
    public List<TipoOrganigrama> listarXTipoOrganigrama() {
        List<TipoOrganigrama> tiposOrganigramas = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT t from TipoOrganigrama as t "
                    + "WHERE t.estReg='A'";
            Query query = session.createQuery(hql);
            tiposOrganigramas = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los tipos Organigramas \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los tipos Organigramas \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return tiposOrganigramas;
    }

    @Override
    public TipoOrganigrama buscarXId(Integer tipOrgiId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        TipoOrganigrama item = (TipoOrganigrama)session.get(TipoOrganigrama.class, tipOrgiId);
        session.close();
        return item;
    }

    @Override
    public List<TipoOrganigrama> listarxOrganigrama(int organigramaId) {
        List<TipoOrganigrama> tiposOrganigramas = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT t from TipoOrganigrama as t "
                    + "JOIN FETCH t.datosOrganigramas do "
                    + "WHERE t.estReg='A' "
                    + "AND do.datOrgiId='"+organigramaId+"'";
            Query query = session.createQuery(hql);
            tiposOrganigramas = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los tipos Organigramas \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los tipos Organigramas \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return tiposOrganigramas;
    }
    
}
