package com.dremo.ucsm.gsc.sigesmed.core.entity.sci;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sci.LibroCajaDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.LibroCajaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.EntidadCantidadModel;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author ucsm
 */
public class PruebasSistemaContable {
    
   public static void main (String args[]) throws ParseException{
   
        
           
           
    
    /***
        List<LibroCaja> listDatos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();  
        
        List<Tesorero> listT=null;
        
        try{
            //listar libro caja activos          
            String hql = "SELECT lc FROM LibroCaja lc join fetch lc.organizacion o join fetch lc.persona p WHERE lc.estReg!='E' AND o.orgId="+1 ;
            Query query = session.createQuery(hql);                        
            listDatos = query.list(); 
            
            for (LibroCaja lib:listDatos){
                 String hqlt = "SELECT t FROM Tesorero t join fetch t.libroCaja lc join fetch t.persona p WHERE t.estReg!='E' AND lc.libCajId="+lib.getLibCajId(); ;
                 Query queryt = session.createQuery(hqlt);                        
                 listT=queryt.list() ; 
     
                 lib.getTesoreros().addAll(listT);
            }
            
       }catch(Exception e){
            System.out.println("No se pudo Listar los Libro Caja\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los Libro Caja \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
           
            
         for(LibroCaja lib:listDatos){
             System.out.println(lib.getPersona().getNom());
             for (Tesorero tes:lib.getTesoreros()){
                   tes.getPersona().getNom();
     
               System.out.println(tes.getPersona().getNom());                              
            }
         }

       
        ***/
   /***  Session session = HibernateUtil.getSessionFactory().openSession();  
     List<Asiento> listDatos=null;
        
        try{
            //listar libro caja activos          
            String hql = "SELECT  a FROM Asiento a join fetch a.detalleCuentas dc join fetch dc.cuentaContable c WHERE dc.estReg!='E' AND c.cueConId in ('102','104') AND a.organizacion=1)";
            Query query = session.createQuery(hql);                        
            listDatos = query.list(); 
            
                     
       }catch(Exception e){
            System.out.println("No se pudo Listar los Libro Caja\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los Libro Caja \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
      ***/
     /***  
       
        List<List<Object>> listDatos= new ArrayList<List<Object>>();
        
        List<Asiento> ObjAsientos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();                  
        
        try{
            //listar transacciones del libro caja activos          
            String hql = "SELECT  a FROM Asiento a join fetch a.detalleCuentas dc join fetch dc.cuentaContable c join fetch a.asiento WHERE dc.estReg!='E' AND a.codLibro in ('C') AND a.organizacion="+1;
            Query query = session.createQuery(hql);                        
            ObjAsientos = query.list();  
            
            List<Object> listaObjetos =new ArrayList<>();
            
              for (Asiento asiento:ObjAsientos){
                  
                  listaObjetos.add(0, asiento);
                  
                  if (asiento.getAsiento().getCodLibro().equals('E')){
                      
                      String hqlC = "SELECT c FROM RegistroCompras c join fetch c.clienteProveedor cp join fetch c.tipoPago tp WHERE c.estReg!='E' AND c.codUniOpeId="+asiento.getCodUniOpeId();
                      Query queryC = session.createQuery(hqlC); 
                      if(queryC!=null){
                          RegistroCompras registroC = (RegistroCompras)queryC.uniqueResult();
                          listaObjetos.add(1, registroC);
                       }                     

                  }
                  
                  else if (asiento.getAsiento().getCodLibro().equals('I')){
                      
                      String hqlC = "SELECT v FROM RegistroVentas v join fetch v.clienteProveedor cp join fetch v.tipoPago tp WHERE v.estReg!='E' AND c.codUniOpeId="+asiento.getCodUniOpeId();
                      Query queryV = session.createQuery(hqlC); 
                      if(queryV!=null){
                          RegistroVentas registroV = (RegistroVentas)queryV.uniqueResult();
                          listaObjetos.add(1, registroV);
                       }                     
                  }
                  else
                      listaObjetos.add(1, null);   
                  
                  listDatos.add(listaObjetos);
                     
            }
            
       }catch(Exception e){
            System.out.println("No se pudo Listar las transacciones del Libro Caja\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo las transacciones del Listar los Libro Caja \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
      
      ***/
      /*** 
         List<List<Object>> listDatos= new ArrayList<>();
        
        List<Asiento> ObjAsientos = null;
        RegistroCompras registroC=null;
        RegistroVentas registroV=null;
      //  List<Object> listaObjetos =new ArrayList<>();
        Session session = HibernateUtil.getSessionFactory().openSession();                  
        
        try{
            //listar transacciones del libro caja activos          
            String hql = "SELECT  DISTINCT a FROM Asiento a join fetch a.organizacion o  join fetch a.detalleCuentas dc   join fetch dc.cuentaContable c WHERE a.estReg!='E' AND o.orgId="+1;
            Query query = session.createQuery(hql);                        
            ObjAsientos = query.list();  
            
              for (Asiento asiento:ObjAsientos){
                   List<Object> listaObjetos =new ArrayList<>(2);
                  listaObjetos.add(0, asiento);
                  
                   if (asiento.getCodLibro().equals('C')){
                      
                      String hqlC = "SELECT c FROM RegistroCompras c join fetch c.clienteProveedor cp join fetch cp.tipoDocumentoIdentidad td join fetch c.tipoPago tp WHERE c.estReg!='E' AND c.codUniOpeId="+asiento.getCodUniOpeId();
                      Query queryC = session.createQuery(hqlC); 
                      if(queryC!=null){
                          registroC = (RegistroCompras)queryC.uniqueResult();
                          listaObjetos.add(1, registroC);
                           System.out.println(registroC.getCodUniOpeId());
                       }                     

                  }
                   else if (asiento.getCodLibro().equals('V')){
                      
                      String hqlC = "SELECT v FROM RegistroVentas v join fetch v.clienteProveedor cp join fetch v.tipoPago tp WHERE v.estReg!='E' AND c.codUniOpeId="+asiento.getCodUniOpeId();
                      Query queryV = session.createQuery(hqlC); 
                      if(queryV!=null){
                          registroV = (RegistroVentas)queryV.uniqueResult();
                          listaObjetos.add(1, registroV);
                       }                     
                  }
                  else
                      listaObjetos.add(1, null);   
                  
                  listDatos.add(listaObjetos);
                     
            }
            
       }catch(Exception e){
            System.out.println("No se pudo Listar las transacciones del Libro Caja\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo las transacciones del Listar los Libro Caja \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
      ***/
       
      /***  Date b=new Date();
        Date a= new Date("Thu Sep 08 2016 00:00:00 GMT-0500 (Hora est. Pacífico, Sudamérica)");
     
        DateFormat fechaHora = new SimpleDateFormat("dd/M/yyyy");
		String convertido = fechaHora.format(a);
		System.out.println(a);
             Date  desde = new SimpleDateFormat("dd/M/yyyy").parse( convertido);
             
             System.out.println(desde);
             ***/
       /*** List<LibroCaja> listDatos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        List<Tesorero> listaT = null;

        try {
            //listar libro caja activos          
            String hql = "SELECT lc FROM LibroCaja lc join fetch lc.persona p join fetch lc.persona p WHERE lc.estReg!='E' AND lc.organizacion.orgId=:p1 ";
            Query query = session.createQuery(hql);
            query.setParameter("p1", 6);
            listDatos = query.list();

            for (int i=0;i<listDatos.size();i++) {
                                  System.out.println(listDatos.get(i));

                 hql = "SELECT t FROM Tesorero t  join fetch t.persona p WHERE t.estReg!='E' AND t.libroCaja.libCajId=:p1";
                 query = session.createQuery(hql);
                 query.setParameter("p1", listDatos.get(i).getLibCajId());
                 listaT =query.list();               
                 listDatos.get(i).getTesoreros().addAll(listaT);
                 if(listDatos.get(i).getTesoreros().isEmpty()){
                     System.out.println("jajaja");
                 }
                 else{
                     System.out.println("jojojo");
                 }
                                     
            }

        } catch (Exception e) {
            System.out.println("No se pudo Listar los Libro Caja\\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los Libro Caja \\n " + e.getMessage());
        } finally {
            session.close();
        }
        
      ***/
     /***
       Session session = HibernateUtil.getSessionFactory().openSession();
        LibroCaja lib = null;
      
        try {
            //listar Libro
            String hql = "SELECT Max(lc) FROM LibroCaja lc  WHERE lc.estReg!='E' AND lc.organizacion.orgId=:p1";             
            Query query = session.createQuery(hql);

            query.setParameter("p1", 7);
           
            query.setMaxResults(1);
            lib = (LibroCaja) query.uniqueResult(); 
                  
        } catch (Exception e) {
            
                System.out.println("No se pudo verificar estado del libro \n " + e.getMessage());
                throw new UnsupportedOperationException("No se pudo verificar estado del libro \n " + e.getMessage());
            
        } finally {
            session.close();
        }
        ***/
       
       /***
        Session session = HibernateUtil.getSessionFactory().openSession();   
        List<HechosLibro> hechos=null; 
        try {
            Date d=new Date();
            Date mesA= d;  mesA.setMonth(d.getMonth()-1); mesA.setDate(1);
            Date M=new Date();
             //listar 
            String hql = "SELECT hl FROM HechosLibro hl join fetch hl.cuenta c join fetch hl.libroCaja lc WHERE hl.estReg='A'  AND lc.libCajId=:p1 AND hl.fecMes>=:p2 AND hl.fecMes<=:p3";
            Query query = session.createQuery(hql);
            query.setParameter("p1", 9);
            query.setParameter("p2", mesA);
            query.setParameter("p3", M);          
            hechos =query.list();
            
          

        } catch (Exception e) {
            System.out.println("No se pudo encontrar hechos : " + "\n" + e.getMessage());
             throw new UnsupportedOperationException("No se pudo encontrar hechos \\n " + e.getMessage());
        } finally {
            session.close();
        }
       ***/
       /***
        List<CuentaContable> listDatos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        LibroCaja lib = new LibroCaja(11);
        try {
            //listar trbajdor
            String hql = "SELECT c.cuenta FROM CuentasEfectivo c WHERE c.estReg='A' AND c.libroCaja=:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", lib);
            listDatos = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo Listar \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar \\n " + e.getMessage());
        } finally {
            session.close();
        }***/
            
         /***      
          * 
          */
       
       /***
        Session session = HibernateUtil.getSessionFactory().openSession();   
        List<ResultadoMensual> resultado= new ArrayList<>(); 
        try {                    
            LibroCaja libro = new LibroCaja(14);
            
            Date desde= new Date("2016/10/01");
            Date hasta = new Date("2016/11/03");
            
           
            desde.setMonth(desde.getMonth()-1);
             
             
             int i= hasta.getMonth()- desde.getMonth();
            
            Date inf= desde;
            Date sup= hasta;                  
            Date e= new Date(hasta.getYear(),hasta.getMonth(),hasta.getDate());
            ResultadoMensual r=null;
             //listar 
            
            for(int j=0;j<i;j++){
                System.out.println(i);
                inf.setMonth(inf.getMonth()); inf.setDate(1);                 
                sup.setMonth(inf.getMonth()); sup.setDate(31);
                 
                if(sup.getMonth()!=inf.getMonth()){
                    sup.setMonth(inf.getMonth()); sup.setDate(30);
                }                
              
                String hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.sci.ResultadoMensual( SUM(hl.impDeb),SUM(hl.impHab)) FROM HechosLibro hl " +
                        " WHERE hl.libroCaja=:p2 AND hl.estReg='A' AND hl.fecMes>=:p3 AND hl.fecMes<=:p4 ";
                  
                    Query query = session.createQuery(hql);
                                        
                    query.setParameter("p2",libro);
                    query.setDate("p3",inf); 
                    query.setDate("p4",sup);
                  
                   r = (ResultadoMensual)query.uniqueResult();
                   r.setNombreMes(inf.getMonth());
                   resultado.add(r);
                   
                  inf.setMonth(inf.getMonth()+1);
                                                                                                          
            }
           
            
                BigDecimal debe= new BigDecimal(BigInteger.ZERO); BigDecimal haber =new BigDecimal(BigInteger.ZERO);
               
                     String hql = "SELECT ( SUM(dC.impDetCue)) FROM  DetalleCuenta dC" +
                        " WHERE dC.asiento.libroCaja=:p2 AND dC.estReg='A' AND dC.asiento.fecAsi>=:p3 AND dC.asiento.fecAsi<=:p4 AND dC.natDetCue=:p1 AND dC.cuentaContable.efeReg=false";
                  
                    Query query = session.createQuery(hql);
                    query.setBoolean("p1",true);         
                    query.setParameter("p2",libro);
                    query.setDate("p3",desde); 
                    query.setDate("p4",e);
                                      
                  if(query.uniqueResult()!=null){                   
                      haber= new BigDecimal(query.uniqueResult().toString());
                  }
                 
                     hql = "SELECT ( SUM(dC.impDetCue)) FROM  DetalleCuenta dC" +
                        " WHERE dC.asiento.libroCaja=:p2 AND dC.estReg='A' AND dC.asiento.fecAsi>=:p3 AND dC.asiento.fecAsi<=:p4 AND dC.natDetCue=:p1 AND dC.cuentaContable.efeReg=false";
                  
                    query = session.createQuery(hql);
                    query.setBoolean("p1",false);         
                    query.setParameter("p2",libro);
                    query.setDate("p3",desde); 
                    query.setDate("p4",e);
                  
                   
                    
                  if(query.uniqueResult()!=null){                      
                      debe= new BigDecimal(query.uniqueResult().toString());
                  }
                                                                                        
                   resultado.add(new ResultadoMensual(e.getMonth(),r.getDebe().add(debe),r.getHaber().add(haber)));                                                   
   
             //listar 
        
            
        } catch (Exception e) {
            System.out.println("No se pudo encontrar hechos : " + "\n" + e.getMessage());
             throw new UnsupportedOperationException("No se pudo encontrar hechos \\n " + e.getMessage());
        } finally {
            session.close();
        }
        
        
        for(ResultadoMensual d:resultado){
                System.out.println(d.getNombreMes() + "---"+d.getDebe()+"-----"+d.getHaber());
        }
                   
        ***/
       
     /***    
         //esta funcion obtiene los hechos mensuales del libro como resultado  
       Session session = HibernateUtil.getSessionFactory().openSession();   
       List<ResultadoMensual> resultado= new ArrayList<>(); 
       LibroCaja libro = new LibroCaja(14);
            
            Date desde= new Date("2016/09/01");
            Date hasta = new Date("2016/11/03");
            
        try {                    
            //se considera que los resultados se generan despues del 1er mes para obtener el saldo mes anterior                                           
            desde.setMonth(desde.getMonth()-1);                          
            int i= hasta.getMonth()- desde.getMonth();
                                              
            ResultadoMensual r=null;
             //listar 
            
            for(int j=0;j<i;j++){
               
                desde.setMonth(desde.getMonth()); desde.setDate(1);                 
                hasta.setMonth(desde.getMonth()); hasta.setDate(31);
                 
                if(hasta.getMonth()!=desde.getMonth()){
                    hasta.setMonth(desde.getMonth()); hasta.setDate(30);
                }                
              
                String hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.sci.ResultadoMensual( SUM(hl.impDeb),SUM(hl.impHab)) FROM HechosLibro hl " +
                        " WHERE hl.libroCaja=:p2 AND hl.estReg='A' AND hl.fecMes>=:p3 AND hl.fecMes<=:p4 ";
                  
                    Query query = session.createQuery(hql);
                                        
                    query.setParameter("p2",libro);
                    query.setDate("p3",desde); 
                    query.setDate("p4",hasta);
                  
                   r = (ResultadoMensual)query.uniqueResult();
                   r.setNombreMes(desde.getMonth());
                   resultado.add(r);
                   
                  desde.setMonth(desde.getMonth()+1);
                                                                                                          
            }
           
            
        
         } catch (Exception e) {
            System.out.println("No se pudo encontrar hechos : " + "\n" + e.getMessage());
             throw new UnsupportedOperationException("No se pudo encontrar hechos \\n " + e.getMessage());
        } finally {
            session.close();
        }
        
        
        for(ResultadoMensual d:resultado){
                System.out.println(d.getNombreMes() + "---"+d.getDebe()+"-----"+d.getHaber());
        }
     
   
   Date f= new Date("2016/10/15"); 
   
  
   int desdeD=f.getDate(); int desdeM=f.getMonth(); int desdeA=f.getYear();
   Date r =new Date(desdeA,desdeM,desdeD);
       System.out.println(r);
       
      BigDecimal u= new BigDecimal(180.50);
      BigDecimal y= new BigDecimal(129.50);

      BigDecimal t = u.subtract(y).setScale(2);
        System.out.println(t);
       ***/
       
        Session session = HibernateUtil.getSessionFactory().openSession();
        List <ResultadosMensualPorCuenta> listaDatos = null;
         LibroCaja libro = null;
            
            Date desde= new Date("2016/09/01");
            Date hasta = new Date("2016/11/03");
            int a=0;
        try{
            String hql = "SELECT lc FROM LibroCaja lc WHERE lc.libCajId=14 AND YEAR(lc.fecApe)=:p1 ";
           
            Query query = session.createQuery(hql);   
            query.setParameter("p1", 2016);

            libro=(LibroCaja)query.uniqueResult();
           
        
        }catch(Exception e){
          
            System.out.println("No se pudo insertar estado de historial" + "\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
   
        System.out.println(libro.getNom());
       
    
   }
}
