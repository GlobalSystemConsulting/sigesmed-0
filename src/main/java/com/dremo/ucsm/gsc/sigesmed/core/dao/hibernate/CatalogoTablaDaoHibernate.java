/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate;

import com.dremo.ucsm.gsc.sigesmed.core.dao.CatalogoTablaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.CatalogoTabla;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author angel
 */
public class CatalogoTablaDaoHibernate extends GenericDaoHibernate<CatalogoTabla> implements CatalogoTablaDao {

    @Override
    public List<Object> buscarRegistrosTabla(String nombreTabla){
        
        List<Object> lista = null;
        
        char a = 'A';
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar jornadas escolare
            //String hql = "SELECT o FROM "+ dato.getName()+" o WHERE o.estReg!='E'";
            //String hql = "SELECT o FROM  o WHERE o.estReg!='E'"; //por defecto verificamos que sea rol estudiante
         //   Query query = session.createSQLQuery(hql);
                       
           // lista = query.list();
        //SELECT * FROM administrativo.tipo_documento o WHERE o.estReg!='E'"
            String sql = "select * from administrativo."+nombreTabla+" d where d.est_reg = ?;";
            SQLQuery query = session.createSQLQuery(sql);
            query.setParameter(0,a);
            lista =(List<Object>)query.list();
            
        }catch(Exception e){
             System.out.println("No se pudo Listar los registros Consulta 1 \\n "+ e.getMessage());

            System.out.println("No se pudo Listar los Registros de la tabla \n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los registro de la tabla \\n "+nombreTabla+" "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return lista;
        
    }
    
     @Override
    public List<Object> buscarRegistrosTabla2(String nombreClase){
        Session session = HibernateUtil.getSessionFactory().openSession();
        String rutaClase = "com.dremo.ucsm.gsc.sigesmed.core.entity."+nombreClase+".class";
        String rutaClase2 ="com.dremo.ucsm.gsc.sigesmed.core.entity."+nombreClase;
       try{
           Object obj = Class.forName(rutaClase2).newInstance();
       }catch(Exception e){
            System.out.println("No se instancion correctamente la clase ----" + e);
       }
        
        
        try{

            Criteria criteria = session.createCriteria(rutaClase);
            criteria.add(Restrictions.eq("estReg",'A'));
            
            List<Object> ldetalle = criteria.list();
            return ldetalle;
        }catch(Exception e){
             System.out.println("No se pudo Listar los registros Consulta 2 \\n "+ e.getMessage());

        }finally{
            session.close();
        }
        return null;
    }
    
    @Override
    public List<Object> buscarRegistrosTabla3(String nombreClase) {
        String rutaClase = "com.dremo.ucsm.gsc.sigesmed.core.entity."+nombreClase;
        
        List<Object> registros = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        
        
        try{
            //listar FuncionSistemas
            //String hql = "SELECT p FROM Persona p" ;
            String hql = "SELECT t FROM "+rutaClase +" t WHERE t.estReg='A'";// 
            //String hql = "SELECT t FROM Trabajador t join fetch t.persona p WHERE t.orgId =:p1"; 
            Query query = session.createQuery(hql);            
           
            registros = query.list();
           
        
        }catch(Exception e){
           
            System.out.println("No se pudo Listar los registros Consulta 3 para la Instancia \\n   "+nombreClase +"--"+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los registros Consulta 3\\n "+nombreClase +"--"+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return registros;
    }
  
}
