package com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "desarrollo_tema_capacitacion", schema = "pedagogico")
public class DesarrolloTemaCapacitacion implements Serializable {
    @Id
    @Column(name = "des_tem_cap_id", unique = true, nullable = false)
    @SequenceGenerator(name = "secuencia_desarrollo_tema_capacitacion", sequenceName = "pedagogico.desarrollo_tema_capacitacion_des_tem_cap_id_seq")
    @GeneratedValue(generator = "secuencia_desarrollo_tema_capacitacion")
    private int desTemCapId;
    
    @Column(name = "tem_cur_cap_id",nullable = false)
    private int temCurCapId;
    
    @Column(name = "sed_cap_id",nullable = false)
    private int sedCapId;
    
    @Column(name = "nom",nullable = false,length = 60)
    private String nom;
    
    @Column(name = "tip",nullable = false,length = 1)
    private Character tip;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_reg",nullable = false)
    private Date fecReg;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_ent",nullable = false)
    private Date fecEnt;
    
    @OneToMany(mappedBy = "desarrolloTemaCapacitacion", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    List<AdjuntoTemaCapacitacion> adjuntos = new ArrayList<>();
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(updatable = false, insertable = false, name = "tem_cur_cap_id"),
        @JoinColumn(updatable = false, insertable = false, name = "sed_cap_id")})
    private TemarioCursoCapacitacion tema;
    
    @OneToMany(mappedBy = "desarrollo", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<EvaluacionDesarrollo> evaluaciones = new HashSet<>(0);
    
    public DesarrolloTemaCapacitacion() {}

    public DesarrolloTemaCapacitacion(int temCurCapId, int sedCapId, String nom, Character tip) {
        this.temCurCapId = temCurCapId;
        this.sedCapId = sedCapId;
        this.nom = nom;
        this.tip = tip;
        this.fecReg = new Date();
        this.fecEnt = new Date();
    }
    
    public int getDesTemCapId() {
        return desTemCapId;
    }

    public void setDesTemCapId(int desTemCapId) {
        this.desTemCapId = desTemCapId;
    }

    public int getTemCurCapId() {
        return temCurCapId;
    }

    public void setTemCurCapId(int temCurCapId) {
        this.temCurCapId = temCurCapId;
    }

    public int getSedCapId() {
        return sedCapId;
    }

    public void setSedCapId(int sedCapId) {
        this.sedCapId = sedCapId;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Character getTip() {
        return tip;
    }

    public void setTip(Character tip) {
        this.tip = tip;
    }

    public Date getFecReg() {
        return fecReg;
    }

    public void setFecReg(Date fecReg) {
        this.fecReg = fecReg;
    }

    public Date getFecEnt() {
        return fecEnt;
    }

    public void setFecEnt(Date fecEnt) {
        this.fecEnt = fecEnt;
    }

    public List<AdjuntoTemaCapacitacion> getAdjuntos() {
        return adjuntos;
    }

    public void setAdjuntos(List<AdjuntoTemaCapacitacion> adjuntos) {
        this.adjuntos = adjuntos;
    }

    public TemarioCursoCapacitacion getTema() {
        return tema;
    }

    public void setTema(TemarioCursoCapacitacion tema) {
        this.tema = tema;
    }

    public Set<EvaluacionDesarrollo> getEvaluaciones() {
        return evaluaciones;
    }

    public void setEvaluaciones(Set<EvaluacionDesarrollo> evaluaciones) {
        this.evaluaciones = evaluaciones;
    }
}
