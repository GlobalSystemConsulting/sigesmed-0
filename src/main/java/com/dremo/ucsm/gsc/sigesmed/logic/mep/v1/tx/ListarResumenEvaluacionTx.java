/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.mep.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mep.EvaluacionPersonalDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.ResumenEvaluacionPersonal;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author geank
 */
public class ListarResumenEvaluacionTx implements ITransaction{
    
    private static Logger  logger = Logger.getLogger(ListarResumenEvaluacionTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        String idTrab = wr.getMetadataValue("trabajador");
        try{
            return obtenerResumen(Integer.parseInt(idTrab));
        }catch(NumberFormatException e){
            logger.log(Level.SEVERE,logger.getName() + ": execute()",e);
        }
            return null;
    }
    private WebResponse obtenerResumen(int idTrab){
        WebResponse response = new WebResponse();
        EvaluacionPersonalDaoHibernate evdh = new EvaluacionPersonalDaoHibernate();
        try{
            List<ResumenEvaluacionPersonal> resumenes = evdh.getResumenEvaluacion(idTrab);
            JSONArray aData = new JSONArray();
            for(ResumenEvaluacionPersonal resumen : resumenes){
                JSONObject jsonDet = new JSONObject(resumen.toString());
                aData.put(jsonDet);
            }
            response.setResponse(MEP.SUCC_RESPONSE_COD);
            response.setResponseMsg(MEP.SUCC_RESPONSE_MESS_LIST);
            response.setData(aData);
        }catch(Exception e ){
            logger.log(Level.SEVERE,logger.getName() + ":listarTrabajadores()",e);
            response.setResponse(MEP.ERR_RESPONSE_COD);
            response.setResponseMsg(MEP.ERR_RESPONSE_MESS_LIST);
        }
        return response;
    }
}
