/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.monitoreo_sesion_apredizaje.plantilla_ficha_monitoreo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.PlantillaFichaMonitoreoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.PlantillaFichaMonitoreo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class EliminarPlantillaFMSATx implements ITransaction{

    private static final Logger logger = Logger.getLogger(EliminarPlantillaFMSATx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject jsonData = (JSONObject)wr.getData();
        int plaId = jsonData.getInt("plaId");
        return eliminarPlantilla(plaId);
    }

    private WebResponse eliminarPlantilla(int plaId) {
        try{
            PlantillaFichaMonitoreoDao plaFicMonDao = (PlantillaFichaMonitoreoDao) FactoryDao.buildDao("sma.PlantillaFichaMonitoreoDao");
            PlantillaFichaMonitoreo plaFicMon = plaFicMonDao.buscarPorId(plaId);
            plaFicMonDao.delete(plaFicMon);
            return WebResponse.crearWebResponseExito("Se elimino correctamente la plantilla de ficha de monitoreo");

        }catch (Exception e){
            logger.log(Level.SEVERE,"eliminarPlantillaFichaMonitoreo",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar la plantilla de ficha de monitoreo");
        }
    }
    
}
