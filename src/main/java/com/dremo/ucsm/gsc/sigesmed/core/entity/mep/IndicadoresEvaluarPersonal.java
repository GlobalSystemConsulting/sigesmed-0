package com.dremo.ucsm.gsc.sigesmed.core.entity.mep;
// Generated 15/07/2016 01:43:01 PM by Hibernate Tools 4.3.1


import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 * In@author geank
 */
@Entity
@Table(name="indicadores_evaluar_personal",schema="pedagogico")
public class IndicadoresEvaluarPersonal  implements java.io.Serializable {

    @EmbeddedId
    @AttributeOverrides( {
        @AttributeOverride(name="detEvaPerId", column=@Column(name="det_eva_per_id", nullable=false) ), 
        @AttributeOverride(name="indFicEvaId", column=@Column(name="ind_fic_eva_id", nullable=false) ) } )
    private IndicadoresEvaluarPersonalId id;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="det_eva_per_id", nullable=false, insertable=false, updatable=false)
    //@Fetch(FetchMode.JOIN)
    private ResumenEvaluacionPersonal resumenEvaluacionPersonal;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ind_fic_eva_id", nullable=false, insertable=false, updatable=false)
    //@Fetch(FetchMode.JOIN)
    private IndicadorFichaEvaluacion indicadorFichaEvaluacion;
    @Column(name="pun", nullable=false)
    private int pun;

    public IndicadoresEvaluarPersonal() {
    }
    public IndicadoresEvaluarPersonal(ResumenEvaluacionPersonal resumenEvaluacionPersonal, IndicadorFichaEvaluacion indicadorFichaEvaluacion, int pun) {
        this.id = new IndicadoresEvaluarPersonalId(resumenEvaluacionPersonal.getResEvaPerId(),indicadorFichaEvaluacion.getIndFicEvaId());
        this.resumenEvaluacionPersonal = resumenEvaluacionPersonal;
        this.indicadorFichaEvaluacion = indicadorFichaEvaluacion;
        this.pun = pun;
    }
    public IndicadoresEvaluarPersonal(IndicadoresEvaluarPersonalId id, ResumenEvaluacionPersonal resumenEvaluacionPersonal, IndicadorFichaEvaluacion indicadorFichaEvaluacion, int pun) {
       this.id = id;
       this.resumenEvaluacionPersonal = resumenEvaluacionPersonal;
       this.indicadorFichaEvaluacion = indicadorFichaEvaluacion;
       this.pun = pun;
    }
   
    public IndicadoresEvaluarPersonalId getId() {
        return this.id;
    }
    
    public void setId(IndicadoresEvaluarPersonalId id) {
        this.id = id;
    }

    public ResumenEvaluacionPersonal getResumenEvaluacionPersonal() {
        return this.resumenEvaluacionPersonal;
    }
    
    public void setResumenEvaluacionPersonal(ResumenEvaluacionPersonal resumenEvaluacionPersonal) {
        this.resumenEvaluacionPersonal = resumenEvaluacionPersonal;
    }

    public IndicadorFichaEvaluacion getIndicadorFichaEvaluacion() {
        return this.indicadorFichaEvaluacion;
    }
    
    public void setIndicadorFichaEvaluacion(IndicadorFichaEvaluacion indicadorFichaEvaluacion) {
        this.indicadorFichaEvaluacion = indicadorFichaEvaluacion;
    }

    
    public int getPun() {
        return this.pun;
    }
    
    public void setPun(int pun) {
        this.pun = pun;
    }
    @Override
    public String toString(){
        return EntityUtil.objectToJSONString(new String[]{"resumenEvaluacionPersonal","indicadorFichaEvaluacion","pun"},new String[]{"resumen","indicador","puntaje"},this);
    }
}


