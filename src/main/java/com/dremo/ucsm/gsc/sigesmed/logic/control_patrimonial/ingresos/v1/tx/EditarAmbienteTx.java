/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.AmbientesDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.Ambientes;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONObject;

/**
 *
 * @author harold
 */
public class EditarAmbienteTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        
        JSONObject requestData = (JSONObject)wr.getData();
        Ambientes amb = null;
        
        try {
            
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            int amb_id = requestData.getInt("amb_id");
            String descripcion = requestData.getString("descripcion");
            String ubicacion = requestData.getString("ubicacion");
            char estado = requestData.getString("estado").charAt(0);
            int area = requestData.getInt("area");
            String inst_cons = requestData.getString("inst_cons");
            Date fec_cons = formatter.parse(requestData.getString("fec_cons"));
            int user = requestData.getInt("user");
            int org_id = requestData.getInt("orgid");
            char est_reg = 'A';

            amb = new Ambientes(amb_id, descripcion, ubicacion, estado, area, inst_cons, fec_cons, org_id, user, est_reg);

            AmbientesDAO amb_dao = (AmbientesDAO) FactoryDao.buildDao("scp.AmbientesDAO");
            amb_dao.update(amb);
            
            return WebResponse.crearWebResponseExito("Ambiente actualizado");
        
        } catch (Exception e) {
            
           return WebResponse.crearWebResponseExito("Ambiente no puede ser actualizado", e);
        }   
    }
    
}
