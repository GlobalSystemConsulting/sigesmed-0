/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.LibroCajaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.LibroCaja;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONObject;

/**
 *
 * @author angel
 */
public class ActualizarEstadoLibroFechCierreTx implements ITransaction{
      @Override
    public WebResponse execute(WebRequest wr){
        LibroCaja libro=null;
        int organizacionID;
        Date fc = new Date();
        Date ac = new Date();
        DateFormat fechaHora = new SimpleDateFormat("yyyy-MM-dd");
       
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();           
            organizacionID = requestData.getInt("organizacionid");
          
            LibroCajaDao libroDao = (LibroCajaDao)FactoryDao.buildDao("sci.LibroCajaDao");
            
            try {                        
                libro = libroDao.estadoLibroCaja(organizacionID);
                
                if(libro != null && libro.getEstReg()=='A'){
                   


                        fc = libro.getFecCie();
                        fechaHora.format(ac);  

                        String fce =  fechaHora.format(fc);  
                        String fhoy =  fechaHora.format(ac);  

                       // System.out.println("fecha cierr - " +fc + "Fecha de hoy  "+ac);
                       // System.out.println("fecha Strimnh  cierr - " +fce + "Fecha de hoy  "+fhoy);

                     //   System.out.print("fechas son iguales ? "+fc.equals(ac));

                       // System.out.print("fechas son iguales Strin ? "+fce.equals(fhoy));

                        if(fce.equals(fhoy)){
                            libroDao.CerrarEstadoLibro(libro.getLibCajId());

                        }

                }else if(libro != null &&libro.getEstReg()=='I'){
                    
                }else{
                     return WebResponse.crearWebResponseError("EL LIBRO SE ENCUENTRA CERRADO, ELIMINADO o NO EXISTE");
                }
                
            }
            catch (Exception e) {
                System.out.println("No se pudo encontrar datos del Libro \n"+e);
                return WebResponse.crearWebResponseError("No se pudo encontrar datos del Libro", e.getMessage() );
            }
            
        }catch(Exception e){
            System.out.println("No se pudo obtener la organizacion a la que pertenece \n"+e);
            return WebResponse.crearWebResponseError("No se pudo obtener la organizacion a la que pertenece", e.getMessage() );
        }
        JSONObject oResponse = new JSONObject();
        oResponse.put("libcajid", libro.getLibCajId());
        oResponse.put("estadoLib", libro.getEstReg());
        
        return WebResponse.crearWebResponseExito("Se Verifico el estado de Libro correctamente",oResponse);   
      
    }
}
    
   