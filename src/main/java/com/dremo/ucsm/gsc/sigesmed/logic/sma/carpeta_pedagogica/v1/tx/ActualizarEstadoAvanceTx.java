/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.carpeta_pedagogica.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.DocenteCarpetaPedagogicaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica.DocenteCarpetaPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class ActualizarEstadoAvanceTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ActualizarEstadoAvanceTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            Integer docCarPedId = requestData.getInt("docCarPedId");
            Integer estAva = requestData.getInt("estAva");     
            return actualizarEstadoAvance(docCarPedId, estAva);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar estado de avance. Error: ",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        } 
    }
    
    private WebResponse actualizarEstadoAvance(Integer docCarPedId, Integer estAva) {
        try{
            DocenteCarpetaPedagogicaDao docenteCarPedDao = (DocenteCarpetaPedagogicaDao)FactoryDao.buildDao("sma.DocenteCarpetaPedagogicaDao");        
            DocenteCarpetaPedagogica docenteCarPed = docenteCarPedDao.buscarPorId(docCarPedId);

            docenteCarPed.setEstAva(estAva);
            docenteCarPedDao.update(docenteCarPed);
            
            JSONObject oResponse = new JSONObject();
            oResponse.put("docCarPedId", docenteCarPed.getDocCarPedId());
            oResponse.put("estAva", docenteCarPed.getEstAva());
            return WebResponse.crearWebResponseExito("Estado de avance actualizado exitosamente",oResponse);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"actualizarEstadoAvance",e);
            return WebResponse.crearWebResponseError("Error, el estado de avance no fue actualizada");
        }
    } 
    
}
