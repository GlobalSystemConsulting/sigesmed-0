/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.web;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.GrupoDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.Grupo;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author abel
 */
public class GrupoDaoHibernate extends GenericDaoHibernate<Grupo> implements GrupoDao{

    @Override
    public List<Grupo> buscarPorOrganizacion(int organizacionID){
        List<Grupo> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT g FROM Grupo g WHERE g.orgId=:p1 and g.estReg!='E'";
            Query query = session.createQuery(hql);
            query.setParameter("p1", organizacionID);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los grupos\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los grupos\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public List<Grupo> buscarConUsuariosPorOrganizacion(int organizacionID){
        List<Grupo> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT DISTINCT g FROM Grupo g LEFT JOIN FETCH g.usuarios WHERE g.orgId=:p1 and g.estReg!='E'";
            Query query = session.createQuery(hql);
            query.setParameter("p1", organizacionID);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los grupos\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los grupos\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public void eliminarUsuarios(int grupoID){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            String hql = "delete from GrupoUsuario where grupoId=:p1";
            
            Query query = session.createQuery(hql);
            query.setParameter("p1", grupoID);
            query.executeUpdate();
            
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo eliminar los usuarios del grupo" + this.getClass().getName() + "\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
}
