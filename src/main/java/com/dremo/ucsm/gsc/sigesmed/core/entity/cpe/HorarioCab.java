package com.dremo.ucsm.gsc.sigesmed.core.entity.cpe;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TrabajadorCargo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sdc.ContenidoPlantilla;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sdc.PropiedadLetra;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="horario_cab" ,schema="administrativo")
public class HorarioCab  implements java.io.Serializable {


    @Id
    @Column(name="hor_cab_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencoa_horario_cab", sequenceName="administrativo.horario_cab_hor_cab_id_seq" )
    @GeneratedValue(generator="secuencoa_horario_cab")
    private Integer horCabId;
    
    @Column(name="est_reg")
    private String estReg;

    @Column(name="hor_cab_des")
    private String horCabDes;  
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="hor_cab_fec")
    private Date horCabFec;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="org_id", nullable=false )
    private Organizacion org;  
    
    @OneToMany(mappedBy="horCabId",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
    private List<HorarioDet> horarioDetalle;
    
    @OneToMany(mappedBy="horaCabId",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
    private List<Trabajador> trabajadores;
    
    @OneToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="hor_cab_car")
    private TrabajadorCargo horCabCar;
    
    public HorarioCab() {
    }

    public HorarioCab(Integer horCabId) {
        this.horCabId = horCabId;
    }

    public HorarioCab(TrabajadorCargo horCabCar, String estReg, String horCabDes, Date horCabFec, Organizacion org) {
        this.horCabCar = horCabCar;
        this.estReg = estReg;
        this.horCabDes = horCabDes;
        this.horCabFec = horCabFec;
        this.org = org;
    }

    public HorarioCab(TrabajadorCargo horCabCar, String estReg, String horCabDes, Date horCabFec, Organizacion org, List<HorarioDet> horarioDetalle) {
        this.horCabCar = horCabCar;
        this.estReg = estReg;
        this.horCabDes = horCabDes;
        this.horCabFec = horCabFec;
        this.org = org;
        this.horarioDetalle = horarioDetalle;
    }

    public HorarioCab(TrabajadorCargo horCabCar, String estReg, String horCabDes, Date horCabFec, Organizacion org, List<HorarioDet> horarioDetalle, List<Trabajador> trabajadores) {
        this.horCabCar = horCabCar;
        this.estReg = estReg;
        this.horCabDes = horCabDes;
        this.horCabFec = horCabFec;
        this.org = org;
        this.horarioDetalle = horarioDetalle;
        this.trabajadores = trabajadores;
    }

    

    
    public Integer getHorCabId() {
        return horCabId;
    }

    public void setHorCabId(Integer horCabId) {
        this.horCabId = horCabId;
    }

    public TrabajadorCargo getHorCabCar() {
        return horCabCar;
    }

    public void setHorCabCar(TrabajadorCargo horCabCar) {
        this.horCabCar = horCabCar;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public List<HorarioDet> getHorarioDetalle() {
        return horarioDetalle;
    }

    public void setHorarioDetalle(List<HorarioDet> horarioDetalle) {
        this.horarioDetalle = horarioDetalle;
    }

    public List<Trabajador> getTrabajadores() {
        return trabajadores;
    }

    public void setTrabajadores(List<Trabajador> trabajadores) {
        this.trabajadores = trabajadores;
    }

    public String getHorCabDes() {
        return horCabDes;
    }

    public void setHorCabDes(String horCabDes) {
        this.horCabDes = horCabDes;
    }

    public Date getHorCabFec() {
        return horCabFec;
    }

    public void setHorCabFec(Date horCabFec) {
        this.horCabFec = horCabFec;
    }

    public Organizacion getOrg() {
        return org;
    }

    public void setOrg(Organizacion org) {
        this.org = org;
    }
    
   
}


