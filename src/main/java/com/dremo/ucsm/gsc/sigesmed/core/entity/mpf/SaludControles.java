/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.mpf;

/**
 *
 * @author Carlos
 */
// Generated 13/02/2017 02:15:53 PM by Hibernate Tools 4.3.1
import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name = "SaludControlesMpf")
@Table(name = "salud_controles", schema = "pedagogico")
public class SaludControles implements java.io.Serializable {

    @Id
    @Column(name = "sal_con_id")
    private Long id;

    
    @Column(name = "per_id")
    private Long estudiante;

    @Column(name = "con_sal", length = 1)
    private Character conSal;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_con", length = 29)
    private Date fecCon;

    @Column(name = "res_con", length = 60)
    private String resCon;

    @Column(name = "obs")
    private String obs;

    @Column(name = "usu_mod")
    private Integer usuMod;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length = 29)
    private Date fecMod;

    @Column(name = "est_reg", length = 1)
    private Character estReg;

    public SaludControles() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Character getConSal() {
        return conSal;
    }

    public void setConSal(Character conSal) {
        this.conSal = conSal;
    }

    public Date getFecCon() {
        return fecCon;
    }

    public void setFecCon(Date fecCon) {
        this.fecCon = fecCon;
    }

    public String getResCon() {
        return resCon;
    }

    public void setResCon(String resCon) {
        this.resCon = resCon;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public Long getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(Long estudiante) {
        this.estudiante = estudiante;
    }

  
}
