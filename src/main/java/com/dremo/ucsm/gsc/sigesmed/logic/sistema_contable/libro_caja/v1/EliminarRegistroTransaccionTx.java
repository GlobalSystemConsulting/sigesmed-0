/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.AsientoLibroCajaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.DetalleCuentaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.LibroCajaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.RegistroComprasDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.Asiento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.DetalleCuenta;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.RegistroCompras;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
/**
 *
 * @author G-16
 */
public class EliminarRegistroTransaccionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int AsientoID = 0;
        int CodUniOpID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            AsientoID = requestData.getInt("AsientoID");
             CodUniOpID = requestData.getInt("CodUniOpID");
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo eliminar, datos incorrectos");
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        AsientoLibroCajaDao AsientoDao = (AsientoLibroCajaDao)FactoryDao.buildDao("sci.AsientoLibroCajaDao");
        LibroCajaDao DetalleAsientoDao = (LibroCajaDao)FactoryDao.buildDao("sci.LibroCajaDao");
        RegistroComprasDao CompraDao = (RegistroComprasDao)FactoryDao.buildDao("sci.RegistroComprasDao");

        
        try{
            AsientoDao.delete(new Asiento(AsientoID));
            //DetalleAsientoDao.delete(new DetalleCuenta(new Asiento(AsientoID)));
            CompraDao.delete(new RegistroCompras(CodUniOpID));
           DetalleAsientoDao.EliminarDetalleCuenta(AsientoID);

        }catch(Exception e){
            System.out.println("No se pudo eliminar el Registro Transaccion\n"+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el Registro Transaccion", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("el Registro Transaccion se elimino correctamente");
        //Fin
    }
}
