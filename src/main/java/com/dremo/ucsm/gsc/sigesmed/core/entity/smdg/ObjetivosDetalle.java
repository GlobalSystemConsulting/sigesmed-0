/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.smdg;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "objetivos_detalle", schema = "institucional")
public class ObjetivosDetalle implements Serializable {
    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ode_id")
    private Integer odeId;
    
//    private static final long serialVersionUID = 1L;
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Basic(optional = false)
//    @Column(name = "ode_id")
//    private Integer odeId;
    
    @Column(name = "ode_ind")
    private String odeInd;
    @Column(name = "ode_met")
    private Integer odeMet;
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "est_reg")
    private String estReg;

    @Column(name = "ode_ava")
    private double odeAva;
    
//    @ManyToOne(fetch=FetchType.LAZY)
//    @JoinColumn(name = "obj_id", referencedColumnName = "obj_id", insertable =  false, updatable = false)    
//    private Objetivos objetivo;   
    
    @JoinColumn(name = "ode_id", referencedColumnName = "obj_id", insertable = false, updatable = false)
    @OneToOne(optional = false, fetch = FetchType.LAZY)
    private Objetivos objetivo;
    
//    @Column(name = "obj_id")
//    private Integer objide;
    
    public ObjetivosDetalle() {
    }

    public ObjetivosDetalle(Integer odeId) {
        this.odeId = odeId;
    }

    public ObjetivosDetalle(String odeInd, Integer odeMet,Integer odeId) {
        this.odeInd = odeInd;
        this.odeMet = odeMet;        
        this.odeId = odeId;
    }
    
    public ObjetivosDetalle(String odeInd, Integer odeMet,Integer odeId, double odeAva) {
        this.odeInd = odeInd;
        this.odeMet = odeMet;        
        this.odeId = odeId;
        this.odeAva = odeAva;
    }
    
    public ObjetivosDetalle(Integer odeId, String odeInd, Integer odeMet) {
        this.odeInd = odeInd;
        this.odeMet = odeMet;        
        this.odeId = odeId;
    }
    
    public ObjetivosDetalle(Integer odeId, String odeInd, Integer odeMet, double odeAva) {        
        this.odeInd = odeInd;
        this.odeMet = odeMet;        
        this.odeId = odeId;
        this.odeAva = odeAva;
    }
    
    public Integer getOdeId() {
        return odeId;
    }

    public void setOdeId(Integer odeId) {
        this.odeId = odeId;
    }

    public String getOdeInd() {
        return odeInd;
    }

    public void setOdeInd(String odeInd) {
        this.odeInd = odeInd;
    }

    public Integer getOdeMet() {
        return odeMet;
    }

    public void setOdeMet(Integer odeMet) {
        this.odeMet = odeMet;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }
    
    public Objetivos getObjetivo() {
        return objetivo;
    }

    public void setObjetivo(Objetivos objetivo) {
        this.objetivo = objetivo;
    }
//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (odeId != null ? odeId.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof ObjetivosDetalle)) {
//            return false;
//        }
//        ObjetivosDetalle other = (ObjetivosDetalle) object;
//        if ((this.odeId == null && other.odeId != null) || (this.odeId != null && !this.odeId.equals(other.odeId))) {
//            return false;
//        }
//        return true;
//    }
//
//    @Override
//    public String toString() {
//        return "entidades.smdg.ObjetivosDetalle[ odeId=" + odeId + " ]";
//    }
//    

//    public Integer getObjide() {
//        return objide;
//    }
//
//    public void setObjide(Integer objide) {
//        this.objide = objide;
//    }

    public double getOdeAva() {
        return odeAva;
    }

    public void setOdeAva(double odeAva) {
        this.odeAva = odeAva;
    }
    
}
