/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.smdg;

import java.io.Serializable;
import java.util.List;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "plantilla_grupo", schema = "institucional")
@FilterDef(name="filtrogrupo")

public class PlantillaGrupo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pgr_id")
    private Integer pgrId;
    @Column(name = "pgr_des")
    private String pgrDes;
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;    
    @Column(name = "est_reg")    
    private String estReg  = "A";
    @OneToMany(cascade=CascadeType.ALL)
    @JoinColumn(name="pgr_id")
//    @LazyCollection(LazyCollectionOption.FALSE)
    @Filter(name="filtrogrupo", condition=" est_reg = 'A'")
    private List<PlantillaIndicadores> indicadores;
    @JoinColumn(name = "tip_id", referencedColumnName = "tip_id")
    @ManyToOne(fetch=FetchType.LAZY)
    private TipoGrupo tipoGrupo;
    @JoinColumn(name = "pfi_id", referencedColumnName = "pfi_ins_id")
    @ManyToOne(fetch=FetchType.LAZY)
    private PlantillaFichaInstitucional plantilla;

    public PlantillaGrupo() {
    }

    public PlantillaGrupo(Integer pgrId) {
        this.pgrId = pgrId;
    }
    
    public PlantillaGrupo(Integer pgrId, PlantillaFichaInstitucional plantilla) {
        this.pgrId = pgrId;
        this.plantilla = plantilla;
    }
    
    public PlantillaGrupo(String pgrDes, TipoGrupo tipoGrupo, PlantillaFichaInstitucional plantilla) {        
        this.pgrDes = pgrDes;        
        this.tipoGrupo = tipoGrupo;
        this.plantilla = plantilla;
    }
    
    public PlantillaGrupo(Integer pgrId, String pgrDes, TipoGrupo tipoGrupo, PlantillaFichaInstitucional plantilla) {        
        this.pgrId = pgrId;
        this.pgrDes = pgrDes;        
        this.tipoGrupo = tipoGrupo;
        this.plantilla = plantilla;
    }    
    

    public Integer getPgrId() {
        return pgrId;
    }
    
    public int getTipoGrupoId(){
        return tipoGrupo.getTipId();
    }

    public void setPgrId(Integer pgrId) {
        this.pgrId = pgrId;
    }

    public String getPgrDes() {
        return pgrDes;
    }

    public void setPgrDes(String pgrDes) {
        this.pgrDes = pgrDes;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    @XmlTransient
    public List<PlantillaIndicadores> getIndicadores() {
        return indicadores;
    }

    public void setIndicadores(List<PlantillaIndicadores> indicadores) {
        this.indicadores = indicadores;
    }

    public TipoGrupo getTipoGrupo() {
        return tipoGrupo;
    }

    public void setTipoGrupo(TipoGrupo tipoGrupo) {
        this.tipoGrupo = tipoGrupo;
    }

    public PlantillaFichaInstitucional getPlantilla() {
        return plantilla;
    }

    public void setPlantilla(PlantillaFichaInstitucional plantilla) {
        this.plantilla = plantilla;
    }

//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (pgrId != null ? pgrId.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof PlantillaGrupo)) {
//            return false;
//        }
//        PlantillaGrupo other = (PlantillaGrupo) object;
//        if ((this.pgrId == null && other.pgrId != null) || (this.pgrId != null && !this.pgrId.equals(other.pgrId))) {
//            return false;
//        }
//        return true;
//    }
//
//    @Override
//    public String toString() {
//        return "entidades.smdg.PlantillaGrupo[ pgrId=" + pgrId + " ]";
//    }
    
}
