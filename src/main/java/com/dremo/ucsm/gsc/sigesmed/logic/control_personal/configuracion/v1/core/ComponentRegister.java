/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.configuracion.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.configuracion.v1.tx.ActualizarConfiguracionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.configuracion.v1.tx.AgregarConfiguracionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.configuracion.v1.tx.EliminarConfiguracionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.configuracion.v1.tx.ListarConfiguracionesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.configuracion.v1.tx.ListarRegistrosMensualesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.configuracion.v1.tx.ListarTrabajadorTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.configuracion.v1.tx.ListarTrabajadorUsuarioTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.configuracion.v1.tx.VerificarOrganizacionTx;



/**
 *
 * @author carlos
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_CONTROL_PERSONAL);        
        
        //Registrnado el Nombre del componente
        component.setName("configuracionControl");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionGET("listarConfiguraciones", ListarConfiguracionesTx.class);
        component.addTransactionGET("listarTrabajador", ListarTrabajadorTx.class);
        component.addTransactionGET("listarTrabajadorUsuario", ListarTrabajadorUsuarioTx.class);
        //listarTrabajadorUsuario
        component.addTransactionPOST("actualizarConfiguracion", ActualizarConfiguracionTx.class);
        component.addTransactionDELETE("eliminarConfiguracion", EliminarConfiguracionTx.class);
       
        component.addTransactionGET("verificarOrganizacion", VerificarOrganizacionTx.class);
        component.addTransactionPOST("agregarConfiguracion", AgregarConfiguracionTx.class);
//        component.addTransactionPUT("eliminarPlantilla", EliminarPlantillaTx.class);
        component.addTransactionGET("listarRegistrosMensuales", ListarRegistrosMensualesTx.class);
      
        
        return component;
    }
}
