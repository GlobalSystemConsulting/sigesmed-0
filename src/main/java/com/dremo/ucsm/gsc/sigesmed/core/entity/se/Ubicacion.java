/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author felipe
 */
@Entity
@Table(name = "ubicacion", schema ="administrativo")

public class Ubicacion implements Serializable {

    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ubi_id")
    private int ubiId;
    
    @Size(max = 2147483647)
    @Column(name = "ubi_cod")
    private String ubiCod;
    
    @Size(max = 2147483647)
    @Column(name = "ubi_nom")
    private String ubiNom;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;

    @Column(name = "est_reg")
    private char estReg;
    
    @Column(name = "des")
    private String des;
    
    @OneToMany(mappedBy = "ubicacion")
    private List<Organigrama> organigrama;

    public Ubicacion() {
    }

    public Ubicacion(int ubiId) {
        this.ubiId = ubiId;
    }

    public int getUbiId() {
        return ubiId;
    }

    public void setUbiId(int ubiId) {
        this.ubiId = ubiId;
    }

    public String getUbiCod() {
        return ubiCod;
    }

    public void setUbiCod(String ubiCod) {
        this.ubiCod = ubiCod;
    }

    public String getUbiNom() {
        return ubiNom;
    }

    public void setUbiNom(String ubiNom) {
        this.ubiNom = ubiNom;
    }
    
    //INICIO -usado para el catalogo
    public int getId(){
        return this.ubiId;
    }
    public void setId(int Id){
        this.ubiId=Id;
    }
    
    public String getNom() {
        return this.ubiNom;
    }
    public void setNom(String nom) {
        this.ubiNom = nom;
    }

    public String getDes() {
        return this.des;
    }
    public void setDes(String des) {
        this.des = des;
    }
    //FIN -usado para el catalogo
    
    public List<Organigrama> getOrganigrama() {
        return organigrama;
    }

    public void setOrganigrama(List<Organigrama> organigrama) {
        this.organigrama = organigrama;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public char getEstReg() {
        return estReg;
    }

    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }

    @Override
    public String toString() {
        return "com.dremo.ucsm.gsc.sigesmed.core.entity.se.Ubicacion[ ubiId=" + ubiId + " ]";
    }
    
}
