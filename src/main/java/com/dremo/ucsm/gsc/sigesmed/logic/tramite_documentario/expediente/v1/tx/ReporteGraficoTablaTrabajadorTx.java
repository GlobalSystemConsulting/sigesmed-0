/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author G-16
 */
public class ReporteGraficoTablaTrabajadorTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        FileJsonObject miGrafico = null;
        GTabla tablaOrg = null;
        JSONObject cabecera = new JSONObject();
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            cabecera.put("Fecha de Incio de Busqueda", requestData.getString("desde"));
             
            JSONArray tabla = requestData.optJSONArray("resumen");
            
            if(tabla!=null && tabla.length() > 0){
                
                float[] cols = {3f,2f,2f,2f,2f};
                tablaOrg = new GTabla(cols);

                String[] labels = {"Trabajador","N° Recibidos","N° Derivados","N° Devueltos","N° Finalizados"};
                tablaOrg.build(labels);
                for(int i = 0; i < tabla.length();i++){
                    JSONObject bo =tabla.getJSONObject(i);
                    
                    String[] fila = new String [5];
                    fila[0] = bo.getString("Responsaletr");
                    fila[1] = ""+bo.getInt("expedientestr");
                    fila[2] = ""+bo.getInt("derivadostr");
                    fila[3] = ""+bo.getInt("devueltostr");
                    fila[4] = ""+bo.getInt("finalizadostr");
                    
                    tablaOrg.processLine(fila);
                }
              
                miGrafico = new FileJsonObject( requestData.optJSONObject("grafico")  );

            }           
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo realizar el reporte, grafico iicorrecto", e.getMessage() );
        }
        //Fin        
        
        
        /*
        *  Repuesta Correcta
        */
        
        JSONObject response = new JSONObject();
        try {
            Mitext r = new Mitext();
            
                r.newLine(1);
                r.agregarTitulo("REPORTE RUTA DEL TRAMITE ");
                r.newLine(1);
                r.agregarImagen64(miGrafico.getData());
                r.newLine(1);
                r.agregarSubtitulos(cabecera);
                r.newLine(2);

                r.agregarTabla(tablaOrg);
            
            
            r.cerrarDocumento();
            response.append("reporte", r.encodeToBase64() );
        } catch (Exception ex) {
            Logger.getLogger(ReporteTx.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return WebResponse.crearWebResponseExito("el reporte se realizo",response);
        //Fin
    }
    
}


