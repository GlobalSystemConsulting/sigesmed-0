/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.asistencia.v1.core;


import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.asistencia.v1.tx.EditarJustificacionAsistenciaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.asistencia.v1.tx.EditarJustificacionInasistenciaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.asistencia.v1.tx.GuardarJustificacionAsistenciaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.asistencia.v1.tx.GuardarJustificacionInasistenciaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.asistencia.v1.tx.HoraServidorTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.asistencia.v1.tx.ListarAsistenciaPosibleToAdicionalTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.asistencia.v1.tx.ListarControlAsistenchaHoyTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.asistencia.v1.tx.ListarControlAsistenchaNextTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.asistencia.v1.tx.ListarControlAsistenchaPrevTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.asistencia.v1.tx.MarcarAsistenciaAulaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.asistencia.v1.tx.MarcarAsistenciaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.asistencia.v1.tx.RegistrarHoraAdicionalTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.asistencia.v1.tx.VerJustificacionInasistenciaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.asistencia.v1.tx.VerJustificacionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.asistencia.v1.tx.VerificarDocenteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.asistencia.v1.tx.VerificarLibroAsistenciaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.asistencia.v1.tx.VerificarTrabajadorTx;

/**
 *
 * @author carlos
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_CONTROL_PERSONAL);        
        
        //Registrnado el Nombre del componente
        component.setName("controlAsistencia");
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        
        component.addTransactionGET("horaServidor", HoraServidorTx.class);
        component.addTransactionGET("marcarAsistencia", MarcarAsistenciaTx.class);
        component.addTransactionGET("marcarAsistenciaAula", MarcarAsistenciaAulaTx.class);
        component.addTransactionGET("verificarLibroAsistencia", VerificarLibroAsistenciaTx.class);
        component.addTransactionGET("verificarTrabajador", VerificarTrabajadorTx.class);
        component.addTransactionGET("verificarDocente", VerificarDocenteTx.class);
        component.addTransactionGET("listarControlAsistenchaHoy", ListarControlAsistenchaHoyTx.class);
        component.addTransactionGET("listarControlAsistenchaNext", ListarControlAsistenchaNextTx.class);
        component.addTransactionGET("listarControlAsistenchaPrev", ListarControlAsistenchaPrevTx.class);
        component.addTransactionGET("verJustificacion", VerJustificacionTx.class);
        component.addTransactionGET("verJustificacionInasistencia", VerJustificacionInasistenciaTx.class);
        component.addTransactionPOST("guardarJustificacionInasistencia", GuardarJustificacionInasistenciaTx.class);
        component.addTransactionPOST("editarJustificacionInasistencia", EditarJustificacionInasistenciaTx.class);
        component.addTransactionPOST("guardarJustificacionAsistencia", GuardarJustificacionAsistenciaTx.class);
        component.addTransactionPOST("editarJustificacionAsistencia", EditarJustificacionAsistenciaTx.class);//no se usa
        component.addTransactionGET("listarAsistenciaPosibleToAdicional", ListarAsistenciaPosibleToAdicionalTx.class);
        //registrarHoraAdicional
        component.addTransactionPOST("registrarHoraAdicional", RegistrarHoraAdicionalTx.class);
//        
//        component.addTransactionPUT("editarDiaEspecial", EditarDiaEspecialTx.class);
// //       
//    //    component.addTransactionGET("listarCalendario", ListarCalendarioTx.class);
//        component.addTransactionPOST("nuevoDiaEspecial", NuevoDiaEspecialTx.class);
//        component.addTransactionDELETE("eliminarDiaEspecial", EliminarDiaEspecialTx.class);
////        component.addTransactionGET("buscarPersona", BuscarPersonaTx.class);
        
        return component;
    }
}
