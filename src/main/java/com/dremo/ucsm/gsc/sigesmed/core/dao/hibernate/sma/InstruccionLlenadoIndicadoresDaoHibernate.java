/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.InstruccionLlenadoIndicadoresDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.InstruccionLlenadoIndicadores;
import org.hibernate.Session;

public class InstruccionLlenadoIndicadoresDaoHibernate extends GenericDaoHibernate<InstruccionLlenadoIndicadores> implements InstruccionLlenadoIndicadoresDao{

    @Override
    public InstruccionLlenadoIndicadores buscarPorId(int insLleIndId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        InstruccionLlenadoIndicadores ili = (InstruccionLlenadoIndicadores)session.get(InstruccionLlenadoIndicadores.class, insLleIndId);
        session.close();
        return ili;
    }
    
}
