/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.organizacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author angel
 */
public class BuscarOrganizacionPadreTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        int organizacionID = 0;
        int orgPadre = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            organizacionID = requestData.getInt("organizacionid");
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("NO se puedo Buscar datos de entrada incorrectos"); //, e.getMessage() );
        }
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        OrganizacionDao moduloDao = (OrganizacionDao)FactoryDao.buildDao("OrganizacionDao");
        try{
            orgPadre = moduloDao.buscarIdOrganizacionYPadre(organizacionID);
        
        }catch(Exception e){
            System.out.println("No se pudo Obtener la ORGANIZACION padre - back\n"+e);
            return WebResponse.crearWebResponseError("NO SE CUENTA CON ORGANIZACION PADRE", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        if(orgPadre != 0){
            JSONObject oResponse = new JSONObject();
            oResponse.put("orgPadre",orgPadre );
             return WebResponse.crearWebResponseExito("se obtuvo la OrgPad Correctamente ",oResponse);
        }
        return WebResponse.crearWebResponseError("No se obtuvo la OrgPad Correctamente");
        //Fin
    }
    
}
