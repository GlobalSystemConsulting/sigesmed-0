/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

@Entity
@Table(name = "valoracion_indicador", schema="pedagogico")
public class ValoracionIndicador implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "val_ind_id")
    private Integer valIndId;
    
    @Size(max = 2147483647)
    @Column(name = "val_ind_nom")
    private String valIndNom;
    
    @Size(max = 2147483647)
    @Column(name = "val_ind_des")
    private String valIndDes;
    
    @Column(name = "val_ind_pun")
    private Integer valIndPun;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.DATE)
    private Date fecMod;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Size(max = 1)
    @Column(name = "est_reg")
    private String estReg;
    
    @JoinColumn(name = "pla_fic_mon_id", referencedColumnName = "pla_fic_mon_id")
    @ManyToOne
    private PlantillaFichaMonitoreo plantillaFM;

    public ValoracionIndicador() {
    }

    public ValoracionIndicador(Integer valIndId) {
        this.valIndId = valIndId;
    }

    public Integer getValIndId() {
        return valIndId;
    }

    public void setValIndId(Integer valIndId) {
        this.valIndId = valIndId;
    }

    public String getValIndNom() {
        return valIndNom;
    }

    public void setValIndNom(String valIndNom) {
        this.valIndNom = valIndNom;
    }
    
    public String getValIndDes() {
        return valIndDes;
    }

    public void setValIndDes(String valIndDes) {
        this.valIndDes = valIndDes;
    }

    public Integer getValIndPun() {
        return valIndPun;
    }

    public void setValIndPun(Integer valIndPun) {
        this.valIndPun = valIndPun;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }
    
    public PlantillaFichaMonitoreo getPlantillaFM() {
        return plantillaFM;
    }

    public void setPlantillaFM(PlantillaFichaMonitoreo plantillaFM) {
        this.plantillaFM = plantillaFM;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (valIndId != null ? valIndId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ValoracionIndicador)) {
            return false;
        }
        ValoracionIndicador other = (ValoracionIndicador) object;
        if ((this.valIndId == null && other.valIndId != null) || (this.valIndId != null && !this.valIndId.equals(other.valIndId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.ValoracionIndicador[ valIndId=" + valIndId + " ]";
    }
    
}
