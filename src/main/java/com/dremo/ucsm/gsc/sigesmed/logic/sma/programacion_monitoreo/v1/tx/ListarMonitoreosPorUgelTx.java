/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.programacion_monitoreo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.MonitoreoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.Monitoreo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class ListarMonitoreosPorUgelTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ListarMonitoreosPorUgelTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        JSONObject requestData = (JSONObject)wr.getData();
        Integer ugelId = requestData.getInt("ugelId");
        
        DateFormat sdo = new SimpleDateFormat("dd-MM-yyyy");
                
        List<Monitoreo> monitoreos = null;
        MonitoreoDao monitoreoDao = (MonitoreoDao)FactoryDao.buildDao("sma.MonitoreoDao");
        
        try{
            monitoreos = monitoreoDao.listarMonitoreosxUgel(ugelId);
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar monitoreos",e);
            System.out.println("No se pudo listar los monitoreos\n"+e);
            return WebResponse.crearWebResponseError("No se pudo listar los monitoreos", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(Monitoreo m:monitoreos ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("monId", m.getMniId());
            oResponse.put("codMon", m.getMniCod());
            oResponse.put("anioMon", m.getMniYy());
            oResponse.put("nomMon", m.getMniNom());
            oResponse.put("idIE", m.getOrganizacion().getOrgId());
            oResponse.put("nomIE", m.getOrganizacion().getNom());
            oResponse.put("nivIE", m.getOrganizacion().getNivDes());
            oResponse.put("nombreURL", m.getMniUrl());
            oResponse.put("fecReg", sdo.format(m.getFecReg()));
            oResponse.put("numEsp", m.getNumEsp());
            oResponse.put("numDoc", m.getNumDoc());
            oResponse.put("estMon", m.getEstMon());
            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Las exposiciones fueron listadas exitosamente", miArray);
    }
    
}
