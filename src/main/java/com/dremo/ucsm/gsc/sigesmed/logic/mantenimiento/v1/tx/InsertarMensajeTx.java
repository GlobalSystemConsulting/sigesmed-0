/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.mantenimiento.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mnt.MensajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mnt.PeticionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mnt.SoporteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.FuncionSistema;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mnt.Mensaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mnt.Peticion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mnt.Soporte;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class InsertarMensajeTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        Mensaje m=null;Peticion p=null; Soporte s=null;
        MensajeDao mensajeDao=(MensajeDao)FactoryDao.buildDao("mnt.MensajeDao");
        PeticionDao petDao=(PeticionDao)FactoryDao.buildDao("mnt.PeticionDao");
        SoporteDao sopDao=(SoporteDao)FactoryDao.buildDao("mnt.SoporteDao");
        char menTipMen=' ';
        FileJsonObject miF=null;
        boolean flagimg=false;
        String nombre="";
        try{
            JSONObject requestData=(JSONObject)wr.getData();
            String menAsu=requestData.getString("asunto");
            String menCtn=requestData.getString("descripcion");
            SimpleDateFormat sdf=new SimpleDateFormat("dd/M/yyyy");
            Date menFecEnv=new Date();
            Date menFecOcu=null;
            boolean flagOcurrencia=requestData.getBoolean("flagOcurrencia");
            if(flagOcurrencia){
                menFecOcu=sdf.parse(requestData.getString("fechaOcurrencia"));
            }
            
            
            
            menTipMen=requestData.getString("tipoMensaje").charAt(0);//Peticion o Sporte
            char menEst='S';//Leido o no Leido
            flagimg=requestData.getBoolean("flagimg");
            if(flagimg){
                nombre=requestData.getString("nombreReal");
                JSONObject imagen=requestData.optJSONObject("imagen");
                
                if(imagen!=null && imagen.length()>0){
                   miF=new FileJsonObject(imagen,nombre);
                    miF.getName();
                }
            }
            
            
            
            
            
            //Remitente            
            UsuarioSession usuAut=new UsuarioSession((requestData.getInt("remitenteID")));
            
     
            //Destinatario
            UsuarioSession usuDest=new UsuarioSession(requestData.getInt("destinatarioID"));
            char menEstReg='A';
            if(flagOcurrencia)
                m=new Mensaje(0, menAsu, menCtn, menFecEnv,menFecOcu, menTipMen, menEst,nombre, usuAut, usuDest, menEstReg);
            else
                m=new Mensaje(0, menAsu, menCtn, menFecEnv, menTipMen, menEst,nombre, usuAut, usuDest, menEstReg);

            
            
            
            if(menTipMen=='P'){
                
                FuncionSistema funcion=new FuncionSistema(requestData.getInt("funcionID"));
                char petTip=requestData.getString("subTipoMensaje").charAt(0);
                char petEstReg='A';
                p=new Peticion(0,m,funcion,petTip,petEstReg);
            }else if(menTipMen=='S'){
                FuncionSistema funcion=new FuncionSistema(requestData.getInt("funcionID"));
                char sopTip=requestData.getString("subTipoMensaje").charAt(0);
                s=new Soporte(0,funcion,m);
                s.setSopTip(sopTip);;
            }
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se puedo registrar el mensaje, datos incorrectos", e.getMessage());
        }
        
        /*
        *Parte de la BD
        */
        
        try{
            mensajeDao.insert(m);
            if(menTipMen=='P')
                petDao.insert(p);
            else if(menTipMen=='S')
                sopDao.insert(s);
            if(flagimg)
                BuildFile.buildFromBase64("mensajes", nombre, miF.getData());
        }catch(Exception e){
            System.out.println("No se puede registrar\n" + e);
            return WebResponse.crearWebResponseError("No se pudo registrar", e.getMessage());
        }
        JSONObject oResponse=new JSONObject();
        oResponse.put("coddoc",m.getMensajeID());
        return WebResponse.crearWebResponseExito("El envio de mensaje se realizo correctamente",oResponse);
    }
    
}
