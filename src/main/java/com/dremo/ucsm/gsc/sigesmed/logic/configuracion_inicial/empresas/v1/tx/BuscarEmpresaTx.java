/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.empresas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.EmpresaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.LibroAsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Empresa;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

/**
 *
 * @author carlos
 */
public class BuscarEmpresaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
       String ruc="";
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            ruc=requestData.getString("ruc");
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo verificar los datos", e.getMessage() );
        }

        
        EmpresaDao empresaDao = (EmpresaDao)FactoryDao.buildDao("EmpresaDao");
        Empresa empresa=null;
        try{
            empresa=empresaDao.buscarEmpresa(ruc);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo buscar por RUC ", e.getMessage() );
        }
        
        if(empresa==null)
            return WebResponse.crearWebResponseError("No se encontro el RUC ");
        JSONObject oRes=new JSONObject();
        
        oRes.put("empresaID", empresa.getEmpId());
        oRes.put("ruc", empresa.getRuc());
        oRes.put("razonSocial", empresa.getRazonSocial());
        oRes.put("estado", empresa.getEstReg()+"");
        oRes.put("telefono",empresa.getNum());
        oRes.put("pagWeb", empresa.getWeb());
        oRes.put("existe", true);
        
        return WebResponse.crearWebResponseExito("Se busco correctamente",oRes);        
        //Fin
    }
    
}

