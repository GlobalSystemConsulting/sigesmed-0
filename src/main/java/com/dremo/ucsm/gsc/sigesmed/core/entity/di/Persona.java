/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dremo.ucsm.gsc.sigesmed.core.entity.di;

import java.io.Serializable;
import java.util.List;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Administrador
 */
//@Entity
@Entity(name = "com.dremo.ucsm.gsm.sigesmed.core.entity.di.Persona")
@Table(name = "persona", schema="pedagogico")
//@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "Persona.findAll", query = "SELECT p FROM Persona p"),
//    @NamedQuery(name = "Persona.findByPerId", query = "SELECT p FROM Persona p WHERE p.perId = :perId"),
//    @NamedQuery(name = "Persona.findByPerCod", query = "SELECT p FROM Persona p WHERE p.perCod = :perCod"),
//    @NamedQuery(name = "Persona.findByApeMat", query = "SELECT p FROM Persona p WHERE p.apeMat = :apeMat"),
//    @NamedQuery(name = "Persona.findByApePat", query = "SELECT p FROM Persona p WHERE p.apePat = :apePat"),
//    @NamedQuery(name = "Persona.findByNom", query = "SELECT p FROM Persona p WHERE p.nom = :nom"),
//    @NamedQuery(name = "Persona.findByFecNac", query = "SELECT p FROM Persona p WHERE p.fecNac = :fecNac"),
//    @NamedQuery(name = "Persona.findByDni", query = "SELECT p FROM Persona p WHERE p.dni = :dni"),
//    @NamedQuery(name = "Persona.findByEmail", query = "SELECT p FROM Persona p WHERE p.email = :email"),
//    @NamedQuery(name = "Persona.findByNum1", query = "SELECT p FROM Persona p WHERE p.num1 = :num1"),
//    @NamedQuery(name = "Persona.findByNum2", query = "SELECT p FROM Persona p WHERE p.num2 = :num2"),
//    @NamedQuery(name = "Persona.findByFij", query = "SELECT p FROM Persona p WHERE p.fij = :fij")})
public class Persona implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "per_id")
    private Long perId;
    @Column(name = "per_cod")
    private String perCod;
    @Basic(optional = false)
    @Column(name = "ape_mat")
    private String apeMat;
    @Basic(optional = false)
    @Column(name = "ape_pat")
    private String apePat;
    @Basic(optional = false)
    @Column(name = "nom")
    private String nom;
    @Basic(optional = false)
    @Column(name = "fec_nac")
    @Temporal(TemporalType.DATE)
    private Date fecNac = new Date();
    @Basic(optional = false)
    @Column(name = "dni")
    private String dni;
    @Column(name = "email")
    private String email;
    @Column(name = "num_1")
    private String num1;
    @Column(name = "num_2")
    private String num2;
    @Column(name = "fij")
    private String fij;
    @Column(name = "per_dir")
    private String perDir;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "persona")
//    private List<Estudiante> estudianteList = new ArrayList<>();
    
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "persona")
//    private List<Trabajador> trabajos = new ArrayList<>();
    
//    @OneToOne(cascade = CascadeType.ALL, mappedBy="persona")    
//    private Apoderado apoderado;
    
    public Persona() {
    }

    public Persona(Long perId) {
        this.perId = perId;
    }
    
    public Persona(String apeMat, String apePat, String nom, String dni, String fij, String perDir) {
        
        this.apeMat = apeMat;
        this.apePat = apePat;
        this.nom = nom;        
        this.dni = dni;        
        this.fij = fij;
        this.perDir = perDir;
    }
    
    
    public Persona(String perCod, String apeMat, String apePat, String nom, Date fecNac, String dni) {
        this.perCod = perCod;
        this.apeMat = apeMat;
        this.apePat = apePat;
        this.nom = nom;
        this.fecNac = fecNac;
        this.dni = dni;
    }
    
    public Persona(Long perId, String apeMat, String apePat, String nom, Date fecNac, String dni) {
        this.perId = perId;
        this.apeMat = apeMat;
        this.apePat = apePat;
        this.nom = nom;
        this.fecNac = fecNac;
        this.dni = dni;
    }

    public Long getPerId() {
        return perId;
    }

    public void setPerId(Long perId) {
        this.perId = perId;
    }

    public String getPerCod() {
        return perCod;
    }

    public void setPerCod(String perCod) {
        this.perCod = perCod;
    }

    public String getApeMat() {
        return apeMat;
    }

    public void setApeMat(String apeMat) {
        this.apeMat = apeMat;
    }

    public String getApePat() {
        return apePat;
    }

    public void setApePat(String apePat) {
        this.apePat = apePat;
    }
    
//    public String getApe() {
//        return this.apePat + " " + this.apeMat;
//    }
    
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getFecNac() {
        return fecNac;
    }

    public void setFecNac(Date fecNac) {
        this.fecNac = fecNac;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNum1() {
        return num1;
    }

    public void setNum1(String num1) {
        this.num1 = num1;
    }

    public String getNum2() {
        return num2;
    }

    public void setNum2(String num2) {
        this.num2 = num2;
    }

    public String getFij() {
        return fij;
    }

    public void setFij(String fij) {
        this.fij = fij;
    }

//    @XmlTransient
//    public List<Estudiante> getEstudianteList() {
//        return estudianteList;
//    }
//
//    public void setEstudianteList(List<Estudiante> estudianteList) {
//        this.estudianteList = estudianteList;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (perId != null ? perId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Persona)) {
            return false;
        }
        Persona other = (Persona) object;
        if ((this.perId == null && other.perId != null) || (this.perId != null && !this.perId.equals(other.perId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.Persona[ perId=" + perId + " ]";
    }

//    public List<Trabajador> getTrabajos() {
//        return trabajos;
//    }
//
//    public void setTrabajos(List<Trabajador> trabajos) {
//        this.trabajos = trabajos;
//    }

//    public Apoderado getApoderado() {
//        return apoderado;
//    }
//
//    public void setApoderado(Apoderado apoderado) {
//        this.apoderado = apoderado;
//    }

    public String getPerDir() {
        return perDir;
    }

    public void setPerDir(String perDir) {
        this.perDir = perDir;
    }
    
}
