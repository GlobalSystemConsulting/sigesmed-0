/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma;

import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica.DocenteCarpetaPedagogica;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 *
 * @author gscadmin
 */
@Entity
@Table(name = "docente", schema = "pedagogico")

public class Docente implements Serializable {
    
    @Id
    @GeneratedValue(generator = "docenteKeyGenerator")
    @org.hibernate.annotations.GenericGenerator(
            name = "docenteKeyGenerator",
            strategy = "foreign",
            parameters = @org.hibernate.annotations.Parameter(
                    name = "property",value = "persona"
            )
    )
    private int doc_id;
    
    @Basic(optional = false)
    @Column(name = "cod_mod")
    private String codMod;
    
    @Column(name = "aut_ess")
    private String autEss;
    
    @Column(name = "reg_lab")
    private String regLab;
    
    @Column(name = "reg_pen")
    private String regPen;
    
    @Column(name = "num_col")
    private String numCol;
    
    @Column(name = "esp")
    private String esp;
    
    @Column(name = "niv")
    private Integer niv;
    
    @OneToOne(fetch = FetchType.LAZY,optional = false)
    @PrimaryKeyJoinColumn
    private Persona persona;
        
    @OneToMany(mappedBy = "docente", fetch = FetchType.LAZY)
    private List<DocenteCarpetaPedagogica> docentesCarPed;

    public Docente() {
    }

    public Docente(Integer doc_id) {
        this.doc_id = doc_id;
    }

    public Docente(Integer doc_id, String codMod) {
        this.doc_id = doc_id;
        this.codMod = codMod;
    }

    public Integer getDocId() {
        return doc_id;
    }

    public void setDocId(Integer docId) {
        this.doc_id = docId;
    }

    public String getCodMod() {
        return codMod;
    }

    public void setCodMod(String codMod) {
        this.codMod = codMod;
    }

    public String getAutEss() {
        return autEss;
    }

    public void setAutEss(String autEss) {
        this.autEss = autEss;
    }

    public String getRegLab() {
        return regLab;
    }

    public void setRegLab(String regLab) {
        this.regLab = regLab;
    }

    public String getRegPen() {
        return regPen;
    }

    public void setRegPen(String regPen) {
        this.regPen = regPen;
    }

    public String getNumCol() {
        return numCol;
    }

    public void setNumCol(String numCol) {
        this.numCol = numCol;
    }

    public String getEsp() {
        return esp;
    }

    public void setEsp(String esp) {
        this.esp = esp;
    }

    public Integer getNiv() {
        return niv;
    }

    public void setNiv(Integer niv) {
        this.niv = niv;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }
    
    public List<DocenteCarpetaPedagogica> getDocentesCarPed() {
        return docentesCarPed;
    }

    public void setDocentesCarPed(List<DocenteCarpetaPedagogica> docentesCarPed) {
        this.docentesCarPed = docentesCarPed;
    }
}
