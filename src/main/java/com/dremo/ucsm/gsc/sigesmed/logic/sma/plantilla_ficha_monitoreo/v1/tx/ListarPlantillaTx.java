/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.plantilla_ficha_monitoreo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.PlantillaFichaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.CompromisosGestionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.CompromisosGestion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.Items;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.PlantillaFicha;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ListarPlantillaTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {        
        /*
        *  Parte para la operacion en la Base de Datos
        */               
        JSONObject requestData = (JSONObject)wr.getData();
        int plaId = requestData.getInt("plaId");
                
        PlantillaFicha plantilla = null;
        PlantillaFichaDao plantillasDao = (PlantillaFichaDao)FactoryDao.buildDao("sma.PlantillaFichaDao");        
        
        try{
            plantilla = plantillasDao.obtenerPlantilla(plaId);
        }catch(Exception e){            
            return WebResponse.crearWebResponseError("No se encontro la plantilla", e.getMessage() );
        }

        JSONArray oplantilla = new JSONArray();        
        List<CompromisosGestion> grupos = plantilla.getCompromisosGestionList();
        CompromisosGestionDao grupoDao = (CompromisosGestionDao)FactoryDao.buildDao("sma.CompromisosGestionDao");
                
        for(CompromisosGestion g:grupos){
//            int gruId = g.getPgrId();
            JSONObject ogrupo = new JSONObject();
            ogrupo.put("comId",g.getCgeId());
            ogrupo.put("comNom",g.getCgeDes());
            
            JSONArray oindicadores = new JSONArray();        
            List<Items> indicadores = grupoDao.obtenerItems(g.getCgeId());            
            for(Items i:indicadores){
                JSONObject oindicador = new JSONObject();
                oindicador.put("iteId",i.getIteId());
                oindicador.put("iteNom",i.getIteDes());
                oindicadores.put(oindicador);
            }
            ogrupo.put("items",oindicadores);            
            oplantilla.put(ogrupo);
            
        }
        
        JSONObject cabecera = new JSONObject();
        
        cabecera.put("plaide", plantilla.getPlfId());
        cabecera.put("codigo", plantilla.getPlfCod());
        cabecera.put("nombre", plantilla.getPlfNom());
        
        oplantilla.put(cabecera);
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",oplantilla);        
                
    }    
}
