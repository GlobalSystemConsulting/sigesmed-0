package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi;

import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.ParientesMMI;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

public class ParienteEstudianteDaoHibernate extends GenericMMIDaoHibernate<ParientesMMI> {

    public List<ParientesMMI> find4Estudiante(long estCod) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<ParientesMMI> domicilios = null;
        String hql;
        Query query;
        try {
            hql = "FROM ParientesMMI par WHERE par.estReg != 'E' and par.id.perId = :hdlEstCod";
            query = session.createQuery(hql);
            query.setLong("hdlEstCod", estCod);
            domicilios = query.list();
        } catch (Exception ex) {
            System.out.println("Error al buscar Parientes con codigo estudiante: " + estCod);
            throw ex;
        } finally {
            session.close();
        }
        return domicilios;
    }
    
    public List<ParientesMMI> parientesByEstIdOrdered(long estId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<ParientesMMI> domicilios = null;
        String hql;
        Query query;
        try {
            hql = "FROM ParientesMMI par WHERE par.estReg != 'E' and par.id.perId = :hdlEstCod "
                    + "ORDER BY par.fecMod DESC";
            query = session.createQuery(hql);
            query.setLong("hdlEstCod", estId);
            domicilios = query.list();
        } catch (Exception ex) {
            System.out.println("Error al buscar Parientes con codigo estudiante: " + estId);
            throw ex;
        } finally {
            session.close();
        }
        return domicilios;
    }

    public long llavePariente(long perId) {
        Class dato = ParientesMMI.class;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Object result = null;
        String hql;
        Query query;
        try {
            hql = "SELECT MAX(par.id.parId) FROM " + dato.getName() + " par WHERE par.id.perId = :hqlPerId";
            query = session.createQuery(hql);
            query.setLong("hqlPerId", perId);
            query.setMaxResults(1);
            result = query.uniqueResult();
        } catch (Exception ex) {
            System.out.println("No se puede ejecutar llave Pariente por Persona " + dato.getClass().getName() + ".\n" + ex.getMessage());
            throw ex;
        } finally {
            session.close();
        }
        if (result == null) {
            return (long) 0;
        } else {
            return Long.parseLong(result.toString());
        }
    }

}
