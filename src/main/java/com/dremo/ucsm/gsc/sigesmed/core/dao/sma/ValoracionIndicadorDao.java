/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.sma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.ValoracionIndicador;
import java.util.List;

public interface ValoracionIndicadorDao extends GenericDao<ValoracionIndicador>{
    List<ValoracionIndicador> listarPorPlaId(int plaId);
    ValoracionIndicador buscarPorId(int valIndId);
    
}
