/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author felipe
 */
@Entity
@Table(name = "datos_organigrama", schema="administrativo")

public class DatosOrganigrama implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "dat_orgi_id")
    private Integer datOrgiId;
    @Size(max = 2147483647)
    @Column(name = "cod_dat_orgi")
    private String codDatOrgi;
    @Size(max = 2147483647)
    @Column(name = "nom_dat_orgi")
    private String nomDatOrgi;
    @Size(max = 2147483647)
    @Column(name = "des_dat_orgi")
    private String desDatOrgi;
    @Column(name = "anio_dat_orgi")
    private Integer anioDatOrgi;
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "est_reg")
    private Character estReg;
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy="datosOrganigramas")
    List<TipoOrganigrama> tiposOrganigramas;

    public DatosOrganigrama() {
    }

    public DatosOrganigrama(Integer datOrgiId) {
        this.datOrgiId = datOrgiId;
    }

    public Integer getDatOrgiId() {
        return datOrgiId;
    }

    public void setDatOrgiId(Integer datOrgiId) {
        this.datOrgiId = datOrgiId;
    }

    public String getCodDatOrgi() {
        return codDatOrgi;
    }

    public void setCodDatOrgi(String codDatOrgi) {
        this.codDatOrgi = codDatOrgi;
    }

    public String getNomDatOrgi() {
        return nomDatOrgi;
    }

    public void setNomDatOrgi(String nomDatOrgi) {
        this.nomDatOrgi = nomDatOrgi;
    }

    public String getDesDatOrgi() {
        return desDatOrgi;
    }

    public void setDesDatOrgi(String desDatOrgi) {
        this.desDatOrgi = desDatOrgi;
    }

    public Integer getAnioDatOrgi() {
        return anioDatOrgi;
    }

    public void setAnioDatOrgi(Integer anioDatOrgi) {
        this.anioDatOrgi = anioDatOrgi;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public List<TipoOrganigrama> getTiposOrganigramas() {
        return tiposOrganigramas;
    }

    public void setTiposOrganigramas(List<TipoOrganigrama> tiposOrganigramas) {
        this.tiposOrganigramas = tiposOrganigramas;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (datOrgiId != null ? datOrgiId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DatosOrganigrama)) {
            return false;
        }
        DatosOrganigrama other = (DatosOrganigrama) object;
        if ((this.datOrgiId == null && other.datOrgiId != null) || (this.datOrgiId != null && !this.datOrgiId.equals(other.datOrgiId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DatosOrganigrama{" + "datOrgiId=" + datOrgiId + ", codDatOrgi=" + codDatOrgi + ", nomDatOrgi=" + nomDatOrgi + ", desDatOrgi=" + desDatOrgi + ", anioDatOrgi=" + anioDatOrgi + ", fecMod=" + fecMod + ", usuMod=" + usuMod + ", estReg=" + estReg + '}';
    }

    
}
