/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.scp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author harold
 */

@Entity
@Table(name="salidas_det", schema="administrativo")
public class SalidasDetalle {
    
    @Id
    @Column(name="sal_det_id" , unique=true, nullable=false)
    @SequenceGenerator(name="secuencia_inv_tra",sequenceName="administrativo.salidas_det_sal_det_id_seq")
    @GeneratedValue(generator="secuencia_inv_tra")
    private int sal_det_id;
    
    @Column(name="cod_bie")
    private int cod_bie;
    
    @Column(name="salidas_id")
    private int salidas_id;
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cod_bie" , insertable=false , updatable=false)
    private BienesMuebles bm;

    public SalidasDetalle() {
    }    
    
    public SalidasDetalle(int sal_det_id, int cod_bie, int salidas_id) {
        this.sal_det_id = sal_det_id;
        this.cod_bie = cod_bie;
        this.salidas_id = salidas_id;        
    }

    public void setSal_det_id(int sal_det_id) {
        this.sal_det_id = sal_det_id;
    }

    public void setCod_bie(int cod_bie) {
        this.cod_bie = cod_bie;
    }

    public void setSalidas_id(int salidas_id) {
        this.salidas_id = salidas_id;
    }

    public void setBm(BienesMuebles bm) {
        this.bm = bm;
    }

    public int getSal_det_id() {
        return sal_det_id;
    }

    public int getCod_bie() {
        return cod_bie;
    }

    public int getSalidas_id() {
        return salidas_id;
    }

    public BienesMuebles getBm() {
        return bm;
    }    
    
}
