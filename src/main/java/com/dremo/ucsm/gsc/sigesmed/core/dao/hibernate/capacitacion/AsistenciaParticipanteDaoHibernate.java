package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.AsistenciaParticipanteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.AsistenciaParticipante;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class AsistenciaParticipanteDaoHibernate extends GenericDaoHibernate<AsistenciaParticipante> implements AsistenciaParticipanteDao {

    private static final Logger logger = Logger.getLogger(AsistenciaParticipanteDaoHibernate.class.getName());

    @Override
    public boolean existenRegistros(int codHor) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            Criteria query = session.createCriteria(AsistenciaParticipante.class)
                    .createCriteria("horario")
                    .add(Restrictions.eq("horSedCapId", codHor))
                    .setProjection(Projections.rowCount());

            return (((Long) query.uniqueResult()).intValue() > 0);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "existenRegistros", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public List<AsistenciaParticipante> buscar(int perId, int codHor, Date tiempo) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            Criteria query = session.createCriteria(AsistenciaParticipante.class)
                    .add(Restrictions.eq("fec", tiempo))
                    .add(Restrictions.eq("estReg", 'G'))
                    .createAlias("horario", "hor")
                    .createAlias("persona", "per")
                    .add(Restrictions.eq("per.perId", perId))
                    .add(Restrictions.eq("hor.horSedCapId", codHor))
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscar", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public List<AsistenciaParticipante> buscarParticipantes(int codHor, Date tiempo) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            Criteria query = session.createCriteria(AsistenciaParticipante.class)
                    .add(Restrictions.eq("fec", tiempo))
                    .createAlias("horario", "hor")
                    .add(Restrictions.eq("hor.horSedCapId", codHor))
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarParticipantes", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public AsistenciaParticipante buscarPorId(int codAsi) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            return (AsistenciaParticipante) session.get(AsistenciaParticipante.class, codAsi);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarPorId", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public List<AsistenciaParticipante> buscarPorHorarioParticipante(int perId, int codHor) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            Criteria query = session.createCriteria(AsistenciaParticipante.class)
                    .createAlias("horario", "hor")
                    .createAlias("persona", "per")
                    .add(Restrictions.eq("per.perId", perId))
                    .add(Restrictions.eq("hor.horSedCapId", codHor))
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarPorHorarioParticipante", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public List<AsistenciaParticipante> buscarPorSede(int codSed) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            Criteria query = session.createCriteria(AsistenciaParticipante.class)
                    .add(Restrictions.eq("estReg", 'P'))
                    .createAlias("horario", "hor")
                    .createAlias("hor.sedeCapacitacion", "sed")
                    .add(Restrictions.eq("sed.sedCapId", codSed))
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarPorSede", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public int totalAsistencias(int codSed) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            String hql = "SELECT p.perId, COUNT(p.perId) FROM AsistenciaParticipante a "
                    + "INNER JOIN a.horario k "
                    + "INNER JOIN a.persona p "
                    + "WHERE k.horSedCapId IN "
                    + "(SELECT h.horSedCapId FROM HorarioSedeCapacitacion h "
                    + "INNER JOIN h.sedeCapacitacion s "
                    + "WHERE s.sedCapId = :sedCapId) "
                    + "GROUP BY p.perId)";

            Query query = session.createQuery(hql);
            query.setParameter("sedCapId", codSed);

            List<Object[]> rows = query.list();
            return (rows.isEmpty()) ? 0 : Integer.parseInt(rows.get(0)[1].toString());
        } catch (Exception e) {
            logger.log(Level.SEVERE, "totalEvaluaciones", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public int obtenerParaCertificacion(int codSed, int perId, Character tip) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            Criteria query = session.createCriteria(AsistenciaParticipante.class)
                    .add(Restrictions.eq("estReg", tip))
                    .createAlias("horario", "hor")
                    .createAlias("hor.sedeCapacitacion", "sed")
                    .createAlias("persona", "per")
                    .add(Restrictions.eq("per.perId", perId))
                    .add(Restrictions.eq("sed.sedCapId", codSed))
                    .setProjection(Projections.rowCount())
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

            return ((Long) query.uniqueResult()).intValue();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "obtenerParaCertificacion", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public Map<Character, Integer> contarReporte(int codSed, int perId) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            String hql = "SELECT a.estReg, COUNT(a.estReg) FROM AsistenciaParticipante a "
                    + "JOIN a.horario k "
                    + "JOIN a.persona p "
                    + "JOIN k.sedeCapacitacion s "
                    + "WHERE s.sedCapId = :sedCapId AND p.perId = :perId "
                    + "GROUP BY a.estReg";

            Query query = session.createQuery(hql);
            query.setParameter("sedCapId", codSed);
            query.setParameter("perId", perId);

            List<Object[]> rows = query.list();
            Map<Character, Integer> result = new HashMap<>();

            for (Object[] row : rows) {
                result.put(row[0].toString().charAt(0), Integer.parseInt(row[1].toString()));
            }

            return result;
        } catch (Exception e) {
            logger.log(Level.SEVERE, "contarReporte", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public List<Date> obtenerFechas(int codSed, int perId) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            String hql = "SELECT a.fec FROM AsistenciaParticipante a "
                    + "JOIN a.horario k "
                    + "JOIN a.persona p "
                    + "JOIN k.sedeCapacitacion s "
                    + "WHERE s.sedCapId = :sedCapId AND p.perId = :perId "
                    + "GROUP BY a.fec "
                    + "ORDER BY a.fec";

            Query query = session.createQuery(hql);
            query.setParameter("sedCapId", codSed);
            query.setParameter("perId", perId);

            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "obtenerFechas", e);
            throw e;
        } finally {
            session.close();
        }
    }
    
    @Override
    public String contarHoras(int codSed, int perId) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            String hql = "SELECT CAST(SUM(k.turFin - k.turIni) AS string) FROM AsistenciaParticipante a "
                    + "JOIN a.horario k "
                    + "JOIN a.persona p "
                    + "JOIN k.sedeCapacitacion s "
                    + "WHERE s.sedCapId = :sedCapId AND p.perId = :perId";

            Query query = session.createQuery(hql);
            query.setParameter("sedCapId", codSed);
            query.setParameter("perId", perId);

            return (String) query.uniqueResult();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "contarHoras", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public Map<String, Character> obtenerRegistros(int codSed, int perId, Date fecReg) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            String hql = "SELECT k.dia, k.turIni, k.turFin, a.estReg FROM AsistenciaParticipante a "
                    + "JOIN a.horario k "
                    + "JOIN a.persona p "
                    + "JOIN k.sedeCapacitacion s "
                    + "WHERE s.sedCapId = :sedCapId AND p.perId = :perId AND a.fec = :fecReg";
              

            Query query = session.createQuery(hql);
            query.setParameter("sedCapId", codSed);
            query.setParameter("perId", perId);
            query.setParameter("fecReg", fecReg);

            List<Object[]> rows = query.list();
            Map<String, Character> result = new HashMap<>();

            for (Object[] row : rows)
                result.put(getDate(Integer.parseInt(row[0].toString())) + " " + row[1] + " - " + row[2], row[3].toString().charAt(0));
            
            return result;
        } catch (Exception e) {
            logger.log(Level.SEVERE, "obtenerRegistros", e);
            throw e;
        } finally {
            session.close();
        }
    }
    
    private String getDate(int day) {
        String name = "";

        switch (day) {
            case 1:
                name = "Domingo";
                break;
            case 2:
                name = "Lunes";
                break;
            case 3:
                name = "Martes";
                break;
            case 4:
                name = "Miércoles";
                break;
            case 5:
                name = "Jueves";
                break;
            case 6:
                name = "Viernes";
                break;
            case 7:
                name = "Sábado";
                break;
        }

        return name;
    }
}
