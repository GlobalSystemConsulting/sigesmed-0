/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.tipo_pago_documento.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.TipoPagoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.TipoPago;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import org.json.JSONObject;

/**
 *
 * @author RE
 */
public class ActualizarTipoPagoTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        TipoPago tipoPago = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            int tipoPagoID = requestData.getInt("tipoPagoID");
            String nombre = requestData.getString("nombre");
       
            String estado = requestData.getString("estado");
            
            tipoPago = new TipoPago((short)tipoPagoID, nombre, new Date(), wr.getIdUsuario(), estado.charAt(0));
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos");
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        TipoPagoDao tipoPagoDao = (TipoPagoDao)FactoryDao.buildDao("sci.TipoPagoDao");
        try{
            tipoPagoDao.update(tipoPago);
        
        }catch(Exception e){
            System.out.println("No se pudo actualizar el tipo de pago\n"+e);
            return WebResponse.crearWebResponseError("No se pudo actualizar el tipo de pago", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Estado Tipo de Pago se actualizo correctamente");
        //Fin
    }
    
}
