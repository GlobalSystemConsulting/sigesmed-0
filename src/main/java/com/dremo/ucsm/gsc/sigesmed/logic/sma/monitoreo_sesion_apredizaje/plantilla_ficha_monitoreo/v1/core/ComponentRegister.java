/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.monitoreo_sesion_apredizaje.plantilla_ficha_monitoreo.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.sma.monitoreo_sesion_apredizaje.plantilla_ficha_monitoreo.v1.tx.*;
public class ComponentRegister implements IComponentRegister{

    @Override
    public WebComponent createComponent() {
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.SISTEMA_MONITOREO_ACOMPANIAMIENTO);        
        
        //Registrando el Nombre del componente
        component.setName("plantilla_ficha_monitoreo");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionGET("listarPlantillasPorIE", ListarPlantillasPorIETx.class);
        component.addTransactionPOST("agregarPlantilla", RegistrarPlantillaFMSATx.class);
        component.addTransactionPUT("actualizarDatosBasicosPlantilla", ActualizarDatosBasicosPlantillaFMTx.class);
         component.addTransactionDELETE("eliminarPlantilla", EliminarPlantillaFMSATx.class);
         
        component.addTransactionGET("listarValoracionesInd", ListarValoracionesIndicadorTx.class);
        component.addTransactionPOST("agregarValoracionInd", RegistrarValoracionIndicadorTx.class);
        component.addTransactionPUT("actualizarValoracionInd", ActualizarValoracionIndicadorTx.class);
        component.addTransactionDELETE("eliminarValoracionInd", EliminarValoracionIndicadorTx.class);
        
        component.addTransactionGET("listarContenidoPlantilla", ListarContenidoPlantillaFMTx.class);
        
        component.addTransactionPOST("agregarCompromisoGP", RegistrarCompromisoGPTx.class);
        component.addTransactionPUT("actualizarCompromisoGP", ActualizarCompromisoGPTx.class);
        component.addTransactionDELETE("eliminarCompromisoGP", EliminarCompromisoGPTx.class);
        
        component.addTransactionPOST("agregarInstruccionLI", RegistrarInstruccionLITx.class);
        component.addTransactionPUT("actualizarInstruccionLI", ActualizarInstruccionLITx.class);
        component.addTransactionDELETE("eliminarInstruccionLI", EliminarInstruccionLITx.class);
        
        component.addTransactionPOST("agregarIndicadorCGP", RegistrarIndicadorCGPTx.class);
        component.addTransactionPUT("actualizarIndicadorCGP", ActualizarIndicadorCGPTx.class);
        component.addTransactionDELETE("eliminarIndicadorCGP", EliminarIndicadorCGPTx.class);
        
        component.addTransactionPOST("verPlantillaPDFSinConsulta", VerPlantillaPDFSinConsultaTx.class);
        component.addTransactionPOST("verPlantillaPDFConConsulta", VerPlantillaPDFConConsultaTx.class);
        return component;
    }
    
}
