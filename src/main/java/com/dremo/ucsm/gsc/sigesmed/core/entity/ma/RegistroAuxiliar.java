package com.dremo.ucsm.gsc.sigesmed.core.entity.ma;


import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

/**
 * Created by Administrador on 24/01/2017.
 */
@Entity
@Table(name = "registro_auxiliar", schema = "pedagogico")
public class RegistroAuxiliar implements java.io.Serializable {
    @Id
    @Column(name = "reg_aux_id",nullable = false, unique = true)
    @SequenceGenerator(name = "registro_auxiliar_reg_aux_id_seq",sequenceName = "pedagogico.registro_auxiliar_reg_aux_id_seq")
    @GeneratedValue(generator = "registro_auxiliar_reg_aux_id_seq")
    private int regAuxId;
    
    
    
    @Column(name = "not_ind", length = 4,nullable = false)
    private String notInd;
    
    @Column(name = "not_ind_doc", length = 4)
    private String notIndDoc;
    
    @Column(name="not_ind_lit" , length = 2)
    private String not_ind_lit;
    
    
    @Column(name="not_ind_lit_doc" , length=2)
    private String not_ind_lit_doc;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length=29)
    private Date fecMod;
    
    @Column(name = "est_reg",length = 1)
    private Character estReg;

    
    
    
    
    public RegistroAuxiliar() {
        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public RegistroAuxiliar(String notInd) {
        this.notInd = notInd;
        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public int getRegAuxId() {
        return regAuxId;
    }

    

    public String getNotInd() {
        return notInd;
    }

    public void setNotInd(String notInd) {
        this.notInd = notInd;
    }

    public String getNotIndDoc() {
        return notIndDoc;
    }

    public void setNotIndDoc(String notIndDoc) {
        this.notIndDoc = notIndDoc;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    @Override
    public String toString() {
        return "RegistroAuxiliar{" + "notInd=" + notInd + ", notIndDoc=" + notIndDoc + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    

    public void setNot_ind_lit(String not_ind_lit) {
        this.not_ind_lit = not_ind_lit;
    }

    public String getNot_ind_lit() {
        return not_ind_lit;
    }

    public String getNot_ind_lit_doc() {
        return not_ind_lit_doc;
    }

    public void setNot_ind_lit_doc(String not_ind_lit_doc) {
        this.not_ind_lit_doc = not_ind_lit_doc;
    }
    
    
    
    
}
