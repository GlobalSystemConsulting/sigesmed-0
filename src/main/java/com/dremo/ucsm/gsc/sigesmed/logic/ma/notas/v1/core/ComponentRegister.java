/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.ma.notas.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.ma.notas.v1.tx.*;

/**
 *
 * @author ucsm
 */
public class ComponentRegister implements IComponentRegister{
    @Override

    public WebComponent createComponent() {
        WebComponent maComponent = new WebComponent(Sigesmed.SUBMODULO_ACADEMICO);
        maComponent.setName("notas_estudiante");
        maComponent.setVersion(1);
        
        maComponent.addTransactionGET("reporteNotasTutor",ReporteNotasTutorTx.class);
        
        maComponent.addTransactionGET("listarConfiguracionNota",ListarConfiguracionNotaTx.class);
        maComponent.addTransactionPOST("crudConfiguracionNota", CrudConfiguracionNotaTx.class);
        return maComponent;
    }
}
