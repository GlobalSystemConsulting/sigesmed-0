/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate;

import com.dremo.ucsm.gsc.sigesmed.core.dao.EmpresaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Empresa;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Carlos
 */
public class EmpresaDaoHibernate extends GenericDaoHibernate<Empresa> implements EmpresaDao {

    @Override
    public List<Empresa> lsitarEmpresas() {
        List<Empresa> lista=null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT  ee FROM public.Empresa ee WHERE ee.estReg<>'E' ORDER BY ee.empId";

            Query query = session.createQuery(hql);
            
            lista = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo buscar las Empresas \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar las Empresas \\n " + e.getMessage());
        } finally {
            session.close();
        }
    
      return lista;
    
    }

    @Override
    public Empresa buscarEmpresa(String ruc) {
       Empresa registro = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT ra FROM public.Empresa ra  WHERE ra.ruc=:p1 AND ra.estReg<>'E' ";

            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            query.setParameter("p1", ruc);
           
            registro = (Empresa)query.uniqueResult();

        } catch (Exception e) {
            System.out.println("No se pudo buscar la Empresa \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar la Empresa \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return registro;
    
    }

   
}
