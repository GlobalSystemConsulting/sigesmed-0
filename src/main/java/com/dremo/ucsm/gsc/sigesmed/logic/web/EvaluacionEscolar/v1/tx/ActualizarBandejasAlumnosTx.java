/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.EvaluacionEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.BandejaEvaluacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.EvaluacionEscolar;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaEvaluacion;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONArray;

/**
 *
 * @author abel
 */
public class ActualizarBandejasAlumnosTx implements ITransaction{
    private Object requestData;

    @Override
    public WebResponse execute(WebRequest wr) {
        
       
       JSONObject requestData = (JSONObject)wr.getData();
       JSONObject alumno = requestData.getJSONObject("alumnos");
       JSONArray miArray = new JSONArray();
       BandejaEvaluacionDao bandejaDao = (BandejaEvaluacionDao)FactoryDao.buildDao("web.BandejaEvaluacionDao");
       try{
           
           SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
           BandejaEvaluacion bandeja = new BandejaEvaluacion(alumno.getInt("bandejaEvaluacionID"), sf.parse(alumno.getString("fechaEntrega")),new Date(),alumno.getInt("nota"),alumno.getString("not_lit"),alumno.getString("estado").charAt(0),alumno.getInt("eva_esc_id"),alumno.getInt("per_id"));
           bandejaDao.update(bandeja);
           
           JSONObject oResponse = new JSONObject();
            
            oResponse.put("not",bandeja.getNota());
            oResponse.put("not_lit",bandeja.getNot_lit());
            
            miArray.put(oResponse);
           
       }
       catch(Exception e){
            System.out.println("No se pudo actualizar Bandejas de Evaluacion\n"+e);
            return WebResponse.crearWebResponseError("No se pudo actualizar Bandejas de Evaluacion", e.getMessage() );
       }
       
       return WebResponse.crearWebResponseExito("La actualizacion de la Bandeja Escolar se realizo correctamente",miArray);
       
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
