package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.ParticipanteCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.PersonaCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.ParticipanteCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.json.JSONArray;
import org.json.JSONObject;

public class ListarPersonasTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(ListarPersonasTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        WebResponse response = null;
        
        switch(data.getInt("opt")) {
            case 0:
                response = buscarPersona(data.getString("dni"));
                break;
                
            case 1: 
                response = buscarParticipante(data.getString("dni"), data.getInt("codSed"));
                break;
                
            case 2:
                response = listarPorOrganizacion(data.getInt("orgId"), data.getInt("sedId"));
                break;

            case 3:
                response = buscarDocenteParticipante(data.getInt("sedCod"));
                break;
        }
        
        return response;
    }
    
    private WebResponse buscarPersona(String dni) {
        try {
            PersonaCapacitacionDao personaCapacitacionDao = (PersonaCapacitacionDao) FactoryDao.buildDao("capacitacion.PersonaCapacitacionDao");
            Persona persona = personaCapacitacionDao.buscarPorDni(dni);

            return WebResponse.crearWebResponseExito("Se listo correctamente", WebResponse.OK_RESPONSE)
                    .setData(new JSONObject(EntityUtil.objectToJSONString(
                                            new String[]{"perId", "nom", "apePat", "apeMat", "email"},
                                            new String[]{"id", "nom", "apePat", "apeMat", "email"},
                                            persona
                                    )));
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarPersona", e);
            return WebResponse.crearWebResponseError("Error al listar los docentes", WebResponse.BAD_RESPONSE);
        }
    }
    
    private WebResponse buscarParticipante(String dni, int codSed) {
        try {
            ParticipanteCapacitacionDao participanteCapacitacionDao = (ParticipanteCapacitacionDao) FactoryDao.buildDao("capacitacion.ParticipanteCapacitacionDao");
            ParticipanteCapacitacion participante = participanteCapacitacionDao.buscarPorSedeDni(dni, codSed);

            return WebResponse.crearWebResponseExito("Se listo correctamente", WebResponse.OK_RESPONSE)
                    .setData(new JSONObject(EntityUtil.objectToJSONString(
                                            new String[]{"perId", "nom", "apePat", "apeMat"},
                                            new String[]{"cod", "nom", "pat", "mat"},
                                            participante.getPersona()
                                    )));
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarPersona", e);
            return WebResponse.crearWebResponseError("Error al listar los docentes", WebResponse.BAD_RESPONSE);
        }
    }
    
    private WebResponse listarPorOrganizacion(int orgId, int sedId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT p FROM PersonaCapacitacion p " +
                            "JOIN p.trabajadores t " +
                            "JOIN t.organizacion o " +
                            "WHERE o.orgId = :orgId";
            
            Query query = session.createQuery(hql);
            System.out.println("QQQQQQQQQQQQQQ"  + orgId + sedId);
            query.setParameter("orgId", orgId);
//            query.setParameter("sedCapId", sedId);
//            query.setParameter("estReg", '*');
            
            List<Persona> personas = query.list();
            System.out.println("QQQQQQQQQQQQQ___"  + personas.size());
            JSONArray array = new JSONArray();
            
            for(Persona person: personas) {
                JSONObject object = new JSONObject();
                object.put("cod", person.getPerId());
                object.put("nom", person.getApePat() + " " + person.getApeMat() + " " + person.getNom());
                object.put("cor", person.getEmail());
                object.put("sel", false);

                array.put(object);
            }

            return WebResponse.crearWebResponseExito("Se listo correctamente", WebResponse.OK_RESPONSE).setData(array);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarPorOrganizacion", e);
            return WebResponse.crearWebResponseError("Error al listar los docentes", WebResponse.BAD_RESPONSE);
        } finally {
            session.close();
        }
    }

    private WebResponse buscarDocenteParticipante(int sedCod) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT p FROM PersonaCapacitacion p " +
                            "WHERE p NOT IN " +
                                "(SELECT c.persona FROM ParticipanteCapacitacion c " +
                                "JOIN c.sedCap s " +
                                "WHERE s.sedCapId = :sedCapId) " +
                            "AND p NOT IN " +
                                "(SELECT t.per FROM CapacitadorCursoCapacitacion t " + 
                                "JOIN t.sedCap s " +
                                "WHERE s.sedCapId = :sedCapId) " +
                            "AND p.estReg != :estReg";
            
            Query query = session.createQuery(hql);
            query.setParameter("sedCapId", sedCod);
            query.setParameter("estReg", '*');
            
            List<Persona> personas = query.list(); 
            JSONArray array = new JSONArray();
            
            for(Persona person: personas) {
                JSONObject object = new JSONObject();
                object.put("cod", person.getPerId());
                object.put("nom", person.getApePat() + " " + person.getApeMat() + " " + person.getNom());
                object.put("aPa", person.getApePat());
                object.put("aMa", person.getApeMat());
                object.put("nCo", person.getNom());
                object.put("dni", person.getDni());
                object.put("cor", person.getEmail());
                array.put(object);
            }          

            return WebResponse.crearWebResponseExito("Se listo correctamente", WebResponse.OK_RESPONSE).setData(array);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarDocenteParticipante", e);
            return WebResponse.crearWebResponseError("Error al listar los docentes", WebResponse.BAD_RESPONSE);
        }
    }
}
