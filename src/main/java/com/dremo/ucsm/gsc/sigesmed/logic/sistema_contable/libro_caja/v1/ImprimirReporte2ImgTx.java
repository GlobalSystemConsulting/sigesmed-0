/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author G-16
 */
public class ImprimirReporte2ImgTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        FileJsonObject miGrafico1 = null;
        FileJsonObject miGrafico2 = null;
        GTabla tablaOrg = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            JSONArray tabla = requestData.optJSONArray("resumen");
            
            if(tabla!=null && tabla.length() > 0){
                
                float[] cols = {0.5f,5f,1.5f,1.5f,1.5f};
                tablaOrg = new GTabla(cols);

                String[] labels = {"N°","Organizacion","N° Expedientes","N° Finalizados","N° Entregados"};
                tablaOrg.build(labels);
                for(int i = 0; i < tabla.length();i++){
                    JSONObject bo =tabla.getJSONObject(i);
                    
                    String[] fila = new String [5];
                    fila[0] = i+1+"";
                    fila[1] = bo.getString("organizacion");
                    fila[2] = ""+bo.getInt("expedientes");
                    fila[3] = ""+bo.getInt("finalizados");
                    fila[4] = ""+bo.getInt("entregados");
                    
                    tablaOrg.processLine(fila);
                }
                
            }
            else  {          
                JSONObject g1 = requestData.getJSONObject("Grafico1");
                             
                JSONObject g2 = requestData.getJSONObject("Grafico2");
                 miGrafico1 = new FileJsonObject( g1);
                miGrafico2 = new FileJsonObject( g2);
            }
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo realizar el reporte, grafico iicorrecto", e.getMessage() );
        }
        //Fin        
        
        
        /*
        *  Repuesta Correcta
        */
        
        JSONObject response = new JSONObject();
        try {
            Mitext r = new Mitext();
            
            
            if(tablaOrg==null){
               r.agregarTitulo(miGrafico1.getName());
                r.newLine(1);
                r.agregarImagen64(miGrafico1.getData());
                r.agregarImagen64(miGrafico2.getData());
            }
            else{
                r.agregarTitulo("Tramites por Organizacion");
                r.newLine(1);
                r.agregarTabla(tablaOrg);
            }
            
            r.cerrarDocumento();
            response.append("reporte", r.encodeToBase64() );
        } catch (Exception ex) {
              return WebResponse.crearWebResponseError("No se pudo realizar el reporte, grafico iicorrecto", ex.getMessage() );

        }
        
        
        return WebResponse.crearWebResponseExito("el reporte se realizo",response);
        //Fin
    }
}