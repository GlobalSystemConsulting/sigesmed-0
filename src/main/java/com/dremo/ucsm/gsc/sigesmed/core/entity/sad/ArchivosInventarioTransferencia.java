/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sad;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 *
 * @author Jeferson
 */
@Entity
@Table(name="archivos_inventario_transferencia" , schema="administrativo")
public class ArchivosInventarioTransferencia implements java.io.Serializable{
    
    @Id
    @Column(name="arc_inv_tra_id" , unique=true , nullable=false)
    @SequenceGenerator(name="secuencia_ar_inv_tra",sequenceName="administrativo.archivos_inventario_transferencia_arc_inv_tra_id_seq")
    @GeneratedValue(generator="secuencia_ar_inv_tra")
    private int arc_inv_tra_id;
    
    @Id
    @Column(name="det_inv_trans")
    private int det_inv_trans;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="det_inv_trans" , insertable=false , updatable=false)
    private DetalleInventarioTransferencia detalle_inventario;
    
    @Column(name="num_fol")
    private int num_fol;
    
    @Column(name="titulo")
    private String titulo;
    
    @Column(name="fecha")
    private Date fecha;
    
    @Column(name="cod_expediente")
    private String cod_expediente;
    
    
    @Column(name="nombre_archivo")
    private String nombre_archivo;
    
    private final static String path="sad";

    

    public void setNombre_archivo(String nombre_archivo) {
        this.nombre_archivo = nombre_archivo;
    }
 
    

    public String getNombre_archivo() {
        return nombre_archivo;
    }
    
    
    
        
    public ArchivosInventarioTransferencia(){
        
    }
    public ArchivosInventarioTransferencia(int arc_inv_tra_id){
        this.arc_inv_tra_id = arc_inv_tra_id;
    }
    public ArchivosInventarioTransferencia(int arc_inv_tra_id , int det_inv_trans){
        this.arc_inv_tra_id = arc_inv_tra_id;
        this.det_inv_trans = det_inv_trans;
        detalle_inventario = new DetalleInventarioTransferencia(det_inv_trans);
    }
    public ArchivosInventarioTransferencia(int arc_inv_tra_id , int det_inv_trans,int num_fol,String tit ,Date fech ,String cod_exp , String file){
        this.arc_inv_tra_id = arc_inv_tra_id;
        this.det_inv_trans = det_inv_trans;
        detalle_inventario = new DetalleInventarioTransferencia(det_inv_trans);
        
        this.num_fol = num_fol;
        this.titulo = tit;
        this.fecha = fech;
        this.cod_expediente = cod_exp; 
        this.nombre_archivo = file;
        
    }
    public void setdetinvtrans(int codigo){
        this.det_inv_trans = codigo;
    }
    public int getdetinvtrans(){
        return this.det_inv_trans;
    }
    public void setnumfol(int num_fol){
        this.num_fol = num_fol;
    }
    public int getnumfol(){
        return this.num_fol;
    }
    
    public void setarcinvtraID(int arch_id){
        this.arc_inv_tra_id = arch_id;
    }
    public int getarcinvtraID(){
        return this.arc_inv_tra_id;
    }
    public void settitulo(String titulo){
        this.titulo = titulo;
    }
    
    public String gettitulo(){
        return this.titulo;
    }
    
    public void setFecha(Date fec){
        this.fecha = fec;
    }
    public Date getfecha(){
        return this.fecha;
    }
    public void setcod_exp(String cod_exp){
        this.cod_expediente = cod_exp;
    }
    public String getcod_exp(){
        return this.cod_expediente;
    }
    
}