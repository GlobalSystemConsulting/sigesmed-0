/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.scp;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author harold
 */

@Entity
@Table(name="causal_salida", schema="administrativo")
public class CausalSalida {
    
    @Id
    @Column(name="cau_sal_id" , unique=true, nullable=false)
    @SequenceGenerator(name="secuencia_inv_tra",sequenceName="administrativo.causal_salida_cau_sal_id_seq")
    @GeneratedValue(generator="secuencia_inv_tra")
    private int cau_sal_id;
    
    @Column(name="des")
    private String des;
    
    @Column(name="fec_mod")
    private Date fec_mod;
    
    @Column(name="usu_mod")
    private int usu_mod;
    
    @Column(name="est_reg")
    private char estReg;

    public CausalSalida() {
    }       

    public CausalSalida(int cau_sal_id, String des, Date fec_mod, int usu_mod, char estReg) {
        this.cau_sal_id = cau_sal_id;
        this.des = des;
        this.fec_mod = fec_mod;
        this.usu_mod = usu_mod;
        this.estReg = estReg;
    }   
    
    public void setCau_sal_id(int cau_sal_id) {
        this.cau_sal_id = cau_sal_id;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public void setFec_mod(Date fec_mod) {
        this.fec_mod = fec_mod;
    }

    public void setUsu_mod(int usu_mod) {
        this.usu_mod = usu_mod;
    }

    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }

    public int getCau_sal_id() {
        return cau_sal_id;
    }

    public String getDes() {
        return des;
    }

    public Date getFec_mod() {
        return fec_mod;
    }

    public int getUsu_mod() {
        return usu_mod;
    }

    public char getEstReg() {
        return estReg;
    }    
    
}
