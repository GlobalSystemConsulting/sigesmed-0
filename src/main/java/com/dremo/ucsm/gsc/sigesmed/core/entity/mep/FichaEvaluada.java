/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.mep;

import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;

/**
 *
 * @author geank
 */
public class FichaEvaluada {
    
    private FichaEvaluacionPersonal fic;
    private int idCon;
    private char tipCon;
    private String nomCon;
    private int idInd;
    private String nomInd;
    private String desInd;
    private ResumenEvaluacionPersonal det;
    private int pun;

    public FichaEvaluacionPersonal getFic() {
        return fic;
    }

    public void setFic(FichaEvaluacionPersonal fic) {
        this.fic = fic;
    }

    public int getIdCon() {
        return idCon;
    }

    public void setIdCon(int idCon) {
        this.idCon = idCon;
    }

    public char getTipCon() {
        return tipCon;
    }

    public void setTipCon(char tipCon) {
        this.tipCon = tipCon;
    }

    public String getNomCon() {
        return nomCon;
    }

    public void setNomCon(String nomCon) {
        this.nomCon = nomCon;
    }

    public int getIdInd() {
        return idInd;
    }

    public void setIdInd(int idInd) {
        this.idInd = idInd;
    }

    public String getNomInd() {
        return nomInd;
    }

    public void setNomInd(String nomInd) {
        this.nomInd = nomInd;
    }

    public String getDesInd() {
        return desInd;
    }

    public void setDesInd(String desInd) {
        this.desInd = desInd;
    }

    public ResumenEvaluacionPersonal getDet() {
        return det;
    }

    public void setDet(ResumenEvaluacionPersonal det) {
        this.det = det;
    }

    public int getPun() {
        return pun;
    }

    public void setPun(int pun) {
        this.pun = pun;
    }
    @Override
    public String toString(){
        return EntityUtil.objectToJSONString(new String[]{"det","fic","idCon","tipCon","nomCon","idInd","nomInd","desInd","pun"}, null, this);
    }
}
