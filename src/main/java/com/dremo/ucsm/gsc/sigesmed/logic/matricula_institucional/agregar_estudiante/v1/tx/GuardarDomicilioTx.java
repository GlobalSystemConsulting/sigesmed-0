package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.agregar_estudiante.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.DomicilioDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.Domicilio;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.DomicilioId;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

public class GuardarDomicilioTx implements ITransaction {

    private static Logger logger = Logger.getLogger(GuardarDomicilioTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        long perId, domId;
        int anyo;
        String domUBI, domTel, domDir, domRef;

        try {
            perId = data.getInt("perId");
            anyo = data.getInt("anyo");
            domUBI = data.getString("domUBI");
            domTel = data.getString("domTel");
            domDir = data.getString("domDir");
            domRef = data.getString("domRef");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error al cargar datos de nuevo domicilio", e);
            return WebResponse.crearWebResponseError("Nose pudo insertar domicilio");
        }

        DomicilioDaoHibernate dhDomicilio = new DomicilioDaoHibernate();

        try {
            domId = dhDomicilio.llaveDomicilio(perId);
            domId = domId + 1;

            Domicilio miDomicilio = new Domicilio();
            miDomicilio.setId(new DomicilioId(domId, perId));
            miDomicilio.setDomAno(anyo);
            miDomicilio.setDomDir(domDir);
            miDomicilio.setDomRef(domRef);
            miDomicilio.setDomTel(domTel);
            miDomicilio.setUbiCod(domUBI);
            miDomicilio.setFecMod(new Date());
            miDomicilio.setEstReg('A');

            dhDomicilio.saveOrUpdate(miDomicilio);

            return WebResponse.crearWebResponseExito("Se guardo el Domicilio exitosamente", miDomicilio);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error Insertar Domicilio", e);
            return WebResponse.crearWebResponseError("Nose pudo insertar Domicilio");
        }
    }
}
