/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author felipe
 */
@Entity
@Table(name = "tipo_organigrama", schema="administrativo")
public class TipoOrganigrama implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "tip_orgi_id")
    private Integer tipOrgiId;
    @Size(max = 2147483647)
    @Column(name = "cod_tip_orgi")
    private String codTipOrgi;
    @Size(max = 2147483647)
    @Column(name = "nom_tip_orgi")
    private String nomTipOrgi;
    @Size(max = 2147483647)
    @Column(name = "des_tip_orgi")
    private String desTipOrgi;
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "est_reg")
    private Character estReg;
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy="tipoOrganigrama")
    List<Organigrama> organigramas;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "dat_orgi_id", referencedColumnName = "dat_orgi_id")   
    private DatosOrganigrama datosOrganigramas;

    public TipoOrganigrama() {
    }

    public TipoOrganigrama(Integer tipOrgiId) {
        this.tipOrgiId = tipOrgiId;
    }

    public Integer getTipOrgiId() {
        return tipOrgiId;
    }

    public void setTipOrgiId(Integer tipOrgiId) {
        this.tipOrgiId = tipOrgiId;
    }

    public String getCodTipOrgi() {
        return codTipOrgi;
    }

    public void setCodTipOrgi(String codTipOrgi) {
        this.codTipOrgi = codTipOrgi;
    }

    public String getNomTipOrgi() {
        return nomTipOrgi;
    }

    public void setNomTipOrgi(String nomTipOrgi) {
        this.nomTipOrgi = nomTipOrgi;
    }

    public String getDesTipOrgi() {
        return desTipOrgi;
    }

    public void setDesTipOrgi(String desTipOrgi) {
        this.desTipOrgi = desTipOrgi;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public DatosOrganigrama getDatosOrganigramas() {
        return datosOrganigramas;
    }

    public void setDatosOrganigramas(DatosOrganigrama datosOrganigramas) {
        this.datosOrganigramas = datosOrganigramas;
    }

    public List<Organigrama> getOrganigramas() {
        return organigramas;
    }

    public void setOrganigramas(List<Organigrama> organigramas) {
        this.organigramas = organigramas;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tipOrgiId != null ? tipOrgiId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoOrganigrama)) {
            return false;
        }
        TipoOrganigrama other = (TipoOrganigrama) object;
        if ((this.tipOrgiId == null && other.tipOrgiId != null) || (this.tipOrgiId != null && !this.tipOrgiId.equals(other.tipOrgiId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TipoOrganigrama{" + "tipOrgiId=" + tipOrgiId + ", codTipOrgi=" + codTipOrgi + ", nomTipOrgi=" + nomTipOrgi + ", desTipOrgi=" + desTipOrgi + ", fecMod=" + fecMod + ", usuMod=" + usuMod + ", estReg=" + estReg + '}';
    }

    
}
