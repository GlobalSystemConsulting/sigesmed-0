/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author angel
 */
public class ImpReciboAuxiliarTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        GTabla tablaRecibo = null;
        JSONObject cabecera = new JSONObject();
        
            String numeroRecibo = null;
            String Descripcion = null;
            String tesoreEncargado = null;
            String nombrePerCom = null;
            String dni = null;
            String fecha =  null;
            double importe ;
            int plazo;
            String nombreOrg = null;
            Date fechaCierre = null;
            String fechaT;
        try{
           
            JSONObject requestData = (JSONObject)wr.getData();  
            numeroRecibo = requestData.optString("numeroD","");
            Descripcion = requestData.getString("Descripcion");
            tesoreEncargado = requestData.getString("tesoreEncargado");
            nombrePerCom = requestData.getString("nombrePerCom");
            dni = requestData.optString("dni");
            fecha = requestData.getString("fechaR");     
            
            importe = requestData.getDouble("importe");
            plazo = requestData.getInt("plazo");
            nombreOrg = requestData.getString("nombreOrg");
           
            
            fechaCierre = new Date(fecha);            
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            fechaT = sdf.format(fechaCierre);
            
            cabecera.put("Organización : ", nombreOrg);
       
         }catch(Exception e){
            System.out.println("No se pudo realizar el reporte,Datos Incorrectos" + e);
            return WebResponse.crearWebResponseError("No se pudo realizar el reporte,Datos Incorrectos", e.getMessage() );
        }
            
          //  JSONArray tabla = requestData.optJSONArray("resumen");
           //JSONArray fecha = requestData.optJSONArray("resumen");
            
          try{
            
                float[] cols = {1f,1f};
                tablaRecibo = new GTabla(cols);
                String[] labels = {"Campos ","Informacion"};
                tablaRecibo.build(labels);
                  
                    String[] fila2 = new String [2];
                    fila2[0] = "Descripción";
                    fila2[1] = Descripcion;                
                    tablaRecibo.processLine(fila2);

                    String[] fila3 = new String [2];
                    fila3[0] = "Tesorero a Cargo";
                    fila3[1] = tesoreEncargado;                
                    tablaRecibo.processLine(fila3);
                    
                    String[] fila4 = new String [2];
                    fila4[0] = "Entregado a ";
                    fila4[1] = nombrePerCom;     
                    tablaRecibo.processLine(fila4);
                    
                    String[] fila5 = new String [2];
                    fila5[0] = "Con DNI";
                    fila5[1] = dni;                
                    tablaRecibo.processLine(fila5);
                    
                    String[] fila6= new String [2];
                    fila6[0] = "Fecha";
                    fila6[1] = fechaT;                
                    tablaRecibo.processLine(fila6);
                    
                    String[] fila7 = new String [2];
                    fila7[0] = "Plazo de Pago(dias)";
                    fila7[1] = String.valueOf(plazo);                
                    tablaRecibo.processLine(fila7);
                    
                    String[] fila8 = new String [2];
                    fila8[0] = "Importe S/.";
                    fila8[1] = String.valueOf(importe);                
                    tablaRecibo.processLine(fila8);
                
                
           
        
        }catch(Exception e){
            System.out.println("No se pudo realizar el reporte, grafico iicorrecto"+e);
            return WebResponse.crearWebResponseError("No se pudo realizar el reporte, grafico iicorrecto", e.getMessage() );
        }
        //Fin        
        
        
        /*
        *  Repuesta Correcta
        */
        
    



        String[] fila = new String [2];

        fila[0] = "Numero de Recibo";
        fila[1] = numeroRecibo;                
        tablaRecibo.processLine(fila);
                    
        JSONObject response = new JSONObject();
        Mitext r = null;        
        
        try {
            r = new Mitext(true,"RECIBO AUXILIAR");
            r.newLine(4);
            r.agregarTitulo("RECIBO AUXILIAR");
        } catch (Exception ex) {
            System.out.println("No se pudo crear el documento \n"+ex);
            Logger.getLogger(ImpReciboAuxiliarTx.class.getName()).log(Level.SEVERE, null, ex);
        }
            
               
          
                r.newLine(4);
                r.agregarSubtitulos(cabecera);
                r.newLine(3);
                 r.agregarTabla(tablaRecibo);
                 
            Table table = new Table(2);
            
            Cell c = new Cell();
            c.setPadding(0);
            c.setTextAlignment(TextAlignment.CENTER);
            c.setBorder(Border.NO_BORDER);
            table.addCell(c);
            
            c = new Cell().add(new Paragraph("--------------------------------------------------"));
            c.setPadding(0);
            c.setTextAlignment(TextAlignment.CENTER);
            c.setBorder(Border.NO_BORDER);
            table.addCell(c);
            
            c = new Cell();
            c.setPadding(0);
            c.setTextAlignment(TextAlignment.CENTER);
            c.setBorder(Border.NO_BORDER);
            table.addCell(c);
            
            Cell cFir = new Cell().add(new Paragraph("FIRMA DE RECEPCION"));
            cFir.setPadding(0);
            cFir.setTextAlignment(TextAlignment.CENTER);
            cFir.setBorder(Border.NO_BORDER);
            table.addCell(cFir);
        
            r.newLine(4);
            r.agregarTabla(table);
            
            r.cerrarDocumento();
            response.append("datareporte", r.encodeToBase64() );
      
        
        
        return WebResponse.crearWebResponseExito("el reporte se realizo",response);
        //Fin
    }
    
}

