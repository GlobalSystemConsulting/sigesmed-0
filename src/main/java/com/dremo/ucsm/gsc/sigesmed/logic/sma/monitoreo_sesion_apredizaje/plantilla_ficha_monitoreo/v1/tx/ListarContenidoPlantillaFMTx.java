/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.monitoreo_sesion_apredizaje.plantilla_ficha_monitoreo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.PlantillaFichaMonitoreoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.CompromisoGestionPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.IndicadorCompromisoGestionPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.InstruccionLlenadoIndicadores;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class ListarContenidoPlantillaFMTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ListarContenidoPlantillaFMTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int plaId = data.getInt("plaId");
        return listarContenido(plaId);
    }

    private WebResponse listarContenido(int plaId) {
        try{
            PlantillaFichaMonitoreoDao plantillaDao = (PlantillaFichaMonitoreoDao) FactoryDao.buildDao("sma.PlantillaFichaMonitoreoDao");
            List<CompromisoGestionPedagogica> compromisos = plantillaDao.listarCompromisos(plaId);
            JSONArray jsonCompromisos = new JSONArray();
            
            for(CompromisoGestionPedagogica compromiso : compromisos){
                JSONObject jsonCompromiso = new JSONObject(EntityUtil.objectToJSONString(
                        new String[]{"comGesPedId", "comGesPedDes", "comGesPedTip"},
                        new String[]{"comId", "comDes", "comTip"},
                        compromiso
                ));
                
                List<InstruccionLlenadoIndicadores> instrucciones = compromiso.getInstruccionesLI();
                JSONArray jsonInstrucciones = new JSONArray();
                for(InstruccionLlenadoIndicadores instruccion: instrucciones){
                    JSONObject jsonInstruccion = new JSONObject(EntityUtil.objectToJSONString(
                        new String[]{"insLleIndId", "insLleIndDes"},
                        new String[]{"insId", "insDes"},
                        instruccion
                    ));
                    
                    List<IndicadorCompromisoGestionPedagogica> indicadores = plantillaDao.listarIndicadores(instruccion.getInsLleIndId());
                    JSONArray jsonIndicadores = new JSONArray(EntityUtil.listToJSONString(
                            new String[]{"indComGesPedId", "indComGesPedDes"},
                            new String[]{"indId","indDes"},
                            indicadores
                    ));
                    
                    jsonInstruccion.put("indicadores",jsonIndicadores);
                    jsonInstrucciones.put(jsonInstruccion);
                }
                
                jsonCompromiso.put("instrucciones",jsonInstrucciones);
                jsonCompromisos.put(jsonCompromiso);
            }
            return WebResponse.crearWebResponseExito("Se listo correctamente el contenido de la plantilla",jsonCompromisos);
        }catch (Exception e){
            logger.log(Level.SEVERE,"listarContenidoPlantilla",e);
            return WebResponse.crearWebResponseError("No se pudo listar el contenido de la plantilla");
        }
    }
    
}
