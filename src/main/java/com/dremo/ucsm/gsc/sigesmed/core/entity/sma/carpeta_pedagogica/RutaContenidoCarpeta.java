/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity(name = "RutaContenidoCarpetaSMA")
@Table(name = "ruta_contenido_carpeta", schema = "pedagogico")
public class RutaContenidoCarpeta implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "rut_con_car_id")
    private Integer rutConCarId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "org_id")
    private int orgId;
    
    @Column(name = "usu_id")
    private Integer usuId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "tip_usu")
    private short tipUsu;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "pat")
    private String pat;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "nom_fil")
    private String nomFil;
    
    @OneToMany(mappedBy = "rutaContenido")
    private List<DocenteCarpetaPedagogica> docentesCarPed;
    
    @JoinColumn(name = "con_sec_car_ped_id", referencedColumnName = "con_sec_car_ped_id")
    @ManyToOne(optional = false)
    private ContenidoSeccionesCarpetaPedagogica contenidoSeccionesCarpetaPedagogica;

    public RutaContenidoCarpeta() {
    }

    public RutaContenidoCarpeta(Integer rutConCarId) {
        this.rutConCarId = rutConCarId;
    }

    public RutaContenidoCarpeta(Integer rutConCarId, int orgId, short tipUsu, String pat, String nomFil) {
        this.rutConCarId = rutConCarId;
        this.orgId = orgId;
        this.tipUsu = tipUsu;
        this.pat = pat;
        this.nomFil = nomFil;
    }

    public Integer getRutConCarId() {
        return rutConCarId;
    }

    public void setRutConCarId(Integer rutConCarId) {
        this.rutConCarId = rutConCarId;
    }

    public int getOrgId() {
        return orgId;
    }

    public void setOrgId(int orgId) {
        this.orgId = orgId;
    }

    public Integer getUsuId() {
        return usuId;
    }

    public void setUsuId(Integer usuId) {
        this.usuId = usuId;
    }

    public short getTipUsu() {
        return tipUsu;
    }

    public void setTipUsu(short tipUsu) {
        this.tipUsu = tipUsu;
    }

    public String getPat() {
        return pat;
    }

    public void setPat(String pat) {
        this.pat = pat;
    }

    public String getNomFil() {
        return nomFil;
    }

    public void setNomFil(String nomFil) {
        this.nomFil = nomFil;
    }

    public List<DocenteCarpetaPedagogica> getDocenteCarpetaList() {
        return docentesCarPed;
    }

    public void setDocenteCarpetaList(List<DocenteCarpetaPedagogica> docentesCarPed) {
        this.docentesCarPed = docentesCarPed;
    }

    public List<DocenteCarpetaPedagogica> getDocentesCarPed() {
        return docentesCarPed;
    }

    public void setDocentesCarPed(List<DocenteCarpetaPedagogica> docentesCarPed) {
        this.docentesCarPed = docentesCarPed;
    }

    public ContenidoSeccionesCarpetaPedagogica getContenidoSeccionesCarpetaPedagogica() {
        return contenidoSeccionesCarpetaPedagogica;
    }

    public void setContenidoSeccionesCarpetaPedagogica(ContenidoSeccionesCarpetaPedagogica contenidoSeccionesCarpetaPedagogica) {
        this.contenidoSeccionesCarpetaPedagogica = contenidoSeccionesCarpetaPedagogica;
    }

    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rutConCarId != null ? rutConCarId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RutaContenidoCarpeta)) {
            return false;
        }
        RutaContenidoCarpeta other = (RutaContenidoCarpeta) object;
        if ((this.rutConCarId == null && other.rutConCarId != null) || (this.rutConCarId != null && !this.rutConCarId.equals(other.rutConCarId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica.RutaContenidoCarpeta[ rutConCarId=" + rutConCarId + " ]";
    }
    
}
