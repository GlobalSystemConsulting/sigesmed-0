/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.dremo.ucsm.gsc.sigesmed.util.ReportXls;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author G-16
 */
public class ReporteRutaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        FileJsonObject miGrafico = null;
        GTabla tablaOrg = null;
        JSONObject cabecera = new JSONObject();
         JSONObject objeto;
        String [] labels2 = null;
        String [] key2 = null;
        JSONArray tabla;
        List lista = new ArrayList();
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            objeto = requestData.getJSONObject("objeto");

            PersonaDao personaDao = (PersonaDao) FactoryDao.buildDao("PersonaDao");
            Persona director = personaDao.buscarPorCod(objeto.getInt("Director"));
            objeto.put("Director", director.getNom() + " " + director.getApePat() + " " + director.getApeMat());
           
            OrganizacionDao organizacionDao = (OrganizacionDao) FactoryDao.buildDao("OrganizacionDao");
            Organizacion org = organizacionDao.buscarConTipoOrganizacion(objeto.getInt("OrganizacionId"));
            objeto.put("Dirección", org.getDir());

            objeto.remove("OrganizacionId");
           
            
            cabecera.put("Fecha de Incio de Busqueda", requestData.getString("desde"));
             
             tabla = requestData.optJSONArray("resumen");
            
            if(tabla!=null && tabla.length() > 0){
                
                float[] cols = {2f,3f,3f,3f,2f};
                tablaOrg = new GTabla(cols);

                String[] labels = {" Expediente "," Area "," Fecha Ingreso "," Fecha Salida "," Estado "};
                 labels2 = labels;

                tablaOrg.build(labels);
                for(int i = 0; i < tabla.length();i++){
                    JSONObject bo =tabla.getJSONObject(i);
                    
                    String[] fila = new String [5];
                    fila[0] = bo.getString("codigoExp");
                    fila[1] = bo.getString("areae");
                    fila[2] = bo.getString("fechaIngreso");
                    fila[3] = bo.getString("fechaSalida");
                    fila[4] = bo.getString("estadoe");

                    tablaOrg.processLine(fila);
                    
                    String [] key =  {"codigoExp","areae","fechaIngreso","fechaSalida","estadoe"};
                    key2 = key;
                    Map<String, String> map = new HashMap<String, String>();
                    map.put("codigoExp", fila[0]);	
                    map.put("areae", fila[1]);
                    map.put("fechaIngreso", fila[2]);
                    map.put("fechaSalida", fila[3]);
                    map.put("estadoe", fila[4]);
                     
                   lista.add(map);
                }
                
            }
            else            
                miGrafico = new FileJsonObject( requestData  );
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo realizar el reporte, grafico iicorrecto", e.getMessage() );
        }
        //Fin        
        
        
        /*
        *  Repuesta Correcta
        */
        
        JSONObject response = new JSONObject();
        try {
            Mitext r = new Mitext();
            
            
            if(tablaOrg==null){
                r.agregarTitulo(miGrafico.getName());
                r.newLine(1);
                r.agregarImagen64(miGrafico.getData());
            }
            else{
                r.newLine(4);
                r.agregarTitulo("REPORTE RUTA DEL TRAMITE ");
                r.newLine(2);
                r.agregarSubtitulos(cabecera);
                r.newLine(3);

                r.agregarTabla(tablaOrg);
            }
            
            r.cerrarDocumento();
             String [] peso = {"4","5","5","5","3"};
            System.out.println("contenido Titulo, keys, pesos , dirext");
            System.out.println("conte "+Arrays.toString(labels2));
            System.out.println("key" +Arrays.toString(key2));
            System.out.println("peso "+Arrays.toString(peso));
            System.out.println("dir "+lista);
            
             ///////////EXCEL-BEGIN
                ReportXls myReportXls = new ReportXls();
                try {
        //           
                    //set header
                    myReportXls.addHeader("SIGESMED MOQUEGUA");

                    //set title1
                    myReportXls.setTitle1("SIGESMED MOQUEGUA");

                    //set title2   
                    myReportXls.setTitle2("REPORTE ESTADISTICO");

                    //Show image
                    myReportXls.setImageTop(ServicioREST.PATH_SIGESMED + "/recursos/img/minedu.png");

                    //add fecha
                    myReportXls.setDate();

                    //create subtitles
                    myReportXls.setSubtitles(objeto);

                    //add directorio
                    
                    myReportXls.fillData(labels2, key2, peso, lista);

                    //close book
                    myReportXls.closeReport();

                } catch (Exception e) {
                    System.out.println("No se pudo generar reporte en xls \n" + e);
                }
            System.out.print("XLS:\n");
            System.out.print(myReportXls.encodeToBase64());
            
            response.append("reporte", r.encodeToBase64() );
           response.append("reporteXls", myReportXls.encodeToBase64() );

        } catch (Exception ex) {
            Logger.getLogger(ReporteTx.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return WebResponse.crearWebResponseExito("el reporte se realizo",response);
        //Fin
    }
    
}


