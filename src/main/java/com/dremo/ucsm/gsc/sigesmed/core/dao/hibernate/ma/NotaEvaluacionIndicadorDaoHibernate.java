package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.ma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.NotaEvaluacionIndicadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.NotaEvaluacionIndicador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.NotaIndicador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Periodo;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import java.util.List;
import org.hibernate.criterion.CriteriaSpecification;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Administrador on 16/01/2017.
 */
public class NotaEvaluacionIndicadorDaoHibernate extends GenericDaoHibernate<NotaEvaluacionIndicador> implements NotaEvaluacionIndicadorDao{
    @Override
    public List<NotaIndicador> listarNotasIndicadorAlumno(int idSes, int idGradAlumno) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String sql = "SELECT nei.notEvaIndId as notevaindid,ind.indAprId as indaprid,ind.nomInd as nomind, nei.gradoEstudiante.graOrgEstId as graieestid, nei.tipNot as tipnot, nei.notaEva as notaeva" +
                    " FROM IndicadorAprendizaje ind" +
                    " INNER JOIN ind.indicadoresSesion isa" +
                    " INNER JOIN isa.sesion sa" +
                    " LEFT OUTER JOIN ind.notasEvaluacion nei" +
                    " WHERE sa.sesAprId =:idSesion and (nei.sesion.sesAprId =:idSesion or nei.sesion.sesAprId is null) and (nei.gradoEstudiante.graOrgEstId =:idGraEst or nei.gradoEstudiante.graOrgEstId is null)";
            Query query = session.createQuery(sql);
            query.setInteger("idSesion",idSes);
            query.setInteger("idGraEst",idGradAlumno);
            query.setResultTransformer(Transformers.aliasToBean(NotaIndicador.class));
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    

    @Override
    public NotaEvaluacionIndicador buscarNotaIndicadorEstudiante(int idSesion, int idInd,int idGraOrEst,char idPerido,int numPer) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql;Query query;
            if(idSesion == 0){
                  hql = "SELECT nei FROM NotaEvaluacionIndicador nei" +
                    " WHERE nei.indicador.indAprId =:indId" +
                    " AND nei.gradoEstudiante.graOrgEstId =:graId AND nei.periodo.perId =:idPer AND nei.numPer =:perNum";
                    query = session.createQuery(hql);
                    
            }else{
                  hql = "SELECT nei FROM NotaEvaluacionIndicador nei" +
                    " WHERE nei.sesion.sesAprId =:sesId AND nei.indicador.indAprId =:indId" +
                    " AND nei.gradoEstudiante.graOrgEstId =:graId AND nei.periodo.perId =:idPer AND nei.numPer =:perNum";
                   query = session.createQuery(hql);
                   query.setInteger("sesId",idSesion);
            }
          
            query.setInteger("indId",idInd);
            query.setInteger("graId",idGraOrEst);
            query.setCharacter("idPer", idPerido);
            query.setInteger("perNum",numPer);

            query.setMaxResults(1);
            return (NotaEvaluacionIndicador)query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public NotaEvaluacionIndicador buscarNotaPorId(int idNota) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT nei FROM NotaEvaluacionIndicador nei" +
                    " WHERE nei.notEvaIndId =:notaId";
            Query query = session.createQuery(hql);
            query.setInteger("notaId",idNota);
            query.setMaxResults(1);
            return (NotaEvaluacionIndicador)query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public Periodo buscarPeriodoId(char periodo) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT p FROM Periodo p" +
                    " WHERE p.perId =:perId";
            Query query = session.createQuery(hql);
            query.setCharacter("perId", periodo);

            query.setMaxResults(1);
            return (Periodo)query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public JSONArray buscarNotasxIndicadorxEstudiante(int idInd, int idGraOrEst, char idPeriodo, int numPer) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT nei.indicador.indAprId, nei.notaEva FROM NotaEvaluacionIndicador nei" +
                    " WHERE nei.indicador.indAprId =:indId" + 
                    " AND nei.gradoEstudiante.graOrgEstId =:graId" +
                    " AND nei.periodo.perId =:idPer AND nei.numPer =:perNum";
            Query query = session.createQuery(hql);
            query.setInteger("indId",idInd);
            query.setInteger("graId",idGraOrEst);
            query.setCharacter("idPer", idPeriodo);
            query.setInteger("perNum",numPer);
            
            List<Object[]> resultado = query.list();
            JSONArray notas = new JSONArray();
            
            for(Object[] r:resultado){
                JSONObject aux = new JSONObject();
                aux.put("indId", Integer.parseInt(r[0].toString()));
                aux.put("nota", r[1].toString());
                
                notas.put(aux);
            }

            return notas;
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public JSONArray buscarIndicadoresxSesionxEstudiante(int idSesion, int idGraOrEst, char idPeriodo, int numPer) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT nei.indicador.indAprId FROM NotaEvaluacionIndicador nei" +
                    " WHERE nei.sesion.sesAprId =:sesId AND nei.gradoEstudiante.graOrgEstId =:graId" +
                    " AND nei.periodo.perId =:idPer AND nei.numPer =:perNum";
            Query query = session.createQuery(hql);
            query.setInteger("sesId",idSesion);
            query.setInteger("graId",idGraOrEst);
            query.setCharacter("idPer", idPeriodo);
            query.setInteger("perNum",numPer);
            
            List<Integer> resultado = query.list();
            JSONArray indicadores = new JSONArray();
            
            for(int i=0 ; i<resultado.size();i++){
                JSONObject aux = new JSONObject();
                aux.put("indId", Integer.parseInt(resultado.get(i).toString()));
                indicadores.put(aux);
            }

            return indicadores;
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public Boolean buscarNota(int idSesion, int idInd, int idGraOrEst, char idPerido, int numPer) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Query query;String hql;
            if(idSesion == 0){
                     hql = "SELECT nei.notEvaIndId FROM NotaEvaluacionIndicador nei" +
                    " WHERE  nei.indicador.indAprId =:indId" +
                    " AND nei.gradoEstudiante.graOrgEstId =:graId AND nei.periodo.perId =:idPer AND nei.numPer =:perNum";
                    query = session.createQuery(hql);
            }else{
                    hql = "SELECT nei.notEvaIndId FROM NotaEvaluacionIndicador nei" +
                    " WHERE nei.sesion.sesAprId =:sesId AND nei.indicador.indAprId =:indId" +
                    " AND nei.gradoEstudiante.graOrgEstId =:graId AND nei.periodo.perId =:idPer AND nei.numPer =:perNum";
                    query = session.createQuery(hql);
                    query.setInteger("sesId",idSesion);
            }
            query.setInteger("indId",idInd);
            query.setInteger("graId",idGraOrEst);
            query.setCharacter("idPer", idPerido);
            query.setInteger("perNum",numPer);

            query.setMaxResults(1);
            
            Boolean resultado = false;
            
            if (query.uniqueResult() != null)
                resultado = true;
            
            return resultado;
            
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
    
    @Override
    public List<NotaEvaluacionIndicador> buscarNotaIndicadoresEstudiante(int idInd, int idGraOrEst, char idPerido, int numPer) {
           // Lista las Notas sin considerar la Sesion de Aprendizaje
        
         Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql;Query query;
            
                  hql = "SELECT nei FROM NotaEvaluacionIndicador nei" +
                    " WHERE nei.indicador.indAprId =:indId" +
                    " AND nei.gradoEstudiante.graOrgEstId =:graId AND nei.periodo.perId =:idPer AND nei.numPer =:perNum";
                    query = session.createQuery(hql);
                    query.setInteger("indId",idInd);
                    query.setInteger("graId",idGraOrEst);
                    query.setCharacter("idPer", idPerido);
                    query.setInteger("perNum",numPer);

            
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
}
