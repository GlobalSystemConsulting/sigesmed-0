/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author Yemi
 */
@Entity
@Table(name = "ficha_evaluacion", schema="pedagogico")
public class FichaEvaluacion implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "fic_eva_id")
    private Integer ficEvaId;
    
    @Size(max = 2147483647)
    @Column(name = "fic_eva_cod")
    private String ficEvaCod;
    
    @Column(name = "fic_eva_gra")
    private Short ficEvaGra;
    
    @Size(max = 2147483647)
    @Column(name = "fic_eva_are")
    private String ficEvaAre;
    
    @Column(name = "fic_eva_fec")
    @Temporal(TemporalType.DATE)
    private Date ficEvaFec;
    
    @Column(name = "fic_eva_pun")
    private Integer ficEvaPun;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.DATE)
    private Date fecMod;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Size(max = 1)
    @Column(name = "est_reg")
    private String estReg;
    
    @JoinColumn(name = "mon_det_id", referencedColumnName = "mon_det_id")
    @ManyToOne
    private MonitoreoDetalle monitoreoDetalle;
    
    @JoinColumn(name = "pla_fic_mon_id", referencedColumnName = "pla_fic_mon_id")
    @ManyToOne
    private PlantillaFichaMonitoreo plantillaFM;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fichaEvaluacion")
    private List<FichaEvaluacionDetalle> fichaEvaluacionDetalleList;
    
    @OneToMany(mappedBy = "fichaEvaluacion")
    private List<CompromisoAsumido> compromisosAsumidos;
    
    @OneToMany(mappedBy = "fichaEvaluacion")
    private List<ComentarioEvaluacion> comentariosEvaluacion;
    
    

    public FichaEvaluacion() {
    }

    public FichaEvaluacion(Integer ficEvaId) {
        this.ficEvaId = ficEvaId;
    }

    public Integer getFicEvaId() {
        return ficEvaId;
    }

    public void setFicEvaId(Integer ficEvaId) {
        this.ficEvaId = ficEvaId;
    }

    public String getFicEvaCod() {
        return ficEvaCod;
    }

    public void setFicEvaCod(String ficEvaCod) {
        this.ficEvaCod = ficEvaCod;
    }

    public Short getFicEvaGra() {
        return ficEvaGra;
    }

    public void setFicEvaGra(Short ficEvaGra) {
        this.ficEvaGra = ficEvaGra;
    }

    public String getFicEvaAre() {
        return ficEvaAre;
    }

    public void setFicEvaAre(String ficEvaAre) {
        this.ficEvaAre = ficEvaAre;
    }

    public Date getFicEvaFec() {
        return ficEvaFec;
    }

    public void setFicEvaFec(Date ficEvaFec) {
        this.ficEvaFec = ficEvaFec;
    }

    public Integer getFicEvaPun() {
        return ficEvaPun;
    }

    public void setFicEvaPun(Integer ficEvaPun) {
        this.ficEvaPun = ficEvaPun;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public List<FichaEvaluacionDetalle> getFichaEvaluacionDetalleList() {
        return fichaEvaluacionDetalleList;
    }

    public void setFichaEvaluacionDetalleList(List<FichaEvaluacionDetalle> fichaEvaluacionDetalleList) {
        this.fichaEvaluacionDetalleList = fichaEvaluacionDetalleList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ficEvaId != null ? ficEvaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FichaEvaluacion)) {
            return false;
        }
        FichaEvaluacion other = (FichaEvaluacion) object;
        if ((this.ficEvaId == null && other.ficEvaId != null) || (this.ficEvaId != null && !this.ficEvaId.equals(other.ficEvaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.FichaEvaluacion[ ficEvaId=" + ficEvaId + " ]";
    }
    
}
