/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.monitoreo_sesion_apredizaje.plantilla_ficha_monitoreo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.PlantillaFichaMonitoreoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.PlantillaFichaMonitoreo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class RegistrarPlantillaFMSATx implements ITransaction{

    private static final Logger logger = Logger.getLogger(RegistrarPlantillaFMSATx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        String plaNom = data.getString("plaNom");
        String plaCod = data.getString("plaCod");
        String plaDes = data.getString("plaDes");
        int orgId = data.getInt("orgId");
        int usuId = data.getInt("usuId");

        return registrarPlantilla(plaNom, plaCod, plaDes, orgId, usuId);
    }

    private WebResponse registrarPlantilla(String plaNom, String plaCod, String plaDes, int orgId, int usuId) {
        try{
            PlantillaFichaMonitoreoDao plaFicMonDao = (PlantillaFichaMonitoreoDao) FactoryDao.buildDao("sma.PlantillaFichaMonitoreoDao");

            PlantillaFichaMonitoreo plantilla = new PlantillaFichaMonitoreo();
            plantilla.setPlaFicMonNom(plaNom);
            plantilla.setPlaFicMonCod(plaCod);
            plantilla.setPlaFicMonDes(plaDes);
            plantilla.setOrganizacion(new Organizacion(orgId));
            plantilla.setFecMod(new Date());
            plantilla.setUsuMod(usuId);
            plantilla.setEstReg("A");
            plaFicMonDao.insert(plantilla);
            
            
            JSONObject oResponse = new JSONObject();
            oResponse.put("plaId", plantilla.getPlaFicMonId());
            oResponse.put("plaNom", plantilla.getPlaFicMonNom());
            oResponse.put("plaCod", plantilla.getPlaFicMonCod());
            oResponse.put("plaDes", plantilla.getPlaFicMonDes());
            oResponse.put("plaOrgId", orgId);

            return WebResponse.crearWebResponseExito("Se registro la plantilla correctamente",oResponse);
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarPlantilla",e);
            return WebResponse.crearWebResponseError("Error al registrar la plantilla");
        }
    }
    
}
