/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.monitoreo_sesion_apredizaje.plantilla_ficha_monitoreo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.ValoracionIndicadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.PlantillaFichaMonitoreo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.ValoracionIndicador;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class RegistrarValoracionIndicadorTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(RegistrarValoracionIndicadorTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        String valIndNom = data.getString("valIndNom");
        String valIndDes = data.getString("valIndDes");
        int valIndPun = data.getInt("valIndPun");
        int plaId = data.getInt("plaId");
        int usuId = data.getInt("usuId");

        return registrarValoracionInd(valIndNom, valIndDes, valIndPun, plaId, usuId);
    }

    private WebResponse registrarValoracionInd(String valIndNom, String valIndDes, int valIndPun, int plaId, int usuId) {
        try{
            ValoracionIndicadorDao valIndDao = (ValoracionIndicadorDao) FactoryDao.buildDao("sma.ValoracionIndicadorDao");

            ValoracionIndicador valoracionInd = new ValoracionIndicador();
            valoracionInd.setValIndNom(valIndNom);
            valoracionInd.setValIndDes(valIndDes);
            valoracionInd.setValIndPun(valIndPun);
            valoracionInd.setPlantillaFM(new PlantillaFichaMonitoreo(plaId));
            valoracionInd.setFecMod(new Date());
            valoracionInd.setUsuMod(usuId);
            valoracionInd.setEstReg("A");
            valIndDao.insert(valoracionInd);
            
            
            JSONObject oResponse = new JSONObject();
            oResponse.put("valIndId", valoracionInd.getValIndId());
            oResponse.put("valIndNom", valoracionInd.getValIndNom());
            oResponse.put("valIndDes", valoracionInd.getValIndDes());
            oResponse.put("valIndPun", valoracionInd.getValIndPun());
            oResponse.put("plaId", plaId);

            return WebResponse.crearWebResponseExito("Se registro el nivel de avance correctamente",oResponse);
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarValoracionIndicador",e);
            return WebResponse.crearWebResponseError("Error al registrar el nivel de avance");
        }
    }
    
}
