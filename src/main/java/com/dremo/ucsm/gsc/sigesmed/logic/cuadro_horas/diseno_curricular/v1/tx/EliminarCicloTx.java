/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.diseno_curricular.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.DisenoCurricularDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.CicloEducativo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author HernanF
 */
public class EliminarCicloTx implements ITransaction {
    
    @Override
    public WebResponse execute(WebRequest wr){
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int cicloID;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            cicloID = requestData.getInt("cicloID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el ciclo, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        DisenoCurricularDao disenoDao = (DisenoCurricularDao)FactoryDao.buildDao("mech.DisenoCurricularDao");
        
        try{
            CicloEducativo ciclo = new CicloEducativo(cicloID);
            disenoDao.eliminarCiclo(ciclo);
        }catch(Exception e){
            System.out.println("No se pudo eliminar el ciclo\n"+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el  ciclor", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El ciclo seleccionada se elimino correctamente");
        //Fin
    }
}
