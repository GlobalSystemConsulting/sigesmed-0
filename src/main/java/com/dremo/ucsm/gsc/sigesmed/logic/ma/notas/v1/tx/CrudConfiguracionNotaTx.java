/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.ma.notas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.ConfiguracionNotaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ConfiguracionNota;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author abel
 */
public class CrudConfiguracionNotaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        try{
             JSONObject data = (JSONObject)wr.getData();
             JSONObject equiDoc = data.getJSONObject("equivalDoc");
             int orgID = data.getInt("organizacionID");
             int docID = data.getInt("docenteID");
             int estado = data.optInt("estado");
             int not_min = equiDoc.getInt("not_min");
             int not_max = equiDoc.getInt("not_max");
             String cod = equiDoc.getString("cod");
             String des = equiDoc.getString("des");
             int conf_id = equiDoc.optInt("conf_id");
             return crearCrudConfiguracionNota(not_min,not_max,cod,des,conf_id,orgID,docID,estado);
            
        }catch(Exception e){
             return WebResponse.crearWebResponseError("Error al Leer los Datos");
        }
    }
    public WebResponse crearCrudConfiguracionNota(int not_min,int not_max,String cod,String des,int conf_id, int orgID ,int docID ,int estado){
            JSONObject oResponse = new JSONObject();
            ConfiguracionNotaDao configNot = (ConfiguracionNotaDao) FactoryDao.buildDao("ma.ConfiguracionNotaDao");
            ConfiguracionNota objetoConfiguracion = new ConfiguracionNota(conf_id,docID,cod,des,not_min,not_max,"A",docID,orgID);
            try{
                switch(estado){
                    case 1:
                        configNot.insert(objetoConfiguracion);oResponse.put("conf_id",objetoConfiguracion.getConf_not_id());
                    return  WebResponse.crearWebResponseExito("Se Registro correctamente la Configuracion de Nota Docente",oResponse); 
                    case 2:
                        configNot.update(objetoConfiguracion);
                    return  WebResponse.crearWebResponseExito("Se Edito correctamente la Configuracion de Nota Docente");  
                    case 3:
                        configNot.delete(objetoConfiguracion);
                    return  WebResponse.crearWebResponseExito("Se Elimino correctamente la Configuracion de Nota Docente"); 
                }
            }
            catch(Exception e){
                 return WebResponse.crearWebResponseError("No se pudo Hacer la Operacion Solicitada, datos incorrectos", e.getMessage() );
            }
        return null;

    }
}
    
    

