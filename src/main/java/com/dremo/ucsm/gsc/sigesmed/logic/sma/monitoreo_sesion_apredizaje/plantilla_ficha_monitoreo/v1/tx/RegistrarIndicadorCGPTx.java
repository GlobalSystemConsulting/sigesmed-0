/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.monitoreo_sesion_apredizaje.plantilla_ficha_monitoreo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.IndicadorCompromisoGestionPedagogicaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.IndicadorCompromisoGestionPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.InstruccionLlenadoIndicadores;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class RegistrarIndicadorCGPTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(RegistrarIndicadorCGPTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        String indDes = data.getString("indDes");
        int insId = data.getInt("insId");
        int usuId = data.getInt("usuId");

        return registrarInstruccionLI(indDes, insId, usuId);
    }

    private WebResponse registrarInstruccionLI(String indDes, int insId, int usuId) {
        try{
            IndicadorCompromisoGestionPedagogicaDao indComGesPedDao = (IndicadorCompromisoGestionPedagogicaDao) FactoryDao.buildDao("sma.IndicadorCompromisoGestionPedagogicaDao");

            IndicadorCompromisoGestionPedagogica indicador = new IndicadorCompromisoGestionPedagogica();
            indicador.setIndComGesPedDes(indDes);
            indicador.setInstruccionLI(new InstruccionLlenadoIndicadores(insId));
            indicador.setFecMod(new Date());
            indicador.setUsuMod(usuId);
            indicador.setEstReg("A");
            indComGesPedDao.insert(indicador);
            
            JSONObject oResponse = new JSONObject();
            oResponse.put("indId", indicador.getIndComGesPedId());
            oResponse.put("indDes", indicador.getIndComGesPedDes());
            oResponse.put("insId", insId);

            return WebResponse.crearWebResponseExito("Se registro el indicador de compromiso de gestión pedagógica correctamente",oResponse);
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarIndicadorGestionPedagogica",e);
            return WebResponse.crearWebResponseError("Error al registrar el indicador de compromiso de gestión pedagógica");
        }
    }
    
}
