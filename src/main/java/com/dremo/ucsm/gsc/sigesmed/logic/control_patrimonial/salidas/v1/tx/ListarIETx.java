/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.salidas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author harold
 */
public class ListarIETx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        JSONArray responseData = new JSONArray();
        
        JSONObject requestData = (JSONObject)wr.getData();
        int org_id = requestData.getInt("org_id");       
                
        try {
            
            OrganizacionDao organizacionDao = (OrganizacionDao) FactoryDao.buildDao("OrganizacionDao");

            Organizacion orgPadre = null;
            
            orgPadre = organizacionDao.buscarConTipoOrganizacionYPadre(org_id);
            
            int orgPadId = orgPadre.getOrganizacionPadre().getOrgId();
            //System.out.println("Organizacion Padre id: " + orgPadre.getOrganizacionPadre().getOrgId());
            List<Organizacion> organizaciones = null;
            organizaciones = organizacionDao.buscarHijosOrganizacion(orgPadId);
            
            for (Organizacion org : organizaciones) {
                
                //Para que no muestre la misma organizacion
                if (org.getOrgId() != org_id) {
                    JSONObject oRes = new JSONObject();
                    oRes.put("id", org.getOrgId());
                    oRes.put("nom", org.getNom());
                    responseData.put(oRes);
                }               
                
            }            
            
        } catch (Exception e) {
            System.out.println("No se pudo listar las organizaciones\n"+e);
            return WebResponse.crearWebResponseError("No se pudo listar las organizaciones", e.getMessage() );
        }
        
        return WebResponse.crearWebResponseExito("Se listo las organizaciones correctamente",responseData);
        
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
