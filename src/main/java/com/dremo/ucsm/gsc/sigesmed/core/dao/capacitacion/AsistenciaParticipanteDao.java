package com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.AsistenciaParticipante;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface AsistenciaParticipanteDao extends GenericDao<AsistenciaParticipante>{
    boolean existenRegistros(int codHor);
    List<AsistenciaParticipante> buscar(int perId, int codHor, Date tiempo);
    List<AsistenciaParticipante> buscarParticipantes(int codHor, Date tiempo);
    AsistenciaParticipante buscarPorId(int codAsi);
    List<AsistenciaParticipante> buscarPorHorarioParticipante(int perId, int codHor);
    List<AsistenciaParticipante> buscarPorSede(int codSed);
    int totalAsistencias(int codSed);
    int obtenerParaCertificacion(int codSed, int perId, Character tip);
    Map<Character, Integer> contarReporte(int codSed, int perId);
    List<Date> obtenerFechas(int codSed, int perId);
    Map<String, Character> obtenerRegistros(int codSed, int perId, Date fecReg);
    String contarHoras(int codSed, int perId);
}
