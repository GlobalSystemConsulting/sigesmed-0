/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;


@Entity
@Table(name = "compromiso_asumido", schema="pedagogico")
public class CompromisoAsumido implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "com_asu_id")
    private Integer comAsuId;
    
    @Column(name = "com_asu_fec")
    @Temporal(TemporalType.DATE)
    private Date comAsuFec;
    
    @Column(name = "com_asu_niv")
    private Short comAsuNiv;
    
    @Size(max = 2147483647)
    @Column(name = "com_asu_des")
    private String comAsuDes;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.DATE)
    private Date fecMod;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Size(max = 1)
    @Column(name = "est_reg")
    private String estReg;
    
    @JoinColumn(name = "fic_eva_id", referencedColumnName = "fic_eva_id")
    @ManyToOne
    private FichaEvaluacion fichaEvaluacion;

    public CompromisoAsumido() {
    }

    public CompromisoAsumido(Integer comAsuId) {
        this.comAsuId = comAsuId;
    }

    public Integer getComAsuId() {
        return comAsuId;
    }

    public void setComAsuId(Integer comAsuId) {
        this.comAsuId = comAsuId;
    }

    public Date getComAsuFec() {
        return comAsuFec;
    }

    public void setComAsuFec(Date comAsuFec) {
        this.comAsuFec = comAsuFec;
    }

    public Short getComAsuNiv() {
        return comAsuNiv;
    }

    public void setComAsuNiv(Short comAsuNiv) {
        this.comAsuNiv = comAsuNiv;
    }

    public String getComAsuDes() {
        return comAsuDes;
    }

    public void setComAsuDes(String comAsuDes) {
        this.comAsuDes = comAsuDes;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (comAsuId != null ? comAsuId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CompromisoAsumido)) {
            return false;
        }
        CompromisoAsumido other = (CompromisoAsumido) object;
        if ((this.comAsuId == null && other.comAsuId != null) || (this.comAsuId != null && !this.comAsuId.equals(other.comAsuId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.CompromisoAsumido[ comAsuId=" + comAsuId + " ]";
    }
    
}
