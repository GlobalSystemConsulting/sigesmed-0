/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.plantilla_grupo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.TipoGrupoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.TipoGrupo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ListarGruposTx implements ITransaction{
    public WebResponse execute(WebRequest wr) {        
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        
//        JSONObject requestData = (JSONObject)wr.getData();
//        int orgId = requestData.getInt("orgId");
                
        List<TipoGrupo> tipos = null;
        TipoGrupoDao tiposDao = (TipoGrupoDao)FactoryDao.buildDao("smdg.TipoGrupoDao");
        
        try{
            tipos = tiposDao.buscarTodos(TipoGrupo.class);        
        }catch(Exception e){            
            return WebResponse.crearWebResponseError("No se pudo Listar las tipos", e.getMessage() );
        }
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(TipoGrupo t:tipos ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("tpoId",t.getTipId());
            oResponse.put("tpoDes",t.getTipNom());            
            miArray.put(oResponse);
        }        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);                        
    }
}
