/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.TipoEspecializadoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.TipoEspecializado;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ReporteTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        FileJsonObject miGrafico = null;
        int orgID=0;
        JSONArray data=null;
        JSONArray etiquetas=null;
        int usuID=0;       
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            miGrafico = new FileJsonObject( requestData.optJSONObject("grafico")  );
            orgID= requestData.getInt("orgID");
            usuID=requestData.getInt("usuID");
            data=( requestData.getJSONArray("data") );
            etiquetas =( requestData.getJSONArray("etiquetas") );
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo realizar el reporte, grafico iicorrecto", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Repuesta Correcta
        */
        OrganizacionDao  orgDao= (OrganizacionDao) FactoryDao.buildDao("OrganizacionDao");  
        Organizacion org=orgDao.buscarConTipoOrganizacionYPadre(orgID);
        
        Persona persona = ((PersonaDao)FactoryDao.buildDao("PersonaDao")).buscarPorID(usuID);
        
        
        JSONObject response = new JSONObject();
        JSONObject temp=new JSONObject();
        temp.put("Institución ", org.getNom());
        temp.put("Director ", persona.getNom()+" "+persona.getApePat()+" "+persona.getApeMat());
        float[] cols = {9f,1f};
        String[] labels={"Tipo Documento","Num. Documentos","Version"};
        
        GTabla t=new GTabla(cols);
        try {
            t.build(labels);
        } catch (IOException ex) {
            Logger.getLogger(ReporteTx.class.getName()).log(Level.SEVERE, null, ex);
        }
        TipoEspecializadoDao tipDao= (TipoEspecializadoDao)FactoryDao.buildDao("rdg.TipoEspecializadoDao");    
        for(int i=0;i<data.length() ;i++){
            String []fila=new String[2];
            TipoEspecializado tip=tipDao.getByAlias(etiquetas.get(i).toString());
            fila[0]= "" + tip.getTesNom() +" - " +tip.getTesAli();
            fila[1]= "" + data.get(i);
            t.processLine(fila);
        }
        try {
            Mitext r = new Mitext();
            r.agregarTitulo(miGrafico.getName());
            r.agregarSubtitulos(temp);
            r.agregarImagen64(miGrafico.getData());
            r.agregarTabla(t);
            r.cerrarDocumento();
            response.append("reporte", r.encodeToBase64() );
        } catch (Exception ex) {
        }
        
        
        return WebResponse.crearWebResponseExito("No se pudo realizar el reporte",response);
        //Fin
    }
}