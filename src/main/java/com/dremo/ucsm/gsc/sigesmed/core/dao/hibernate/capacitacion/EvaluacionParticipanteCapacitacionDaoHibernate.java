package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.EvaluacionParticipanteCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.EvaluacionParticipanteCapacitacion;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class EvaluacionParticipanteCapacitacionDaoHibernate extends GenericDaoHibernate<EvaluacionParticipanteCapacitacion> implements EvaluacionParticipanteCapacitacionDao {
    
    private static final Logger logger = Logger.getLogger(EvaluacionParticipanteCapacitacionDaoHibernate.class.getName());
    
    @Override
    public EvaluacionParticipanteCapacitacion buscarUltima(int codEva, int perId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            Criteria query = session.createCriteria(EvaluacionParticipanteCapacitacion.class)
                    .add(Restrictions.eq("evaCurCapId", codEva))
                    .add(Restrictions.eq("perId", perId))
                    .addOrder(Order.desc("fecApl"))
                    .setMaxResults(1);

            return (EvaluacionParticipanteCapacitacion) query.uniqueResult();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarUltima", e);
            throw e;
        } finally {
            session.close();
        }
    }
    
    @Override
    public int contarEvaluaciones(int codEva, int perId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            Criteria query = session.createCriteria(EvaluacionParticipanteCapacitacion.class)
                    .add(Restrictions.eq("evaCurCapId", codEva))
                    .add(Restrictions.eq("perId", perId))
                    .setProjection(Projections.rowCount());

            return ((Long) query.uniqueResult()).intValue();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "contarEvaluaciones", e);
            throw e;
        } finally {
            session.close();
        }
    }
    
    @Override
    public List<Integer> listarParticipantes(int codEva) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            Criteria query = session.createCriteria(EvaluacionParticipanteCapacitacion.class)
                    .add(Restrictions.eq("evaCurCapId", codEva))
                    .setProjection(Projections.property("perId"))
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            
            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarParticipantes", e);
            throw e;
        } finally {
            session.close();
        }
    }
    
    @Override
    public List<Double> obtenerParaCertificacion(int codSed, int perId, double notMin) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            String hql = "SELECT a.notObt FROM EvaluacionParticipanteCapacitacion a "
                            + "WHERE (a.evaCurCapId, a.fecApl) IN " +
                                "(SELECT e.evaCurCapId, MAX(e.fecApl) FROM EvaluacionParticipanteCapacitacion e " +
                                "WHERE e.estReg = :estReg AND e.perId = :perId AND e.notObt >= :notObt AND e.evaCurCapId IN " +
                                    "(SELECT c.evaCurCapId FROM EvaluacionCursoCapacitacion c "
                                        + "INNER JOIN c.sede s "
                                        + "WHERE s.sedCapId = :sedCapId AND c.tip = :tip) " +
                                " GROUP BY e.evaCurCapId)";
            
            Query query = session.createQuery(hql);
            query.setParameter("sedCapId", codSed);            
            query.setParameter("estReg", 'C');            
            query.setParameter("notObt", notMin);
            query.setParameter("tip", 'P');            
            query.setParameter("perId", perId);
                                    
            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "obtenerParaCertificacion", e);
            throw e;
        } finally {
            session.close();
        }
    }
}
