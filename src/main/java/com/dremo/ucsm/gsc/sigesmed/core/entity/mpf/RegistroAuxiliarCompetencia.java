package com.dremo.ucsm.gsc.sigesmed.core.entity.mpf;


import javax.persistence.*;
import java.util.Date;

/**
 * @author Carlos
 */
@Entity(name="RegistroAuxiliarCompetenciaMpf")
@Table(name = "registro_auxiliar_competencia", schema = "pedagogico")
public class RegistroAuxiliarCompetencia implements java.io.Serializable {
    @Id
    @Column(name = "reg_aux_com_id",nullable = false, unique = true)
    @SequenceGenerator(name = "registro_auxiliar_competencia_reg_aux_com_id_seq", sequenceName = "pedagogico.registro_auxiliar_competencia_reg_aux_com_id_seq")
    @GeneratedValue(generator = "registro_auxiliar_competencia_reg_aux_com_id_seq")
    private int regAuxComId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gra_ie_est_id")
    private GradoIeEstudiante gradoEst;
    
   
    
    @Column(name = "not_com",length = 2)
    private String nota;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length=29)
    private Date fecMod;
    @Column(name = "est_reg",length = 1)
    private Character estReg;

    public RegistroAuxiliarCompetencia() {
        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public RegistroAuxiliarCompetencia(String nota) {
        this.nota = nota;
        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public int getRegAuxComId() {
        return regAuxComId;
    }

    public void setRegAuxComId(int regAuxComId) {
        this.regAuxComId = regAuxComId;
    }

    public GradoIeEstudiante getGradoEst() {
        return gradoEst;
    }

    public void setGradoEst(GradoIeEstudiante gradoEst) {
        this.gradoEst = gradoEst;
    }

    

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
}
