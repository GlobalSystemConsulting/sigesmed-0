/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.inventario_transferencia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.InventarioTransferenciaDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.InventarioTransferencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.DetalleInventarioTransferencia;


import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.text.SimpleDateFormat;

/**
 *
 * @author admin
 */
public class EditarInventarioTransferenciaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        InventarioTransferencia it = null;
        DetalleInventarioTransferencia dit = null;
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date fecha ;
        
        
        
        try{
           JSONObject requestData = (JSONObject)wr.getData();
           fecha = formatter.parse(requestData.getString("fec"));
           
           int inv_tra_id = requestData.getInt("inv_tra_id");
           int ser_doc_id = requestData.getInt("ser_doc_id");
           String cod = requestData.getString("cod");
         //  String res = requestData.getString("res");
           char esta = 'A';
          it = new InventarioTransferencia(inv_tra_id,ser_doc_id,cod,fecha,esta); 
          it.setInvTransDet(new ArrayList<DetalleInventarioTransferencia>()); 
          
            /*DATA INVENTARIO TRANSFERENCIA DETALLE*/
                    String asunto = requestData.getString("asu");
                    int met_lin = requestData.getInt("met_lin");
                  //  int res_con =0; 

                 //   int loc = requestData.getInt("loc");
                    String alm = requestData.getString("alm");
                    int est = requestData.getInt("est");
                    int bal = requestData.getInt("bal");
                    int fil = requestData.getInt("fil");
                    int cue = requestData.getInt("cue");
                    int caj = requestData.getInt("caj");
                    int det_inv_tra = requestData.getInt("det_inv_trans");  
                    
                    dit = new DetalleInventarioTransferencia(det_inv_tra,it ,ser_doc_id,fecha,asunto,met_lin,alm,est,bal,fil,cue,caj);
                    it.getInvTransDet().add(dit);
          
          
          
        }catch(Exception e){
             System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo editar el Inventario de Transferencia , datos incorrectos", e.getMessage() ); 
        }
           InventarioTransferenciaDAO in_tra_dao = (InventarioTransferenciaDAO)FactoryDao.buildDao("sad.InventarioTransferenciaDAO");
        try{
            in_tra_dao.update(it);
        }catch(Exception e){
             System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo editar el Inventario de Transferencia ", e.getMessage() );
        }   
        return  WebResponse.crearWebResponseExito("El Inventario de Transferencia se pudo editar correctamente");
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
