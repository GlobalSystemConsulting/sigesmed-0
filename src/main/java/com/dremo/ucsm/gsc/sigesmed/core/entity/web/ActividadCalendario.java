package com.dremo.ucsm.gsc.sigesmed.core.entity.web;

import com.dremo.ucsm.gsc.sigesmed.core.entity.FuncionSistema;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "actividad_calendario", schema = "pedagogico")
public class ActividadCalendario implements Serializable {

    @Id
    @Column(name = "act_cal_id", unique = true, nullable = false)
    @SequenceGenerator(name = "secuencia_actividad_calendario", sequenceName = "pedagogico.actividad_calendario_act_cal_id_seq")
    @GeneratedValue(generator = "secuencia_actividad_calendario")
    private int actCalId;

    @Column(name = "tit", nullable = false)
    private String tit;

    @Column(name = "des", nullable = false)
    private String des;

    @Column(name = "tip_act", nullable = false)
    private char tipAct;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_ini", nullable = false)
    private Date fecIni;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_fin", nullable = false)
    private Date fecFin;

    @Column(name = "est_reg", nullable = false)
    private char estReg;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usu_ses_id")
    private UsuarioSession usuario;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "act_pad_id")
    private ActividadCalendario actividadPadre;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fun_sis_id")
    private FuncionSistema funcion;

    @Column(name = "not_ini")
    private boolean notIni;

    @Column(name = "not_fin")
    private boolean notFin;

    public ActividadCalendario() {
    }
    
    public ActividadCalendario(ActividadCalendario calendar) {
        this.tit = calendar.tit;
        this.des = calendar.des;
        this.tipAct = calendar.tipAct;
        this.fecIni = calendar.fecIni;
        this.fecFin = calendar.fecFin;
        this.estReg = 'A';
        this.notIni = calendar.notIni;
        this.notFin = calendar.notFin;
    }
    
    public ActividadCalendario(String tit, String des, char tipAct, Date fecIni, Date fecFin) {
        this.tit = tit;
        this.des = des;
        this.tipAct = tipAct;
        this.fecIni = fecIni;
        this.fecFin = fecFin;
        this.estReg = 'A';
        this.notIni = false;
        this.notFin = false;
    }

    public int getActCalId() {
        return actCalId;
    }

    public void setActCalId(int actCalId) {
        this.actCalId = actCalId;
    }

    public String getTit() {
        return tit;
    }

    public void setTit(String tit) {
        this.tit = tit;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public char getTipAct() {
        return tipAct;
    }

    public void setTipAct(char tipAct) {
        this.tipAct = tipAct;
    }

    public Date getFecIni() {
        return fecIni;
    }

    public void setFecIni(Date fecIni) {
        this.fecIni = fecIni;
    }

    public Date getFecFin() {
        return fecFin;
    }

    public void setFecFin(Date fecFin) {
        this.fecFin = fecFin;
    }

    public char getEstReg() {
        return estReg;
    }

    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }

    public UsuarioSession getUsuario() {
        return usuario;
    }

    public void setUsuario(int usuSesId) {
        this.usuario = new UsuarioSession(usuSesId);
    }

    public ActividadCalendario getActividadPadre() {
        return actividadPadre;
    }

    public void setActividadPadre(ActividadCalendario actividadPadre) {
        this.actividadPadre = actividadPadre;
    }

    public FuncionSistema getFuncion() {
        return funcion;
    }

    public void setFuncion(int funSisId) {
        this.funcion = new FuncionSistema(funSisId);
    }

    public boolean isNotIni() {
        return notIni;
    }

    public void setNotIni(boolean notIni) {
        this.notIni = notIni;
    }

    public boolean isNotFin() {
        return notFin;
    }

    public void setNotFin(boolean notFin) {
        this.notFin = notFin;
    }
}
