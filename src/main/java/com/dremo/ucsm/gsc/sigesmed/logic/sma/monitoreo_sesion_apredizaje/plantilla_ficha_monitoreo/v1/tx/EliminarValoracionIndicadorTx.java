/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.monitoreo_sesion_apredizaje.plantilla_ficha_monitoreo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.ValoracionIndicadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.ValoracionIndicador;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class EliminarValoracionIndicadorTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(EliminarValoracionIndicadorTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject jsonData = (JSONObject)wr.getData();
        int valIndId = jsonData.getInt("valIndId");
        return eliminarValoracionInd(valIndId);
    }

    private WebResponse eliminarValoracionInd(int valIndId) {
        try{
            ValoracionIndicadorDao valIndDao = (ValoracionIndicadorDao) FactoryDao.buildDao("sma.ValoracionIndicadorDao");
            ValoracionIndicador valInd = valIndDao.buscarPorId(valIndId);
            valIndDao.delete(valInd);
            return WebResponse.crearWebResponseExito("Se elimino correctamente el nivel de avance");

        }catch (Exception e){
            logger.log(Level.SEVERE,"eliminarValoracionIndicador",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el nivel de avance");
        }
    }
    
}
