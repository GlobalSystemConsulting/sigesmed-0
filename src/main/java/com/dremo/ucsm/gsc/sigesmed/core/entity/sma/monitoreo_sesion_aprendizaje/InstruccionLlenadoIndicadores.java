/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

@Entity
@Table(name = "instruccion_llenado_indicadores", schema="pedagogico")
public class InstruccionLlenadoIndicadores implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ins_lle_ind_id")
    private Integer insLleIndId;
    
    @Size(max = 2147483647)
    @Column(name = "ins_lle_ind_des")
    private String insLleIndDes;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Size(max = 1)
    @Column(name = "est_reg")
    private String estReg;
    
    @JoinColumn(name = "com_ges_ped_id", referencedColumnName = "com_ges_ped_id")
    @ManyToOne
    private CompromisoGestionPedagogica compromisoGP;
    
    @OneToMany(mappedBy = "instruccionLI")
    private List<IndicadorCompromisoGestionPedagogica> indicadoresCGP;

    public InstruccionLlenadoIndicadores() {
    }

    public InstruccionLlenadoIndicadores(Integer insLleIndId) {
        this.insLleIndId = insLleIndId;
    }

    public Integer getInsLleIndId() {
        return insLleIndId;
    }

    public void setInsLleIndId(Integer insLleIndId) {
        this.insLleIndId = insLleIndId;
    }

    public String getInsLleIndDes() {
        return insLleIndDes;
    }

    public void setInsLleIndDes(String insLleIndDes) {
        this.insLleIndDes = insLleIndDes;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public CompromisoGestionPedagogica getCompromisoGP() {
        return compromisoGP;
    }

    public void setCompromisoGP(CompromisoGestionPedagogica compromisoGP) {
        this.compromisoGP = compromisoGP;
    }

    public List<IndicadorCompromisoGestionPedagogica> getIndicadoresCGP() {
        return indicadoresCGP;
    }

    public void setIndicadoresCGP(List<IndicadorCompromisoGestionPedagogica> indicadoresCGP) {
        this.indicadoresCGP = indicadoresCGP;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (insLleIndId != null ? insLleIndId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InstruccionLlenadoIndicadores)) {
            return false;
        }
        InstruccionLlenadoIndicadores other = (InstruccionLlenadoIndicadores) object;
        if ((this.insLleIndId == null && other.insLleIndId != null) || (this.insLleIndId != null && !this.insLleIndId.equals(other.insLleIndId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.InstruccionLlenadoIndicadores[ insLleIndId=" + insLleIndId + " ]";
    }
    
}
