/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sad;


import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Jeferson
 */

 @Entity
 @Table(name="inventario_transferencia" , schema="administrativo")
public class InventarioTransferencia implements java.io.Serializable {
   
    @Id
    @Column(name="inv_tra_id" , unique=true, nullable=false)
    @SequenceGenerator(name="secuencia_inv_tra",sequenceName="administrativo.inventario_transferencia_inv_tra_id_seq")
    @GeneratedValue(generator="secuencia_inv_tra")
    private int inv_tra_id;
    
    @Id
    private int ser_doc_id;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ser_doc_id" , insertable=false , updatable=false)
    private SerieDocumental serie;
    
    @Column(name="cod")
    private String codigo;
    
    @Column(name="fec")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fec;
    
    @Column(name="est_reg")
    private char estReg;
    
    @OneToMany(mappedBy="inv_tra" , cascade= CascadeType.ALL )
    private List<PrestamoSerieDocumental> prestamos_serie_documental;
    
    @OneToMany(mappedBy="inv_tra" , cascade= CascadeType.ALL)
    private List<DetalleInventarioTransferencia> inv_tra_det;
    
    
    public InventarioTransferencia(){
        
    }
    public InventarioTransferencia(int inv_id){
        this.inv_tra_id = inv_id;
    }
    public InventarioTransferencia(int inv_id,int ser_doc_id){
        this.inv_tra_id = inv_id;
        this.ser_doc_id = ser_doc_id;
        
        serie = new SerieDocumental(ser_doc_id);
    }
    public InventarioTransferencia(int inv_id,int ser_doc_id, String cod , Date fec  , char est){
        this.inv_tra_id = inv_id;
        this.ser_doc_id = ser_doc_id;
        this.codigo = cod;
        this.fec = fec;
       
        this.estReg = est;
        serie = new SerieDocumental(ser_doc_id);
    }
    // Para Editar el Inventario permaneciendo la fecha de dicho inventario
    public InventarioTransferencia(int inv_id,int ser_doc_id, String cod   ,char est){
        this.inv_tra_id = inv_id;
        this.ser_doc_id = ser_doc_id;
        this.codigo = cod;
        this.estReg = est;
        
        serie = new SerieDocumental(ser_doc_id);
    }
    
    public void setInvTransId(int inv_tra_id){
        this.inv_tra_id = inv_tra_id;  
    }
    public int getInvTransId(){
        return this.inv_tra_id;
    }
    public void setSerieDocumental(SerieDocumental serie){
        this.serie = serie;
    }
    public SerieDocumental getSerieDocumental(){
        return this.serie;
    }
    
    public void setCodigo(String cod){
        this.codigo = cod;
    }
    
    public String getCodigo(){
       return this.codigo;
    }
    
    public void setFecha(Date fec){
        this.fec = fec;
    }
    public Date getFecha(){
        return this.fec;
    }
   
    public void setPresSerDoc(List<PrestamoSerieDocumental> pres_ser_doc){
        this.prestamos_serie_documental = pres_ser_doc;
    }
    public List<PrestamoSerieDocumental> getPresSerDoc(){
        return this.prestamos_serie_documental;
    }
    
    public void setInvTransDet(List<DetalleInventarioTransferencia> det_inv_trans){
        this.inv_tra_det = det_inv_trans;
    }
    public List<DetalleInventarioTransferencia> getInvTransDet(){
        return this.inv_tra_det;
    }
    
    public int getSerDocId(){
        return this.ser_doc_id;
    }

    public char getEstado() {
        return estReg;
    }

    public void setEstado(char estado) {
        this.estReg = estado;
    }
    
    
    
}
