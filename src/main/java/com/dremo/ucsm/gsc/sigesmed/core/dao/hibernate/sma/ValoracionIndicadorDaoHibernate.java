/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.ValoracionIndicadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.ValoracionIndicador;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class ValoracionIndicadorDaoHibernate extends GenericDaoHibernate<ValoracionIndicador> implements ValoracionIndicadorDao{

    @Override
    public List<ValoracionIndicador> listarPorPlaId(int plaId) {
        List<ValoracionIndicador> valoracionesIndicador = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            String hql = "SELECT vi from ValoracionIndicador as vi "
                    + "INNER JOIN vi.plantillaFM pl "
                    + "WHERE pl.plaFicMonId=:plaCod AND vi.estReg='A'"; 
            Query query = session.createQuery(hql); 
            query.setInteger("plaCod",plaId);
            valoracionesIndicador = query.list();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los niveles de avance de la plantilla. Error: "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los niveles de avance de la plantilla. Error: "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return valoracionesIndicador;
    }

    @Override
    public ValoracionIndicador buscarPorId(int valIndId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        ValoracionIndicador vi = (ValoracionIndicador)session.get(ValoracionIndicador.class, valIndId);
        session.close();
        return vi;
    }
    
}
