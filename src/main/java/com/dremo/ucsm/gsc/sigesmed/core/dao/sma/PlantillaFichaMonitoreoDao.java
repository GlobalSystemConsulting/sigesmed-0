/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.sma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.CompromisoGestionPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.ConfiguracionPlantilla;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.IndicadorCompromisoGestionPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.InstruccionLlenadoIndicadores;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.PlantillaFichaMonitoreo;
import java.util.List;

public interface PlantillaFichaMonitoreoDao extends GenericDao<PlantillaFichaMonitoreo>{
    List<PlantillaFichaMonitoreo> listarPlantillasPorIE(int orgId);
    List<ConfiguracionPlantilla> listarConfiguracionCarpeta(int plaId);
    PlantillaFichaMonitoreo buscarPorId(int plaFicMonId);
    List<CompromisoGestionPedagogica> listarCompromisos(int plaId);
    List<IndicadorCompromisoGestionPedagogica> listarIndicadores(int insId);
}
