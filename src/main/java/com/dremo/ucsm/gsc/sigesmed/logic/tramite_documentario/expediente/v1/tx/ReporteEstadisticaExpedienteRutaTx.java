/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.std.HistorialExpedienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.HistorialExpediente;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author G-16
 */
public class ReporteEstadisticaExpedienteRutaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int expedienteID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            expedienteID = requestData.getInt("expedienteID");
            
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar el historial del expediente, datos incorrectos", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<HistorialExpediente> historial = null;
        
        HistorialExpedienteDao historialDao = (HistorialExpedienteDao)FactoryDao.buildDao("std.HistorialExpedienteDao");
        try{
            historial =historialDao.buscarPorExpedienteCodigo(expedienteID);
            
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar el historial ", e.getMessage() );
        }
        //Fin
        
        
        JSONArray aHistoriales = new JSONArray();
        for(HistorialExpediente h: historial){
            JSONObject oHistorial = new JSONObject();
            oHistorial.put("historialID",h.getHisExpId());
            oHistorial.put("codigoExp",h.getExpediente().getCodigo());
          
            oHistorial.put("areaID",h.getAreaId());
            oHistorial.put("areae",h.getArea().getNom());
            
            oHistorial.put("estadoID",h.getEstado().getEstExpId());
            oHistorial.put("estadoe",h.getEstado().getNom());
            
            oHistorial.put("responsableID",h.getResId());
            oHistorial.put("responsable",h.getResponsable().getNombrePersona());
            
            oHistorial.put("fechaIngreso",h.getFecEnv());
            oHistorial.put("fechaSalida",h.getFecAte());
            
            
            aHistoriales.put(oHistorial);
        }
        
      
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",aHistoriales);
    }
    
}
