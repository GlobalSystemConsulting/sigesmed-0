/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.monitoreo_sesion_apredizaje.plantilla_ficha_monitoreo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.ValoracionIndicadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.ValoracionIndicador;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class ListarValoracionesIndicadorTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ListarValoracionesIndicadorTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject requestData = (JSONObject) wr.getData();
        int plaId = requestData.getInt("plaId");
        return listar(plaId);
    }
    private WebResponse listar(int plaId){
        ValoracionIndicadorDao valIndDao = (ValoracionIndicadorDao) FactoryDao.buildDao("sma.ValoracionIndicadorDao");
        List<ValoracionIndicador> valoracionesInd = new ArrayList<ValoracionIndicador>();
        try{
            valoracionesInd = valIndDao.listarPorPlaId(plaId);
        }catch (Exception e){
            logger.log(Level.SEVERE,"Error al listar los niveles de avance",e);
            return WebResponse.crearWebResponseError("Error al listar los niveles de avance");
        }
        
        JSONArray miArray = new JSONArray();
        for(ValoracionIndicador vi:valoracionesInd ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("valIndId",vi.getValIndId());
            oResponse.put("valIndNom",vi.getValIndNom());
            oResponse.put("valIndDes",vi.getValIndDes());
            oResponse.put("valIndPun",vi.getValIndPun());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se listo correctamente",miArray);
    }
    
}
