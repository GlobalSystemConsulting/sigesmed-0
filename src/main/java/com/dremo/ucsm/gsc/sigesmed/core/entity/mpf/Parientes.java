/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.mpf;

/**
 *
 * @author carlos
 */
// Generated 13/02/2017 02:15:53 PM by Hibernate Tools 4.3.1
import java.util.Date;
import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name = "com.dremo.ucsm.gsm.sigesmed.core.entity.mpf.Parientes")
@Table(name = "parientes", schema = "public")
public class Parientes implements java.io.Serializable {

    @Id
    @Column(name = "par_id", unique = true, nullable = false)
    private Integer personaByParId;

    @ManyToOne()
    @JoinColumn(name = "per_id", nullable = false, insertable = false, updatable = false)
    private Persona personaByPerId;

    @Column(name = "par_viv")
    private Boolean parViv;

    @Column(name = "par_viv_est")
    private Boolean parVivEst;

    @Column(name = "usu_mod")
    private Integer usuMod;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length = 29)
    private Date fecMod;

    @Column(name = "est_reg", length = 1)
    private String estReg;

    public Parientes() {
    }

    public Persona getPersonaByPerId() {
        return this.personaByPerId;
    }

    public void setPersonaByPerId(Persona personaByPerId) {
        this.personaByPerId = personaByPerId;
    }

    public Integer getPersonaByParId() {
        return this.personaByParId;
    }

    public void setPersonaByParId(Integer personaByParId) {
        this.personaByParId = personaByParId;
    }

    public Boolean getParViv() {
        return this.parViv;
    }

    public void setParViv(Boolean parViv) {
        this.parViv = parViv;
    }

    public Boolean getParVivEst() {
        return this.parVivEst;
    }

    public void setParVivEst(Boolean parVivEst) {
        this.parVivEst = parVivEst;
    }

    public Integer getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return this.fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public String getEstReg() {
        return this.estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

}
