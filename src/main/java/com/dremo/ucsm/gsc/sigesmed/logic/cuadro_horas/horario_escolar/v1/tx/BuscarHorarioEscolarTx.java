/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.horario_escolar.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.HorarioEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.HorarioDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.HorarioEscolar;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
/**
 *
 * @author abel
 */
public class BuscarHorarioEscolarTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int planNivelID = 0;
        int gradoID = 0;
        char seccionID = ' ';
        try{
            
            JSONObject r = (JSONObject)wr.getData();
            
            planNivelID = r.getInt("planNivelID");
            gradoID = r.getInt("gradoID");
            seccionID = r.getString("seccionID").charAt(0);
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar el Horario Escolar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        HorarioEscolar horario = null;
        try{
            HorarioEscolarDao horarioDao = (HorarioEscolarDao)FactoryDao.buildDao("mech.HorarioEscolarDao");
            
             horario = horarioDao.buscarHorario(planNivelID, gradoID, seccionID);
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar el Horario Escolar", e.getMessage() );
        }
        //Fin
        
        if(horario != null){
        
            JSONObject res = new JSONObject();
            res.put("horarioID", horario.getHorarioId());
            res.put("horaInicio", horario.getHorIni());
            res.put("horaFin", horario.getHorFin());
            res.put("planNivelID", horario.getPlaNivId());
            res.put("gradoID", horario.getGraId());
            res.put("seccionID", horario.getSecId());

            JSONArray aDet = new JSONArray();
            for(HorarioDetalle d : horario.getDetalles() ){
                JSONObject oDet = new JSONObject();
                oDet.put("horarioDetalleID", d.getHorDetId() );
                oDet.put("horaInicio", d.getHorIni());
                oDet.put("horaI", d.getHorIni().getHours());
                oDet.put("minutoI", d.getHorIni().getMinutes());
                oDet.put("horaFin", d.getHorFin());
                oDet.put("horaF", d.getHorFin().getHours());
                oDet.put("minutoF", d.getHorFin().getMinutes());
                oDet.put("aulaID", d.getAulId() );
                oDet.put("areaID", d.getAreCurId() );
                oDet.put("diaID", ""+d.getDisId() );
                oDet.put("plazaID", d.getPlaMagId() );
                
                aDet.put(oDet);
            }
            res.put("detalle",aDet);

            return WebResponse.crearWebResponseExito("Se encontro el Horario Escolar",res);
        }
        else{
            return WebResponse.crearWebResponseError("No se existe un horario para esta seccion");
        }
    }
}