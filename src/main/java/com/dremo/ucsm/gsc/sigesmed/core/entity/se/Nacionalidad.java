/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author felipe
 */
@Entity
@Table(name = "nacionalidad",schema="administrativo")
public class Nacionalidad implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "nac_id")
    private int nacId;
    
    @Size(max = 2147483647)
    @Column(name = "nac_nom")
    private String nacNom;
    
    @Column(name = "est_reg")
    private char estReg;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "des")
    private String des;
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "nacionalidad")
    List<Persona> persona;

    public Nacionalidad() {
    }

    public Nacionalidad(int nacId) {
        this.nacId = nacId;
    }

    public int getNacId() {
        return nacId;
    }

    public void setNacId(int nacId) {
        this.nacId = nacId;
    }

    public String getNacNom() {
        return nacNom;
    }

    public void setNacNom(String nacNom) {
        this.nacNom = nacNom;
    }

    public char getEstReg() {
        return estReg;
    }

    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public List<Persona> getPersona() {
        return persona;
    }

    public void setPersona(List<Persona> persona) {
        this.persona = persona;
    }
    
    //INICIO -usado para el catalogo
    public int getId(){
        return this.nacId;
    }
    public void setId(int Id){
        this.nacId=Id;
    }
    
    public String getNom() {
        return this.nacNom;
    }
    public void setNom(String nom) {
        this.nacNom = nom;
    }

    public String getDes() {
        return this.des;
    }
    public void setDes(String des) {
        this.des = des;
    }
    //FIN -usado para el catalogo
    

    @Override
    public String toString() {
        return "com.dremo.ucsm.gsc.sigesmed.core.entity.se.Nacionalidad[ nacId=" + nacId + " ]";
    }
    
}
