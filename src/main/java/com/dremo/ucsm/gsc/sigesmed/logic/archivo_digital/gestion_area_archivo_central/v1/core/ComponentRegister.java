/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.gestion_area_archivo_central.v1.core;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;

import com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.gestion_area_archivo_central.v1.tx.EditarUnidadOrganicaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.gestion_area_archivo_central.v1.tx.EliminarUnidadOrganicaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.gestion_area_archivo_central.v1.tx.ListarUnidadOrganicaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.gestion_area_archivo_central.v1.tx.RegistrarUnidadOrganicaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.gestion_area_archivo_central.v1.tx.RegistrarUnidadesOrganicasTx;


/**
 *
 * @author Jeferson
 */
public class ComponentRegister implements IComponentRegister{

    @Override
    public WebComponent createComponent() {
        
        // Asignando al modulo al cual Pertenece
         WebComponent component = new WebComponent(Sigesmed.SISTEMA_ARCHIVO_DIGITAL);
          //Registrando el Nombre del Componente
        component.setName("gestion_area_archivo_central");
        //version del componente
        component.setVersion(1);
        
        // Lista de Operaciones de Logica , propias del componente
       
        // OJO
        component.addTransactionPUT("editar_unidad_organica",EditarUnidadOrganicaTx.class); // falta para editar
        component.addTransactionDELETE("eliminar_unidad_organica",EliminarUnidadOrganicaTx.class);
        component.addTransactionGET("listar_unidad_organica", ListarUnidadOrganicaTx.class);
        component.addTransactionPOST("registrar_unidad_organica", RegistrarUnidadOrganicaTx.class);
        component.addTransactionPOST("registrar_unidades_organicas",RegistrarUnidadesOrganicasTx.class);
        
        return component;
         
       
    }
    
}
