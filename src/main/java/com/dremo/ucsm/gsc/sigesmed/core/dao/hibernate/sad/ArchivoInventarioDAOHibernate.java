/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sad;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.ArchivoInventarioDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.ArchivosInventarioTransferencia;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;

import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.PrestamoSerieDocumental;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
/**
 *
 * @author admin
 */
public class ArchivoInventarioDAOHibernate extends GenericDaoHibernate<ArchivosInventarioTransferencia> implements ArchivoInventarioDAO {

    @Override
    public List<ArchivosInventarioTransferencia> buscarPorCodigo(int codigo) {
        
         List<ArchivosInventarioTransferencia> ait = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT  archivos FROM ArchivosInventarioTransferencia archivos WHERE archivos.arc_inv_tra_id=:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1",codigo);
            ait = query.list();
        }catch(Exception e){
            System.out.println("No se pudo Obtener el Archivo\\n "+ e.getMessage());
             throw new UnsupportedOperationException("No se pudo Obtener el Archivo\\n "+ e.getMessage());
        }
        finally{
            session.close();
        }
        return ait;
        
       
        
      // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    @Override
    public List<ArchivosInventarioTransferencia> listarArchivos(int codigo) {
        
        List<ArchivosInventarioTransferencia> ait = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT archivos FROM ArchivosInventarioTransferencia archivos WHERE archivos.det_inv_trans=:p1 ";
            Query query = session.createQuery(hql);
            query.setParameter("p1",codigo);
            ait = query.list();
            
        }catch(Exception e){
            System.out.println("No se pudo listar los Archivos \\n" + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los Archivos \\n "+ e.getMessage());           
        }
        finally{
            session.close();
        }
        return ait;
        
      // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<ArchivosInventarioTransferencia> listarArchivosSerie(int codSerie){
        
        List<ArchivosInventarioTransferencia> ait = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT archivos FROM ArchivosInventarioTransferencia archivos JOIN FETCH archivos.detalle_inventario WHERE archivos.detalle_inventario.ser_doc_id=:p1 ";
            Query query = session.createQuery(hql);
            query.setParameter("p1",codSerie);
            ait = query.list();
            
        }catch(Exception e){
            System.out.println("No se pudo listar los Archivos \\n" + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los Archivos \\n "+ e.getMessage());           
        }
        finally{
            session.close();
        }
        return ait;
    }
    
    
}
