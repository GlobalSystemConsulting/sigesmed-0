/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.registro_trabajador.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.TrabajadorCargoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.LibroAsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.PersonalDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TrabajadorCargo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author fredd
 */
public class EditarTrabajadorTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        Trabajador nuevoTrabajador = null;
        DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
        Persona nuevaPersona = null;
        Organizacion org=null;
        String dni;
        JSONArray miArray = new JSONArray();
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            JSONObject rTrabajador = requestData.getJSONObject("trabajador");
            JSONObject rTrabajadorId = requestData.getJSONObject("trabajadorId");          
            JSONObject rPersona = requestData.getJSONObject("persona");
            
            Integer perId = rPersona.getInt("perId");
            Integer traId = rTrabajadorId.getInt("traId");
            System.out.println("aqui traId: " + traId);
            
            
                Integer organizacionID = rTrabajador.getInt("org");
                String fechaIngreso = rTrabajador.getString("fechaIngreso");
                Double salario = rTrabajador.getDouble("salario");
                Integer cargo = rTrabajador.getInt("cargo");
                String tipo = rTrabajador.getString("tipo");
                dni = rPersona.getString("dni");
                nuevaPersona=new Persona(perId,dni);
                org=new Organizacion(organizacionID);
                TrabajadorCargo trabCargo=new TrabajadorCargo(Short.valueOf(cargo+""));
                nuevoTrabajador=new Trabajador(traId, nuevaPersona, org,Short.valueOf("0"),sdi.parse(fechaIngreso.substring(0,10)), BigDecimal.valueOf(salario), new Date(),wr.getIdUsuario(), tipo,"A", trabCargo, null);

        }catch(Exception e){
            System.out.println();
            e.printStackTrace();
            return WebResponse.crearWebResponseError("Datos incorecctos"); //, e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        PersonalDao personalDao = (PersonalDao)FactoryDao.buildDao("cpe.PersonalDao");
        LibroAsistenciaDao libroAsistenciaDao=(LibroAsistenciaDao)FactoryDao.buildDao("cpe.LibroAsistenciaDao");
        try{   
            personalDao.update(nuevoTrabajador);
            nuevoTrabajador=libroAsistenciaDao.buscarTrabajadorPorDNI(dni, org.getOrgId()); 
            
           
        }catch(Exception e){
            System.out.println("No se pudo actualizar el Trabajador\n"+e);
            e.printStackTrace();
            return WebResponse.crearWebResponseError("No se pudo actualizar el Trabajador", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
            oResponse.put("dni",dni);
            oResponse.put("nombreCompleto",nuevoTrabajador.getPersona().getNombrePersona());
            oResponse.put("fechaNac",sdf.format(nuevoTrabajador.getPersona().getFecNac()));
            oResponse.put("fechaIn",sdf.format(nuevoTrabajador.getFecIng()));
            oResponse.put("cargo",nuevoTrabajador.getTraCar().getCrgTraNom());
            oResponse.put("salario",nuevoTrabajador.getSal());
            miArray.put(oResponse);
        
        return WebResponse.crearWebResponseExito("La actualizacion del Trabajador se realizo correctamente", miArray);
        //Fin
            
    }
    
}
