/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.LibroCajaDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author angel
 */
public class ActualizarEstadoLibroCITx  implements ITransaction{   

    @Override
    public WebResponse execute(WebRequest wr) {
         
        
         int id;
         String estado;
    
        try{            
             
            JSONObject requestData = (JSONObject)wr.getData();  
            id = requestData.getInt("idLibro");
            estado = requestData.getString("EstadoLibro");
          
            
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo obtener datos", e.getMessage() );
        }
        
        LibroCajaDao libroDao = (LibroCajaDao)FactoryDao.buildDao("sci.LibroCajaDao");
      
              
        try {         
            if("C".equals(estado)){
                System.out.println("Inactivando Libro");
                libroDao.CerrarEstadoLibro(id);
            }
            else if("I".equals(estado)){
                System.out.println("Cerrando  Libro");
                libroDao.InactivarEstadoLibro(id);
            }
              
            
        } catch (Exception e) {
            System.out.println("No se pudo Actualizo en Libro,Errodatos del Libro \n"+e);
            return WebResponse.crearWebResponseError("No se pudo Actualizo en Libro,Erro datos del Libro", e.getMessage() );
        }
        /*
        *  Repuesta Correcta
        */ 
     
        JSONObject oResponse = new JSONObject();
            
           
        oResponse.put("idLibro",id); 
       
       
        
        
        
        return WebResponse.crearWebResponseExito("Se Actualizo en Libro Correctamente",oResponse);        
        //Fin
    }
    
    
}
