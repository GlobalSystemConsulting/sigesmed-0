package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.CursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.MetodologiaCursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.TecnicaCursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.CursoCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.MetodologiaCursoCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.TecnicaCursoCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class RegistrarMetodologiaTx implements ITransaction {
    
    private static final Logger logger = Logger.getLogger(RegistrarMetodologiaTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try{
            JSONObject data = (JSONObject) wr.getData();            
            int codCap = data.getInt("cod");

            CursoCapacitacionDao capacitacionDao = (CursoCapacitacionDao) FactoryDao.buildDao("capacitacion.CursoCapacitacionDao");
            CursoCapacitacion capacitacion = capacitacionDao.buscarPorId(codCap);
            MetodologiaCursoCapacitacionDao metodologiaCursoCapacitacionDao = (MetodologiaCursoCapacitacionDao) FactoryDao.buildDao("capacitacion.MetodologiaCursoCapacitacionDao");
            TecnicaCursoCapacitacionDao tecnicaCursoCapacitacionDao = (TecnicaCursoCapacitacionDao) FactoryDao.buildDao("capacitacion.TecnicaCursoCapacitacionDao");
            
            JSONArray metodologias = data.getJSONArray("metodologias");
            
            for(int i = 0;i < metodologias.length();i++) {
                MetodologiaCursoCapacitacion metodologia = new MetodologiaCursoCapacitacion(metodologias.getJSONObject(i).getString("des"));
                metodologia.setCursoCapacitacion(capacitacion);
                metodologiaCursoCapacitacionDao.insert(metodologia);
                
                JSONArray tecnicas = metodologias.getJSONObject(i).getJSONArray("tecnicas");
                
                if(tecnicas.length() > 0) {
                    for(int j = 0;j < tecnicas.length();j++) {
                        TecnicaCursoCapacitacion tecnica = new TecnicaCursoCapacitacion(tecnicas.getJSONObject(j).getString("des"));
                        tecnica.setMetodologiaCursoCapacitacion(metodologia);
                        tecnicaCursoCapacitacionDao.insert(tecnica);
                    }
                }                
            }     
            
            return WebResponse.crearWebResponseExito("Las metodologías del curso de capacitación fueron creadas correctamente", WebResponse.OK_RESPONSE);
        } catch (Exception e){
            logger.log(Level.SEVERE,"registrarMetodologia",e);
            return WebResponse.crearWebResponseError("No se pudo registrar las metodologías del curso de capacitación",WebResponse.BAD_RESPONSE);
        }
    }    
}
