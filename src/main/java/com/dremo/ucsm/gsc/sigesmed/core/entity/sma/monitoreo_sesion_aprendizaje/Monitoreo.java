/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje;

import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.Organizacion;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

@Entity
@Table(name = "monitoreo", schema="pedagogico")
public class Monitoreo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "mon_id")
    private Integer monId;
    
    @Size(max = 2147483647)
    @Column(name = "mon_cod")
    private String monCod;
    
    @Size(max = 2147483647)
    @Column(name = "mon_anio")
    private String monAnio;
    
    @Size(max = 2147483647)
    @Column(name = "mon_nom_doc")
    private String monNomDoc;
    
    @Size(max = 2147483647)
    @Column(name = "mon_url")
    private String monUrl;
    
    @Column(name = "mon_fec_reg")
    @Temporal(TemporalType.DATE)
    private Date monFecReg;
    
    @Column(name = "mon_num_esp")
    private Integer monNumEsp;
    
    @Column(name = "mon_num_doc")
    private Integer monNumDoc;
    
    @Column(name = "mon_num_vis")
    private BigInteger monNumVis;
    
    @Size(max = 2)
    @Column(name = "mon_est")
    private String monEst;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.DATE)
    private Date fecMod;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Size(max = 1)
    @Column(name = "est_reg")
    private String estReg;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="org_id", nullable=false)
    private Organizacion organizacion;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "monitoreo")
    private List<MonitoreoDetalle> monitoreosDetalle;

    public Monitoreo() {
    }

    public Monitoreo(Integer monId) {
        this.monId = monId;
    }

    public Integer getMonId() {
        return monId;
    }

    public void setMonId(Integer monId) {
        this.monId = monId;
    }

    public String getMonCod() {
        return monCod;
    }

    public void setMonCod(String monCod) {
        this.monCod = monCod;
    }

    public String getMonAnio() {
        return monAnio;
    }

    public void setMonAnio(String monAnio) {
        this.monAnio = monAnio;
    }

    public String getMonNomDoc() {
        return monNomDoc;
    }

    public void setMonNomDoc(String monNomDoc) {
        this.monNomDoc = monNomDoc;
    }

    public String getMonUrl() {
        return monUrl;
    }

    public void setMonUrl(String monUrl) {
        this.monUrl = monUrl;
    }

    public Date getMonFecReg() {
        return monFecReg;
    }

    public void setMonFecReg(Date monFecReg) {
        this.monFecReg = monFecReg;
    }

    public Integer getMonNumEsp() {
        return monNumEsp;
    }

    public void setMonNumEsp(Integer monNumEsp) {
        this.monNumEsp = monNumEsp;
    }

    public Integer getMonNumDoc() {
        return monNumDoc;
    }

    public void setMonNumDoc(Integer monNumDoc) {
        this.monNumDoc = monNumDoc;
    }

    public BigInteger getMonNumVis() {
        return monNumVis;
    }

    public void setMonNumVis(BigInteger monNumVis) {
        this.monNumVis = monNumVis;
    }

    public String getMonEst() {
        return monEst;
    }

    public void setMonEst(String monEst) {
        this.monEst = monEst;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (monId != null ? monId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Monitoreo)) {
            return false;
        }
        Monitoreo other = (Monitoreo) object;
        if ((this.monId == null && other.monId != null) || (this.monId != null && !this.monId.equals(other.monId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.Monitoreo[ monId=" + monId + " ]";
    }
    
}
