/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.alerta_sistema.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.AlertaSistemaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.AlertaSistema;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

/**
 *
 * @author abel
 */
public class EliminarAlertaSistemaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int alertaID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            alertaID = requestData.getInt("alertaID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo eliminar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        AlertaSistemaDao alertaDao = (AlertaSistemaDao)FactoryDao.buildDao("AlertaSistemaDao");
        try{
            alertaDao.deleteAbsolute(new AlertaSistema(alertaID));
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar la alerta del Sistema \n"+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar la alerta del Sistema \n", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("La alerta del Sistema se elimino correctamente");
        //Fin
    }
}
