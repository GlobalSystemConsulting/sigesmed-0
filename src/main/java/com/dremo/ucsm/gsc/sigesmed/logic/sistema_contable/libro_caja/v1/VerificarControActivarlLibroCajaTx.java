/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.ControlLibroDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.LibroCajaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.ControlLibro;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONObject;

/**
 *
 * @author angel
 */
public class VerificarControActivarlLibroCajaTx implements ITransaction{
      @Override
    public WebResponse execute(WebRequest wr){
        
        ControlLibro control = null;
        int organizacionID;
        int libroCajaId;
        Date fa = new Date();
        Date ac = new Date();
        
        DateFormat fechaHora = new SimpleDateFormat("yyyy-MM-dd");
       
        
        ControlLibroDao controlLibroDao = (ControlLibroDao)FactoryDao.buildDao("sci.ControlLibroDao");
        LibroCajaDao libroDao = (LibroCajaDao)FactoryDao.buildDao("sci.LibroCajaDao");
        
        try{
             
            JSONObject requestData = (JSONObject)wr.getData();           
            organizacionID = requestData.getInt("organizacionPadre");
            libroCajaId = requestData.getInt("libroCajaId");
            
         }catch(Exception e){
            System.out.println("No se pudo obtener la organizacion a la que pertenece \n"+e);
            return WebResponse.crearWebResponseError("No se pudo obtener la organizacion a la que pertenece", e.getMessage() );
        }
        
        
        try {   
            control = controlLibroDao.buscarControl(organizacionID);
            fa = control.getFecRea();
        } catch (Exception e) {
            System.out.println("No se pudo encontrar el control para el libro - back \n"+e);
            return WebResponse.crearWebResponseError("No se pudo encontrar o No Existe un control para el libro - back", e.getMessage() );
        }

        try{                    

           
                   
                   // fa = control.getFecRea();
                    fechaHora.format(ac);  

                    String fce =  fechaHora.format(fa);  
                    String fhoy =  fechaHora.format(ac);  
/*
                    System.out.println("fecha apertura - " +fa+ "Fecha de hoy  "+ac);
                    System.out.println("fecha Strimnh  apertura - " +fce + "Fecha de hoy  "+fhoy);

                    System.out.print("fechas son iguales ? "+fa.equals(ac));

                    System.out.print("fechas son iguales Strin ? "+fce.equals(fhoy));*/
//actualizamos el libro 
                    if(fce.equals(fhoy)){
                        libroDao.actualizarEstadoLibro(libroCajaId);
                     //   controlLibroDao.delete(control);
                    }


        }
        catch (Exception e) {
            System.out.println("No se actualizo el libro A del Libro \n"+e);
            return WebResponse.crearWebResponseError("No se actualizo el libro A del Libro", e.getMessage() );
        }
            
        
       String c = "correcto";
        
        return WebResponse.crearWebResponseExito("Se ACTIVO el libro por control",c);   
      
    }
}
    