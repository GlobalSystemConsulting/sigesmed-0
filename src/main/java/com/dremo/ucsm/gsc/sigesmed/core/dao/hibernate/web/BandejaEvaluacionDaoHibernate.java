/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.web;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.BandejaEvaluacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.EvaluacionEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.EvaluacionEscolar;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaEvaluacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.RespuestaEvaluacion;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author abel
 */
public class BandejaEvaluacionDaoHibernate extends GenericDaoHibernate<BandejaEvaluacion> implements BandejaEvaluacionDao{

    @Override
    public BandejaEvaluacion buscar_bandeja_alumno(int band_esc_id) {
        BandejaEvaluacion bandeja;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT b FROM BandejaEvaluacion b  WHERE b.banEvaId=:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", band_esc_id);
            query.setMaxResults(1);
            bandeja = ((BandejaEvaluacion)query.uniqueResult());
        
        }catch(Exception e){
            System.out.println("No se pudo Obtener la Bandeja del Alumno\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Obtener la Bandeja del Alumno\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return bandeja;
        
     //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<RespuestaEvaluacion> buscar_respuestas_alumno(int band_esc_id) {
        
        List<RespuestaEvaluacion> respuestas ;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT res FROM RespuestaEvaluacion res  WHERE res.ban_eva_esc_id=:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", band_esc_id);
            respuestas = query.list();
            
        }catch(Exception e){
              System.out.println("No se pudo Obtener las Respuestas del Alumno\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Obtener las Respuestas del Alumno\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return respuestas;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

  
}
