/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.monitoreo_sesion_apredizaje.plantilla_ficha_monitoreo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.InstruccionLlenadoIndicadoresDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.InstruccionLlenadoIndicadores;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class EliminarInstruccionLITx implements ITransaction{

    private static final Logger logger = Logger.getLogger(EliminarInstruccionLITx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject jsonData = (JSONObject)wr.getData();
        int insId = jsonData.getInt("insId");
        return eliminarInstruccionLI(insId);
    }

    private WebResponse eliminarInstruccionLI(int insId) {
        try{
            InstruccionLlenadoIndicadoresDao insLleIndDao = (InstruccionLlenadoIndicadoresDao) FactoryDao.buildDao("sma.InstruccionLlenadoIndicadoresDao");
            InstruccionLlenadoIndicadores insLI = insLleIndDao.buscarPorId(insId);
            insLleIndDao.delete(insLI);
            return WebResponse.crearWebResponseExito("Se elimino correctamente la instruccion de llenado de indicador(es)");

        }catch (Exception e){
            logger.log(Level.SEVERE,"eliminarInstruccionLlenadoIndicadores",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar la instruccion de llenado de indicador(es)");
        }
    }
}
