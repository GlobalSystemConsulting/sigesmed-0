/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.ma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Area;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ListaUtiles;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Grado;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Nivel;
import java.util.List;

/**
 *
 * @author ucsm
 */
public interface ListaUtilesDao extends GenericDao<ListaUtiles>{
    ListaUtiles buscarPorId(int idListaUtiles);
    List<ListaUtiles> listarListasPorOrganizacion(int idOrganizacion);
    
    List<ListaUtiles> listarListasPorUsuario(int idUsuario);
    List<ListaUtiles> listarListasPorOrganizacionUsuario(int idOrganizacion, int idUsuario);
    Nivel buscarNivelPorId(int id); 
    Grado buscarGradoPorId(int id);
}
