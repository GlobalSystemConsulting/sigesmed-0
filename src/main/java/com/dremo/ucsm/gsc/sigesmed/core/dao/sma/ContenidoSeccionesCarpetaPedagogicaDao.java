/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.sma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica.ContenidoSeccionesCarpetaPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica.EsquemaCarpeta;
import java.util.List;

public interface ContenidoSeccionesCarpetaPedagogicaDao extends GenericDao<ContenidoSeccionesCarpetaPedagogica>{
    public List<EsquemaCarpeta> listarEsquemaCarpeta(int carId);
}
