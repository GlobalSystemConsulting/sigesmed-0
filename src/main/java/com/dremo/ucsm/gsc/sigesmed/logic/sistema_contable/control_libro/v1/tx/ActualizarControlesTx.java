package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.control_libro.v1.tx;


import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.ControlLibroDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.ControlLibro;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author angel
 */
public class ActualizarControlesTx implements ITransaction{
    
    @Override
    public WebResponse execute(WebRequest wr){
        //Parte de la operacion con la Base de Datos
        
        List<ControlLibro> controles=null;
        int organizacionID=0;
        int year=0;
        Date today = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String fToday = sdf.format(today);
        
        try { 
            today = sdf.parse(fToday);
        } catch (ParseException ex) {
            System.out.println("NO se pudo convertir la fecha  \n"+ex);
        }
        ControlLibroDao controlesDao = (ControlLibroDao)FactoryDao.buildDao("sci.ControlLibroDao");
        
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();                                
                         
            organizacionID = requestData.getInt("organizacionID"); 
            year = requestData.getInt("year"); 
             System.out.println ("añooooo-----"+year);
           
            
        }catch(Exception e){
            
             System.out.println("No se pudo Obtener datos \n"+e);
            return WebResponse.crearWebResponseError("No se pudo obtener datos", e.getMessage() );
        }
        
        try {
          
            controles= controlesDao.listarControles(organizacionID,year);
            
        } catch (Exception e) {
            System.out.println("No se pudo Listar los Controles\n"+e);
            return WebResponse.crearWebResponseError("No se pudo listar los Controles", e.getMessage() );
        }
        
        Date fa = new Date();
        for(ControlLibro c:controles){
           fa = c.getFecRea();
           System.out.println("LA FECHA ACTUAL ES "+today.compareTo(fa)+"....."+today+"Que la fecha de apertura"+fa);
           if((today.compareTo(fa))>0){
               controlesDao.delete(c);
               
           }
        }
        
        try {
          
            controles= controlesDao.listarControles(organizacionID,year);
            
        } catch (Exception e) {
            System.out.println("No se pudo Listar los Controles\n"+e);
            return WebResponse.crearWebResponseError("No se pudo listar los Controles", e.getMessage() );
        }
        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        DateFormat fechaHora = new SimpleDateFormat("dd/MM/yyyy");
       
            for(ControlLibro c:controles ){
                JSONObject oResponse = new JSONObject();
                oResponse.put("controlD",c.getConLibId());
                oResponse.put("descripcion",c.getNom());
                oResponse.put("fechaPreCierre",fechaHora.format(c.getFecPre()));
                oResponse.put("fechaCierre",fechaHora.format(c.getFecCie()));
               // oResponse.put("fechaCierre",fechaHora.format(c.getFecCie()));
                oResponse.put("fechaReApertura",fechaHora.format(c.getFecRea()));
                oResponse.put("edi",false);
              
                oResponse.put("estado",""+c.getEstReg());
                miArray.put(oResponse);
            }
     
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente LOS CONTROLES ",miArray);        
        //Fin
        
        
        
    }
    
}
