/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.monitoreo_sesion_apredizaje.plantilla_ficha_monitoreo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.PlantillaFichaMonitoreoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.ValoracionIndicadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.CompromisoGestionPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.IndicadorCompromisoGestionPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.InstruccionLlenadoIndicadores;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.ValoracionIndicador;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class VerPlantillaPDFConConsultaTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(VerPlantillaPDFConConsultaTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        return generarPlantillaPDF(data);
    }

    private WebResponse generarPlantillaPDF(JSONObject dataPlantilla) {
        try{
            PlantillaFichaMonitoreoDao plantillaDao = (PlantillaFichaMonitoreoDao) FactoryDao.buildDao("sma.PlantillaFichaMonitoreoDao");
            ValoracionIndicadorDao valIndDao = (ValoracionIndicadorDao) FactoryDao.buildDao("sma.ValoracionIndicadorDao");
            
            List<CompromisoGestionPedagogica> compromisos = plantillaDao.listarCompromisos(dataPlantilla.getInt("plaId"));
            List<ValoracionIndicador> nivelesAvance = valIndDao.listarPorPlaId(dataPlantilla.getInt("plaId"));
            
            JSONArray jsonCompromisos = new JSONArray();
            int canInd = 0;
            for(CompromisoGestionPedagogica compromiso : compromisos){
                JSONObject jsonCompromiso = new JSONObject(EntityUtil.objectToJSONString(
                        new String[]{"comGesPedId", "comGesPedDes", "comGesPedTip"},
                        new String[]{"comId", "comDes", "comTip"},
                        compromiso
                ));
                
                List<InstruccionLlenadoIndicadores> instrucciones = compromiso.getInstruccionesLI();
                JSONArray jsonInstrucciones = new JSONArray();
                for(InstruccionLlenadoIndicadores instruccion: instrucciones){
                    JSONObject jsonInstruccion = new JSONObject(EntityUtil.objectToJSONString(
                        new String[]{"insLleIndId", "insLleIndDes"},
                        new String[]{"insId", "insDes"},
                        instruccion
                    ));
                    
                    List<IndicadorCompromisoGestionPedagogica> indicadores = plantillaDao.listarIndicadores(instruccion.getInsLleIndId());
                    canInd += indicadores.size();
                    JSONArray jsonIndicadores = new JSONArray(EntityUtil.listToJSONString(
                            new String[]{"indComGesPedId", "indComGesPedDes"},
                            new String[]{"indId","indDes"},
                            indicadores
                    ));
                    
                    jsonInstruccion.put("indicadores",jsonIndicadores);
                    jsonInstrucciones.put(jsonInstruccion);
                }
                
                jsonCompromiso.put("instrucciones",jsonInstrucciones);
                jsonCompromisos.put(jsonCompromiso);
            }
            dataPlantilla.put("totInd", canInd);
            System.out.println(dataPlantilla);
            String b64 = generarBase64CC(dataPlantilla, nivelesAvance, jsonCompromisos);
            return WebResponse.crearWebResponseExito("Se listo correctamente los datos",new JSONObject().put("file",b64));
        }catch (Exception e){
            logger.log(Level.SEVERE,"verPlantillaPDF",e);
            return WebResponse.crearWebResponseError("No se pudocrear la plantilla");
        }
    }
    private String generarBase64CC(JSONObject dataPlantilla, List<ValoracionIndicador> nivelesAvance, JSONArray contenido) throws Exception{
        
        int totInd = dataPlantilla.getInt("totInd");
        
        //Creación del reporte
        Mitext mitext = new Mitext(false,"");
        
        //Cabecera
        Table cabecera = new Table(1);  
        cabecera.addCell(new Cell().setBorder(Border.NO_BORDER).add(new Paragraph("FICHA N° " + dataPlantilla.getString("plaCod").toUpperCase()).setBold().setFontSize(15).setTextAlignment(TextAlignment.CENTER)));
        cabecera.addCell(new Cell().setBorder(Border.NO_BORDER).add(new Paragraph("MONITOREO: " + dataPlantilla.getString("plaNom").toUpperCase()).setBold().setFontSize(15)).setTextAlignment(TextAlignment.CENTER));
        cabecera.addCell(new Cell().setBorder(Border.NO_BORDER).add(new Paragraph(dataPlantilla.getString("plaDes")).setFontSize(15).setTextAlignment(TextAlignment.CENTER)));
        cabecera.setBackgroundColor(Color.GRAY);
        mitext.agregarTabla(cabecera);
        
        mitext.agregarParrafo("");
        
        //Datos de identificación
        mitext.agregarParrafo("DATOS DE IDENTIFICACIÓN");
        /*Datos de IE*/
        float[] columnWidthsIE = {2,1};
        Table datosIE = new Table(columnWidthsIE);  
        datosIE.setWidthPercent(100);
        datosIE.addCell(new Cell(1,2).add(new Paragraph("DATOS DE LA INSTITUCIÓN EDUCATIVA: ").setFontColor(Color.WHITE).setBold().setFontSize(13).setTextAlignment(TextAlignment.LEFT)).setBackgroundColor(Color.DARK_GRAY));
        datosIE.addCell(new Cell(1,1).add(new Paragraph("NOMBRE: ").setFontSize(12).setTextAlignment(TextAlignment.LEFT)));
        datosIE.addCell(new Cell(1,1).add(new Paragraph("CÓDIGO MODULAR: ").setFontSize(12).setTextAlignment(TextAlignment.LEFT)));
        datosIE.addCell(new Cell(1,1).add(new Paragraph("DIRECCIÓN: ").setFontSize(12).setTextAlignment(TextAlignment.LEFT)));
        datosIE.addCell(new Cell(1,1).add(new Paragraph("UGEL: ").setFontSize(12).setTextAlignment(TextAlignment.LEFT)));
        datosIE.addCell(new Cell(1,1).add(new Paragraph("NIVEL: Inicial( ) Primaria( ) Secundaria( )").setFontSize(12).setTextAlignment(TextAlignment.LEFT)));
        datosIE.addCell(new Cell(1,1).add(new Paragraph("DRE: ").setFontSize(12).setTextAlignment(TextAlignment.LEFT)));
        mitext.agregarTabla(datosIE);
        
        mitext.agregarParrafo("");
        
        /*Datos del Observador*/
        float[] columnWidthsO = {2,6,2,1,1,1};
        Table datosObservador = new Table(columnWidthsO);  
        datosObservador.setWidthPercent(100);
        datosObservador.addCell(new Cell(1,6).add(new Paragraph("DATOS DEL OBSERVADOR: ").setFontColor(Color.WHITE).setBold().setFontSize(13).setTextAlignment(TextAlignment.LEFT)).setBackgroundColor(Color.DARK_GRAY));
        datosObservador.addCell(new Cell(2,1).add(new Paragraph("1. Cargo").setFontSize(12).setTextAlignment(TextAlignment.LEFT)).setBackgroundColor(Color.LIGHT_GRAY));
        datosObservador.addCell(new Cell(2,1).add(new Paragraph("Director( ) Subdirector de nivel( ) Coordinador académico( ) Coordinador del área ( ) Otro cargo ( ) Especificar:_______________________").setFontSize(12).setTextAlignment(TextAlignment.LEFT)));
        datosObservador.addCell(new Cell(2,1).add(new Paragraph("2. Fecha del monitoreo").setFontSize(12).setTextAlignment(TextAlignment.LEFT)).setBackgroundColor(Color.LIGHT_GRAY));
        datosObservador.addCell(new Cell(1,1).add(new Paragraph("").setFontSize(12).setTextAlignment(TextAlignment.LEFT)).setHeight(60));
        datosObservador.addCell(new Cell(1,1).add(new Paragraph("").setFontSize(12).setTextAlignment(TextAlignment.LEFT)).setHeight(60));
        datosObservador.addCell(new Cell(1,1).add(new Paragraph("").setFontSize(12).setTextAlignment(TextAlignment.LEFT)).setHeight(60));
        datosObservador.addCell(new Cell(1,1).add(new Paragraph("Día").setFontSize(12).setTextAlignment(TextAlignment.CENTER)));
        datosObservador.addCell(new Cell(1,1).add(new Paragraph("Mes").setFontSize(12).setTextAlignment(TextAlignment.CENTER)));
        datosObservador.addCell(new Cell(1,1).add(new Paragraph("Año").setFontSize(12).setTextAlignment(TextAlignment.CENTER)));
        mitext.agregarTabla(datosObservador);
        
        mitext.agregarParrafo("");
        
        /*Datos del docente observado*/
        float[] columnWidthsDO = {2,6,3,4};
        Table datosDocObs = new Table(columnWidthsDO);  
        datosDocObs.setWidthPercent(100);
        datosDocObs.addCell(new Cell(1,4).add(new Paragraph("Datos del docente observado->Datos a ser registrados consultando al docente").setFontColor(Color.WHITE).setBold().setFontSize(13).setTextAlignment(TextAlignment.LEFT)).setBackgroundColor(Color.DARK_GRAY));
        datosDocObs.addCell(new Cell(1,1).add(new Paragraph("3. Apellidos y Nombres").setFontSize(12).setTextAlignment(TextAlignment.LEFT)).setBackgroundColor(Color.LIGHT_GRAY));
        datosDocObs.addCell(new Cell(1,1).add(new Paragraph("").setFontSize(12).setTextAlignment(TextAlignment.LEFT)));
        datosDocObs.addCell(new Cell(1,1).add(new Paragraph("4. Especialidad").setFontSize(12).setTextAlignment(TextAlignment.LEFT)).setBackgroundColor(Color.LIGHT_GRAY));
        datosDocObs.addCell(new Cell(1,1).add(new Paragraph("").setFontSize(12).setTextAlignment(TextAlignment.LEFT)));
        mitext.agregarTabla(datosDocObs);
        
        mitext.agregarParrafo("");
        
        /*Datos de la sesión observadoa*/
        float[] columnWidthsSO = {2,4,2,1,1,1};
        Table datosSesObs = new Table(columnWidthsSO);  
        datosSesObs.setWidthPercent(100);
        datosSesObs.addCell(new Cell(1,6).add(new Paragraph("Datos de la sesión observada->Datos a ser registrados mediante la observación").setFontColor(Color.WHITE).setBold().setFontSize(13).setTextAlignment(TextAlignment.LEFT)).setBackgroundColor(Color.DARK_GRAY));
        datosSesObs.addCell(new Cell(1,6).add(new Paragraph("5. Área o áreas desarrolladas  Anotar en el siguiente espacio").setFontSize(12).setTextAlignment(TextAlignment.LEFT)).setBackgroundColor(Color.LIGHT_GRAY));
        datosSesObs.addCell(new Cell(1,1).add(new Paragraph("6. Denominación de la sesión:").setFontSize(12).setTextAlignment(TextAlignment.LEFT)).setBackgroundColor(Color.LIGHT_GRAY));
        datosSesObs.addCell(new Cell(1,5).add(new Paragraph("").setFontSize(12).setTextAlignment(TextAlignment.LEFT)));
        datosSesObs.addCell(new Cell(1,1).add(new Paragraph("7. Nivel educativo: ").setFontSize(12).setTextAlignment(TextAlignment.LEFT)).setBackgroundColor(Color.LIGHT_GRAY));
        datosSesObs.addCell(new Cell(1,1).add(new Paragraph("Inicial( ) Primaria( ) Secundaria( )").setFontSize(12).setTextAlignment(TextAlignment.CENTER)));
        datosSesObs.addCell(new Cell(1,1).add(new Paragraph("8. Grado(s) o año(s) en el aula: ").setFontSize(12).setTextAlignment(TextAlignment.LEFT)).setBackgroundColor(Color.LIGHT_GRAY));
        datosSesObs.addCell(new Cell(1,1).add(new Paragraph("").setFontSize(12).setTextAlignment(TextAlignment.LEFT)));
        datosSesObs.addCell(new Cell(1,1).add(new Paragraph("9.Sección: ").setFontSize(12).setTextAlignment(TextAlignment.LEFT)).setBackgroundColor(Color.LIGHT_GRAY));
        datosSesObs.addCell(new Cell(1,1).add(new Paragraph("").setFontSize(12).setTextAlignment(TextAlignment.LEFT)));
        datosSesObs.addCell(new Cell(1,1).add(new Paragraph("10.Turno: ").setFontSize(12).setTextAlignment(TextAlignment.LEFT)).setBackgroundColor(Color.LIGHT_GRAY));
        datosSesObs.addCell(new Cell(1,1).add(new Paragraph("Mañana( ) Tarde( )").setFontSize(12).setTextAlignment(TextAlignment.CENTER)));
        datosSesObs.addCell(new Cell(1,1).add(new Paragraph("11.Duración de la sesión observada: ").setFontSize(12).setTextAlignment(TextAlignment.LEFT)).setBackgroundColor(Color.LIGHT_GRAY));
        datosSesObs.addCell(new Cell(1,3).add(new Paragraph("_______ hrs., ______min.").setFontSize(12).setTextAlignment(TextAlignment.CENTER)));
        mitext.agregarTabla(datosSesObs);
        
        mitext.agregarParrafo("");
        
        //Niveles de avance
        Map<Integer,String> puntajesNA = new TreeMap<>();
        float[] columnWidthsNA = {4, 8, 2};
        Table tablaNA = new Table(columnWidthsNA);  
        tablaNA.setWidthPercent(100);
        tablaNA.addCell(new Cell(1,3).add(new Paragraph("NIVELES DE AVANCE").setFontColor(Color.WHITE).setBold().setFontSize(13).setTextAlignment(TextAlignment.LEFT)).setBackgroundColor(Color.DARK_GRAY));

        for(int i=0;i<nivelesAvance.size();i++){
            int puntaje = nivelesAvance.get(i).getValIndPun();
            String nomNA = nivelesAvance.get(i).getValIndNom();
            
            tablaNA.addCell(new Cell(1,1).add(new Paragraph(nomNA).setFontSize(12).setTextAlignment(TextAlignment.LEFT)));
            tablaNA.addCell(new Cell(1,1).add(new Paragraph(nivelesAvance.get(i).getValIndDes()).setFontSize(12).setTextAlignment(TextAlignment.LEFT)));
            tablaNA.addCell(new Cell(1,1).add(new Paragraph(""+puntaje).setFontSize(12).setTextAlignment(TextAlignment.CENTER)));
            puntajesNA.put(puntaje,nomNA);
        }
        mitext.agregarTabla(tablaNA);
        
        mitext.agregarParrafo("");
        mitext.agregarParrafo("");
        mitext.agregarParrafo("");
        
        //Intervalos de puntaje
        Table tablaIntervalosPuntaje = new Table(3);  
        for(Map.Entry<Integer,String> entry : puntajesNA.entrySet()){
            tablaIntervalosPuntaje.addCell(new Cell(1,1).add(new Paragraph(entry.getValue()).setBold().setFontSize(12).setTextAlignment(TextAlignment.CENTER)).setBackgroundColor(Color.LIGHT_GRAY));
        }
        
        List<Integer> intervalos = new ArrayList();
        int centinela = 0;
        for(Map.Entry<Integer,String> entry : puntajesNA.entrySet()){
            int intervalo = totInd*entry.getKey();
            intervalos.add(intervalo);
            
            if(intervalos.size()>1){
                int base = intervalos.get(centinela-1)+1;
                tablaIntervalosPuntaje.addCell(new Cell(1,1).add(new Paragraph(base+"-"+intervalo).setFontSize(12).setTextAlignment(TextAlignment.CENTER)));
            }else
               tablaIntervalosPuntaje.addCell(new Cell(1,1).add(new Paragraph(""+intervalo).setFontSize(12).setTextAlignment(TextAlignment.CENTER))); 
            centinela++;
        }
        mitext.agregarTabla(tablaIntervalosPuntaje);
        
        mitext.agregarParrafo("");
        
        //Desarrollo dela sesión de aprendizaje
        mitext.agregarParrafo("I. DESARROLLO DE LA SESIÓN DE APRENDIZAJE");
        
        int numColTDSA = 2+nivelesAvance.size();
        float[] colWidthTDSA = new float[numColTDSA];
        colWidthTDSA[0] = 1;
        colWidthTDSA[1] = 10;
        for(int z=2;z<colWidthTDSA.length;z++){
            colWidthTDSA[z]=1;
        }
        Table tablaDSA = new Table(colWidthTDSA);  
        tablaDSA.setWidthPercent(100);
        
        int totalInd = 0;
        for(int i=0;i<contenido.length();i++){
            String com = mostrarTipoCompromiso(contenido.getJSONObject(i).getString("comTip").charAt(0)) + contenido.getJSONObject(i).getString("comDes");
            tablaDSA.addCell(new Cell(1,numColTDSA).add(new Paragraph(com).setFontColor(Color.WHITE).setBold().setFontSize(12).setTextAlignment(TextAlignment.LEFT)).setBackgroundColor(Color.DARK_GRAY));
            
            JSONArray instrucciones = (JSONArray)contenido.getJSONObject(i).get("instrucciones");
            int totIndxCom=0;
            for(int j=0;j<instrucciones.length();j++){
                tablaDSA.addCell(new Cell(1,2).add(new Paragraph(instrucciones.getJSONObject(j).getString("insDes")).setBold().setItalic().setFontSize(12).setTextAlignment(TextAlignment.LEFT)).setBackgroundColor(Color.LIGHT_GRAY));
                tablaDSA.addCell(new Cell(1,numColTDSA-2).add(new Paragraph("Valoración").setBold().setFontSize(12).setTextAlignment(TextAlignment.CENTER)).setBackgroundColor(Color.LIGHT_GRAY));
                
                JSONArray indicadores = (JSONArray)instrucciones.getJSONObject(j).get("indicadores");
                for(int k=0;k<indicadores.length();k++){
                    totIndxCom++;
                    totalInd++;
                    tablaDSA.addCell(new Cell(1,1).add(new Paragraph(""+totalInd).setBold().setFontSize(12).setTextAlignment(TextAlignment.CENTER)).setBackgroundColor(Color.LIGHT_GRAY));
                    tablaDSA.addCell(new Cell(1,1).add(new Paragraph(indicadores.getJSONObject(k).getString("indDes")).setFontSize(12).setTextAlignment(TextAlignment.JUSTIFIED)));
                    
                    for(Map.Entry<Integer,String> entry : puntajesNA.entrySet()){
                        tablaDSA.addCell(new Cell(1,1).add(new Paragraph(""+entry.getKey()).setFontColor(Color.LIGHT_GRAY).setFontSize(12).setTextAlignment(TextAlignment.CENTER)));
                    }
                }
            }
            
            tablaDSA.addCell(new Cell(1,2).add(new Paragraph("Subtotal").setFontSize(12).setTextAlignment(TextAlignment.CENTER)).setBackgroundColor(Color.LIGHT_GRAY));
            for(Map.Entry<Integer,String> entry : puntajesNA.entrySet()){
                int subtotal = entry.getKey()*totIndxCom;
                tablaDSA.addCell(new Cell(1,1).add(new Paragraph(""+subtotal).setFontSize(12).setTextAlignment(TextAlignment.CENTER)));
            }
        }
        tablaDSA.addCell(new Cell(1,2).add(new Paragraph("Total Final").setBold().setFontSize(12).setTextAlignment(TextAlignment.CENTER)).setBackgroundColor(Color.LIGHT_GRAY));
        for(Map.Entry<Integer,String> entry : puntajesNA.entrySet()){
            int total = entry.getKey()*totalInd;
            tablaDSA.addCell(new Cell(1,1).add(new Paragraph(""+total).setBold().setFontSize(12).setTextAlignment(TextAlignment.CENTER)));
        }
        
        mitext.agregarTabla(tablaDSA);
        mitext.agregarParrafo("");
        mitext.cerrarDocumento();
        return mitext.encodeToBase64();
    }
    
    String mostrarTipoCompromiso(Character tipo){
        switch(tipo){
            case '4': return "COMPROMISO 4: ";
            case '5': return "COMPROMISO 5: ";
            case '6': return "COMPROMISO 6: ";
            case '7': return "COMPROMISO 7: ";
        }
        return "";
    }
    
}
