/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.plan_estudios.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.PlanEstudiosDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Periodo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;

/**
 *
 * @author abel
 */
public class ListarPeriodosTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<Periodo> periodos = null;
        PlanEstudiosDao planDao = (PlanEstudiosDao)FactoryDao.buildDao("mech.PlanEstudiosDao");
        try{
            periodos = planDao.listarPeriodos();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los periodos\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los periodos, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(Periodo periodo:periodos ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("periodoID",""+periodo.getPerId() );
            oResponse.put("nombre",periodo.getNom());
            oResponse.put("factor",periodo.getFacPer());
            oResponse.put("fecha",periodo.getFecMod().toString());
            oResponse.put("estado",""+periodo.getEstReg());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente los periodos",miArray);        
        //Fin
    }
    
}

