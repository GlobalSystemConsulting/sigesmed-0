/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.proyectos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.ProyectosDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ListarProyectosxOrganizacionTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {        
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        
        JSONObject requestData = (JSONObject)wr.getData();
        int orgId = requestData.getInt("orgId");
                
        List<Object[]> proyectos = null;
        ProyectosDao proyectosDao = (ProyectosDao)FactoryDao.buildDao("smdg.ProyectosDao");
        
        try{
            proyectos = proyectosDao.listarProyectos(orgId);
            
        
        }catch(Exception e){            
            return WebResponse.crearWebResponseError("No se pudo Listar las proyectos", e.getMessage() );
        }
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(Object[] p : proyectos){
            JSONObject oResponse = new JSONObject();
            oResponse.put("proid",p[0]);
            oResponse.put("pronom",p[1]);            
            oResponse.put("prores",p[2]);
            oResponse.put("proini",p[3]);
            oResponse.put("profin",p[4]);
            oResponse.put("prodoc",p[5]);
            oResponse.put("proresnom",p[6]+ " " +p[7]+ " " +p[8]);
            oResponse.put("proresdni",p[9]);
            oResponse.put("url",Sigesmed.UBI_ARCHIVOS+"/smdg/"+p[5]);
            oResponse.put("proava",p[10]);
             //Fecha de modificacion de avance Sigesmed.UBI_ARCHIVOS+
            
            miArray.put(oResponse);
            
        }
        System.out.println(miArray.toString());
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
                
    }
}
