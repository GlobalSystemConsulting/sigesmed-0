/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.smdg;

import java.io.Serializable;
import java.util.Set;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "plantilla_indicadores", schema = "institucional")

public class PlantillaIndicadores implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pin_id")
    private Integer pinId;
    @Column(name = "pin_des")
    private String pinDes;
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "est_reg")
    private String estReg  = "A";
//    @OneToMany(cascade=CascadeType.ALL)
//    @JoinColumn(name="pin_id")
//    @LazyCollection(LazyCollectionOption.FALSE)
//    private Set<PlantillaItems> items;
    @JoinColumn(name = "pgr_id", referencedColumnName = "pgr_id")
    @ManyToOne(fetch=FetchType.EAGER)
    private PlantillaGrupo grupo;

    public PlantillaIndicadores(){
    }
    
    public PlantillaIndicadores(Integer pinId) {
        this.pinId = pinId;
    }
    
    public PlantillaIndicadores(Integer pinId, PlantillaGrupo grupo) {
        this.pinId = pinId;
        this.grupo = grupo;
    }
    
    public PlantillaIndicadores(Integer pinId, PlantillaGrupo grupo, String pinDes) {
        this.pinId = pinId;
        this.grupo = grupo;
        this.pinDes = pinDes;
    }

    public PlantillaIndicadores(String pinDes, PlantillaGrupo grupo) {        
        this.pinDes = pinDes;
        this.grupo = grupo;
    }
    
    public Integer getPinId() {
        return pinId;
    }

    public void setPinId(Integer pinId) {
        this.pinId = pinId;
    }

    public String getPinDes() {
        return pinDes;
    }

    public void setPinDes(String pinDes) {
        this.pinDes = pinDes;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

//    @XmlTransient
//    public Set<PlantillaItems> getItems() {
//        return items;
//    }
//
//    public void setItems(Set<PlantillaItems> items) {
//        this.items = items;
//    }

    public PlantillaGrupo getGrupo() {
        return grupo;
    }

    public void setGrupo(PlantillaGrupo grupo) {
        this.grupo = grupo;
    }

//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (pinId != null ? pinId.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof PlantillaIndicadores)) {
//            return false;
//        }
//        PlantillaIndicadores other = (PlantillaIndicadores) object;
//        if ((this.pinId == null && other.pinId != null) || (this.pinId != null && !this.pinId.equals(other.pinId))) {
//            return false;
//        }
//        return true;
//    }
//
//    @Override
//    public String toString() {
//        return "entidades.smdg.PlantillaIndicadores[ pinId=" + pinId + " ]";
//    }
//    
}
