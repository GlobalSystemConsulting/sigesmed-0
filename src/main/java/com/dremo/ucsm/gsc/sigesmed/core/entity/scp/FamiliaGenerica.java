/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.scp;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;

/**
 *
 * @author Jeferson
 */

@Entity
@Table(name="familia_generica", schema="administrativo")
public class FamiliaGenerica implements java.io.Serializable {
    
    @Id
    @Column(name="fam_gen_id", unique= true , nullable=false)
    @SequenceGenerator(name="secuencia_det_inv_tra",sequenceName="administrativo.familia_generica_fam_gen_id_seq")
    @GeneratedValue(generator="secuencia_det_inv_tra")
    private int fam_gen_id;

    @Id
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(name="cla_gen_id",referencedColumnName="cla_gen_id",insertable=true,updatable=false),
        @JoinColumn(name="gru_gen_id",referencedColumnName="gru_gen_id",insertable=true,updatable=false)
    })   
    private ClaseGenerica clase_generica;
    
    
    @Column(name="cla_gen_id", unique= true , nullable=false)
    private int cla_gen_id;
    
    @Column(name="gru_gen_id", unique= true , nullable=false)
    private int gru_gen_id;

    @Column(name="cod", insertable = false , updatable=false)
    private String cod;
    
     @Column(name="nom", insertable = false , updatable=false)
    private String nom;

    @Column(name="fec_mod", insertable = false , updatable=false)
    private Date fec_mod;
    
    @Column(name="usu_mod", insertable = false , updatable=false)
    private int usu_mod;
    
    @Column(name="est_reg", insertable = false , updatable=false)
    private char est_reg;
    
    
    public void setFam_gen_id(int fam_gen_id) {
        this.fam_gen_id = fam_gen_id;
    }

    public void setCla_gen_id(int cla_gen_id) {
        this.cla_gen_id = cla_gen_id;
    }

    public void setClase_generica(ClaseGenerica clase_generica) {
        this.clase_generica = clase_generica;
    }

    public void setGru_gen_id(int gru_gen_id) {
        this.gru_gen_id = gru_gen_id;
    }


    public void setCod(String cod) {
        this.cod = cod;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setFec_mod(Date fec_mod) {
        this.fec_mod = fec_mod;
    }

    public void setUsu_mod(int usu_mod) {
        this.usu_mod = usu_mod;
    }

    public void setEst_reg(char est_reg) {
        this.est_reg = est_reg;
    }

    public int getFam_gen_id() {
        return fam_gen_id;
    }

    public int getCla_gen_id() {
        return cla_gen_id;
    }

    public ClaseGenerica getClase_generica() {
        return clase_generica;
    }

    public int getGru_gen_id() {
        return gru_gen_id;
    }

    public String getCod() {
        return cod;
    }

    public String getNom() {
        return nom;
    }

    public Date getFec_mod() {
        return fec_mod;
    }

    public int getUsu_mod() {
        return usu_mod;
    }

    public char getEst_reg() {
        return est_reg;
    }

    public FamiliaGenerica(int fam_gen_id, int cla_gen_id, int gru_gen_id, String cod, String nom, Date fec_mod, int usu_mod, char est_reg) {
        this.fam_gen_id = fam_gen_id;
        this.cla_gen_id = cla_gen_id;
        this.gru_gen_id = gru_gen_id;
        this.cod = cod;
        this.nom = nom;
        this.fec_mod = fec_mod;
        this.usu_mod = usu_mod;
        this.est_reg = est_reg;
    }

    public FamiliaGenerica(int fam_gen_id, ClaseGenerica clase_generica, String cod, String nom, Date fec_mod, int usu_mod, char est_reg) {
        this.fam_gen_id = fam_gen_id;
        this.clase_generica = clase_generica;
        this.cod = cod;
        this.nom = nom;
        this.fec_mod = fec_mod;
        this.usu_mod = usu_mod;
        this.est_reg = est_reg;
    }

    public FamiliaGenerica() {
    }
    
    
    
}
