/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje;

public class ConfiguracionPlantilla implements java.io.Serializable{
    private Integer plaFicMonId;
    private Integer comGesPedId;
    private String comDes;
    private Integer insLleIndId;
    private String insDes;
    private Integer indComGesPedId;
    private String indDes;

    public ConfiguracionPlantilla() {
    }

    public Integer getPlaFicMonId() {
        return plaFicMonId;
    }

    public void setPlaFicMonId(Integer plaFicMonId) {
        this.plaFicMonId = plaFicMonId;
    }

    public Integer getComGesPedId() {
        return comGesPedId;
    }

    public void setComGesPedId(Integer comGesPedId) {
        this.comGesPedId = comGesPedId;
    }

    public String getComDes() {
        return comDes;
    }

    public void setComDes(String comDes) {
        this.comDes = comDes;
    }

    public Integer getInsLleIndId() {
        return insLleIndId;
    }

    public void setInsLleIndId(Integer insLleIndId) {
        this.insLleIndId = insLleIndId;
    }

    public String getInsDes() {
        return insDes;
    }

    public void setInsDes(String insDes) {
        this.insDes = insDes;
    }

    public Integer getIndComGesPedId() {
        return indComGesPedId;
    }

    public void setIndComGesPedId(Integer indComGesPedId) {
        this.indComGesPedId = indComGesPedId;
    }

    public String getIndDes() {
        return indDes;
    }

    public void setIndDes(String indDes) {
        this.indDes = indDes;
    }
}
