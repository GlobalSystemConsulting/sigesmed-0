/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.carpeta_pedagogica.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.DocenteCarpetaPedagogicaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica.DocenteCarpetaPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica.RutaContenidoCarpeta;
import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.sma.carpeta_pedagogica.ArchivoCarPed;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;


public class ListarCarpetasPorUGELTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ListarCarpetasPorUGELTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject requestData = (JSONObject)wr.getData();
        int orgPadId = requestData.getInt("ugel");
        return listar(orgPadId);
    }
    
    public WebResponse listar(int orgPadId) {
        List<DocenteCarpetaPedagogica> carpetas = new ArrayList<DocenteCarpetaPedagogica>();
        DocenteCarpetaPedagogicaDao carpetasDao = (DocenteCarpetaPedagogicaDao)FactoryDao.buildDao("sma.DocenteCarpetaPedagogicaDao");
        
        try{
            carpetas = carpetasDao.listarPorUGEL(orgPadId);
        
        }catch(Exception e){
            System.out.println("No se pudieron listar las carpetas de la UGEL. Error: "+e);
            return WebResponse.crearWebResponseError("No se pudieron listar las carpetas de la UGEL. Error: ", e.getMessage());
        }
        
       
        JSONArray miArray = new JSONArray();
        Calendar cal = new GregorianCalendar();
        int anio = cal.get(Calendar.YEAR);
        for(DocenteCarpetaPedagogica carpeta:carpetas ){
            JSONObject oResponse = new JSONObject();
            int carEta = carpeta.getCarpeta().getEta();
            oResponse.put("carId", carpeta.getCarpeta().getCarDigId());
            oResponse.put("carEta", carEta);
            oResponse.put("carDes", carpeta.getCarpeta().getDes());
            oResponse.put("carEst", carEta==anio?"Vigente":"No Vigente");
            
            oResponse.put("docDat", carpeta.getDocente().getPersona().getNom()+" "+
                    carpeta.getDocente().getPersona().getApePat()+" "+
                    carpeta.getDocente().getPersona().getApeMat());
            oResponse.put("docId", carpeta.getDocente().getDocId());
            
            oResponse.put("ieId", carpeta.getOrganizacion().getOrgId());
            oResponse.put("ieDat", carpeta.getOrganizacion().getNom());
            
            oResponse.put("conDes", carpeta.getDes());
            
            int estAva = carpeta.getEstAva();
            //Completo o en Proceso
            if(estAva==1 || estAva ==2){
                RutaContenidoCarpeta rutaContenido = carpetasDao.buscarRutaConId(carpeta.getDocCarPedId());
                String route = ServicioREST.PATH_SIGESMED + File.separator + Sigesmed.UBI_ARCHIVOS + File.separator + ArchivoCarPed.ARCHIVO_CAR_PED_PATH + File.separator;
                Path path = Paths.get(route + rutaContenido.getNomFil());
                if(Files.exists(path)){
                    oResponse.put("rutConExi", true);
                    oResponse.put("rutConMen", "");
                    oResponse.put("rutConPath", rutaContenido.getPat());
                    oResponse.put("rutConNomFil", rutaContenido.getNomFil());
                } else{
                    oResponse.put("rutConExi", false);
                    oResponse.put("rutConMen", "El archivo no esta disponible");
                    oResponse.put("rutConPath", "");
                    oResponse.put("rutConNomFil", "");
                } 
            //Pendiente
            } else {  
                oResponse.put("rutConExi", false);
                oResponse.put("rutConMen", "No se ha subido el archivo");
                oResponse.put("rutConPath", "");
                oResponse.put("rutConNomFil", "");
            }
            
            oResponse.put("docCarPedId", carpeta.getDocCarPedId());
            oResponse.put("estAva", estAva);
            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se listo correctamente",miArray);
    }
}
