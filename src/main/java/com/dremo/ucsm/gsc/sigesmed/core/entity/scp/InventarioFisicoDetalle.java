/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.scp;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;

/**
 *
 * @author Jeferson
 */
@Entity
@Table(name="inventario_fisico_det", schema="administrativo")
public class InventarioFisicoDetalle {
    
    
    @Id
    @Column(name="inv_fis_det", unique= true , nullable=false)
    @SequenceGenerator(name="secuencia_det_inv_tra",sequenceName="administrativo.inventario_fisico_det_inv_fis_det_seq")
    @GeneratedValue(generator="secuencia_det_inv_tra")
    private int inv_ini_det;
    

    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(name="inv_fis_id",referencedColumnName="inv_fis_id",insertable=true,updatable=false),
        @JoinColumn(name="mov_ing_id",referencedColumnName="mov_ing_id",insertable=true,updatable=false)
    })
    private InventarioFisico inv_inicial;
    
   
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cod_bie" , insertable=false , updatable=false)
    private BienesMuebles cod_bie;
    
    @Column(name="inv_fis_id" , insertable=false ,updatable=false)
    private int inv_ini_id;
    
    @Column(name="mov_ing_id" , insertable=false ,updatable=false)
    private int mov_ing_id;
    
    @Column(name="cod_bie")
    private int cod_bien;
    
    

    public void setInv_inicial(InventarioFisico inv_inicial) {
        this.inv_inicial = inv_inicial;
    }

    public void setBien_inmueble(BienesMuebles bien_inmueble) {
        this.cod_bie = bien_inmueble;
    }

    public void setInv_inm_id(int inv_inm_id) {
        this.inv_ini_id = inv_inm_id;
    }

    public void setMov_ing_id(int mov_ing_id) {
        this.mov_ing_id = mov_ing_id;
    }

    public void setInv_ini_det(int inv_ini_det) {
        this.inv_ini_det = inv_ini_det;
    }

    public int getInv_ini_det() {
        return inv_ini_det;
    }

    

    public InventarioFisico getInv_inicial() {
        return inv_inicial;
    }

    public BienesMuebles getBien_inmueble() {
        return cod_bie;
    }

    public int getInv_inm_id() {
        return inv_ini_id;
    }

    public int getMov_ing_id() {
        return mov_ing_id;
    }

    
    public void setInv_ini_id(int inv_ini_id) {
        this.inv_ini_id = inv_ini_id;
    }

    public void setCod_bien(int cod_bien) {
        this.cod_bien = cod_bien;
    }

    public int getInv_ini_id() {
        return inv_ini_id;
    }

    public int getCod_bien() {
        return cod_bien;
    }

    
    
    public InventarioFisicoDetalle() {
    }

    public InventarioFisicoDetalle(int inv_ini_det, BienesMuebles cod_bie, int inv_ini_id, int mov_ing_id) {
        this.inv_ini_det = inv_ini_det;
        this.cod_bie = cod_bie;
        this.inv_ini_id = inv_ini_id;
        this.mov_ing_id = mov_ing_id;
    }

    public InventarioFisicoDetalle(int inv_ini_det, int inv_ini_id, int mov_ing_id, int cod_bien) {
        this.inv_ini_det = inv_ini_det;
        this.inv_ini_id = inv_ini_id;
        this.mov_ing_id = mov_ing_id;
        this.cod_bien = cod_bien;
    }

    public InventarioFisicoDetalle(int inv_ini_det, InventarioFisico inv_inicial, int cod_bien) {
        this.inv_ini_det = inv_ini_det;
        this.inv_inicial = inv_inicial;
        this.cod_bien = cod_bien;
    }
    
    
    
    
    
    

}

