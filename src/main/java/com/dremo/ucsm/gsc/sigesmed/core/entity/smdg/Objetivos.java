/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.smdg;

import java.io.Serializable;
import java.util.List;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "objetivos", schema = "institucional")
public class Objetivos implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "obj_id")
    private Integer objId;
    @Column(name = "obj_tip")
    private String objTip;    
    @Column(name = "obj_cua")
    private String objCua;
    @Column(name = "obj_des")
    private String objDes;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "obj_ava")
    private Double objAva;
    @Column(name = "obj_fec")
    @Temporal(TemporalType.DATE)
    private Date objFec;
    @Column(name = "obj_doc")
    private String objDoc;
    @Column(name = "obj_url")
    private String objUrl;
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "est_reg")
    private String estReg = "A";
    @Column(name = "usu_id")
    private Integer usuId;
    
//    @JoinColumn(name = "ite_ide")
//    @ManyToOne(fetch=FetchType.LAZY)
//    private ItemFile item;
    
    @Column(name = "ite_ide")
    private int iteide;
        
//    @OneToMany(mappedBy = "objetivo")
//    private List<ObjetivosResponsables> responsables;
        
//    @OneToMany(cascade=CascadeType.ALL)
//    @JoinColumn(name="obj_id")
//    private ObjetivosDetalle detalle;
    
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "objetivo", fetch = FetchType.LAZY)
    private ObjetivosDetalle detalle;

    public Objetivos() {
    }

    public Objetivos(Integer objId) {
        this.objId = objId;
    }
    
    public Objetivos(String objTip, String objCua, String objDes, Double objAva, Date objFec, Integer usuId, int iteide) {
        this.objTip = objTip;
        this.objCua = objCua;
        this.objDes = objDes;
        this.objAva = objAva;
        this.objFec = objFec;        
        this.usuId = usuId;
        this.iteide = iteide;
    }
            
    public Objetivos(Integer objId, String objTip, String objCua, String objDes, Double objAva, Date objFec, Integer usuId, int iteide) {
        this.objId = objId;
        this.objTip = objTip;
        this.objCua = objCua;
        this.objDes = objDes;
        this.objAva = objAva;
        this.objFec = objFec;        
        this.usuId = usuId;
        this.iteide = iteide;
    }
    
    
    

    public Integer getObjId() {
        return objId;
    }

    public void setObjId(Integer objId) {
        this.objId = objId;
    }

    public String getObjTip() {
        return objTip;
    }

    public void setObjTip(String objTip) {
        this.objTip = objTip;
    }

    public String getObjDes() {
        return objDes;
    }

    public void setObjDes(String objDes) {
        this.objDes = objDes;
    }

    public Double getObjAva() {
        return objAva;
    }

    public void setObjAva(Double objAva) {
        this.objAva = objAva;
    }

    public Date getObjFec() {
        return objFec;
    }

    public void setObjFec(Date objFec) {
        this.objFec = objFec;
    }

    public String getObjDoc() {
        return objDoc;
    }

    public void setObjDoc(String objDoc) {
        this.objDoc = objDoc;
    }

    public String getObjUrl() {
        return objUrl;
    }

    public void setObjUrl(String objUrl) {
        this.objUrl = objUrl;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public Integer getUsuId() {
        return usuId;
    }

    public void setUsuId(Integer usuId) {
        this.usuId = usuId;
    }

    public String getObjCua() {
        return objCua;
    }

    public void setObjCua(String objCua) {
        this.objCua = objCua;
    }
    
//    public ItemFile getItem() {
//        return item;
//    }
//
//    public void setItem(ItemFile item) {
//        this.item = item;
//    }

    public int getIteid() {
        return iteide;
    }
    
    public void setIteid(int iteid) {
        this.iteide = iteid;
    }

//    @XmlTransient
//    public List<ObjetivosResponsables> getResponsables() {
//        return responsables;
//    }
//
//    public void setResponsables(List<ObjetivosResponsables> responsables) {
//        this.responsables = responsables;
//    }
//
//    @XmlTransient
    public ObjetivosDetalle getDetalle() {
        return detalle;
    }

    public void setDetalle(ObjetivosDetalle detalle) {
        this.detalle = detalle;
    }

//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (objId != null ? objId.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof Objetivos)) {
//            return false;
//        }
//        Objetivos other = (Objetivos) object;
//        if ((this.objId == null && other.objId != null) || (this.objId != null && !this.objId.equals(other.objId))) {
//            return false;
//        }
//        return true;
//    }
//
//    @Override
//    public String toString() {
//        return "entidades.smdg.Objetivos[ objId=" + objId + " ]";
//    }    

    
}
