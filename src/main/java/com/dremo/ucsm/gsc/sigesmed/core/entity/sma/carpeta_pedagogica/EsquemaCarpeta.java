/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica;

public class EsquemaCarpeta implements java.io.Serializable{
    private Integer carDigId;
    private Integer secCarPedId;
    private Integer ord;
    private String secNom;
    private Integer conSecCarPedId;
    private String conNom;

    public EsquemaCarpeta() {
    }

    public Integer getCarDigId() {
        return carDigId;
    }

    public Integer getSecCarPedId() {
        return secCarPedId;
    }

    public Integer getOrd() {
        return ord;
    }

    public String getSecNom() {
        return secNom;
    }

    public Integer getConSecCarPedId() {
        return conSecCarPedId;
    }

    public String getConNom() {
        return conNom;
    }

    public void setCarDigId(Integer carDigId) {
        this.carDigId = carDigId;
    }

    public void setSecCarPedId(Integer secCarPedId) {
        this.secCarPedId = secCarPedId;
    }

    public void setOrd(Integer ord) {
        this.ord = ord;
    }

    public void setSecNom(String secNom) {
        this.secNom = secNom;
    }

    public void setConSecCarPedId(Integer conSecCarPedId) {
        this.conSecCarPedId = conSecCarPedId;
    }

    public void setConNom(String conNom) {
        this.conNom = conNom;
    }
}