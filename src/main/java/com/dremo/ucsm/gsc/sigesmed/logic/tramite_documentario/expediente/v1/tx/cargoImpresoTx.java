/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.dao.std.HistorialExpedienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.std.TipoTramiteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.TipoTramite;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author angell
 */
public class cargoImpresoTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        JSONObject objeto = null;
        String NumCargo = null;
        GTabla tablaOrg = null;
        TipoTramite TipoTramiteU;
        String nombreTramite;
        int diasTramite ;
        
         int id ;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            id = requestData.getInt("tipoTramiteID");

            objeto = requestData.getJSONObject("objeto");
             NumCargo = requestData.optString("numeroCargo");
           
            PersonaDao personaDao = (PersonaDao) FactoryDao.buildDao("PersonaDao");
            Persona director = personaDao.buscarPorCod(objeto.getInt("Director"));
            objeto.put("Director", director.getNom() + " " + director.getApePat() + " " + director.getApeMat());
           
            OrganizacionDao organizacionDao = (OrganizacionDao) FactoryDao.buildDao("OrganizacionDao");
            Organizacion org = organizacionDao.buscarConTipoOrganizacion(objeto.getInt("OrganizacionId"));
            objeto.put("Dirección", org.getDir());
            
            objeto.remove("OrganizacionId");
            
            TipoTramiteDao tipoTramiteDao = (TipoTramiteDao)FactoryDao.buildDao("std.TipoTramiteDao");
            
            TipoTramiteU = tipoTramiteDao.buscarPorID(id);
            
             
            nombreTramite = TipoTramiteU.getNom();
            diasTramite = TipoTramiteU.getDur();
                    
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo realizar el reporte, Datos Incorrectos", e.getMessage() );
        }
        int numExpe;
         HistorialExpedienteDao hisDao = (HistorialExpedienteDao)FactoryDao.buildDao("std.HistorialExpedienteDao");
         numExpe= hisDao.numeroExpedientesDia();
         
         objeto.put("NUMERO DE EXPEDIENTE DEL DIA  N°-", Integer.toString(numExpe));
         
         JSONObject response = new JSONObject();
        try {
            float[] cols = {4f,4f,4f,4f};
            tablaOrg = new GTabla(cols);
            String [] labels =  {"       "," Codigo"," Tipo de Tramite", "Duracion(dias)"};
            tablaOrg.build(labels);
                
            String[] fila = new String [4];
                fila[0] = "NUMERO CARGO";
                fila[1] = NumCargo;
                fila[2] = nombreTramite;
                fila[3] = Integer.toString(diasTramite);
            tablaOrg.processLine(fila);
            
            
            Mitext r = new Mitext();
            
            
            
                r.agregarTitulo("   CARGO GENERADO  ");
                r.agregarSubtitulos(objeto);
                r.newLine(1);
               r.agregarTabla(tablaOrg);
                //r.agregarTabla(tablaOrg);
            
            
            r.cerrarDocumento();
            
              response.append("cargo", r.encodeToBase64() );
            
            
        } catch (Exception ex) {
            Logger.getLogger(ReporteTx.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return WebResponse.crearWebResponseExito("el reporte se realizo",response);
        //Fin
    }
}
