/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.ItemFileDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.ItemFile;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ProtegerDocumentoTx implements ITransaction{
  

    Integer ideUsu = 0;
   //Codigo de proteccion
    String codProt = "";
   Integer ideFil = 0;
    
    @Override
    public WebResponse execute(WebRequest wr) {
        //Comenzando proceso de proteccion basica
        //Lectura de datos
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            ideUsu = requestData.getInt("idUsuario");
            codProt = requestData.getString("codProt");
            ideFil = requestData.getInt("ideFil");
        }catch(Exception e){

            return WebResponse.crearWebResponseError("No se pudo leer datos enviados" + e.getMessage());
        }
        

        
        ItemFile fileActual = null;
        
        try{
            //Una vez creado el registro de proteccion lo enlazamos con el archivo
            ItemFileDao iteFileDao = (ItemFileDao)FactoryDao.buildDao("rdg.ItemFileDao");
            fileActual = iteFileDao.buscarPorID(ideFil);
            fileActual.setIteProt(codProt);
            //En este caso es una actualizacion al archivo
//            fileActual.setIteCodPro(objProtFile);
            iteFileDao.update(fileActual);
            
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo insertar datos" + e.getMessage());
        }
        JSONObject res = new JSONObject();
        //res.put("codProt", fileActual.getIteCodPro().getPrtCod());
        //Finalmente si pasa todo devolvemos el codigo de proteccion insertado
        return WebResponse.crearWebResponseExito("Se actualizo el codigo de proteccion",res);
    }
}
