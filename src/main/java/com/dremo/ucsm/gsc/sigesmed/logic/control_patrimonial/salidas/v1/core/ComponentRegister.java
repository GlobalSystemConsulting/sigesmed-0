/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.salidas.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.salidas.v1.tx.ListarCausalSalidaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.salidas.v1.tx.ListarSalidasTx;


import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.salidas.v1.tx.ListarBajasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.salidas.v1.tx.ListarCausalBajaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.salidas.v1.tx.RegistrarBajaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.salidas.v1.tx.RegistrarCausalBajaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.salidas.v1.tx.ListarIETx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.salidas.v1.tx.RegistrarSalidasTx;
/**
 *
 * @author Administrador
 */
public class ComponentRegister implements IComponentRegister {

    @Override
    public WebComponent createComponent() {
        
        
        // Asignando al modulo al cual Pertenece
         WebComponent component = new WebComponent(Sigesmed.MODULO_SISTEMA_CONTROL_PATRIMONIAL);
         
        //Registrando el Nombre del Componente
        component.setName("salidas");
        //version del componente
        component.setVersion(1); 
        
        
        component.addTransactionGET("listar_bajas",ListarBajasTx.class);
        component.addTransactionGET("listar_causal_baja",ListarCausalBajaTx.class);
        component.addTransactionPOST("registrar_baja",RegistrarBajaTx.class);
        component.addTransactionPOST("registrar_causal_baja",RegistrarCausalBajaTx.class);
        
        component.addTransactionPOST("registrar_salida",RegistrarSalidasTx.class);
        component.addTransactionGET("listar_salidas",ListarSalidasTx.class);
        component.addTransactionGET("listar_causal_salida",ListarCausalSalidaTx.class);   
        
        component.addTransactionGET("listar_ie",ListarIETx.class);   
        
        return component;
        
        
 //       throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
