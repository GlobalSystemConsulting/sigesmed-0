package com.dremo.ucsm.gsc.sigesmed.core.entity.cpe;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="horario_det" ,schema="administrativo")
public class HorarioDet  implements java.io.Serializable {


    @Id
    @Column(name="hor_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_horario_det", sequenceName="administrativo.horario_det_hor_id_seq" )
    @GeneratedValue(generator="secuencia_horario_det")
    private Integer horDetId;
    
    @Temporal(TemporalType.TIME)
    @Column(name="hor_ing")
    private Date fecIngreso;
    
    @Temporal(TemporalType.TIME)
    @Column(name="hor_sal")
    private Date fecSalida;
    
    @Column(name="est_reg")
    private String estReg;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="hor_cab_id", nullable=false )
    private HorarioCab horCabId;  
    
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="dia_sem_id", nullable=false )
    private DiaSemana diaSemId; 

    public HorarioDet() {
    }

    public HorarioDet(Integer horDetId, Date fecIngreso, Date fecSalida, String estReg, HorarioCab horCabId, DiaSemana diaSemId) {
        this.horDetId = horDetId;
        this.fecIngreso = fecIngreso;
        this.fecSalida = fecSalida;
        this.estReg = estReg;
        this.horCabId = horCabId;
        this.diaSemId = diaSemId;
    }

    public HorarioDet(Date fecIngreso, Date fecSalida, String estReg, HorarioCab horCabId, DiaSemana diaSemId) {
        this.fecIngreso = fecIngreso;
        this.fecSalida = fecSalida;
        this.estReg = estReg;
        this.horCabId = horCabId;
        this.diaSemId = diaSemId;
    }

    public Integer getHorDetId() {
        return horDetId;
    }

    public void setHorDetId(Integer horDetId) {
        this.horDetId = horDetId;
    }

    public Date getFecIngreso() {
        return fecIngreso;
    }

    public void setFecIngreso(Date fecIngreso) {
        this.fecIngreso = fecIngreso;
    }

    public Date getFecSalida() {
        return fecSalida;
    }

    public void setFecSalida(Date fecSalida) {
        this.fecSalida = fecSalida;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public HorarioCab getHorCabId() {
        return horCabId;
    }

    public void setHorCabId(HorarioCab horCabId) {
        this.horCabId = horCabId;
    }

    public DiaSemana getDiaSemId() {
        return diaSemId;
    }

    public void setDiaSemId(DiaSemana diaSemId) {
        this.diaSemId = diaSemId;
    }
    
    
    
}


