package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.agregar_estudiante.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.ParienteDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.ParientesMMI;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class ListarParientesTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {

        JSONObject data = (JSONObject) wr.getData();
        JSONObject persona = data.getJSONObject("persona");

        ParienteDaoHibernate hb = new ParienteDaoHibernate();
        JSONArray jsonParientes = new JSONArray();
        long perId;
        List<ParientesMMI> misParientes;
        String nombreCompleto;
        try {
            perId = persona.getInt("perId");
            misParientes = hb.find4Estudiante(perId);

            for (ParientesMMI par : misParientes) {
                JSONObject temp = new JSONObject();
                nombreCompleto = par.getPersonaByParId().getNom() + " " + par.getPersonaByParId().getApePat() + " " + par.getPersonaByParId().getApeMat();
                temp.put("parPerId", par.getId().getPerId());
                temp.put("parParId", par.getId().getParId());
                temp.put("parParDNI", par.getPersonaByParId().getDni());
                temp.put("parParDes", nombreCompleto);

                if (par.getParViv() != null) {
                    temp.put("parViv", par.getParViv());
                    if (par.getParViv()) {
                        temp.put("parVivDes", "Si");
                    } else {
                        temp.put("parVivDes", "No");
                    }
                } else {
                    temp.put("parViv", true);
                    temp.put("parVivDes", "Si");
                }

                if (par.getParVivEst() != null) {
                    temp.put("parVivEst", par.getParVivEst());
                    if (par.getParVivEst()) {
                        temp.put("parVivEstDes", "Si");
                    } else {
                        temp.put("parVivEstDes", "No");
                    }
                }else {
                    temp.put("parVivEst", false);
                    temp.put("parVivEstDes", "No");
                }

                temp.put("tpaId", par.getTipoPariente().getTpaId());
                temp.put("tpaDes", par.getTipoPariente().getTpaDes());
                temp.put("parTel", par.getPersonaByParId().getNum1());
                jsonParientes.put(temp);
            }

        } catch (Exception e) {
            return WebResponse.crearWebResponseError("Error al cargar los parientes! ", e.getMessage());
        }

        return WebResponse.crearWebResponseExito("Listaron las parientes", jsonParientes);
    }

}
