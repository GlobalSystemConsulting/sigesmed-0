/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.monitoreo_sesion_apredizaje.plantilla_ficha_monitoreo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.ValoracionIndicadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.ValoracionIndicador;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class ActualizarValoracionIndicadorTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ActualizarValoracionIndicadorTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            Integer valIndId = requestData.getInt("valIndId");
            String valIndNom = requestData.getString("valIndNom");  
            String valIndDes = requestData.getString("valIndDes");
            Integer valIndPun = requestData.getInt("valIndPun");
            return actualizarValoracionInd(valIndId, valIndNom, valIndDes, valIndPun);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar estado de avance. Error: ",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        } 
    }
    
    private WebResponse actualizarValoracionInd(Integer valIndId, String valIndNom, String valIndDes, Integer valIndPun) {
        try{
            ValoracionIndicadorDao valIndDao = (ValoracionIndicadorDao)FactoryDao.buildDao("sma.ValoracionIndicadorDao");        
            ValoracionIndicador valInd = valIndDao.buscarPorId(valIndId);

            valInd.setValIndNom(valIndNom);
            valInd.setValIndDes(valIndDes);
            valInd.setValIndPun(valIndPun);
            valIndDao.update(valInd);
            
            JSONObject oResponse = new JSONObject();
            oResponse.put("valIndId", valInd.getValIndId());
            oResponse.put("valIndNom", valInd.getValIndNom());
            oResponse.put("valIndDes", valInd.getValIndDes());
            oResponse.put("valIndPun", valInd.getValIndPun());
            return WebResponse.crearWebResponseExito("Nivel de avance actualizado exitosamente",oResponse);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"actualizarValoracionIndicador",e);
            return WebResponse.crearWebResponseError("Error, el nivel de avance no fue actualizado");
        }
    } 
}
