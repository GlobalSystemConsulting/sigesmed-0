/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TrabajadorOrganigramaDetalleDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Cargo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Categoria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Planilla;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TrabajadorOrganigramaDetalle;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author felipe
 */
public class TrabajadorOrganigramaDetalleDaoHibernate extends GenericDaoHibernate<TrabajadorOrganigramaDetalle> implements TrabajadorOrganigramaDetalleDao{

    @Override
    public TrabajadorOrganigramaDetalle buscarPorId(int traId, int orgiId) {
        
        TrabajadorOrganigramaDetalle objeto = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            String hql = "SELECT DISTINCT tod FROM TrabajadorOrganigramaDetalle tod "
                    + "LEFT JOIN FETCH tod.organigrama org "
                    + "LEFT JOIN FETCH org.tipoOrganigrama tipOrg "
                    + "LEFT JOIN FETCH tipOrg.datosOrganigramas datOrg "
                    + "WHERE tod.traId='"+traId+"' "
                    + "AND tod.orgiId='"+orgiId+"' ";
            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            
            objeto = (TrabajadorOrganigramaDetalle)query.uniqueResult();

        } catch (Exception e) {
            System.out.println("No se pudo obtener el detalle organigrama del trabajador\\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo obtener el detalle organigrama del trabajador\\n " + e.getMessage());
        } finally {
            session.close();
        }
        return objeto;
    }
    @Override
    public Planilla buscarPlanilla(int plaId) {
        
        Planilla objeto = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            String hql = "SELECT pla FROM Planilla pla "
                    + "WHERE pla.plaId='"+plaId+"' ";
            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            
            objeto = (Planilla)query.uniqueResult();

        } catch (Exception e) {
            System.out.println("No se pudo obtener la planilla del trabajador\\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo obtener la planilla del trabajador\\n " + e.getMessage());
        } finally {
            session.close();
        }
        return objeto;
    }
    @Override
    public Categoria buscarCategoria(int catId) {
        
        Categoria objeto = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            String hql = "SELECT cat FROM Categoria cat "
                    + "WHERE cat.catId='"+catId+"' ";
            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            
            objeto = (Categoria)query.uniqueResult();

        } catch (Exception e) {
            System.out.println("No se pudo obtener la categoria del trabajador\\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo obtener la categoria del trabajador\\n " + e.getMessage());
        } finally {
            session.close();
        }
        return objeto;
    }
    @Override
    public Cargo buscarCargo(int carId) {
        
        Cargo objeto = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            String hql = "SELECT car FROM Cargo car "
                    + "WHERE car.carId='"+carId+"' ";
            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            
            objeto = (Cargo)query.uniqueResult();

        } catch (Exception e) {
            System.out.println("No se pudo obtener el cargo del trabajador\\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo obtener el cargo del trabajador\\n " + e.getMessage());
        } finally {
            session.close();
        }
        return objeto;
    }
    @Override
    public void eliminarRegistroBD(int traId, int orgiId) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            String hql = "DELETE FROM TrabajadorOrganigramaDetalle "
                    + "WHERE traId='"+traId+"' AND orgiId='"+orgiId+"'";
            Query query = session.createQuery(hql);
            query.executeUpdate();
        } catch (Exception e) {
            System.out.println("No se pudo eliminar el registro del trabajadorOrganigramaDetalle\\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo eliminar el registro del trabajadorOrganigramaDetalle\\n " + e.getMessage());
        } finally {
            session.close();
        }
    }
}