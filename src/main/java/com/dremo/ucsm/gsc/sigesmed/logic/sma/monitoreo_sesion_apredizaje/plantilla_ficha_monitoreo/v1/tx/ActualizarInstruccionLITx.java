/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.monitoreo_sesion_apredizaje.plantilla_ficha_monitoreo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.InstruccionLlenadoIndicadoresDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.InstruccionLlenadoIndicadores;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class ActualizarInstruccionLITx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ActualizarInstruccionLITx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            Integer insId = requestData.getInt("insId");
            String insDes = requestData.getString("insDes");  
            return actualizarInstruccionLI(insId, insDes);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar instrucción de llenado de indicador(es). Error: ",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        } 
    }
    
    private WebResponse actualizarInstruccionLI(Integer insId, String insDes) {
        try{
            InstruccionLlenadoIndicadoresDao insLleIndDao = (InstruccionLlenadoIndicadoresDao)FactoryDao.buildDao("sma.InstruccionLlenadoIndicadoresDao");        
            InstruccionLlenadoIndicadores insLI = insLleIndDao.buscarPorId(insId);

            insLI.setInsLleIndDes(insDes);
            insLleIndDao.update(insLI);
            
            JSONObject oResponse = new JSONObject();
            oResponse.put("insId", insLI.getInsLleIndId());
            oResponse.put("insDes", insLI.getInsLleIndDes());
            return WebResponse.crearWebResponseExito("Instrucción de llenado de indicador(es) actualizada exitosamente",oResponse);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"actualizarInstruccionLlenadoIndicadores",e);
            return WebResponse.crearWebResponseError("Error, la instrucción de llenado de indicador(es) no fue actualizada");
        }
    }
}
