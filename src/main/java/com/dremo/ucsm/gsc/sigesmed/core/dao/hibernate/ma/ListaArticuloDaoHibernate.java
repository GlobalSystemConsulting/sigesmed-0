/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.ma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.scec.CargoDeComisionDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.ListaArticuloDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ArticuloEscolar;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ListaArticulo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ListaUtiles;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author ucsm
 */
public  class ListaArticuloDaoHibernate extends GenericDaoHibernate<ListaArticulo> implements ListaArticuloDao{
    private static Logger logger = Logger.getLogger(CargoDeComisionDaoHibernate.class.getName());
    @Override
    public void eliminarArticulosDeLista(int idList){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        
        try{
            String sql1 = "delete from pedagogico.lista_articulo  where list_uti_id = :lista_id";
            SQLQuery query = session.createSQLQuery(sql1);
            query.setParameter("lista_id",idList);
            query.executeUpdate();
            tx.commit();
        }catch (Exception e){
            tx.rollback();
            logger.log(Level.SEVERE,"eliminarArticulos",e);
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<ArticuloEscolar> listarArticulosDeLista(int idLista) {
        
        Session session = HibernateUtil.getSessionFactory().openSession();
      
        try{
            
            String sql1 = "SELECT a FROM ArticuloEscolar a INNER JOIN a.listas l  WHERE l.listaUtiles.listUtiId =:lista_id";
            Query query = session.createQuery(sql1);
            query.setParameter("lista_id",idLista);
            return query.list();
           
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
    @Override
    public ListaArticulo buscarArticulo (int idLista, int idArticulo) {
        
        Session session = HibernateUtil.getSessionFactory().openSession();
      
        try{
            String sql1 = "SELECT la FROM ListaArticulo la WHERE la.listaUtiles.listUtiId =:lista_id AND la.articulo.artEscId =:art";
            Query query = session.createQuery(sql1);
            query.setInteger("lista_id",idLista);
            query.setInteger("art",idArticulo);
            query.setMaxResults(1);
            return (ListaArticulo)query.uniqueResult();
           
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public ListaArticulo buscarPorId(int id) {
        ListaArticulo objeto = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //String hql = "SELECT DISTINCT u FROM ListaUtiles u INNER JOIN FETCH u.nivel INNER JOIN FETCH u.area INNER JOIN FETCH u.grado INNER JOIN FETCH u.seccion WHERE u.listUtiId =:idLis";
            String hql = "SELECT DISTINCT u FROM ListaArticulo u WHERE u.listUtiId =:idLis";
        
            Query query = session.createQuery(hql);
            query.setParameter("idLis", id);
            query.setMaxResults(1);
            //buscando 
            objeto =  (ListaArticulo)query.uniqueResult(); 
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar la lista \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar la lista \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objeto;
    }
}
