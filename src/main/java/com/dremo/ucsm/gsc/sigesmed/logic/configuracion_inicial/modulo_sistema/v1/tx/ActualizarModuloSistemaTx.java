/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.modulo_sistema.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ModuloSistemaDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ModuloSistema;
import java.util.Date;

/**
 *
 * @author Administrador
 */
public class ActualizarModuloSistemaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        ModuloSistema actualizarModuloSistema = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            int moduloID = requestData.getInt("moduloID");
            String codigo = requestData.getString("codigo");
            String nombre = requestData.getString("nombre");
            String descripcion = requestData.getString("descripcion");
            String icono = requestData.getString("icono");
            String estado = requestData.getString("estado");
            
            actualizarModuloSistema = new ModuloSistema(moduloID, codigo, nombre, descripcion,icono, new Date(), 1, estado.charAt(0), null);
        
        }catch(Exception e){
            System.out.println("No se pudo actualizar, datos incorrectos\n"+e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        ModuloSistemaDao moduloDao = (ModuloSistemaDao)FactoryDao.buildDao("ModuloSistemaDao");
        try{
            moduloDao.update(actualizarModuloSistema);
        
        }catch(Exception e){
            System.out.println("No se pudo actualizar Modulo del Sistema\n"+e);
            return WebResponse.crearWebResponseError("No se pudo actualizar el Modulo del Sistema", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Modulo Sistema se actualizo correctamente");
        //Fin
    }
    
}
