/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.ascenso.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.AscensoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CargoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CategoriaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Ascenso;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Cargo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Categoria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx.AgregarOperacionAuditoria;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;


public class AgregarAscensoByTipoTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(AgregarAscensoByTipoTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        Ascenso ascenso = new Ascenso();;
        int ficEscId = 0;
        int opcion = 0;

        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat sdo = new SimpleDateFormat("dd/MM/yyyy");
        
        try {
            JSONObject requestData = (JSONObject) wr.getData();
            ficEscId = requestData.getJSONObject("ascenso").getInt("ficEscId");

            Character tip = requestData.getJSONObject("ascenso").getString("tip").charAt(0);
            int tipDocId = requestData.getJSONObject("ascenso").getInt("tipDocId");
            String numDoc = requestData.getJSONObject("ascenso").getString("numDoc");
            Date fecDoc = sdi.parse(requestData.getJSONObject("ascenso").getString("fecDoc").substring(0, 10));
            Date fecEfe = sdi.parse(requestData.getJSONObject("ascenso").getString("fecEfe").substring(0, 10));
            String mot = requestData.getJSONObject("ascenso").getString("mot");
           
            ascenso.setFichaEscalafonaria(new FichaEscalafonaria(ficEscId));
            ascenso.setTip(tip);
            ascenso.setTipDocId(tipDocId);
            ascenso.setNumDoc(numDoc);
            ascenso.setFecDoc(fecDoc);
            ascenso.setFecEfe(fecEfe);
            ascenso.setMot(mot);
            ascenso.setFecMod(new Date());
            ascenso.setUsuMod(wr.getIdUsuario());
            ascenso.setEstReg('A');
            
            int orgiAntId = requestData.getJSONObject("ascenso").getInt("orgiAntId");
            int orgiPostId = requestData.getJSONObject("ascenso").getInt("orgiPostId");
            int catAntId = requestData.getJSONObject("ascenso").getInt("catAntId");
            int carAntId = requestData.getJSONObject("ascenso").getInt("carAntId");
            int catPostId = requestData.getJSONObject("ascenso").getInt("catPostId");
            int carPostId = requestData.getJSONObject("ascenso").getInt("carPostId");

            ascenso.setOrgiAntId(orgiAntId);
            ascenso.setOrgiPostId(orgiPostId);
            ascenso.setCatAntId(catAntId);
            ascenso.setCarAntId(carAntId);
            ascenso.setCatPostId(catPostId);
            ascenso.setCarPostId(carPostId);
        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE,"Datos nuevo ascenso",e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage());
        }
        //Fin

        /*
        *  Parte para la operacion en la Base de Datos
         */
        AscensoDao ascensoDao = (AscensoDao) FactoryDao.buildDao("se.AscensoDao");
        CategoriaDao categoriaDao = (CategoriaDao)FactoryDao.buildDao("se.CategoriaDao");
        CargoDao cargoDao = (CargoDao)FactoryDao.buildDao("se.CargoDao");
        Categoria categoria = null;
        Cargo cargo = null;
        
        try {
            ascensoDao.insert(ascenso);
            ///////////AUDITORIA/////////////
            AgregarOperacionAuditoria.agregarAltaAuditoria("Se agrego ascenso", "Detalle: "+ficEscId+" ficha ", wr.getIdUsuario() , "---", "xxx.xxx.xxx.xxx");
            ///////////AUDITORIA/////////////
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Agregar nuevo ascenso",e);
            System.out.println(e);
        }    

        /*
        *  Repuesta Correcta
         */
        JSONObject oResponse = new JSONObject();
        oResponse.put("ascId", ascenso.getAscId());
        oResponse.put("tip", ascenso.getTip());
        oResponse.put("tipDocId", ascenso.getTipDocId());
        oResponse.put("numDoc", ascenso.getNumDoc());
        oResponse.put("fecDoc", sdo.format(ascenso.getFecDoc()));
        oResponse.put("fecEfe", sdo.format(ascenso.getFecEfe()));
        oResponse.put("mot", ascenso.getMot());
        
        //Historico
        if(ascenso.getOrgiAntId()!=null && ascenso.getOrgiAntId()>0)
            oResponse.put("orgiAntId", ascenso.getOrgiAntId());
        else
            oResponse.put("orgiAntId", 0);

        if(ascenso.getCatAntId()!=null && ascenso.getCatAntId()>0){
            categoria=categoriaDao.obtenerDetalle(ascenso.getCatAntId());
            oResponse.put("catAntId", ascenso.getCatAntId());
            oResponse.put("nomAntCat", categoria.getNomCat());
            oResponse.put("nomAntPla", categoria.getPlanilla().getNomPla());
        }
        else{
            oResponse.put("catAntId", 0);
            oResponse.put("nomAntCat", "");
            oResponse.put("nomAntPla", "");
        }

        if(ascenso.getCarAntId()!=null && ascenso.getCarAntId()>0){
            cargo=cargoDao.buscarPorId(ascenso.getCarAntId());
            oResponse.put("carAntId", ascenso.getCarAntId());
            oResponse.put("nomAntCar", cargo.getNomCar());            
        }
        else{
            oResponse.put("carAntId", 0);
            oResponse.put("nomAntCar", "");
        }

        //post
        if(ascenso.getOrgiPostId()!=null && ascenso.getOrgiPostId()>0)
            oResponse.put("orgiPostId", ascenso.getOrgiPostId());
        else
            oResponse.put("orgiPostId", 0);

        if(ascenso.getCatPostId()!=null && ascenso.getCatPostId()>0){
            categoria=categoriaDao.obtenerDetalle(ascenso.getCatPostId());
            oResponse.put("catPostId", ascenso.getCatPostId());
            oResponse.put("nomPostCat", categoria.getNomCat());
            oResponse.put("nomPostPla", categoria.getPlanilla().getNomPla());
        }
        else{
            oResponse.put("catPostId", 0);
            oResponse.put("nomPostCat", "");
            oResponse.put("nomPostPla", "");
        }

        if(ascenso.getCarPostId()!=null && ascenso.getCarPostId()>0){
            cargo=cargoDao.buscarPorId(ascenso.getCarPostId());
            oResponse.put("carPostId", ascenso.getCarPostId());
            oResponse.put("nomPostCar", cargo.getNomCar());            
        }
        else{
            oResponse.put("carPostId", 0);
            oResponse.put("nomPostCar", "");
        }
                        
        return WebResponse.crearWebResponseExito("El registro del ascenso se realizo correctamente", oResponse);
        //Fin
    }
    
}
