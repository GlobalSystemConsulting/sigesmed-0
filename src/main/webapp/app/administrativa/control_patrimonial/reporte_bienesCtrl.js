app.controller("reporte_bienesCtrl", ["$rootScope","$scope", "NgTableParams", "crud", "modal", function ($rootScope,$scope, NgTableParams, crud, modal) {

    $scope.reporte = {
        org_id: "",
        fec_ini: "",
        fec_fin: "",
        fec_ini_md: new Date,
        fec_cie_md: new Date,
        tip_ane: 0
    };
    
    $scope.maxDateInicio;
    $scope.minDateCierre = new Date();

    $scope.anexos = {};/*Lista de Anexos*/
        

    $scope.generarReporte = function (orgId) {
        $scope.reporte.nombreAnexo = $scope.buscarAnexo($scope.reporte.tip_ane);
       
        $scope.reporte.fec_ini = convertirFecha($scope.reporte.fec_ini_md);
        $scope.reporte.fec_fin = convertirFecha($scope.reporte.fec_cie_md);

        $scope.reporte.org_id = orgId;
         var objeto = {
                Organizacion: $rootScope.usuMaster.organizacion.nombre,
                Director: $rootScope.usuMaster.usuario.usuarioID,
                OrganizacionId: $rootScope.usuMaster.organizacion.organizacionID
//                Departamento: "Arequipa",
//                Provincia: "Arequipa",
//                Distrito: "Paucarpata"
            };
         $scope.reporte.objeto = objeto;    
        /* Verificamos que todos los campos esten llenados */
        var accept = true;
        for (var atrib in $scope.reporte) {
            if ($scope.reporte[atrib] == "") {
                console.log(atrib);
                accept = false;
                break;
            }
        }
        if (accept == true) { /*Todos los campos estan llenados correctamente*/

            var request = crud.crearRequest('ingresos', 1, 'reporte_bienes');
            request.setData($scope.reporte);
            
            crud.listar("/controlPatrimonial", request, function (response) {
                
                if (response.responseSta){
                    verDocumentoPestana(response.data[0].datareporte);
                    var link = document.createElement("a");
                    link.href = response.data[0].reporteXls ;
                    //set the visibility hidden so it will not effect on your web-layout
                    link.style = "visibility:hidden";
                    link.download = "reporteBienes.xls";
                    //this part will append the anchor tag and remove it after automatic click
                    document.body.appendChild(link);
                    link.click();
                    document.body.removeChild(link);
                }
                
            }, function (data) {
                console.info(data);
            });
        } else {
            modal.mensaje("ERROR", "Verifique que todos los campos esten llenados");
        }

    };

    $scope.listar_anexos = function () {
        var request = crud.crearRequest('configuracion_patrimonial', 1, 'listar_anexos');
        crud.listar("/controlPatrimonial", request, function (data) {
            $scope.anexos = data.data;
        }, function (data) {
            console.info(data);
        });
    };
    $scope.buscarAnexo= function (id){
        
        var length = $scope.anexos.length;
        for(i = 0; i < length ; i++){
            if($scope.anexos[i].ane_id == id){
               return $scope.anexos[i].ane_nom;
            }
        }
    };
    $scope.seleccion_fecha_inicial = function () {

        $scope.minDateCierre = $scope.reporte.fec_ini_md;
    }

    $scope.seleccion_fecha_cierre = function () {

        $scope.maxDateInicio = $scope.reporte.fec_cie_md;

    }

}]);

