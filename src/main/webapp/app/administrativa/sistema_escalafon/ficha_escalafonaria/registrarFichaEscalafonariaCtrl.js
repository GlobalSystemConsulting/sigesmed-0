app.requires.push('angularModalService');
app.requires.push('ngAnimate');
app.controller('registrarFichaEscalafonariaCtrl', ['$scope', '$rootScope', '$http', 'NgTableParams', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $http, NgTableParams, crud, modal, ModalService) {
        //Variables y funciones para controlar la vista
        $scope.optionsDatosPersonales = [true, false, false, false, false];
        $scope.optionsDatosAcademicos = [true, false, false, false, false, false];

        $scope.updateOptions = function (numOption, optionsSet) {
            for (var i = 0; i < optionsSet.length; i += 1) {
                optionsSet[i] = false;
            }
            optionsSet[numOption] = true;
        };

        $scope.status_1 = {close: true, statusGD: false, statusLD: false};
        $scope.changeDiscapacidadSi = function () {
            $scope.nuevaFichaEscalafonaria.perDis = true;
        };
        $scope.changeDiscapacidadNo = function () {
            $scope.nuevaFichaEscalafonaria.perDis = false;
        };

        $scope.statusDA = false;
        $scope.changeDASi = function () {
            $scope.statusDA = false;
        };
        $scope.changeDANo = function () {
            $scope.statusDA = true;
        };

        $rootScope.parsearBooleano = function (i) {
            var o = "";
            if (i) {
                o = "Si";
            } else {
                o = "No";
            }
            return o;
        };

        $rootScope.parsearBooleano2 = function (i) {
            var o = "";
            if (i) {
                o = "Habilitado";
            } else {
                o = "No Habilitado";
            }
            return o;
        };
        
        $rootScope.perId = 0;
        $rootScope.habPestana = true;
        
        $rootScope.paramsModParientes = {count: 10};
        $rootScope.settingModParientes = {counts: []};
        $rootScope.settingModParientes.dataset = [];
        $rootScope.tablaModParientes = new NgTableParams($rootScope.paramsModParientes, $rootScope.settingModParientes);

        $rootScope.paramsModForEdu = {count: 10};
        $rootScope.settingModForEdu = {counts: []};
        $rootScope.settingModForEdu.dataset = [];
        $rootScope.tablaModForEdu = new NgTableParams($rootScope.paramsModForEdu, $rootScope.settingModForEdu);

        $rootScope.paramsModEstCom = {count: 10};
        $rootScope.settingModEstCom = {counts: []};
        $rootScope.settingModEstCom.dataset = [];
        $rootScope.tablaModEstCom = new NgTableParams($rootScope.paramsModEstCom, $rootScope.settingModEstCom);

        $rootScope.paramsModExpPon = {count: 10};
        $rootScope.settingModExpPon = {counts: []};
        $rootScope.settingModExpPon.dataset = [];
        $rootScope.tablaModExpPon = new NgTableParams($rootScope.paramsModExpPon, $rootScope.settingModExpPon);

        $rootScope.paramsModPublicaciones = {count: 10};
        $rootScope.settingModPublicaciones = {counts: []};
        $rootScope.settingModPublicaciones.dataset = [];
        $rootScope.tablaModPublicaciones = new NgTableParams($rootScope.paramsModPublicaciones, $rootScope.settingModPublicaciones);

        $rootScope.paramsModColegiaturas = {count: 10};
        $rootScope.settingModColegiaturas = {counts: []};
        $rootScope.settingModColegiaturas.dataset = [];
        $rootScope.tablaModColegiaturas = new NgTableParams($rootScope.paramsModModColegiaturas, $rootScope.settingModColegiaturas);

        $rootScope.paramsModCapacitaciones = {count: 10};
        $rootScope.settingModCapacitaciones = {counts: []};
        $rootScope.settingModCapacitaciones.dataset = [];
        $rootScope.tablaModCapacitaciones = new NgTableParams($rootScope.paramsModCapacitaciones, $rootScope.settingModCapacitaciones);

        $rootScope.paramsModReconocimientos = {count: 10};
        $rootScope.settingModReconocimientos = {counts: []};
        $rootScope.settingModReconocimientos.dataset = [];
        $rootScope.tablaModReconocimientos = new NgTableParams($rootScope.paramsModReconocimientos, $rootScope.settingModReconocimientos);

        $rootScope.paramsModDemeritos = {count: 10};
        $rootScope.settingModDemeritos = {counts: []};
        $rootScope.settingModDemeritos.dataset = [];
        $rootScope.tablaModDemeritos = new NgTableParams($rootScope.paramsModDemeritos, $rootScope.settingModDemeritos);
        
        $scope.sexo = [{id: "F", title: "Femenino"}, {id: "M", title: "Masculino"}];
        $scope.afps = ["20530","25897"];
        
        //Carga de Catalogo EstadoCivil
        $scope.estadoCivil = [];
        function listarEstadoCivil() {
            var request = crud.crearRequest('catalogo_escalafon', 1, 'listarEstadoCivil');
            crud.listar("/sistema_escalafon", request, function (data) {
                $scope.estadoCivil = data.data;
            }, function (data) {
                console.info(data);
            });
        };
        listarEstadoCivil();
        
        //Carga de Catalogo Nacionalidad/Pais
        $scope.nacionalidad = [];
        function listarNacionalidad() {
            var request = crud.crearRequest('datos_personales', 1, 'listarNacionalidad');
            crud.listar("/sistema_escalafon", request, function (data) {
                $scope.nacionalidad = data.data;
            }, function (data) {
                console.info(data);
            });
        }
        ;
        listarNacionalidad();
        
        //Carga de Catalogo Idioma
        $scope.idiomas = [];
        function listarIdioma() {
            var request = crud.crearRequest('datos_personales', 1, 'listarIdioma');
            crud.listar("/sistema_escalafon", request, function (data) {
                $scope.idiomas = data.data;
            }, function (data) {
                console.info(data);    });
        }
        ;
        listarIdioma();
        
        $scope.tiposDocumentos = [];
        function listarTiposDoc() {
            var request = crud.crearRequest('catalogo_escalafon', 1, 'listarTiposDoc');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    console.log(response);
                    $scope.tiposDocumentos = response.data;
                }
            }, function (error) {
                console.info(error);
            });
        }
        listarTiposDoc();
        
        $scope.tiposFormacion2 = [];
        function listarTiposFormacion() {
            var request = crud.crearRequest('catalogo_escalafon', 1, 'listarTiposFor');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    console.log(response);
                    $scope.tiposFormacion2 = response.data;
                }
            }, function (error) {
                console.info(error);
            });
        }
        listarTiposFormacion();
        
        $scope.nivelesAcademicos = [];
        function listarNivelesAcademicos() {
            var request = crud.crearRequest('catalogo_escalafon', 1, 'listarNivAca');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.nivelesAcademicos = response.data;
                }
            }, function (error) {
                console.info(error);
            });
        }
        listarNivelesAcademicos();
        
        $scope.tiposEstudioComplementario = [];
        function listarTiposEstudiosComplementarios() {
            var request = crud.crearRequest('catalogo_escalafon', 1, 'listarTiposEstCom');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.tiposEstudioComplementario = response.data;
                }
            }, function (error) {
                console.info(error);
            });
        }
        listarTiposEstudiosComplementarios();

        $scope.nivelesEstudioComplementario = [];
        function listarNivelesEstudiosComplementarios() {
            var request = crud.crearRequest('catalogo_escalafon', 1, 'listarNivEstCom');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.nivelesEstudioComplementario = response.data;
                }
            }, function (error) {
                console.info(error);
            });
        }
        listarNivelesEstudiosComplementarios();
        
        $scope.boolRegistro=false;
        //Global
        $rootScope.boolBoton=true;
        $rootScope.boolCampos=true;
        
        $scope.nuevoTrabajador = {
            traId: 0
        };
        $scope.nuevaFichaEscalafonaria = {
            ficEscId: 0
        };
        
        $scope.nuevaDireccionDNI = {tipDir: "R", nomDir: "", depDir: "", proDir: "", disDir: "", num: "", dpto: "", int: "", mz: "", lote: "", km: "", bloque: "", etapa: "", nomZon: "", desRef: ""};
        $scope.nuevaDireccionDA = {tipDir: "A", nomDir: "", depDir: "", proDir: "", disDir: "", num: "", dpto: "", int: "", mz: "", lote: "", km: "", bloque: "", etapa: "", nomZon: "", desRef: ""};
        
        $scope.direcciones = [];
        
        $scope.agregarDatosGenerales2 = function () {

            //if (!$scope.statusDA) {
                //$scope.direcciones = [$scope.nuevaDireccionDNI];
            //} else {
                $scope.direcciones = [$scope.nuevaDireccionDNI, $scope.nuevaDireccionDA];
            //}
            
            if ($scope.nuevaPersona.apePat.length < 1 || $scope.nuevaPersona.nom.length<1 ||
                 $scope.nuevaPersona.sex === 0 || $scope.nuevaPersona.estCivId === 0) {
                    modal.mensaje("ALERTA", "Hay campos obligatorios que debe llenar");
                return;
            }
            
            if ($scope.boolSalud.valS === 'SI' && $scope.nuevaPersona.autEss.length < 15) {
                modal.mensaje("ALERTA", "El código autogenerado de ESSALUD debe tener 15 caracteres");
                return;
            }
            if ($scope.nuevaPersona.sisPen === 'AFP' && $scope.nuevaPersona.codCuspp.length < 12) {
                modal.mensaje("ALERTA", "El código COD. CUSPP debe tener 12 caracteres");
                return;
            }
            
            //MODAL ORGANIGRAMA
            var data = {
                title: "Información Jerárquica y de Ubicación",
                orgId: $rootScope.usuMaster.organizacion.organizacionID,
                persona: $scope.nuevaPersona,
                direcciones: $scope.direcciones
            };
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/agregarTrabajador.html",
                controller: "agregarTrabajadorRegLabCtrl",
                inputs: {
                    miData: data
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.buscarPersona = function () {
            if ($scope.nuevaPersona.dni === '') {
                modal.mensaje("ALERTA", "Ingrese DNI");
                return;
            }
            var request = crud.crearRequest('datos_personales', 1, 'buscarPersonaTrabajador');
            request.setData({dni: $scope.nuevaPersona.dni});
            crud.listar("/sistema_escalafon", request, function (data) {
                modal.mensaje("CONFIRMACION", data.responseMsg);
                $scope.boolRegistro=true;
                if (!data.responseSta) {//Caso 0: No existe la Persona
                    $rootScope.boolCampos=false;
                    $scope.limpiarCampos();
                    $scope.nuevaPersona.dni=request.data.dni;
                    $scope.nuevaPersona.caso=0;
                    $rootScope.boolBoton=false;
                    $scope.botonCargo = false;
                }
                else{
                    if(data.data.persona.caso===1)//Si existe la persona, trabajador, ficha solo ver los campos y habilitar boton de trabajador
                    {
                        $rootScope.boolCampos=true;
                        $scope.botonCargo = true;
                        $scope.nuevaPersona = {
                            perId: data.data.persona.perId,
                            //perCod: data.persona.perCod,
                            apePat: data.data.persona.apePat,
                            apeMat: data.data.persona.apeMat,
                            nom: data.data.persona.nom,
                            sex: data.data.persona.sex,
                            estCivId: data.data.persona.estCivId,
                            eda: calcularEdad(data.data.persona.fecNac),
                            dni: data.data.persona.dni,
                            pas: data.data.persona.pas,
                            estAseEss: data.data.persona.estAseEss, // === "" ? false : data.data.persona.estAseEss,
                            autEss: data.data.persona.autEss,
                            fij: data.data.persona.fij,
                            num1: data.data.persona.num1,
                            num2: data.data.persona.num2,
                            email: data.data.persona.email,
                            nacId: data.data.persona.nacId, //
                            depNac: data.data.persona.depNac,
                            proNac: data.data.persona.proNac,
                            disNac: data.data.persona.disNac,
                            fecNac: data.data.persona.fecNac===""?"":convertirFechaStr(data.data.persona.fecNac),
                            //2 Direcciones?Abajo
                            sisPen: data.data.persona.sisPen=== "" ? "NUL" : data.data.persona.sisPen,
                            tipAfp: data.data.persona.tipAfp,
                            codCuspp: data.data.persona.codCuspp,
                            fecIngAfp: data.data.persona.fecIngAfp===""?"":convertirFechaStr(data.data.persona.fecIngAfp),
                            fecTraAfp: data.data.persona.fecTraAfp===""?"":convertirFechaStr(data.data.persona.fecTraAfp),
                            perDis: data.data.persona.perDis === "" ? false : data.data.persona.perDis,
                            regCon: data.data.persona.regCon,
                            idiomId: data.data.persona.idiomId, //
                            licCond: data.data.persona.licCond,
                            bonCaf: data.data.persona.bonCaf,
                            caso: 1
                        };
                        $scope.boolSalud = {
                            valS: ($scope.nuevaPersona.estAseEss) === true ? 'SI' : 'NO'
                        };
                        $scope.boolDis = {
                            val: ($scope.nuevaPersona.perDis) === true ? 'SI' : 'NO'
                        };
                        $rootScope.boolBoton=true;
                        $scope.loadProLugNac();
                        $scope.loadDisLugNac();
                    }
                    if(data.data.persona.caso===2)//Caso 2
                    {
                        $rootScope.boolCampos=false;
                        $scope.limpiarCampos();
                        $scope.nuevaPersona.perId=data.data.persona.perId;
                        $scope.nuevaPersona.apePat=data.data.persona.apePat;
                        $scope.nuevaPersona.apeMat=data.data.persona.apeMat;
                        $scope.nuevaPersona.nom=data.data.persona.nom;
                        $scope.nuevaPersona.sex=data.data.persona.sex;
                        $scope.nuevaPersona.fecNac=data.data.persona.fecNac === "" ? "" : new Date(data.data.persona.fecNac);
                        $scope.nuevaPersona.fij=data.data.persona.fij;
                        $scope.nuevaPersona.num1=data.data.persona.num1;
                        $scope.nuevaPersona.num2=data.data.persona.num2;
                        $scope.nuevaPersona.email=data.data.persona.email;
                        $scope.nuevaPersona.dni=request.data.dni;
                        $scope.nuevaPersona.caso=2;
                        $rootScope.boolBoton=false;
                        $scope.botonCargo = false;
                    }
                }
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.reiniciarDatos = function () {
            $scope.boolRegistro = false;
            $rootScope.boolBoton = true;
            $scope.botonCargo = false;
            $scope.tabPrincipal=4;
            //Global
            $rootScope.habPestana = true;
        };
        $scope.limpiarCampos = function () {
            $scope.nuevaPersona = {
                perId: 0,
                apePat: "",
                apeMat: "",
                nom: "",
                sex: "",
                estCivId: 0,
                pas: "",
                estAseEss: false,
                autEss: "",
                fij: "",
                num1: "",
                num2: "",
                email: "",
                nacId: 0,
                depNac: "",
                proNac: "",
                disNac: "",
                fecNac: "",
                //2 Direcciones?Abajo
                sisPen: "NUL",
                tipAfp: "",
                codCuspp: "",
                fecIngAfp: "",
                fecTraAfp: "",
                perDis: false,
                regCon: "",
                idiomId: 0,
                licCond: "",
                bonCaf: "",
                estado: 'A'
            };
        
            $scope.boolSalud = {
                valS: 'NO'
            };
            $scope.boolDis = {
                val: 'NO'
            };
            $scope.botonCargo = false;
        };
        
        $scope.botonCargo = false;
        
        $http.get('../recursos/json/departamentos.json').success(function (data) {
            $scope.departamentosLugNac = data;
            $scope.departamentosDirDNI = data;
            $scope.departamentosDirDA = data;
        });

        $scope.loadProLugNac = function () {
            $http.get('../recursos/json/provincias.json').success(function (data) {
                var dep = $scope.departamentosLugNac.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.nuevaPersona.depNac;
                })[0];
                $scope.provinciasLugNac = data[dep.id_ubigeo];
                $scope.distritosLugNac = [];
            });
        };
        
        $scope.loadDisLugNac = function () {
            $http.get('../recursos/json/distritos.json').success(function (data) {
                var pro = $scope.provinciasLugNac.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.nuevaPersona.proNac;
                })[0];
                $scope.distritosLugNac = data[pro.id_ubigeo];
            });
        };

        $scope.loadProDirDNI = function () {
            $http.get('../recursos/json/provincias.json').success(function (data) {
                var dep = $scope.departamentosDirDNI.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.nuevaDireccionDNI.depDir;
                })[0];
                $scope.provinciasDirDNI = data[dep.id_ubigeo];
                $scope.distritosDirDNI = [];
            });
        };

        $scope.loadDisDirDNI = function () {
            $http.get('../recursos/json/distritos.json').success(function (data) {
                var pro = $scope.provinciasDirDNI.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.nuevaDireccionDNI.proDir;
                })[0];
                $scope.distritosDirDNI = data[pro.id_ubigeo];
            });
        };

        $scope.loadProDirDA = function () {
            $http.get('../recursos/json/provincias.json').success(function (data) {
                var dep = $scope.departamentosDirDA.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.nuevaDireccionDA.depDir;
                })[0];
                $scope.provinciasDirDA = data[dep.id_ubigeo];
                $scope.distritosDirDA = [];
            });
        };

        $scope.loadDisDirDA = function () {
            $http.get('../recursos/json/distritos.json').success(function (data) {
                var pro = $scope.provinciasDirDA.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.nuevaDireccionDA.proDir;
                })[0];
                $scope.distritosDirDA = data[pro.id_ubigeo];
            });
        };
        
        //Agregar Nueva formación educativa
        $scope.showNuevaFormacionEducativa = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/agregarFormacionEducativa.html",
                controller: "agregarNuevaFormacionEducativaCtrl",
                inputs: {
                    title: "Nueva Formacion Educativa",
                    personaId: $rootScope.perId,
                    tipFors: $scope.tiposFormacion2,
                    niveles: $scope.nivelesAcademicos,
                    tipDocs: $scope.tiposDocumentos,
                    paises: $scope.nacionalidad
                            //fichaEscalafonariaId: $scope.nuevaFichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };

        //Agregar nuevo pariente
        $scope.showNuevoPariente = function () {
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/agregarPariente.html",
                controller: "agregarNuevoParienteCtrl",
                inputs: {
                    title: "Nuevo Pariente",
                    personaId: $rootScope.perId,
                    sexo: $scope.sexo,
                    actDni: $scope.nuevaPersona.dni
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };

        $scope.showNuevaColegiatura = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/agregarColegiatura.html",
                controller: "agregarNuevaColegiaturaCtrl",
                inputs: {
                    title: "Nueva Colegiatura",
                    personaId: $rootScope.perId
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };

        $scope.showNuevoEstudioComplementario = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/agregarEstudioComplementario.html",
                controller: "agregarNuevoEstudioComplementarioCtrl",
                inputs: {
                    title: "Nuevo Estudio Complementario",
                    personaId: $rootScope.perId,
                    tiposEstCom: $scope.tiposEstudioComplementario,
                    nivelesEstCom: $scope.nivelesEstudioComplementario
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.showNuevaExposicion = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/agregarExposicion.html",
                controller: "agregarNuevaExposicionCtrl",
                inputs: {
                    title: "Nueva Exposicion y/o Ponencia",
                    personaId: $rootScope.perId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.showNuevaPublicacion = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/agregarPublicacion.html",
                controller: "agregarNuevaPublicacionCtrl",
                inputs: {
                    title: "Nueva Publicacion",
                    personaId: $rootScope.perId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.showNuevaCapacitacion = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/agregarCapacitacion.html",
                controller: "agregarNuevaCapacitacionCtrl",
                inputs: {
                    title: "Nueva capacitación",
                    personaId: $rootScope.perId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.showNuevoReconocimiento = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/agregarReconocimiento.html",
                controller: "agregarNuevoReconocimientoCtrl",
                inputs: {
                    title: "Nuevo Reconocimiento",
                    personaId: $rootScope.perId,
                    tipDocs: $scope.tiposDocumentos
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };

        $scope.showNuevoDemerito = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/agregarDemerito.html",
                controller: "agregarNuevoDemeritoCtrl",
                inputs: {
                    title: "Nueva Demerito",
                    personaId: $rootScope.perId,
                    tipDocs: $scope.tiposDocumentos
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };

        $scope.prepararEditarPariente = function (p) {
            var parienteSel = JSON.parse(JSON.stringify(p));
            console.log(parienteSel);
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/editarPariente.html",
                controller: "editarParienteCtrl",
                inputs: {
                    title: "Editar datos del pariente",
                    pariente: parienteSel,
                    personaId: $rootScope.perId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarFormacionEducativa = function (fe) {
            var formacionEducativaSel = JSON.parse(JSON.stringify(fe));
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/editarFormacionEducativa.html",
                controller: "editarFormacionEducativaCtrl",
                inputs: {
                    title: "Editar datos de Formacion Educativa",
                    formacionEducativa: formacionEducativaSel,
                    personaId: $rootScope.perId,
                    tipFors: $scope.tiposFormacion2,
                    niveles: $scope.nivelesAcademicos,
                    tipDocs: $scope.tiposDocumentos,
                    paises: $scope.nacionalidad
                }
            }).then(function (modal) {
                modal.element.modal();
            });

        };

        $scope.prepararEditarColegiatura = function (c) {
            var colegiaturaSel = JSON.parse(JSON.stringify(c));

            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/editarColegiatura.html",
                controller: "editarColegiaturaCtrl",
                inputs: {
                    title: "Editar datos de Colegiatura",
                    colegiatura: colegiaturaSel,
                    personaId: $rootScope.perId,
                }
            }).then(function (modal) {
                modal.element.modal();
            });

        };

        $scope.prepararEditarEstudioComplementario = function (e) {
            var estComSel = JSON.parse(JSON.stringify(e));

            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/editarEstudioComplementario.html",
                controller: "editarEstudioComplementarioCtrl",
                inputs: {
                    title: "Editar datos de Estudio Complementario",
                    personaId: $rootScope.perId,
                    estudioComplementario: estComSel,
                    tiposEstCom: $scope.tiposEstudioComplementario,
                    nivelesEstCom: $scope.nivelesEstudioComplementario
                }
            }).then(function (modal) {
                modal.element.modal();
            });

        };

        $scope.prepararEditarExposicion = function (e) {
            var expSel = JSON.parse(JSON.stringify(e));

            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/editarExposicion.html",
                controller: "editarExposicionCtrl",
                inputs: {
                    title: "Editar datos de Exposicion",
                    exposicion: expSel,
                    personaId: $rootScope.perId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarPublicacion = function (p) {
            var pubSel = JSON.parse(JSON.stringify(p));
            console.log(pubSel);
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/editarPublicacion.html",
                controller: "editarPublicacionCtrl",
                inputs: {
                    title: "Editar datos de Publicacion",
                    publicacion: pubSel,
                    personaId: $rootScope.perId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarCapacitacion = function (c) {
            var capSel = JSON.parse(JSON.stringify(c));

            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/editarCapacitacion.html",
                controller: "editarCapacitacionCtrl",
                inputs: {
                    title: "Editar datos de Capacitacion",
                    capacitacion: capSel,
                    personaId: $rootScope.perId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarReconocimiento = function (r) {
            var recSel = JSON.parse(JSON.stringify(r));
            //console.log("recuperar");
            //console.log(recSel);
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/editarReconocimiento.html",
                controller: "editarReconocimientoCtrl",
                inputs: {
                    title: "Editar datos de Reconocimiento",
                    reconocimiento: recSel,
                    personaId: $rootScope.perId,
                    tipDocs: $scope.tiposDocumentos
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarDemerito = function (d) {
            var demSel = JSON.parse(JSON.stringify(d));
            console.log(demSel);
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/editarDemerito.html",
                controller: "editarDemeritoCtrl",
                inputs: {
                    title: "Editar datos de Demerito",
                    demerito: demSel,
                    personaId: $rootScope.perId,
                    tipDocs: $scope.tiposDocumentos
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.eliminarPariente = function (p) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar el pariente?", function () {
                var request = crud.crearRequest('familiares', 1, 'eliminarFamiliar');
                request.setData({parId: p.parId, perId: $rootScope.perId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModParientes.settings().dataset, function (item) {
                            return p === item;
                        });
                        $rootScope.tablaModParientes.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarFormacionEducativa = function (fe) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la formación educativa?", function () {
                var request = crud.crearRequest('formacion_educativa', 1, 'eliminarFormacionEducativa');
                request.setData({forEduId: fe.forEduId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModForEdu.settings().dataset, function (item) {
                            return fe === item;
                        });
                        $rootScope.tablaModForEdu.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarColegiatura = function (c) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la colegiatura?", function () {
                var request = crud.crearRequest('colegiatura', 1, 'eliminarColegiatura');
                request.setData({colId: c.colId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModColegiaturas.settings().dataset, function (item) {
                            return c === item;
                        });
                        $rootScope.tablaModColegiaturas.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarEstudioComplementario = function (ec) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar el estudio complementario?", function () {
                var request = crud.crearRequest('estudio_complementario', 1, 'eliminarEstudioComplementario');
                request.setData({estComId: ec.estComId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModEstCom.settings().dataset, function (item) {
                            return ec === item;
                        });
                        $rootScope.tablaModEstCom.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarExposicion = function (ep) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la exposicion y/o ponencia?", function () {
                var request = crud.crearRequest('exposicion_ponencia', 1, 'eliminarExposicion');
                request.setData({expId: ep.expId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModExpPon.settings().dataset, function (item) {
                            return ep === item;
                        });
                        $rootScope.tablaModExpPon.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarPublicacion = function (p) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la publicacion?", function () {
                var request = crud.crearRequest('publicacion', 1, 'eliminarPublicacion');
                request.setData({pubId: p.pubId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModPublicaciones.settings().dataset, function (item) {
                            return p === item;
                        });
                        $rootScope.tablaModPublicaciones.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarCapacitacion = function (c) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la capacitacion?", function () {
                var request = crud.crearRequest('capacitacion', 1, 'eliminarCapacitacion');
                request.setData({capId: c.capId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModCapacitaciones.settings().dataset, function (item) {
                            return c === item;
                        });
                        $rootScope.tablaModCapacitaciones.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarReconocimiento = function (r) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la reconocimiento?", function () {
                var request = crud.crearRequest('reconocimientos', 1, 'eliminarReconocimiento');
                console.log(r);
                request.setData({recId: r.recId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModReconocimientos.settings().dataset, function (item) {
                            return r === item;
                        });
                        $rootScope.tablaModReconocimientos.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarDemerito = function (d) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la demerito?", function () {
                var request = crud.crearRequest('demeritos', 1, 'eliminarDemerito');
                request.setData({demId: d.demId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModDemeritos.settings().dataset, function (item) {
                            return d === item;
                        });
                        $rootScope.tablaModDemeritos.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

    }]);
app.controller('agregarTrabajadorRegLabCtrl', ['$scope', '$rootScope', 'miData', 'close', 'NgTableParams', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, miData, close, NgTableParams, crud, modal, ModalService) {
    var dataInput = miData;
    console.log("dataInput ", dataInput);
    $scope.title = dataInput.title;
    $scope.organigramas = [];
    $scope.planillas = [];
    $scope.categorias = [];
    $scope.cargos = [];
    $scope.organigrama = {id:0};
    $rootScope.paramsOrganismos = {count: 10};
    $rootScope.settingOrganismos = {counts: []};
    $rootScope.tablaOrganismos = new NgTableParams($rootScope.paramsOrganismos, $rootScope.settingOrganismos);
    
    $scope.arregloSelects = [];
    $scope.organismosArray = [];
    $scope.newTraOrgDetalle = {
        orgiId: 0,
        ubiId: 0,
        plaId: 0,
        catId: 0,
        carId: 0
    };
    
    $scope.desplazamientoSel = {
        tipDocIdIngRe: 0,
        numDoc: "",
        fecDoc: "",
        jorLabIdIngRe: 0,
        fecIni: ""
    };
        
    $scope.mostrarMensaje = false;
    
    $scope.actualizarDescendiente = function(index, orgiPadId, isLast){
        var orgFil = buscarObjetos($scope.organismosArray[index+1], 'orgIdPad',orgiPadId);
        if(!isLast){
            $rootScope.settingOrganismos.dataset[index+1].organismosFil = orgFil;
            $rootScope.tablaOrganismos.reload();
        }else{
            $scope.newTraOrgDetalle.orgiId = $scope.arregloSelects[index].orgId;
            $scope.newTraOrgDetalle.ubiId = $scope.arregloSelects[index].ubiId;
        }
    };
    listarDatosOrganigramas();
    function listarDatosOrganigramas() {
        var request = crud.crearRequest('datosOrgaConfiguracion', 1, 'listarDatosOrg');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
        crud.listar("/sistema_escalafon", request, function (response) {
            if(response.responseSta){ 
                $scope.organigramas = response.data;
                listarTO();
            }
            else{
                modal.mensaje("ERROR", "No se pudo listar los datos de organigrama");
            }
        }, function (data) {
        console.info(data);
    });
    };
    
    $scope.listarDatosOrg = function(){
        var request = crud.crearRequest('datosOrgaConfiguracion', 1, 'listarDatosOrg');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
        crud.listar("/sistema_escalafon", request, function (response) {
            if(response.responseSta){ 
                $scope.organigramas = response.data;
                listarTO();
            }
            else{
                modal.mensaje("ERROR", "No se pudo listar los datos de organigrama");
            }
        }, function (data) {
            console.info(data);
        });
    };
    
    //listarTO();
    function listarTO() {
        if($scope.organigrama.id !==0){
            console.log("listando tipos de organismos 1");
            var request = crud.crearRequest('tiposOrganigramaConfiguracion', 1, 'listarTiposOrganismosxOrgi');
            request.setData({organigramaId:$scope.organigrama.id});

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
                crud.listar("/sistema_escalafon", request, function (response) {
                    if(response.responseSta){
                        console.log(response);
                        if(response.data.length===0){
                            $scope.mensajeOrganigrama = "No existen niveles y entidades definidos para el organigrama";
                            $scope.mostrarMensaje = true;
                        }else{ 
                            $scope.organismosArray = [];
                            $scope.arregloSelects = [];
                            response.data.forEach(function(item){
                                $scope.organismosArray.push(item.organismos);
                            });
                            $scope.listarDetalleOrganigrama();
                            $rootScope.settingOrganismos.dataset = response.data;
                            //asignando la posicion en el arreglo a cada objeto
                            iniciarPosiciones($rootScope.settingOrganismos.dataset);
                            $rootScope.tablaOrganismos.settings($rootScope.settingOrganismos);
                        }

                    }
                    else{
                        modal.mensaje("ERROR", "No se pudo listar los datos de organigrama");
                    }
                }, function (data) {
                console.info(data);
            });
        }
    };
    $scope.listarTiposOrganismos = function(){
        console.log($scope.organigrama.id);
        if($scope.organigrama.id !== null){
            console.log("listando tipos de organismos 2");
            var request = crud.crearRequest('tiposOrganigramaConfiguracion', 1, 'listarTiposOrganismosxOrgi');
            request.setData({organigramaId:$scope.organigrama.id});

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
                crud.listar("/sistema_escalafon", request, function (response) {
                    if(response.responseSta){ 
                        console.log(response);
                        if(response.data.length===0){
                            $scope.mensajeOrganigrama = "No existen niveles y entidades definidos para el organigrama";
                            $scope.mostrarMensaje = true;
                        }else{ 
                            $scope.organismosArray = [];
                            $scope.arregloSelects = [];
                            response.data.forEach(function(item){
                                $scope.organismosArray.push(item.organismos);
                                //$scope.arregloSelects.push({orgId: 0, nomOrg:""});
                            });
                            $scope.listarDetalleOrganigrama();
                            $rootScope.settingOrganismos.dataset = response.data;
                            //asignando la posicion en el arreglo a cada objeto
                            iniciarPosiciones($rootScope.settingOrganismos.dataset);
                            $rootScope.tablaOrganismos.settings($rootScope.settingOrganismos);
                        }
                    }
                    else{
                        modal.mensaje("ERROR", "No se pudo listar los datos de organigrama");
                    }
                }, function (data) {
                console.info(data);
            });
        }
    };
    
    listarPlanillas();
    function listarPlanillas() {
        var request = crud.crearRequest('planillaConfiguracion', 1, 'listarPlanillas');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
                crud.listar("/sistema_escalafon", request, function (response) {
                    if(response.responseSta){ 
                        $scope.planillas = response.data;
                        $scope.listarCategorias();
                    }
                    else{
                        modal.mensaje("ERROR", "No se pudo listar las planillas");
                    }
                }, function (data) {
                console.info(data);
            });
    };
    
    $scope.listarCategorias = function(){
        console.log($scope.newTraOrgDetalle);
        var request = crud.crearRequest('categoriaConfiguracion', 1, 'listarxPlanilla');
        request.setData({plaId:$scope.newTraOrgDetalle.plaId});
    
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if(response.responseSta){ 
                    console.log(response);
                    $scope.categorias = response.data;
                }
                else{
                    modal.mensaje("ERROR", "No se pudo listar las categorías");
                }
            }, function (data) {
            console.info(data);
        });
    };
    
    listarCargos();
    function listarCargos() {
        var request = crud.crearRequest('cargoConfiguracion', 1, 'listarCargos');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
                crud.listar("/sistema_escalafon", request, function (response) {
                    if(response.responseSta){ 
                        $scope.cargos = response.data;
                    }
                    else{
                        modal.mensaje("ERROR", "No se pudo listar los cargos");
                    }
                }, function (data) {
                console.info(data);
            });
    };
    
    $scope.listarDetalleOrganigrama = function(){
        if($scope.newTraOrgDetalle.orgiId>0){
            $scope.arregloSelects = new Array($scope.organismosArray.length);
            for(var i=$scope.organismosArray.length-1; i>-1; i--){
                $scope.arregloSelects[i]= {orgId: 0, nomOrg:""};
                if(i===$scope.organismosArray.length-1){
                    $scope.arregloSelects[i] = buscarObjeto($scope.organismosArray[i], 'orgId',$scope.newTraOrgDetalle.orgiId);
                }else{
                    $scope.arregloSelects[i] = buscarObjeto($scope.organismosArray[i], 'orgId',$scope.arregloSelects[i+1].orgIdPad);
                }
            }
        }
    };
    
    $scope.tiposDocumentos = [];
    $scope.jornadasLaborales = []; 
    
    listarTiposDoc();
    listarJornadasLab();
    
    function listarTiposDoc(){
        var request = crud.crearRequest('catalogo_escalafon', 1, 'listarTiposDoc');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
        crud.listar("/sistema_escalafon", request, function (response) {
            if(response.responseSta){
                console.log(response);
                $scope.tiposDocumentos = response.data;
            }
        }, function (error) {
            console.info(error);
        });
    }
    
    function listarJornadasLab(){
        var request = crud.crearRequest('catalogo_escalafon', 1, 'listarJornadas');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
        crud.listar("/sistema_escalafon", request, function (response) {
            if(response.responseSta){
                console.log(response);
                $scope.jornadasLaborales = response.data;
            }
        }, function (error) {
            console.info(error);
        });
    }
    
    $scope.registrarTraOrgDet = function(){
        var request = crud.crearRequest('datos_personales', 1, 'agregarDatosPersonales');
        request.setData({
            direcciones: miData.direcciones,
            orgId: miData.orgId,
            traOrgDet: $scope.newTraOrgDetalle,
            nuevoDespla: $scope.desplazamientoSel,
            persona: miData.persona
        });   
        crud.insertar("/sistema_escalafon", request, function (response) {
            modal.mensaje("CONFIRMACION", response.responseMsg);
            if (response.responseSta) {//Si registro con exito
                //Global
                $rootScope.perId = response.data.perId;
                $rootScope.habPestana = miData.persona.caso===1?true:false;
                $rootScope.boolBoton = true;
                $rootScope.boolCampos = true;
            }
        }, function (data) {
            console.info(data);
        });
    };
}]);

