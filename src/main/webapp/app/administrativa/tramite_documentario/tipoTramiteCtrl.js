app.controller("tipoTramiteCtrl",["$scope","NgTableParams","crud","modal", function ($scope,NgTableParams,crud,modal){
    
    $scope.tipoOrganizaciones = [];
    $scope.entidades = [];
    var areas = [];
    var tipoAreas = [];
    
    //Implenetacion del controlador
    
    
    var params = {count: 10};
    var setting = { counts: []};
    $scope.miTabla = new NgTableParams(params, setting);
    
    $scope.tupas = [{id:true,title:"tupa"},{id:false,title:"no tupa"}];
    $scope.tipos = [{id:true,title:"externo"},{id:false,title:"interno"}];
    
    //variable que servira para crear un nuevo tipo de tramite
    $scope.tipoTramite = {codigo:"",nombre:"",descripcion:"",tipo:true,tupa:true,costo:0.0,duracion:1,tipoOrganizacionID:0};
    //variable temporal, para la seleccion de un tramite
    $scope.tipoTramiteSel = {};
    
    //varible para la creacion de un nuevo requisito
    $scope.requisito = {descripcion:"",archivo:{},edi:false};
    $scope.requisitoSel = {descripcion:"",nombreArchivo:"",archivo:{},edi:false};
    //lista de requisitos del tramite
    $scope.requisitos = [];    
    
    //varible para la creacion de una ruta
    $scope.ruta = {areaOri:{},areaDes:{},descripcion:""};
    $scope.rutaSel = {areaOriID:{},areaDes:{},descripcion:""};
    //lista de rutas del tramite
    $scope.rutas = [];    
    
    $scope.listarTiposTramites = function(){
        //preparamos un objeto request
        var request = crud.crearRequest('tipoTramite',1,'listarTipoTramite');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.listar("/tramiteDocumentario",request,function(data){
            setting.dataset = data.data;
            iniciarPosiciones(setting.dataset);
            $scope.miTabla.settings(setting);
        },function(data){
            console.info(data);
        });
    };
    $scope.agregarTipoTramite = function(orgID){
        
        var request = crud.crearRequest('tipoTramite',1,'insertarTipoTramite');
        
        if(!$scope.tipoTramite.tupa)
            $scope.tipoTramite.tipoOrganizacionID = orgID;
        
        //if($scope.requisitos.length>0)
        $scope.tipoTramite.requisitos = $scope.requisitos;
        
        //if($scope.rutas.length>0)
        $scope.tipoTramite.rutas = $scope.rutas;
        
        request.setData($scope.tipoTramite);
        
        crud.insertar("/tramiteDocumentario",request,function(response){
            
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                //recuperamos las variables que nos envio el servidor
                $scope.tipoTramite.tipoTramiteID = response.data.tipoTramiteID;
                $scope.tipoTramite.codigo = response.data.codigo;
                
                //insertamos el elemento a la lista
                insertarElemento(setting.dataset,$scope.tipoTramite);
                $scope.miTabla.reload();
                //reiniciamos las variables
                $scope.tipoTramite = {codigo:"",nombre:"",descripcion:"",tipo:true,tupa:true,costo:0.0,duracion:1,tipoOrganizacionID:0};
                $scope.requisitos = [];
                $scope.rutas = [];
                $scope.requisito = {descripcion:"",archivo:{},edi:false};
                $scope.ruta = {areaOri:{},areaDes:{},descripcion:""};
                //cerramos la ventana modal
                $('#modalNuevoTramite').modal('hide');
            }            
        },function(data){
            console.info(data);
        });
        
    };
    $scope.eliminarTipoTramite = function(i,idTipoTramite){
        
        modal.mensajeConfirmacion($scope,"seguro que desea eliminar este tipo de tramite",function(){
        
            var request = crud.crearRequest('tipoTramite',1,'eliminarTipoTramite');
            request.setData({tipoTramiteID:idTipoTramite});

            crud.eliminar("/tramiteDocumentario",request,
            function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    eliminarElemento(setting.dataset,i);
                    $scope.miTabla.reload();
                }

            },function(data){
            });
        });
    };
    $scope.prepararEditar = function(t){
        
        $scope.rutaSel = {areaOri:{},areaDes:{},descripcion:""};
        $scope.requisitoSel = {descripcion:"",nombreArchivo:"",archivo:{},edi:false};
        $scope.tipoTramiteSel = JSON.parse(JSON.stringify(t));
        
        if( $scope.tipoTramiteSel.tupa )
            $scope.destinos2 = tipoAreas;
        else
            $scope.destinos2 = areas;
        
        var request = crud.crearRequest('tipoTramite',1,'listarRequisito');
        request.setData({tipoTramiteID:$scope.tipoTramiteSel.tipoTramiteID,tupa:$scope.tipoTramiteSel.tupa});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las usuarios de exito y error
        crud.listar("/tramiteDocumentario",request,function(response){
            $scope.tipoTramiteSel.requisitos = response.data.requisitos;            
            if( $scope.tipoTramiteSel.tupa )
                response.data.rutas.forEach(function(item){                
                    for(var i=0;$scope.destinos2.length > i;i++)
                        if( item.areaDesID == $scope.destinos2[i].tipoAreaID ){
                            item.areaDes = $scope.destinos2[i];
                            break;
                        }
                    for(var i=0;$scope.destinos2.length > i;i++)
                        if( item.areaOriID == $scope.destinos2[i].tipoAreaID ){
                            item.areaOri = $scope.destinos2[i];
                            break;
                        }
                });
            else
                response.data.rutas.forEach(function(item){                
                    for(var i=0;$scope.destinos2.length > i;i++)
                        if( item.areaDesID == $scope.destinos2[i].areaID ){
                            item.areaDes = $scope.destinos2[i];
                            break;
                        }
                    for(var i=0;$scope.destinos2.length > i;i++)
                        if( item.areaOriID == $scope.destinos2[i].areaID ){
                            item.areaOri = $scope.destinos2[i];
                            break;
                        }
                });
            $scope.tipoTramiteSel.rutas = response.data.rutas;
            $('#modalEditarTramite').modal('show');
        },function(data){
            console.info(data);
        });
        
        
    };
    $scope.editarTipoTramite = function(orgID){
        
        if(!$scope.tipoTramiteSel.tupa)
            $scope.tipoTramiteSel.tipoOrganizacionID = orgID;
        
        var request = crud.crearRequest('tipoTramite',1,'actualizarTipoTramite');
        request.setData($scope.tipoTramiteSel);
                
        crud.actualizar("/tramiteDocumentario",request,function(response){
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                //actualizando
                setting.dataset[$scope.tipoTramiteSel.i] = $scope.tipoTramiteSel;
                $scope.miTabla.reload();
                $('#modalEditarTramite').modal('hide');
            }
        },function(data){
            console.info(data);
        });
    };
    $scope.eliminarTiposTramites = function(){
        modal.mensaje("CONFIRMACION","no se puede eliminar tipos de tramites");
    };
    
    //requisitos
    $scope.agregarRequisito = function(tipo){
        //si estamos editando
        if(tipo){
            if($scope.requisitoSel.descripcion =="" ){
                modal.mensaje("CONFIRMACION","ingrese descripcion del requisito");
                return;
            }
            $scope.tipoTramiteSel.requisitos.push($scope.requisitoSel);
            $scope.requisitoSel = {descripcion:"",archivo:{}};
        }
        //si estamos agregando
        else{
            if($scope.requisito.descripcion =="" ){
                modal.mensaje("CONFIRMACION","ingrese descripcion del requisito");
                return;
            }
            $scope.requisitos.push($scope.requisito);
            $scope.requisito = {descripcion:"",archivo:{}};
        }        
    };
    $scope.editarRequisito = function(i,r,modo){
        //si estamso editando
        if(r.edi){
            if(modo)
                $scope.tipoTramiteSel.requisitos[i] = r.copia;                
            else
                $scope.requisitos[i] = r.copia;
        }
        //si queremos editar
        else{
            r.copia = JSON.parse(JSON.stringify(r));
            r.edi =true;            
        }
    };
    $scope.eliminarRequisito = function(i,r,modo){
        //si estamso cancelando la edicion
        if(r.edi){
            r.edi = false;
            delete r.copia;
        }
        //si queremos eliminar el elemento
        else{
            if(modo)
                $scope.tipoTramiteSel.requisitos.splice(i,1);            
            else
                $scope.requisitos.splice(i,1);
        }
    };
    
    //rutas
    $scope.agregarRuta = function(tipo){
        if(tipo){
            if($scope.rutaSel.areaOri == null || $scope.rutaSel.areaDes == null )
                return;
            if($scope.tipoTramite.tupa){
                $scope.rutaSel.areaOriID = $scope.rutaSel.areaOri.tipoAreaID;
                $scope.rutaSel.areaDesID = $scope.rutaSel.areaDes.tipoAreaID;
            }
            else{
                $scope.rutaSel.areaOriID = $scope.rutaSel.areaOri.areaID;
                $scope.rutaSel.areaDesID = $scope.rutaSel.areaDes.areaID;
            }        
            $scope.tipoTramiteSel.rutas.push($scope.rutaSel);        
            $scope.rutaSel = {areaOri:$scope.rutaSel.areaDes,areaDesID:{},descripcion:""};
        }
        else{
            if($scope.ruta.areaOri == null || $scope.ruta.areaDes == null )
                return;
            if($scope.tipoTramite.tupa){
                $scope.ruta.areaOriID = $scope.ruta.areaOri.tipoAreaID;
                $scope.ruta.areaDesID = $scope.ruta.areaDes.tipoAreaID;
            }
            else{
                $scope.ruta.areaOriID = $scope.ruta.areaOri.areaID;
                $scope.ruta.areaDesID = $scope.ruta.areaDes.areaID;
            }        
            $scope.rutas.push($scope.ruta);        
            $scope.ruta = {areaOri:$scope.ruta.areaDes,areaDesID:{},descripcion:""};
        }
    };
    
    $scope.editarRuta = function(i,r,modo){
        //si estamso editando
        if(r.edi){
            if(modo){
                $scope.tipoTramiteSel.rutas[i] = r.copia;
                $scope.tipoTramiteSel.rutas[i].areaOriID = r.copia.areaOri.tipoAreaID;
                $scope.tipoTramiteSel.rutas[i].areaDesID = r.copia.areaDes.tipoAreaID;
            }
            else{
                $scope.rutas[i] = r.copia;
                $scope.rutas[i].areaOriID = r.copia.areaOri.tipoAreaID;
                $scope.rutas[i].areaDesID = r.copia.areaDes.tipoAreaID;
            }
        }
        //si queremos editar
        else{
            r.copia = JSON.parse(JSON.stringify(r));
            r.edi =true;            
        }
    };
    $scope.eliminarRuta = function(i,r,modo){
        //si estamso cancelando la edicion
        if(r.edi){
            r.edi = false;
            delete r.copia;
        }
        //si queremos eliminar el elemento
        else{
            if(modo)
                $scope.tipoTramiteSel.rutas.splice(i,1);
            else
                $scope.rutas.splice(i,1);
        }
    };
    $scope.verificarTupa = function(modo){
        if(modo){
            $scope.tipoTramiteSel.rutas = [];
            if( $scope.tipoTramiteSel.tupa )
                $scope.destinos2 = tipoAreas;
            else
                $scope.destinos2 = areas;
        }
        else{
            $scope.rutas = [];
            if( $scope.tipoTramite.tupa )
                $scope.destinos = tipoAreas;
            else
                $scope.destinos = areas;
        }
    };
    function listarTipoOrganizaciones (){
        //preparamos un objeto request
        var request = crud.crearRequest('tipoOrganizacion',1,'listarTipoOrganizaciones');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las organizaciones de exito y error
        crud.listar("/configuracionInicial",request,function(data){
            $scope.tipoOrganizaciones  = data.data;            	
            $scope.tipoOrganizaciones.splice(0,1);
        },function(data){
            console.info(data);
        });
    };
    function listarAreas(organizacionID){
        //preparamos un objeto request
        var request = crud.crearRequest('area',1,'listarAreasPorOrganizacion');
        request.setData({organizacionID:organizacionID});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las areas de exito y error
        crud.listar("/configuracionInicial",request,function(data){
            areas = data.data;
        },function(data){
            console.info(data);
        });
    };
    function listarTipoAreas (){
        //preparamos un objeto request
        var request = crud.crearRequest('area',1,'listarTipoAreas');
        crud.listar("/configuracionInicial",request,function(data){
            tipoAreas = data.data;
            $scope.verificarTupa();
        },function(data){
            console.info(data);
        });
    };    
    $scope.iniciarDatos = function(orgID){
        $scope.listarTiposTramites();        
        listarAreas(orgID);
        listarTipoAreas();
        listarTipoOrganizaciones ();        
    };
}]);
