app.controller("organizacionCtrl", ["$scope", "NgTableParams", "crud", "modal", '$http', function ($scope, NgTableParams, crud, modal, $http) {

        $scope.datosUbigeo = {
            departamentos: [],
            provincias: [],
            distritos: [],
            dpto: -1,
            prov: -1,
            dist: -1
        };

        $scope.loadDpto = function () {
            $http.get('../recursos/json/departamentos.json').success(function (data) {
                $scope.datosUbigeo.departamentos = data;
            });
            $scope.datosUbigeo.dpto = 01;
            $scope.datosUbigeo.prov = -1;
            $scope.datosUbigeo.dist = -1;
        };

        $scope.loadProv = function () {
            $http.get('../recursos/json/provincias.json').success(function (data) {
                var dep = $scope.datosUbigeo.departamentos.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.datosUbigeo.dpto;
                })[0];
                $scope.datosUbigeo.provincias = data[dep.id_ubigeo];
                $scope.datosUbigeo.distritos = [];
                $scope.datosUbigeo.prov = -1;
                $scope.datosUbigeo.dist = -1;
            });
        };

        $scope.loadDist = function () {
            $http.get('../recursos/json/distritos.json').success(function (data) {
                var pro = $scope.datosUbigeo.provincias.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.datosUbigeo.prov;
                })[0];
                $scope.datosUbigeo.distritos = data[pro.id_ubigeo];
                $scope.datosUbigeo.dist = -1;
            });
        };

        $scope.modificarUbigeo = function () {
            if ($scope.datosUbigeo.dpto === -1 ||
                    $scope.datosUbigeo.prov === -1 ||
                    $scope.datosUbigeo.dist === -1) {
                modal.mensaje("Error", "Seleccione adecuadamente los campos de ubicacion");
                return;
            }
            $scope.nuevaOrganizacion.ubigeo = $scope.datosUbigeo.dpto.toString() +
                    $scope.datosUbigeo.prov.toString() + $scope.datosUbigeo.dist.toString();
            $('#modalUbigeo').modal('hide');
        };

        $scope.buscarUbigeo = function () {
            $scope.loadDpto();
            $('#modalUbigeo').modal('show');
        };
        
        $scope.buscarUbigeoSel = function () {
            $scope.loadDpto();
            $('#modalUbigeoSel').modal('show');
        };
        
        $scope.modificarUbigeoSel = function () {
            if ($scope.datosUbigeo.dpto === -1 ||
                    $scope.datosUbigeo.prov === -1 ||
                    $scope.datosUbigeo.dist === -1) {
                modal.mensaje("Error", "Seleccione adecuadamente los campos de ubicacion");
                return;
            }
            $scope.organizacionSel.ubigeo = $scope.datosUbigeo.dpto.toString() +
                    $scope.datosUbigeo.prov.toString() + $scope.datosUbigeo.dist.toString();
            $('#modalUbigeoSel').modal('hide');
        };



        $scope.tipoOrganizaciones = [];
        $scope.nuevaOrganizacion = {
            codigo: "",
            nombre: "",
            alias: "",
            descripcion: "",
            direccion: "",
            estado: 'A',
            tipoOrganizacionID: "0",
            organizacionPadreID: "0",
            ubigeo: "",
            organizacionGestion: "P",
            organizacionCaracteristica: "PM",
            organizacionPrograma: "-",
            organizacionForma: "Esc",
            organizacionVariante: "-",
            organizacionModalidad: "EBR"
        };
        $scope.organizacionSel = {};

        var paramsOrganizacion = {count: 10};
        var settingOrganizacion = {counts: []};
        $scope.tablaOrganizacion = new NgTableParams(paramsOrganizacion, settingOrganizacion);

        $scope.estados = [{id: 'A', title: "activo"}, {id: 'E', title: "eliminado"}];

        $scope.listarOrganizaciones = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('organizacion', 1, 'listarOrganizaciones');
            crud.listar("/configuracionInicial", request, function (data) {
                settingOrganizacion.dataset = data.data;
                iniciarPosiciones(settingOrganizacion.dataset);
                $scope.tablaOrganizacion.settings(settingOrganizacion);
            }, function (data) {
                console.info(data);
            });
        };
        $scope.agregarOrganizacion = function () {

            var request = crud.crearRequest('organizacion', 1, 'insertarOrganizacion');
            request.setData($scope.nuevaOrganizacion);

            crud.insertar("/configuracionInicial", request, function (response) {

                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    //recuperamos las variables que nos envio el servidor
                    $scope.nuevaOrganizacion.organizacionID = response.data.organizacionID;
                    $scope.nuevaOrganizacion.fecha = response.data.fecha;

                    //insertamos el elemento a la lista
                    insertarElemento(settingOrganizacion.dataset, $scope.nuevaOrganizacion);
                    $scope.tablaOrganizacion.reload();
                    //reiniciamos las variables
                    $scope.nuevaOrganizacion = {codigo: "", nombre: "", alias: "", descripcion: "", direccion: "", estado: 'A', tipoOrganizacionID: "0", organizacionPadreID: "0"};
                    //cerramos la ventana modal
                    $('#modalNuevo').modal('hide');
                }
            }, function (data) {
                console.info(data);
            });

        };
        $scope.eliminarOrganizacion = function (i, idDato) {

            modal.mensajeConfirmacion($scope, "seguro que desea eliminar la organizacion", function () {

                var request = crud.crearRequest('organizacion', 1, 'eliminarOrganizacion');
                request.setData({organizacionID: idDato});

                crud.eliminar("/configuracionInicial", request, function (response) {

                    modal.mensaje("CONFIRMACION", response.responseMsg);
                    if (response.responseSta) {
                        eliminarElemento(settingOrganizacion.dataset, i);
                        $scope.tablaOrganizacion.reload();
                    }

                }, function (data) {
                    console.info(data);
                });

            });



        };
        $scope.prepararEditar = function (t) {
            $scope.organizacionSel = JSON.parse(JSON.stringify(t));
            $('#modalEditar').modal('show');
        };
        $scope.editarOrganizacion = function () {

            var request = crud.crearRequest('organizacion', 1, 'actualizarOrganizacion');
            request.setData($scope.organizacionSel);

            crud.actualizar("/configuracionInicial", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    //actualizando
                    settingOrganizacion.dataset[$scope.organizacionSel.i] = $scope.organizacionSel;
                    $scope.tablaOrganizacion.reload();
                    $('#modalEditar').modal('hide');
                }
            }, function (data) {
                console.info(data);
            });
        };
        $scope.getOrganizaciones = function () {
            return settingOrganizacion.dataset;
        };


        listarTipoOrganizaciones();

        function listarTipoOrganizaciones() {
            //preparamos un objeto request
            var request = crud.crearRequest('tipoOrganizacion', 1, 'listarTipoOrganizaciones');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las organizaciones de exito y error
            crud.listar("/configuracionInicial", request, function (data) {
                $scope.tipoOrganizaciones = data.data;
            }, function (data) {
                console.info(data);
            });
        }
        ;


    }]);